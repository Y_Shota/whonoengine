#pragma once
#include <string>

using namespace std::literals::string_literals;


namespace ResourcePath {

const auto DEBUG_DIR = "Debug/"s;
const auto DEBUG_COLLISION_DIR = "Debug/Collision/"s;

const auto TITLE_DIR = "TitleScene/"s;
const auto PLAY_DIR = "PlayScene/"s;
const auto RESULT_DIR = "ResultScene/"s;

const auto CHARACTER_DIR = PLAY_DIR + "Character/"s;

const auto INVENTORY_DIR = PLAY_DIR + "Inventory/"s;

const auto ITEM_DIR = PLAY_DIR + "Item/"s;
const auto ITEM_ICON_DIR = ITEM_DIR + "Icon/"s;
const auto ITEM_MODEL_DIR = ITEM_DIR + "Model/"s;
const auto ITEM_SOUND_DIR = ITEM_DIR + "Sound/"s;

const auto STAGE_DIR = PLAY_DIR + "Stage/"s;
const auto STAGE_OBJECT_DIR = STAGE_DIR +  "Object/"s;

}
