#pragma once
#include <exception>


namespace Whono::Window {

/**
* \brief WindowsAPIに関する例外
*/
class XAudio2Exception final : public std::exception {

public:
	explicit XAudio2Exception(const char* const msg) : std::exception(msg) {}
};

}