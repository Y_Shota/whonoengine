#pragma once
#include "../Engine/SceneObject.hpp"

/**
 * \brief リザルトシーン管理クラス
 */
class ResultScene final : public SceneObject {

public:
	//シーン名
	static constexpr auto SCENE_NAME = "ResultScene";
	
public:
	/**
	 * \brief コンストラクタ
	 */
	ResultScene();

	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
};

