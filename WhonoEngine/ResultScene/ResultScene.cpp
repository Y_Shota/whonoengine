#include "ResultScene.hpp"

#include "ResultObject.hpp"

ResultScene::ResultScene()
	: SceneObject(SCENE_NAME) {}

void ResultScene::Initialize() {

	//リザルト用オブジェクト生成
	Instantiate<ResultObject>(this);
}
