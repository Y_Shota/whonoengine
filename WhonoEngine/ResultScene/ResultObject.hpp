#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief リザルトシーン用のオブジェクトクラス
 */
class ResultObject final : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "ResultObject";

private:
	int hBackGroundImage_;	//背景画像ハンドル
	int hBoxImage_;			//ボックス画像ハンドル
	int hFormat_;			//テキスト書式ハンドル
	int hBGM_;				//BGM音声ハンドル

public:
	/**
	 * \brief コンストラクタ
	 */
	ResultObject();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
};

