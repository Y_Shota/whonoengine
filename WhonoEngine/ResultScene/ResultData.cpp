#include "ResultData.hpp"

#include <algorithm>
#include <iomanip>
#include <sstream>


/**
 * \brief プレイヤデータ保持用構造体
 */
struct ResultData::PlayerDatum {

	int id;		//プレイヤID
	int point;	//点数
	int kill;	//キル数
	int death;	//デス数
};

ResultData::ResultData()
	: sortMode_(SORT_MODE::POINT) {

	//ソート用関数設定
	using enum SORT_MODE;
	sortFunctionMap_[POINT] = [](auto&& a, auto&& b) { return a.point > b.point; };
	sortFunctionMap_[ID] = [](auto&& a, auto&& b) { return a.id < b.id; };
	sortFunctionMap_[KILL] = [](auto&& a, auto&& b) { return a.kill > b.kill; };
	sortFunctionMap_[DEATH] = [](auto&& a, auto&& b) { return a.death < b.death; };
}

ResultData::~ResultData() = default;

void ResultData::AddData(int id, int kill, int death) {

	playerData_.emplace_back(id + 1, kill - death, kill, death);
}

void ResultData::Sort(SORT_MODE mode) {

	//ソート実行
	sortMode_ = mode;
	std::ranges::sort(playerData_, sortFunctionMap_.at(sortMode_));
}

void ResultData::NextSort() {

	sortMode_ = static_cast<SORT_MODE>((static_cast<int>(sortMode_) + 1) % static_cast<int>(SORT_MODE::UNKNOWN));
	std::ranges::sort(playerData_, sortFunctionMap_.at(sortMode_));
}

auto ResultData::GetSize() const noexcept -> int {

	return static_cast<int>(playerData_.size());
}

auto ResultData::ToString(int index) const -> std::string {

	if (index < 0 || index >= GetSize())	return "";
	
	const auto& [id, point, kill, death] = playerData_[index];

	//文字列ストリームにいれて変換
	std::stringstream ss;
	ss << std::left
		<< "プレイヤ" << std::setw(2) << id << "   "
		<< "点数　" << std::setw(2) << point << "   "
		<< "撃破数　" << std::setw(2) << kill << "   "
		<< "死亡数　" << std::setw(2) << death;

	return ss.str();
}
