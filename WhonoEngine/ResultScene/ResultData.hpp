#pragma once
#include <functional>
#include <string>
#include <unordered_map>

#include "../Engine/ISingleton.hpp"

/**
 * \brief ゲームの結果をリザルトシーンで保持するためのクラス
 */
class ResultData : public Whono::DesignPattern::ISingleton<ResultData> {

public:
	/**
	 * \brief プレイヤデータのソート種類列挙
	 */
	enum class SORT_MODE : unsigned char {
		POINT,		//点数
		ID,			//ID
		KILL,		//キル数
		DEATH,		//デス数
		UNKNOWN
	};

private:
	struct PlayerDatum;						//インナー構造体
	std::vector<PlayerDatum> playerData_;	//プレイヤデータ配列

	//ソート用関数オブジェクト型のエイリアス
	using SortFunc = std::function<bool(const PlayerDatum&, const PlayerDatum&)>;
	std::unordered_map<SORT_MODE, SortFunc> sortFunctionMap_;	//ソート関数マップ

	SORT_MODE sortMode_;					//ソートモード
	
public:
	/**
	 * \brief コンストラクタ
	 */
	ResultData();
	/**
	 * \brief デストラクタ
	 */
	~ResultData();
	
	/**
	 * \brief データを追加する
	 * \param id プレイヤID
	 * \param kill キル数
	 * \param death デス数
	 */
	void AddData(int id, int kill, int death);
	/**
	 * \brief ソートする
	 * \param mode ソート方法
	 */
	void Sort(SORT_MODE mode = SORT_MODE::POINT);
	/**
	 * \brief ソートのモードをインクリメントする
	 */
	void NextSort();
	/**
	 * \brief プレイヤデータのサイズを取得する
	 * \return サイズ
	 */
	auto GetSize() const noexcept -> int;
	/**
	 * \brief 指定インデックスのプレイヤデータを文字列で取得する
	 * \param index インデックス
	 * \return プレイヤデータの文字列
	 */
	auto ToString(int index) const -> std::string;
};
