#include "ResultObject.hpp"

#include "../ResourcePath.hpp"

#include "../Engine/Audio.hpp"
#include "../Engine/Input.hpp"
#include "../Engine/SceneManager.hpp"
#include "../Engine/Text.hpp"

#include "../TitleScene/TitleScene.hpp"

#include "ResultData.hpp"

namespace {

constexpr auto BACKGROUND_FILE_NAME = "title_back.png";
constexpr auto BOX_FILE_NAME = "basic_box.png";
constexpr auto BOX_IMAGE_SCALE = Vector2::Set(6, 1);
constexpr auto FONT_SIZE = 32;
constexpr auto FONT_SIZE_HALF = FONT_SIZE * 0.5f;
constexpr auto BGM_FILE_NAME = "result_bgm.wav";
constexpr auto BGM_VOLUME = 0.5f;
}

ResultObject::ResultObject()
	: GameObject(OBJECT_NAME)
	, hBackGroundImage_(Image::UNALLOCATED_HANDLE)
	, hBoxImage_(Image::UNALLOCATED_HANDLE)
	, hFormat_(Text::UNALLOCATED_HANDLE)
	, hBGM_(Audio::UNALLOCATED_HANDLE) {}

void ResultObject::Initialize() {

	using namespace ResourcePath;

	hBackGroundImage_ = Image::Load(TITLE_DIR + BACKGROUND_FILE_NAME);
	
	hBoxImage_ = Image::Load(RESULT_DIR + BOX_FILE_NAME);
	Image::SetScale(hBoxImage_, BOX_IMAGE_SCALE);
	
	hFormat_ = Text::CreateFormat("UD デジタル 教科書体 N-R", FONT_SIZE);
	Text::SetBasePosition(hFormat_, BASE_POSITION::LEFT_CENTER);
	Text::SetAlignment(hFormat_, DWRITE_TEXT_ALIGNMENT_CENTER);

	hBGM_ = Audio::Load(RESULT_DIR + BGM_FILE_NAME);
	Audio::IsLoop(hBGM_, true);
	Audio::Play(hBGM_, BGM_VOLUME);
	
	if (auto* resultData = ResultData::GetInstance()) {

		resultData->Sort();
	}
}

void ResultObject::Update() {

	if (Input::IsKeyDown(VK_TAB)) {
		if (auto* resultData = ResultData::GetInstance()) {

			resultData->NextSort();
		}
	}

	if (Input::IsKeyDown(VK_RETURN)) {
		if (auto* sceneManager = SceneManager::GetInstance()) {

			sceneManager->ChangeScene(TitleScene::SCENE_NAME);
		}
	}
}

void ResultObject::Render() {

	Image::SetFullSize(hBackGroundImage_);
	Image::Render(hBackGroundImage_);
	
	if (auto* resultData = ResultData::GetInstance()) {

		auto size = resultData->GetSize();
		auto position = Vector2::Set(0, -FONT_SIZE_HALF * size);
		auto positionOffset = Vector2::Set(0, FONT_SIZE);
		for (auto i = 0; i < size; ++i) {

			Image::SetPosition(hBoxImage_, position + Vector2::Set(0, FONT_SIZE_HALF));
			Image::Render(hBoxImage_);
			Text::SetPosition(hFormat_, position, BASE_POSITION::LEFT_CENTER);
			Text::Render(hFormat_, resultData->ToString(i));
			position += positionOffset;
		}
	}
}

void ResultObject::Finalize() {

	Audio::Stop(hBGM_);
	
	ResultData::Destroy();
}
