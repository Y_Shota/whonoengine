#pragma once
#include <d3d11_1.h>
#include <d2d1_1.h>

#include "ComPtrDefinition.hpp"


/**
 * \brief DirectX本体に関する処理
 * \author whono
 * \version 1.0
 * \since 1.0
 */
namespace Whono::DirectX11 {

/**
 * \brief DirectXの初期化処理
 * \throw DirectXException
 * \throw WinException
 */
void Initialize() noexcept(false);

/**
 * \brief 初期化済みかどうか
 * \return 初期化済みなら真
 */
[[nodiscard]]
auto IsInitialized() noexcept -> bool;

/**
 * \brief DirectXの最終処理
 */
void Finalize() noexcept;

/**
 * \brief 毎フレーム行う処理
 * \throw DirectXException
 * \throw WinException
 */
void FrameUpdate() noexcept(false);

/**
 * \brief ウィンドウサイズに合わせてバックバッファ等をリサイズする
 */
void Resize();

/**
 * \brief D3Dデバイスコンデキストを取得
 * \return D3Dデバイスコンテキスト
 */
[[nodiscard]]
auto GetD3DDeviceContext() noexcept -> CComPtrRef<ID3D11DeviceContext>;

/**
 * \brief D3Dデバイスを取得
 * \return D3Dデバイス
 */
[[nodiscard]]
auto GetD3DDevice() noexcept -> CComPtrRef<ID3D11Device>;

/**
 * \brief 描画先をデフォルトの状態にする
 */
void SetDefaultRenderTarget() noexcept;

/**
 * \brief D2Dデバイスを取得
 * \return D2Dデバイス
 */
[[nodiscard]]
auto GetD2DDevice() noexcept -> CComPtrRef<ID2D1Device>;
/**
 * \brief D2Dデバイスを取得
 * \return D2Dデバイス
 */
[[nodiscard]]
auto GetD2DeviceContext() noexcept -> CComPtrRef<ID2D1DeviceContext>;

/**
 * \brief DWファクトリを取得
 * \return DWファクトリ
 */
[[nodiscard]]
auto GetDwFactory() noexcept -> CComPtrRef<IDWriteFactory>;
}
