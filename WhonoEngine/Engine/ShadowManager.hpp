#pragma once
#include <memory>

namespace Whono::GameOperation {

/**
 * \brief 影の描画に関する処理を行うクラス
 */
class ShadowManager {

public:
	/**
	 * \brief コンストラクタ
	 */
	ShadowManager();
	/**
	 * \brief デストラクタ
	 */
	~ShadowManager();

	/**
	 * \brief 初期化処理
	 */
	void Initialize() const noexcept(false);
	/**
	 * \brief 終了処理
	 */
	void Finalize();

	/**
	 * \brief 描画開始処理
	 */
	void Begin() const;
	/**
	 * \brief 描画終了処理
	 */
	void End() const;
	

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽用
};

}

