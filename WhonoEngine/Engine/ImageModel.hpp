#pragma once
#include "IRenderer3D.hpp"

#include <memory>


namespace Whono::RenderObject {


/**
 * \brief 画像データ読み込みを3D空間にレンダリングする
 * \author whono
 * \version 1.0
 * \since 1.0
 */
class ImageModel final : public IRenderer3D {

public:
	/**
	 * \brief コンストラクタ
	 */
	ImageModel();

	/**
	 * \brief デストラクタ
	 */
	~ImageModel();

	
	/**
	 * \brief 画像リソースを読み込み描画に必要な情報を生成する
	 * \param filePath 画像リソースファイルパス
	 * \return 
	 */
	auto Load(const std::filesystem::path& filePath) -> bool override;

	/**
	 * \brief 変形情報をもとに画像を描画する
	 * \param transform 変形情報
	 * \param frame アニメーションのフレーム数
	 */
	void Render(const GameOperation::Transform* transform, int frame = 0) override;

	/**
	 * \brief アニメーションを持っているかどうか
	 * \return 持っている場合真
	 */
	auto HasAnimation() -> bool override;

	/**
	 * \brief ボーンの座標を取得する
	 * \param boneName ボーンの名前
	 * \param frame アニメーションのフレーム数の指定
	 * \return ボーンの座標　ない場合無効値
	 */
	auto GetBonePosition(const std::string& boneName, int frame = 0) -> std::optional<DirectX::XMVECTOR> override;

	/**
	 * \brief ボーンの変形情報を取得する
	 * \param boneName ボーンの名前
	 * \param frame アニメーションのフレーム数の指定
	 * \return ボーンの変形情報　ない場合は無効値
	 */
	auto GetBoneTransform(const std::string& boneName, int frame = 0) -> std::unique_ptr<GameOperation::Transform> override;
	
	/**
	 * \brief モデル空間でのサイズを取得する
	 * \return サイズ
	 */
	auto GetExtents() -> DirectX::XMVECTOR override;
	/**
	 * \brief モデル座標系での中心座標を取得する
	 * \return 中心座標
	 */
	auto GetCenterPosition() -> DirectX::XMVECTOR override;
	
	/**
	 * \brief レイキャストを行う
	 * \param origin レイの始点
	 * \param direction レイの方向
	 * \param dist [out] 衝突したときの距離
	 * \param normal [out] 衝突したポリゴンの法線
	 * \return 衝突したなら真
	 */
	auto RayCast(DirectX::CXMVECTOR origin, DirectX::CXMVECTOR direction, float* dist, DirectX::XMVECTOR* normal) const -> bool override;
	
	/**
	 * \brief 解放処理
	 */
	void Release() override;
private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽
};


}