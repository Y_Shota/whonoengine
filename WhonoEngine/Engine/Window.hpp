#pragma once

#define NOMINMAX
#include <Windows.h>

/**
 * \brief ウィンドウに関する処理
 * \author whono
 * \version 1.0
 * \since 1.0
 */
namespace Whono::Window {
	
constexpr CHAR CLASS_NAME[] = "Pulverization Gemology";		//ウィンドウクラス名
constexpr CHAR WINDOW_NAME[] = "Pulverization Gemology";	//ウィンドウ名

/**
 * \brief ウィンドウを初期化し、表示する
 * \param hInstance インスタンスハンドル
 * \param nCmdShow コマンド
 * \throw WinException
 */
void InitWindow(HINSTANCE hInstance, int nCmdShow) noexcept(false);

/**
 * \brief ウィンドウハンドルの取得
 * \return ウィンドウハンドル
 */
[[nodiscard]]
auto GetWindowHandle() noexcept -> HWND;

/**
 * \brief ウィンドウのアクティブ状態の取得
 * \return ウィンドウの状態
 */
[[nodiscard]]
auto IsActive() -> bool;

/**
 * \brief ウィンドウのサイズを取得
 * \return ウィンドウサイズ
 */
[[nodiscard]]
auto GetWindowSize() -> RECT;
/**
 * \brief ウィンドウの幅を取得する
 * \return ウィンドウ幅
 */
[[nodiscard]]
auto GetWindowWidth() -> UINT;
/**
* \brief ウィンドウの高さを取得する
* \return ウィンドウ高
*/
[[nodiscard]]
auto GetWindowHeight() -> UINT;
/**
 * \brief ウィンドウの幅を取得する
 * \return ウィンドウ幅
 */
[[nodiscard]]
auto GetWindowWidthF() -> float;
/**
 * \brief ウィンドウの高さを取得する
 * \return ウィンドウ高
 */
[[nodiscard]]
auto GetWindowHeightF() -> float;
/**
* \brief 変更前のウィンドウの幅を取得する
* \return 変更前のウィンドウ幅
*/
[[nodiscard]]
auto GetWindowPrevWidthF() -> float;
/**
* \brief 変更前のウィンドウの高さを取得する
* \return 変更前のウィンドウ高
*/
[[nodiscard]]
auto GetWindowPrevHeightF() -> float;
/**
 * \brief ウィンドウのアスペクト比を取得する
 * \return アスペクト比
 */
[[nodiscard]]
auto GetAspectRatio() -> float;

};