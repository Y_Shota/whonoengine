#pragma once
#include "ICollider.hpp"

#include <DirectXCollision.h>
#include <memory>


namespace Whono::GameOperation {


/**
 * \brief 錐台の衝突判定を検証するためのクラス
 */
class FrustumCollider final : public ICollider {
	
public:
	/**
	 * \brief コンストラクタ
	 * \param origin 始点
	 * \param oriented 姿勢
	 * \param slope 各方向のスロープの大きさ
	 * \param nearZ 最小距離
	 * \param farZ 最大距離
	 */
	FrustumCollider(DirectX::CXMVECTOR origin, DirectX::CXMVECTOR oriented, DirectX::CXMVECTOR slope, float nearZ, float farZ);
	/**
	 * \brief デストラクタ
	 */
	~FrustumCollider();

	/**
	 * \brief 中心座標を取得
	 * \return 中心座標
	 */
	[[nodiscard]]
	auto GetCenterPosition() const -> DirectX::XMVECTOR override;

	/**
	 * \brief 受け取った座標から領域内でもっとも近いであろう点を返す
	 * \param point 比較対象
	 * \return 領域内の座標
	 */
	[[nodiscard]]
	auto GetNearestControlPoint(DirectX::CXMVECTOR point) const -> DirectX::XMVECTOR override;

	/**
	 * \brief 受け取ったベクトルに射影した長さの最大値を取得
	 * \param vector 射影するベクトル
	 * \return 射影した長さの最大値
	 */
	[[nodiscard]]
	auto GetProjectionLengthMax(DirectX::CXMVECTOR vector) const -> float override;
	
	/**
	 * \brief 所持させるゲームオブジェクトを設定
	 * \param gameObject ゲームオブジェクト
	 */
	void SetGameObject(GameObject* gameObject) override;
	/**
	 * \brief 境界線を描画する
	 */
	void Render() const override;
	/**
	 * \brief 衝突判定を行う
	 * \param target 検証相手
	 * \return 当たったら真
	 */
	[[nodiscard]]
	auto IsHit(ICollider* target) const -> bool override;

	/**
	 * \brief レイキャストを行う
	 * \param origin 発射地点
	 * \param direction 方向
	 * \param[out] dist 当たった時の距離
	 * \param[out] normal 当たったポリゴンの法線
	 * \return 当たったなら真
	 */
	auto RayCast(DirectX::CXMVECTOR origin, DirectX::CXMVECTOR direction, float* dist, DirectX::XMVECTOR* normal) const -> bool override { return false; }

	/**
	 * \brief DirectXのバウンディングを取得する
	 * \return DirectXのBoundingFrustum
	 */
	[[nodiscard]]
	auto GetBounding() const noexcept -> DirectX::BoundingFrustum;

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽
};

}