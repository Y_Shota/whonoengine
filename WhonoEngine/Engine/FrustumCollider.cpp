#include "FrustumCollider.hpp"

#include "CollisionCalculator.hpp"
#include "GameObject.hpp"

using namespace std;
using namespace DirectX;


namespace Whono::GameOperation {

struct FrustumCollider::Impl {

	/**
	 * \brief バウンディングを設定する
	 * \param origin 始点
	 * \param oriented 姿勢
	 * \param slope 各方向のスロープの大きさ
	 * \param nearZ 最小距離
	 * \param farZ 最大距離
	 */
	void SetBounding(CXMVECTOR origin, CXMVECTOR oriented, CXMVECTOR slope, float nearZ, float farZ) noexcept;
	/**
	 * \brief バウンディングを設定する
	 * \param projection プロジェクション行列
	 */
	void SetBounding(CXMMATRIX projection) noexcept;
	
	BoundingFrustum bounding;	//計算用バウンディング
	GameObject* gameObject;		//所有者
	int hModel;					//モデルハンドル
};

FrustumCollider::FrustumCollider(CXMVECTOR origin, CXMVECTOR oriented, CXMVECTOR slope, float nearZ, float farZ) {

	pImpl_ = make_unique<Impl>();
	pImpl_->gameObject = nullptr;
	pImpl_->hModel = -1;

	pImpl_->SetBounding(origin, oriented, slope, nearZ, farZ);
}

FrustumCollider::~FrustumCollider() = default;

auto FrustumCollider::GetCenterPosition() const -> XMVECTOR {

	//todo 中心座標の抽出
	return g_XMZero;
}

auto FrustumCollider::GetNearestControlPoint(CXMVECTOR point) const -> XMVECTOR {

	//todo 平面の少し内側の点を計算
	return GetCenterPosition();
}

auto FrustumCollider::GetProjectionLengthMax(CXMVECTOR vector) const -> float {

	//todo
	return 0.f;
}

void FrustumCollider::SetGameObject(GameObject* gameObject) {

	pImpl_->gameObject = gameObject;
}

void FrustumCollider::Render() const {

	//todo 描画処理
}

auto FrustumCollider::IsHit(ICollider* target) const -> bool {

	if (auto* sphere = dynamic_cast<SphereCollider*>(target)) {

		return CollisionCalculator::IsHit(this, sphere);
	}
	if (auto* box = dynamic_cast<BoxCollider*>(target)) {

		return CollisionCalculator::IsHit(this, box);
	}
	if (auto* obb = dynamic_cast<ObbCollider*>(target)) {

		return CollisionCalculator::IsHit(this, obb);
	}
	if (auto* frustum = dynamic_cast<FrustumCollider*>(target)) {

		return CollisionCalculator::IsHit(this, frustum);
	}
	if (auto* capsule = dynamic_cast<CapsuleCollider*>(target)) {

		return CollisionCalculator::IsHit(this, capsule);
	}
	return false;
}

auto FrustumCollider::GetBounding() const noexcept -> BoundingFrustum {

	BoundingFrustum bounding;
	auto matrix = pImpl_->gameObject->GetTransform()->GetWorldMatrix();

	pImpl_->bounding.Transform(bounding, matrix);

	return bounding;
}

void FrustumCollider::Impl::SetBounding(CXMVECTOR origin, CXMVECTOR oriented, CXMVECTOR slope, float nearZ, float farZ) noexcept {

	XMStoreFloat3(&bounding.Origin, origin);
	XMStoreFloat4(&bounding.Orientation, oriented);
	bounding.RightSlope = slope.m128_f32[0];
	bounding.LeftSlope = slope.m128_f32[1];
	bounding.TopSlope = slope.m128_f32[2];
	bounding.BottomSlope = slope.m128_f32[3];
	bounding.Near = nearZ;
	bounding.Far = farZ;
}

void FrustumCollider::Impl::SetBounding(CXMMATRIX projection) noexcept {

	bounding = BoundingFrustum(projection);
}


}
