#include "Input.hpp"

#include <array>
#include <iostream>
#include <Xinput.h>

#include "Window.hpp"
#include "WinException.hpp"

#pragma comment(lib, "xinput.lib")

using namespace std;
using namespace DirectX;


namespace Whono::Input {

constexpr int KEY_MASK = 0x80;
	
array<BYTE, 256> keyState;
array<BYTE, 256> prevKeyState;
int anyKeyState = 0;
int prevAnyKeyState = 0;

struct MouseState {
	bool rDoubleClick = false;
	bool lDoubleClick = false;
	bool mDoubleClick = false;
	POINT screenPos = { 0, 0 };
	XMVECTOR clientPos = g_XMZero;
};

MouseState mouseState;
MouseState prevMouseState;
bool isCenterClip = false;
XMVECTOR centerPosition = g_XMZero;

constexpr int MAX_PAD = 4;
array<XINPUT_STATE, MAX_PAD> padStates;
array<XINPUT_STATE, MAX_PAD> prevPadStates;

float GetAnalogValue(int raw, int max, int deadZone) noexcept;

void Initialize() noexcept(false) {
		
	keyState.fill(0);
	auto result = ::GetKeyboardState(keyState.data());
	if (!result)	throw Window::WinException("::GetKeyboardState");

	prevKeyState = keyState;

	result = ::GetCursorPos(&mouseState.screenPos);
	if (!result)	throw Window::WinException("::GetCurSorPos");

	prevMouseState = mouseState;

	for (auto i = 0u; i < padStates.size(); ++i) {

		::XInputGetState(i, &padStates[i]);
	}
	prevPadStates = padStates;
}

void PreProcess() noexcept(false) {

	if (Window::IsActive()) {

		auto result = ::GetKeyboardState(keyState.data());
		if (!result)	throw Window::WinException("::GetKeyboardState");

		anyKeyState = 0;
		for (auto i = 0; auto & key : keyState) {

			//マウスは無視
			if (i != VK_LBUTTON && i != VK_RBUTTON && i != VK_MBUTTON) {

				if (key & KEY_MASK)	++anyKeyState;
			}
			++i;
		}
		anyKeyState -= 4;	//常に押されているキーが4つある
		
		result = ::GetCursorPos(&mouseState.screenPos);
		if (!result)	throw Window::WinException("::GetCurSorPos");

		for (auto i = 0u; i < padStates.size(); ++i) {

			::XInputGetState(i, &padStates[i]);
		}

		if (isCenterClip) {

			auto hWnd = Window::GetWindowHandle();
			RECT client;
			GetClientRect(hWnd, &client);
			POINT point = {
				client.left + (client.right - client.left) / 2,
				client.top + (client.bottom - client.top) / 2 };
			centerPosition = XMVectorSet(static_cast<float>(point.x), static_cast<float>(point.y), 0, 0);
			ClientToScreen(hWnd, &point);
			SetCursorPos(point.x, point.y);
		}
	}
	else {

		keyState.fill(0);
		prevKeyState.fill(0);
		ZeroMemory(&mouseState, sizeof(mouseState));
		ZeroMemory(&prevMouseState, sizeof(prevMouseState));
	}
}

void PostProcess() noexcept {

	prevKeyState = keyState;
	prevAnyKeyState = anyKeyState;

	prevMouseState = mouseState;

	prevPadStates = padStates;

	if (mouseState.lDoubleClick)	mouseState.lDoubleClick = false;
	if (mouseState.rDoubleClick)	mouseState.rDoubleClick = false;
	if (mouseState.mDoubleClick)	mouseState.mDoubleClick = false;
}

auto IsKey(unsigned keyCode) -> bool {

	return keyState[keyCode] & KEY_MASK;
}

auto IsKeyDown(unsigned keyCode) -> bool {
	
	return keyState[keyCode] & ~prevKeyState[keyCode] & KEY_MASK;
}

auto IsKeyUp(unsigned keyCode) -> bool {

	return ~keyState[keyCode] & prevKeyState[keyCode] & KEY_MASK;
}

auto IsAnyKey() -> bool {

	return anyKeyState > 0;
}

auto IsAnyKeyDown() -> bool {

	return anyKeyState > prevAnyKeyState;
}

auto IsAnyKeyUp() -> bool {

	return anyKeyState < prevAnyKeyState;
}

auto GetPressKeyCount() -> int {

	return anyKeyState;
}

auto IsCursorInWindow() -> bool {

	return !(mouseState.clientPos.m128_f32[0] < 0 || mouseState.clientPos.m128_f32[1] < 0);
}

auto GetCursorPos() -> CXMVECTOR {

	return mouseState.clientPos;
}

auto GetCursorMove() -> XMVECTOR {
	
	//過去か現在が画面外だった場合比較しない
	if (XMVector2Less(mouseState.clientPos, g_XMZero))		return g_XMZero;
	if (XMVector2Less(prevMouseState.clientPos, g_XMZero))	return g_XMZero;

	//中心にクリップ中の時は中心との差
	if (isCenterClip) {

		auto xyMove = XMVectorSelect(g_XMZero, XMVectorSubtract(mouseState.clientPos, centerPosition), g_XMSelect1100);
		auto mouseWheelDelta = XMVectorSelect(g_XMZero, XMVectorSubtract(mouseState.clientPos, prevMouseState.clientPos), g_XMMaskZ);
		return XMVectorAdd(xyMove, mouseWheelDelta);
	}
	
	return XMVectorSubtract(mouseState.clientPos, prevMouseState.clientPos);
}

auto IsDoubleClickL() noexcept -> bool {

	return mouseState.lDoubleClick;
}

auto IsDoubleClickR() noexcept -> bool {

	return mouseState.rDoubleClick;
}

auto IsDoubleClickM() noexcept -> bool {

	return mouseState.mDoubleClick;
}

auto IsPadButton(unsigned buttonCode, unsigned padID) -> bool {

	return padStates[padID].Gamepad.wButtons & buttonCode;
}

auto IsPadButtonDown(unsigned buttonCode, unsigned padID) -> bool {

	auto& buttonState = padStates[padID].Gamepad.wButtons;
	auto& prevButtonState = prevPadStates[padID].Gamepad.wButtons;
	return buttonState & ~prevButtonState & buttonCode;
}

auto IsPadButtonUp(unsigned buttonCode, unsigned padID) -> bool {

	auto& buttonState = padStates[padID].Gamepad.wButtons;
	auto& prevButtonState = prevPadStates[padID].Gamepad.wButtons;
	return ~buttonState & prevButtonState & buttonCode;
}

auto GetPadStickL(unsigned padID) -> XMVECTOR {

	auto x = GetAnalogValue(padStates[padID].Gamepad.sThumbLX, SHRT_MAX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
	auto y = GetAnalogValue(padStates[padID].Gamepad.sThumbLY, SHRT_MAX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
	return XMVectorSet(x, y, 0, 0);
}

auto GetPadStickR(unsigned padID) -> XMVECTOR {

	auto x = GetAnalogValue(padStates[padID].Gamepad.sThumbRX, SHRT_MAX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
	auto y = GetAnalogValue(padStates[padID].Gamepad.sThumbRY, SHRT_MAX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
	return XMVectorSet(x, y, 0, 0);
}

auto GetPadTriggerL(unsigned padID) -> float {
	
	return GetAnalogValue(padStates[padID].Gamepad.bLeftTrigger, UCHAR_MAX, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
}

auto GetPadTriggerR(unsigned padID) -> float {

	return GetAnalogValue(padStates[padID].Gamepad.bRightTrigger, UCHAR_MAX, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
}

void SetPadVibration(WORD powL, WORD powR, unsigned padID) {

	XINPUT_VIBRATION vibration;
	vibration.wLeftMotorSpeed = powL;
	vibration.wRightMotorSpeed = powR;
	::XInputSetState(padID, &vibration);
}

void ClipCenterMouse(bool isClip) {

	isCenterClip = isClip;
	ShowCursor(!isCenterClip);
}

auto MouseProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) -> bool {

	switch (message) {
		
	case WM_LBUTTONDBLCLK:		//左がダブルクリックされた
		mouseState.lDoubleClick = true;
		return true;
		
	case WM_RBUTTONDBLCLK:		//右がダブルクリックされた
		mouseState.rDoubleClick = true;
		return true;
		
	case WM_MBUTTONDBLCLK:		//中央がダブルクリックされた
		mouseState.mDoubleClick = true;
		return true;
		
	case WM_MOUSELEAVE:			//マウスがウィンドウから出た
		mouseState.clientPos.m128_f32[0] = -1;
		mouseState.clientPos.m128_f32[1] = -1;
		return true;
		
	case WM_MOUSEMOVE: {		//マウスが動いた

		mouseState.clientPos.m128_f32[0] = LOWORD(lParam);
		mouseState.clientPos.m128_f32[1] = HIWORD(lParam);
		
		//WM_MOUSELEAVEが送られるようにする
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = hWnd;
		TrackMouseEvent(&tme);
		return true;
	}
	case WM_MOUSEWHEEL:			//マウスホイールが動いた
		mouseState.clientPos.m128_f32[2] += GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA;
		return true;
		
	default:
		return false;
	}
}

float GetAnalogValue(int raw, int max, int deadZone) noexcept {

	if (auto absResult = abs(raw); absResult < deadZone)	return 0;

	auto result = static_cast<float>(raw);

	if (result > 0) return (result - deadZone) / (max - deadZone);
		
	return (result + deadZone) / (max - deadZone);
}

}
