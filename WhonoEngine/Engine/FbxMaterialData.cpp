#include "FbxMaterialData.hpp"

#include <filesystem>
#include <ranges>
#include <unordered_map>


#include "FbxHelper.hpp"


using namespace std;
using namespace DirectX;

namespace fs = filesystem;

using TexturePtr = unique_ptr<Whono::RenderObject::Texture>;



namespace Whono::RenderObject {

struct FbxMaterialData::Impl {

	void Initialize(FbxSurfaceMaterial* surfaceMaterial);
	void AddTexture(const string& key, const FbxProperty& property);
	auto GetTexture(const std::string& key) const -> Texture*;
	
	string		name;				//マテリアル名
	uint64_t	id;					//ユニークなID
	XMVECTOR	ambient;			//環境光（自身を照らす光）
	XMVECTOR	diffuse;			//拡散反射光（自身の持つ色味）
	XMVECTOR	emissive;			//放射光（自身が放つ光）
	float		transparent;		//透過度（自身の透過度）
	XMVECTOR	specular;			//鏡面反射光（ハイライト負荷）
	float		shininess = 1.f;	//光沢（ハイライト強度）
	float		reflection;			//反射率（環境マップの強度）
	unordered_map<string, TexturePtr> textureMap;	//各種テクスチャを保持
};


FbxMaterialData::FbxMaterialData() {

	pImpl_ = make_unique<Impl>();
}

FbxMaterialData::~FbxMaterialData() = default;

void FbxMaterialData::Initialize(FbxSurfaceMaterial* surfaceMaterial) const {

	pImpl_->Initialize(surfaceMaterial);
}

auto FbxMaterialData::GetName() const noexcept -> const string& {

	return pImpl_->name;
}

auto FbxMaterialData::GetUniqueId() const noexcept -> uint64_t {

	return pImpl_->id;
}

auto FbxMaterialData::GetAmbient() const noexcept -> CXMVECTOR {

	return pImpl_->ambient;
}

auto FbxMaterialData::GetDiffuse() const noexcept -> CXMVECTOR {

	return pImpl_->diffuse;
}

auto FbxMaterialData::GetEmissive() const noexcept -> CXMVECTOR {

	return pImpl_->emissive;
}

auto FbxMaterialData::GetTransparent() const noexcept -> float {

	return pImpl_->transparent;
}

auto FbxMaterialData::GetSpecular() const noexcept -> CXMVECTOR {

	return pImpl_->specular;
}

auto FbxMaterialData::GetShininess() const noexcept -> float {

	return pImpl_->shininess;
}

auto FbxMaterialData::GetReflection() const noexcept -> float {

	return pImpl_->reflection;
}

auto FbxMaterialData::GetTexture(const std::string& key) const -> Texture* {

	return pImpl_->GetTexture(key);
}

void FbxMaterialData::Release() {

	for (auto& texture : pImpl_->textureMap | views::values) {

		texture->Release();
		texture.reset();
	}
	pImpl_->textureMap.clear();
	pImpl_.reset();
}

void FbxMaterialData::Impl::Initialize(FbxSurfaceMaterial* surfaceMaterial) {

	name = surfaceMaterial->GetName();
	id = surfaceMaterial->GetUniqueID();
	
	//ambient
	auto property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sAmbient);
	if (property.IsValid()) {

		const auto& color = property.Get<FbxDouble3>();
		ambient = FbxHelper::ConvertToXmvector(color);
		AddTexture(FbxSurfaceMaterial::sAmbient, property);
	}

	//diffuse
	property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sDiffuse);
	if (property.IsValid()) {

		const auto& color = property.Get<FbxDouble3>();
		diffuse = FbxHelper::ConvertToXmvector(color);
		AddTexture(FbxSurfaceMaterial::sDiffuse, property);
	}

	//emissive
	property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sEmissive);
	if (property.IsValid()) {

		const auto& color = property.Get<FbxDouble3>();
		emissive = FbxHelper::ConvertToXmvector(color);
		AddTexture(FbxSurfaceMaterial::sEmissive, property);
	}

	//transparent
	property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sTransparentColor);
	if (property.IsValid()) {

		const auto& color = property.Get<FbxDouble>();
		transparent = static_cast<float>(color);
		AddTexture(FbxSurfaceMaterial::sTransparentColor, property);
	}

	//specular
	property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sSpecular);
	if (property.IsValid()) {

		const auto& color = property.Get<FbxDouble3>();
		specular = FbxHelper::ConvertToXmvector(color);
		specular.m128_f32[3] = 0;
		AddTexture(FbxSurfaceMaterial::sSpecular, property);
	}

	//shininess
	property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sShininess);
	if (property.IsValid()) {

		const auto& color = property.Get<FbxDouble>();
		shininess = static_cast<float>(color);
		AddTexture(FbxSurfaceMaterial::sShininess, property);
	}

	//reflection
	property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sReflection);
	if (property.IsValid()) {

		const auto& color = property.Get<FbxDouble>();
		reflection = static_cast<float>(color);
		AddTexture(FbxSurfaceMaterial::sReflection, property);
	}

	//bumpMap
	property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sBump);
	if (property.IsValid()) {

		AddTexture(FbxSurfaceMaterial::sBump, property);
	}

	//normalMap
	property = surfaceMaterial->FindProperty(FbxSurfaceMaterial::sNormalMap);
	if (property.IsValid()) {

		AddTexture(FbxSurfaceMaterial::sNormalMap, property);
	}
}

void FbxMaterialData::Impl::AddTexture(const string& key, const FbxProperty& property) {

	//テクスチャ情報がないなら無視
	if (property.GetSrcObjectCount<FbxFileTexture>() <= 0)	return;

	//テクスチャの情報取得
	auto* fbxTexture = property.GetSrcObject<FbxFileTexture>();

	//相対パスを取得
	string utf6TexturePath = fbxTexture->GetRelativeFileName();

	//Ansiに変換
	char* convertedTexturePath;
	FbxUTF8ToAnsi(utf6TexturePath.c_str(), convertedTexturePath, nullptr);
	
	fs::path texturePath = convertedTexturePath;

	//テクスチャ作成ロード
	auto texture =  make_unique<Texture>();
	texture->Load(texturePath.filename());
	
	textureMap.emplace(key, move(texture));
}

auto FbxMaterialData::Impl::GetTexture(const std::string& key) const -> Texture* {
	
	auto itr = textureMap.find(key);
	
	if (itr == textureMap.end())	return nullptr;

	return itr->second.get();
}

}
