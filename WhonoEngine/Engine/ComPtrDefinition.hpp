#pragma once

#define NOMINMAX
#include <wrl/client.h>

using Microsoft::WRL::ComPtr;

namespace Whono {

template<class T>
using ComPtrRef = ComPtr<T>&;
template<class T>
using CComPtr = const ComPtr<T>;
template<class T>
using CComPtrRef = const ComPtr<T>&;

}