#include "String.hpp"
#include <filesystem>

using namespace std;


namespace Whono::String {

auto ConvertToWString(const string& source) -> wstring {

	return filesystem::path(source);
}

auto ConvertToString(const wstring& source) -> string {

	return filesystem::path(source).string();
}

}
