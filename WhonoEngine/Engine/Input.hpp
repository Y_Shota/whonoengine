#pragma once
#define NOMINMAX
#include <activation.h>
#include <DirectXMath.h>

// ReSharper disable once CppUnusedIncludeDirective
#include "KeyCodeDefinition.hpp"

/**
 * \brief 入力機器に関する処理
 * \author whono
 * \version 1.0
 * \since 1.0
 */
namespace Whono::Input {

/**
 * \brief 初期化処理 
 * \throw WinException
 */
void Initialize() noexcept(false);


/**
 * \brief フレーム開始時に行う処理　各入力機器の状態を更新
 * \throw WinException
 */
void PreProcess() noexcept(false);

/**
 * \brief フレーム終了時に行う処理　各入力機器の過去の状態を保持
 */
void PostProcess() noexcept;


/**
 * \brief キーの入力状態の取得
 * \param keyCode キーコード VK_〇〇
 * \return キーの状態
 */
[[nodiscard]]
auto IsKey(unsigned keyCode) -> bool;

/**
 * \brief キーを押下した瞬間のみTRUE
 * \param keyCode キーコード VK_〇〇
 * \return キーの状態
 */
[[nodiscard]]
auto IsKeyDown(unsigned keyCode) -> bool;

/**
 * \brief キーを離した瞬間のみTRUE
 * \param keyCode キーコード VK_〇〇
 * \return キーの状態
 */
[[nodiscard]]
auto IsKeyUp(unsigned keyCode) -> bool;


[[nodiscard]]
auto IsAnyKey() -> bool;

[[nodiscard]]
auto IsAnyKeyDown() -> bool;

[[nodiscard]]
auto IsAnyKeyUp() -> bool;

[[nodiscard]]
auto GetPressKeyCount() -> int;

/**
 * \brief カーソルがメインウィンドウ内にいるかどうか
 * \return カーソルがメインウィンドウ内にいるか
 */
[[nodiscard]]
auto IsCursorInWindow() -> bool;

/**
 * \brief カーソルのクライアント座標の取得 範囲外の場合負
 * \return 座標
 */
[[nodiscard]]
auto GetCursorPos() -> DirectX::CXMVECTOR;

/**
 * \brief カーソルの移動量の取得　zマウスホイール
 * \return 変化量
 */
[[nodiscard]]
auto GetCursorMove() -> DirectX::XMVECTOR;


/**
 * \brief マウスの左ダブルクリックの瞬間の取得
 * \return ダブルクリックしたかどうか
 */
[[nodiscard]]
auto IsDoubleClickL() noexcept -> bool;

/**
 * \brief マウスの右ダブルクリックの瞬間の取得
 * \return ダブルクリックしたかどうか
 */
[[nodiscard]]
auto IsDoubleClickR() noexcept -> bool;

/**
 * \brief マウスの中央ダブルクリックの瞬間の取得
 * \return ダブルクリックしたかどうか
 */
[[nodiscard]]
auto IsDoubleClickM() noexcept -> bool;


/**
 * \brief ゲームパッドのボタンの入力状態の取得
 * \param buttonCode ボタンコード PAD_〇〇
 * \param padID = 0 パッドのID
 * \return ボタンの状態
 */
[[nodiscard]]
auto IsPadButton(unsigned buttonCode, unsigned padID = 0)  -> bool;

/**
 * \brief ゲームパッドのボタンを押下した瞬間のみTRUE
 * \param buttonCode ボタンコード PAD_〇〇
 * \param padID = 0 パッドのID
 * \return ボタンの状態
 */
[[nodiscard]]
auto IsPadButtonDown(unsigned buttonCode, unsigned padID = 0) -> bool;

/**
 * \brief ゲームパッドのボタンを離した瞬間のみTRUE
 * \param buttonCode ボタンコード PAD_〇〇
 * \param padID = 0 パッドのID
 * \return ボタンの状態
 */
[[nodiscard]]
auto IsPadButtonUp(unsigned buttonCode, unsigned padID = 0) -> bool;

/**
 * \brief 左スティックの傾きの取得
 * \param padID = 0 パッドのID
 * \return 傾き
 */
[[nodiscard]]
auto GetPadStickL(unsigned padID = 0) -> DirectX::XMVECTOR;

/**
 * \brief 右スティックの傾きの取得
 * \param padID = 0 パッドのID
 * \return 傾き
 */
[[nodiscard]]
auto GetPadStickR(unsigned padID = 0) -> DirectX::XMVECTOR;

/**
 * \brief 左トリガーの状態の取得
 * \param padID = 0 パッドのID
 * \return 押し加減
 */
[[nodiscard]]
auto GetPadTriggerL(unsigned padID = 0) -> float;

/**
 * \brief 右トリガーの状態の取得
 * \param padID = 0 パッドのID
 * \return 押し加減
 */
[[nodiscard]]
auto GetPadTriggerR(unsigned padID = 0) -> float;

/**
 * \brief パッドへ振動を指示する
 * \param powL 左モーターの強度
 * \param powR 右モーターの強度
 * \param padID = 0 パッドのID
 */
void SetPadVibration(WORD powL, WORD powR, unsigned padID = 0);

/**
 * \brief マウスをウィンドウの中心にクリップするか
 * \param isClip クリップするかどうか
 */
void ClipCenterMouse(bool isClip);

/**
 * \brief マウスに関するメッセージを処理する
 * \param hWnd ウィンドウハンドル
 * \param message メッセージ
 * \param wParam メッセージ付加情報
 * \param lParam メッセージ付加情報
 * \return 処理を行ったかどうか
 */
[[nodiscard]]
auto MouseProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)->bool;
	
};

