#include "ShadowManager.hpp"

#include <DirectXColors.h>
#include <wincodec.h>

#include "ComPtrDefinition.hpp"
#include "Direct3DHelper.hpp"
#include "DirectX11.hpp"
#include "DirectXException.hpp"
#include "GraphicsPipeline.hpp"
#include "Input.hpp"
#include "Light.hpp"
#include "SceneManager.hpp"
#include "SceneObject.hpp"
#include "ScreenGrab11.hpp"


namespace {

constexpr auto TEXTURE_SIZE = 4096u;		//シャドウマップのサイズ
constexpr auto VS_CONSTANT_LIGHT_SLOT = 2;	//ライト行列定数スロット
constexpr auto PS_SAMPLER_SLOT = 10;		//サンプラーのスロット
constexpr auto PS_MAP_SLOT = 10;			//シャドウマップのスロット
constexpr auto VS_CONSTANT_SHADOW_SLOT = 2;	//シャドウ行列定数スロット
//テクスチャバイアス
constexpr auto SHADOW_BIAS = DirectX::XMMATRIX{
	{ 0.5f, 0.0f, 0.0f, 0.0f },
	{ 0.0f, -0.5f, 0.0f, 0.0f },
	{ 0.0f, 0.0f, 1.0f, 0.0f },
	{ 0.5f, 0.5f, 0.0f, 1.0f }
};
}


namespace Whono::GameOperation {

struct LightMatrix {

	DirectX::XMMATRIX viewMatrix;
	DirectX::XMMATRIX projectionMatrix;
};

struct ShadowManager::Impl {

	ComPtr<ID3D11Texture2D>				shadowMap;		//シャドウマップ
	ComPtr<ID3D11DepthStencilView>		mapWriter;		//シャドウマップ書き込み用
	ComPtr<ID3D11ShaderResourceView>	mapReader;		//シャドウマップ読み込み用
	ComPtr<ID3D11SamplerState>			mapSampler;		//マップの読み取り方
	D3D11_VIEWPORT						viewport;		//描画範囲範囲
	ComPtr<ID3D11Buffer>				constantLight;	//ライト行列定数
	ComPtr<ID3D11Buffer>				constantShadow;	//シャドウ行列定数
	
	void Initialize() noexcept(false);
	void Finalize();

	void Begin() const;
	void End();
};


ShadowManager::ShadowManager() {

	pImpl_ = std::make_unique<Impl>();
}

ShadowManager::~ShadowManager() = default;

void ShadowManager::Initialize() const noexcept(false) {

	pImpl_->Initialize();
}

void ShadowManager::Finalize() {

	pImpl_->Finalize();
	pImpl_.reset();
}

void ShadowManager::Begin() const {

	pImpl_->Begin();
}

void ShadowManager::End() const {

	pImpl_->End();
}


void ShadowManager::Impl::Initialize() noexcept(false) {

	Finalize();

	//デバイス取得
	const auto& d3dDevice = DirectX11::GetD3DDevice();

	//シャドウマップの構成
	D3D11_TEXTURE2D_DESC descShadow;
	descShadow.Width = TEXTURE_SIZE;
	descShadow.Height = TEXTURE_SIZE;
	descShadow.MipLevels = 1;
	descShadow.ArraySize = 1;
	descShadow.Format = DXGI_FORMAT_R24G8_TYPELESS;	//シェーダーリソースに使用可能な形式
	descShadow.SampleDesc.Count = 1;
	descShadow.SampleDesc.Quality = 0;
	descShadow.Usage = D3D11_USAGE_DEFAULT;
	descShadow.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	descShadow.CPUAccessFlags = 0;
	descShadow.MiscFlags = 0;

	//シャドウマップ作成
	auto hr = d3dDevice->CreateTexture2D(&descShadow, nullptr, &shadowMap);
	if (FAILED(hr))	throw DirectX11::DirectXException("ID3D11Device::CreateTexture2D");

	//デプスビューとしての構成
	D3D11_DEPTH_STENCIL_VIEW_DESC descShadowDepthView{};
	descShadowDepthView.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descShadowDepthView.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descShadowDepthView.Texture2D.MipSlice = 0;

	hr = d3dDevice->CreateDepthStencilView(shadowMap.Get(), &descShadowDepthView, &mapWriter);
	if (FAILED(hr))	throw DirectX11::DirectXException("ID3D11Device::CreateDepthStencilView");

	//シェーダリソースとしての構成
	D3D11_SHADER_RESOURCE_VIEW_DESC descShadowResourceView{};
	descShadowResourceView.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	descShadowResourceView.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	descShadowResourceView.Texture2D.MipLevels = 1;

	hr = d3dDevice->CreateShaderResourceView(shadowMap.Get(), &descShadowResourceView, &mapReader);
	if (FAILED(hr))	throw DirectX11::DirectXException("ID3D11Device::CreateShaderResourceView");

	//サンプラの構成
	CD3D11_SAMPLER_DESC descSampler(
		D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR,
		D3D11_TEXTURE_ADDRESS_BORDER,
		D3D11_TEXTURE_ADDRESS_BORDER,
		D3D11_TEXTURE_ADDRESS_BORDER,
		0,
		1,
		D3D11_COMPARISON_LESS_EQUAL,
		DirectX::Colors::White,
		0,
		D3D11_FLOAT32_MAX
	);

	hr = d3dDevice->CreateSamplerState(&descSampler, &mapSampler);
	if (FAILED(hr))	throw DirectX11::DirectXException("ID3D11Device::CreateSamplerState");

	viewport = CD3D11_VIEWPORT{
		0.f, 0.f,
		static_cast<FLOAT>(TEXTURE_SIZE), static_cast<FLOAT>(TEXTURE_SIZE)
	};

	//ライト行列の定数バッファ作成
	constantLight = Direct3DHelper::CreateConstantBuffer(sizeof(LightMatrix));
	constantShadow = Direct3DHelper::CreateConstantBuffer(sizeof(DirectX::XMMATRIX));
}

void ShadowManager::Impl::Finalize() {

	mapSampler.Reset();
	mapWriter.Reset();
	mapReader.Reset();
	shadowMap.Reset();
}

void ShadowManager::Impl::Begin() const {
	
	//コンテキスト取得
	const auto& d3dContext = DirectX11::GetD3DDeviceContext();

	//シェーダリソースバインドを外す
	ID3D11ShaderResourceView* dummy[1] = { nullptr };
	d3dContext->PSSetShaderResources(PS_MAP_SLOT, _countof(dummy), dummy);

	//lightビュー射影行列定数設定
	auto* sceneManager = SceneManager::GetInstance();
	if (auto* currentScene = sceneManager->GetCurrentScene()) {
		if (auto* light = currentScene->GetLight()) {

			//定数バッファセット
			d3dContext->VSSetConstantBuffers(VS_CONSTANT_LIGHT_SLOT, 1, constantLight.GetAddressOf());

			//ライト行列取得
			LightMatrix lightMatrix;
			lightMatrix.viewMatrix = XMMatrixTranspose(light->GetViewMatrix());
			lightMatrix.projectionMatrix = XMMatrixTranspose(light->GetProjectionMatrix());

			//リソース更新
			D3D11_MAPPED_SUBRESOURCE mappedSubResource;
			d3dContext->Map(constantLight.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
			memcpy_s(mappedSubResource.pData, mappedSubResource.RowPitch, &lightMatrix, sizeof lightMatrix);

			d3dContext->Unmap(constantLight.Get(), 0);
		}
	}
	
	//描画範囲
	d3dContext->RSSetViewports(1, &viewport);

	//デプスビューに指定
	d3dContext->OMSetRenderTargets(0, nullptr, mapWriter.Get());

	//マップをクリア
	d3dContext->ClearDepthStencilView(mapWriter.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);

	//シェーダのステージ設定
	SetPipeline(GraphicsPipeline::PIPELINE_TYPE::SHADOW_MAP);
}

void ShadowManager::Impl::End() {
	
	//コンテキスト取得
	const auto& d3dContext = DirectX11::GetD3DDeviceContext();
	
	//描画先を戻す
	DirectX11::SetDefaultRenderTarget();
	
	//シャドウ行列定数設定
	auto* sceneManager = SceneManager::GetInstance();
	if (auto* currentScene = sceneManager->GetCurrentScene()) {
		if (auto* light = currentScene->GetLight()) {

			//定数バッファセット
			d3dContext->VSSetConstantBuffers(VS_CONSTANT_SHADOW_SLOT, 1, constantShadow.GetAddressOf());
			
			auto shadowMatrix = XMMatrixTranspose(light->GetViewMatrix() * light->GetProjectionMatrix() * SHADOW_BIAS);
			
			//リソース更新
			D3D11_MAPPED_SUBRESOURCE mappedSubResource;
			d3dContext->Map(constantShadow.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
			memcpy_s(mappedSubResource.pData, mappedSubResource.RowPitch, &shadowMatrix, sizeof shadowMatrix);

			d3dContext->Unmap(constantShadow.Get(), 0);
		}
	}
	
	//リソース送信
	d3dContext->PSSetSamplers(PS_SAMPLER_SLOT, 1, mapSampler.GetAddressOf());
	d3dContext->PSSetShaderResources(PS_MAP_SLOT, 1, mapReader.GetAddressOf());
}

}

