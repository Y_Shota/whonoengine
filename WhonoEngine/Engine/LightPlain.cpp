#include "LightPlain.hpp"

#include "Camera.hpp"
#include "SceneManager.hpp"
#include "SceneObject.hpp"

namespace  {

constexpr auto EYE_OFFSET = -25;
constexpr auto PROJECTION_SIZE = 100;
constexpr auto NEAR_Z = 0.0001f;
constexpr auto FAR_Z = 100.f;
}

namespace Whono::GameOperation {

LightPlain::LightPlain()
	: Light(LIGHT_TYPE::PLAIN)
	, direction_(XMVector3Normalize({ 1, -1, 1, 0  })) {}

auto LightPlain::GetViewMatrix() -> XMMATRIX {

	//一つ目のカメラを取得
	auto* sceneManager = SceneManager::GetInstance();
	if (auto* currentScene = sceneManager->GetCurrentScene()) {
		if (auto* camera = currentScene->GetCamera()) {

			const auto& lookPosition = camera->GetTargetPosition();
			return XMMatrixLookAtLH( direction_ * EYE_OFFSET + lookPosition, lookPosition, g_XMIdentityR1);
		}
	}
	return XMMatrixIdentity();
}

auto LightPlain::GetProjectionMatrix() -> XMMATRIX {

	return XMMatrixOrthographicLH(PROJECTION_SIZE, PROJECTION_SIZE, NEAR_Z, FAR_Z);
}

void LightPlain::SetDirection(CXMVECTOR direction) noexcept {

	direction_ = XMVector3Normalize(direction);
}

auto LightPlain::GetDirection() const noexcept -> CXMVECTOR {

	return direction_;
}

auto LightPlain::Create() -> std::unique_ptr<LightPlain> {

	return std::make_unique<LightPlain>();
}

}
