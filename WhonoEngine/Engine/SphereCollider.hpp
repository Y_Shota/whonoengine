#pragma once
#include "ICollider.hpp"

#include <DirectXCollision.h>
#include <memory>


namespace Whono::GameOperation {


/**
 * \brief 球の衝突判定を検証するためのクラス
 */
class SphereCollider final : public ICollider {

public:
	/**
	 * \brief コンストラクタ
	 * \param position 中心座標
	 * \param radius 半径
	 */
	SphereCollider(DirectX::CXMVECTOR position, float radius);
	/**
	 * \brief デストラクタ
	 */
	~SphereCollider();

	/**
	 * \brief 中心座標を取得
	 * \return 中心座標
	 */
	[[nodiscard]]
	auto GetCenterPosition() const -> DirectX::XMVECTOR override;

	/**
	 * \brief 受け取った座標から領域内でもっとも近いであろう点を返す
	 * \param point 比較対象
	 * \return 領域内の座標
	 */
	[[nodiscard]]
	auto GetNearestControlPoint(DirectX::CXMVECTOR point) const -> DirectX::XMVECTOR override;

	/**
	 * \brief 受け取ったベクトルに射影した長さの最大値を取得
	 * \param vector 射影するベクトル
	 * \return 射影した長さの最大値
	 */
	[[nodiscard]]
	auto GetProjectionLengthMax(DirectX::CXMVECTOR vector) const -> float override;
	
	/**
	 * \brief 所持させるゲームオブジェクトを設定
	 * \param gameObject ゲームオブジェクト
	 */
	void SetGameObject(GameObject* gameObject) override;
	/**
	 * \brief 境界線を描画する
	 */
	void Render() const override;
	/**
	 * \brief 衝突判定を行う
	 * \param target 検証相手
	 * \return 当たったら真
	 */
	[[nodiscard]]
	auto IsHit(ICollider* target) const -> bool override;

	/**
	 * \brief レイキャストを行う
	 * \param origin 発射地点
	 * \param direction 方向
	 * \param[out] dist 当たった時の距離
	 * \param[out] normal 当たったポリゴンの法線
	 * \return 当たったなら真
	 */
	auto RayCast(DirectX::CXMVECTOR origin, DirectX::CXMVECTOR direction, float* dist, DirectX::XMVECTOR* normal) const -> bool override;

	/**
	 * \brief DirectXのバウンディングを取得する
	 * \return DirectXのBoundingBox
	 */
	[[nodiscard]]
	auto GetBounding() const noexcept -> DirectX::BoundingSphere;

	/**
	 * \brief インスタンスを作成する
	 * \param position = g_XMZero 中心座標
	 * \param radius = 1.f 半径
	 * \return インスタンス
	 */
	[[nodiscard]]
	static auto Create(DirectX::CXMVECTOR position = DirectX::g_XMZero, float radius = 1.f)->std::unique_ptr<SphereCollider>;
	/**
	 * \brief インスタンスを作成する
	 * \param x 中心X座標
	 * \param y 中心Y座標
	 * \param z 中心Z座標
	 * \param radius = 1.f 半径
	 * \return インスタンス
	 */
	[[nodiscard]]
	static auto Create(float x, float y, float z, float radius = 1.f) -> std::unique_ptr<SphereCollider>;

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽
};

}
