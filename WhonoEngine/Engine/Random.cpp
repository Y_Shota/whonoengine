#include "Random.hpp"

#include <random>
#include <optional>

using namespace std;


namespace Whono::Random {


random_device seedGenerator{};
mt19937 random(seedGenerator());


void SetSeed(int seed) {

	random.seed(seed);
}

void SetRandomSeed() {

	random.seed(seedGenerator());
}

void NextBytes(unsigned char* bytes, size_t size) {

	uniform_int_distribution<> distribution(0, UCHAR_MAX);
	for (auto i = 0u; i < size; ++i) {

		bytes[i] = distribution(random);
	}
}

auto NextInt(int bound) -> int {

	uniform_int_distribution<> distribution(0, bound);
	return distribution(random);
}

auto NextLongLong() -> long long {

	uniform_int_distribution<long long> distribution(0, LLONG_MAX);
	return distribution(random);
}

auto NextBool() -> bool {

	bernoulli_distribution distribution(0.5);
	return distribution(random);
}

auto NextFloat() -> float {

	uniform_real_distribution<float> distribution(0.f, 1.f);
	return distribution(random);
}

auto NextDouble() -> double {
	
	uniform_real_distribution<> distribution(0., 1.);
	return distribution(random);
}

auto NextGaussian() -> double {
	
	normal_distribution<> distribution(0.0, 1.0);
	return distribution(random);
}

auto NextPoisson(double lambda) -> double {
	
	poisson_distribution<> distribution(lambda);
	return distribution(random);
}


}