#include "Time.hpp"

#include <chrono>
#include <iostream>
#include <thread>

using namespace std;
using namespace chrono;


namespace Whono::Time {

auto fps = 0;
auto frameTimeMs = 0.f;
auto elapsedTimeFMs = 0.f;
auto elapsedTimeMs = 0ull;
auto workTime = 0.f;
auto lastUpdateTime = system_clock::now();
auto startTime = system_clock::now();
auto count = 0;

void CalculationSleep() {
	
	//スリープでの維持が不安定だったため指定時間になるまでループ
	while (true) {
		
		if (IsUpdateTime())	break;
	}
	UpdateData();
}

auto GetFps() noexcept -> int {

	return fps;
}

auto GetFrameTimeMs() noexcept -> float {

	return frameTimeMs;
}

auto GetFrameTimeS() noexcept -> float {

	return frameTimeMs * 0.001f;
}

void ResetSceneTimer() noexcept {

	elapsedTimeMs = 0;
	frameTimeMs = 0;
	lastUpdateTime = system_clock::now();
}

auto GetElapsedTimeMs() noexcept -> float {

	return elapsedTimeMs + elapsedTimeFMs;	
}

auto GetElapsedTimeS() noexcept -> float {

	return (elapsedTimeMs + elapsedTimeFMs) * 0.001f;
}

auto GetFrameErrorRatio() noexcept -> float {

#ifdef _DEBUG

	return 1.f;
#else

	return frameTimeMs * FRAME_RATE / 1000;
#endif
	
}

void PrintDebugData() {
	
	cout << GetFps() << '\t'
		<< GetFrameTimeMs() << '\t'
		<< GetFrameErrorRatio() << '\t'
		<< GetElapsedTimeMs() << '\t'
		<< std::endl;
}

auto IsUpdateTime() -> bool {

	auto nowTime = system_clock::now();
	auto delta = nowTime - lastUpdateTime;

	return delta * FRAME_RATE >= 1.s;
}

void UpdateData() {

	auto nowTime = system_clock::now();
	duration<float, milli> workTime = nowTime - lastUpdateTime;
	Time::workTime = workTime.count();

	//FPS計算
	if (nowTime - startTime >= 1.s) {

		fps = count;
		count = 0;
		startTime = nowTime;
	}
	count++;

	//1フレームあたりの時間算出
	duration<float, milli> frame = nowTime - lastUpdateTime;
	frameTimeMs = frame.count();

	//経過時間更新
	elapsedTimeFMs += frameTimeMs;
	elapsedTimeMs += static_cast<unsigned>(elapsedTimeFMs);
	elapsedTimeFMs = fmod(elapsedTimeFMs, 1.f);
	
	lastUpdateTime = nowTime;

	//CPUの使用率を下げるらしい
	this_thread::sleep_for(1ms);
}


};