#include "FbxMeshData.hpp"

#define NOMINMAX

#include <array>
#include <d3d11_1.h>
#include <DirectXCollision.h>
#include <list>
#include <optional>
#include <string>
#include <vector>

#include "Direct3DHelper.hpp"
#include "FbxHelper.hpp"

using namespace std;
using namespace DirectX;


namespace Whono::RenderObject {


constexpr int VERTEX_PER_POLYGON = 3;


struct FbxMeshData::Vertex {

	XMVECTOR position;
	XMVECTOR normal;
	XMVECTOR tangent;
	XMVECTOR binormal;
	XMVECTOR uv;
	XMVECTOR blendWeights = g_XMZero;
	XMVECTORI32 blendIndices = { -1, -1, -1, -1 };
};

struct DeformerData {

	int		skeletonId;
	float	weight;
};

struct ClusterData {

	uint64_t	linkNodeId;
	FbxAMatrix	bindPoseInverse;
};


struct FbxMeshData::Impl {

	void Initialize(FbxMesh* mesh) noexcept(false);

	void CreateVertexBuffer(FbxMesh* mesh) noexcept(false);
	void CreateIndexBuffer(FbxMesh* mesh) noexcept(false);
	void SetMaterialId(FbxMesh* mesh);
	void CreateDeformer(FbxMesh* mesh);

	auto RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool;
	void GetMinMaxPosition(XMVECTOR* min, XMVECTOR* max) const;

	void Release();
	
	string						name;			//メッシュ名
	ComPtr<ID3D11Buffer>		vertexBuffer;	//頂点シェーダに渡す情報
	ComPtr<ID3D11Buffer>		indexBuffer;	//頂点シェーダへ渡す順番
	vector<Vertex>				vertices;		//頂点情報
	vector<int>					indices;		//インデックス情報
	vector<list<DeformerData>>	deformers;		//デフォーマ情報
	vector<ClusterData>			clusters;		//クラスター情報
	uint64_t					materialId;	//メッシュのマテリアル判別番号
};


FbxMeshData::FbxMeshData() {

	pImpl_ = make_unique<Impl>();
}

FbxMeshData::~FbxMeshData() = default;

void FbxMeshData::Initialize(FbxMesh* mesh) const {

	pImpl_->Initialize(mesh);
}

auto FbxMeshData::GetMaterialId() const noexcept -> uint64_t {

	return pImpl_->materialId;
}

auto FbxMeshData::GetIndexCount() const noexcept -> unsigned {

	return static_cast<unsigned>(pImpl_->indices.size());
}

auto FbxMeshData::GetIndexBuffer() const noexcept -> struct ID3D11Buffer* {

	return pImpl_->indexBuffer.Get();
}

auto FbxMeshData::GetVertexBuffer() const noexcept -> struct ID3D11Buffer* {

	return pImpl_->vertexBuffer.Get(); 
}

auto FbxMeshData::GetClusterCount() const noexcept -> size_t {

	return pImpl_->clusters.size();
}

auto FbxMeshData::GetClusterBindPose(int i) const noexcept -> XMMATRIX {

	return FbxHelper::ConvertToXmmatrix(pImpl_->clusters[i].bindPoseInverse);
}

auto FbxMeshData::GetClusterLinkUniqueId(int i) const noexcept -> uint64_t {

	return pImpl_->clusters[i].linkNodeId;
}

auto FbxMeshData::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool {

	return pImpl_->RayCast(origin, direction, dist, normal);
}

void FbxMeshData::GetMinMaxPosition(XMVECTOR* min, XMVECTOR* max) const {

	pImpl_->GetMinMaxPosition(min, max);
}

void FbxMeshData::Release() {

	pImpl_->Release();
	pImpl_.reset();
}

auto FbxMeshData::GetVertexDataSize() noexcept -> unsigned {

	return sizeof(Vertex);
}

void FbxMeshData::Impl::Initialize(FbxMesh* mesh) noexcept(false) {

	name = mesh->GetName();
	CreateDeformer(mesh);
	CreateVertexBuffer(mesh);
	CreateIndexBuffer(mesh);
	SetMaterialId(mesh);
}

void FbxMeshData::Impl::CreateVertexBuffer(FbxMesh* mesh) noexcept(false) {

	//ポリゴンの数を取得
	auto polygonCount = mesh->GetPolygonCount();
	
	//全ポリゴン
	for (auto i = 0; i < polygonCount; ++i) {

		auto polygonVertexIndex = mesh->GetPolygonVertexIndex(i);
		
		for (auto j = 0; j < VERTEX_PER_POLYGON; ++j) {
			
			//調べる頂点の番号
			auto index = mesh->GetPolygonVertex(i, j);

			//座標
			auto position = mesh->GetControlPointAt(index);
			
			//uv座標
			auto uvIndex = mesh->GetTextureUVIndex(i, j, FbxLayerElement::eTextureDiffuse);
			auto uv = mesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(uvIndex);

			//法線
			FbxVector4 normal;
			mesh->GetPolygonVertexNormal(i, j, normal);
			
			//接線
			auto* elementTangent = mesh->GetElementTangent();
			auto tangent = elementTangent ? elementTangent->GetDirectArray().GetAt(polygonVertexIndex + j) : FbxVector4();

			//縦法線
			auto* elementBinormal = mesh->GetElementBinormal();
			auto binormal = elementBinormal ? elementBinormal->GetDirectArray().GetAt(polygonVertexIndex + j) : FbxVector4();

			//取得した情報を配列に保存
			vertices[index].position = FbxHelper::ConvertToXmvector(position);
			vertices[index].normal = FbxHelper::ConvertToXmvector(normal);
			vertices[index].tangent = FbxHelper::ConvertToXmvector(tangent);
			vertices[index].binormal = FbxHelper::ConvertToXmvector(binormal);
			vertices[index].uv = FbxHelper::ConvertToXmvector(uv);
		}
	}
	vertexBuffer = Direct3DHelper::CreateVertexBuffer(vertices.data(), sizeof(Vertex) * static_cast<unsigned>(vertices.size()));
}

void FbxMeshData::Impl::CreateIndexBuffer(FbxMesh* mesh) noexcept(false) {

	auto indexSize = mesh->GetPolygonVertexCount();
	auto* index = mesh->GetPolygonVertices();
	indices.assign(index, index + indexSize);

	for (size_t i = 0u; i < indices.size(); i += 3) {

		swap(indices[i], indices[i + 2]);
	}
	
	indexBuffer = Direct3DHelper::CreateIndexBuffer(indices.data(), sizeof(int) * static_cast<unsigned>(indices.size()));
}

void FbxMeshData::Impl::SetMaterialId(FbxMesh* mesh) {
	
	auto* elementMaterial = mesh->GetElementMaterial();
	auto index = elementMaterial->GetIndexArray().GetAt(1);
	auto* material = mesh->GetNode()->GetSrcObject<FbxSurfaceMaterial>(index);
	materialId = material->GetUniqueID();
}

void FbxMeshData::Impl::CreateDeformer(FbxMesh* mesh) {
	
	//頂点数分でリサイズ
	vertices.resize(mesh->GetControlPointsCount());
	
	//スキンアニメーションの数を取得
	auto skinNum = mesh->GetSrcObjectCount<FbxSkin>();

	//ない場合無視
	if (skinNum <= 0)	return;
	
	//スキンアニメーションの一つ目を取得
	auto* skin = mesh->GetSrcObject<FbxSkin>();

	//スケルトンベースのデフォーマ
	struct DeformerDataRaw {

		int skeletonId;
		vector<int> indices;
		vector<double> weights;
	};
	vector<DeformerDataRaw> rawDeformers;
	
	//頂点にに影響するクラスターをすべて取得
	auto clusterNum = skin->GetClusterCount();
	rawDeformers.resize(clusterNum);
	clusters.resize(clusterNum);
	for (auto i = 0; i < clusterNum; ++i) {

		auto* cluster = skin->GetCluster(i);

		//スケルトンのIDを取得
		clusters[i].linkNodeId = cluster->GetLink()->GetUniqueID();

		//初期姿勢を取得
		FbxAMatrix linkMatrix, matrix;
		cluster->GetTransformLinkMatrix(linkMatrix);
		cluster->GetTransformMatrix(matrix);
		clusters[i].bindPoseInverse = linkMatrix.Inverse() * matrix;
		

		auto& deformer = rawDeformers[i];

		//idを設定
		deformer.skeletonId = i;
		
		//頂点インデックスとウェイト配列を取得
		auto indicesNum = cluster->GetControlPointIndicesCount();
		auto* indices = cluster->GetControlPointIndices();
		auto* weights = cluster->GetControlPointWeights();

		//配列格納
		deformer.indices.assign(indices, indices + indicesNum);
		deformer.weights.assign(weights, weights + indicesNum);
	}

	//扱いやすい頂点ベースのデフォーマに変換
	deformers.resize(mesh->GetPolygonVertexCount());
	for (const auto& deformer : rawDeformers) {

		const auto& indices = deformer.indices;
		const auto& weights = deformer.weights;
		
		for (auto i = 0; i < deformer.indices.size(); ++i) {

			deformers[indices[i]].emplace_back(
				DeformerData{deformer.skeletonId, static_cast<float>(weights.at(i))}
			);
		}
	}

	//比重が高い順にソート
	for (auto& deformer : deformers) {

		deformer.sort([](auto&& a, auto&& b) { return a.weight > b.weight; });
	}
	
	for (auto i = 0u; i < deformers.size(); ++i) {

		auto itr = deformers[i].cbegin();
		for (auto j = 0; j < 4; ++j) {

			if (itr == deformers[i].cend())	break;
			
			vertices[i].blendIndices.i[j] = (*itr).skeletonId;
			vertices[i].blendWeights.m128_f32[j] = (*itr).weight;

			++itr;
		}
	}
}

auto FbxMeshData::Impl::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool  {

	if (!dist || !normal)	return false;
	
	auto normalOptional = make_optional<XMVECTOR>();
	auto isHitEvenOnce = false;
	auto distance = numeric_limits<float>::max();
	
	for (auto i = 0u; i < indices.size(); i += VERTEX_PER_POLYGON) {

		auto buffer = 0.f;
		
		array<XMVECTOR, VERTEX_PER_POLYGON> v{};
		for (auto j = 0; j < VERTEX_PER_POLYGON; ++j) {

			v[j] = vertices[indices[i + j]].position;
		}
		auto isHit = TriangleTests::Intersects(origin, direction, v[0], v[1], v[2], buffer);
		if (isHit) {

			if (buffer != 0.f && distance > buffer) {

				distance = buffer;
				normalOptional = vertices[indices[i]].normal;
			}
			isHitEvenOnce = true;
		}
	}
	if (*dist > distance)			*dist = distance;
	if (normalOptional.has_value())	*normal = normalOptional.value();
	
	return isHitEvenOnce;
}

void FbxMeshData::Impl::GetMinMaxPosition(XMVECTOR* min, XMVECTOR* max) const {

	for (const auto& vertex : vertices) {

		const auto& position = vertex.position;

		auto selectMin = XMVectorLess(position, *min);
		auto selectMax = XMVectorGreater(position, *max);

		*min = XMVectorSelect(*min, position, selectMin);
		*max = XMVectorSelect(*max, position, selectMax);
	}
}


void FbxMeshData::Impl::Release() {

	vertexBuffer.Reset();
	indexBuffer.Reset();
}


}

