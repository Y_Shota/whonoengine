#include "Audio.hpp"

#include <Windows.h>
#include <mmsystem.h>
#include <xaudio2.h>

#include "ComPtrDefinition.hpp"
#include "../XAudio2Exception.hpp"

#pragma comment(lib, "winmm.lib")

using namespace std;

namespace fs = filesystem;


namespace Whono::Audio {

constexpr auto DIRECTORY_PATH = "Assets/";

struct WaveData {

	WAVEFORMATEX format;
	unique_ptr<BYTE[]> data;
	unsigned long dataSize;
};

struct AudioData {

	fs::path filePath;
	shared_ptr<WaveData> wave;
	XAUDIO2_BUFFER buffer;
	vector<IXAudio2SourceVoice*> sourceVoices;
	bool resident = false;
};

ComPtr<IXAudio2> xAudio;
IXAudio2MasteringVoice* masteringVoice;
vector<unique_ptr<AudioData>> data;

auto HasHandle(int handle) -> bool;

void Initialize() {

	auto hr = XAudio2Create(&xAudio);
	if (FAILED(hr))	throw Window::XAudio2Exception("XAudio2Create()");

	hr = xAudio->CreateMasteringVoice(&masteringVoice);
	if (FAILED(hr))	throw Window::XAudio2Exception("IXAudio2::CreateMasteringVoice()");

	masteringVoice->SetVolume(0.5f);
}

auto Load(const fs::path& filePath) -> int {
	
	auto defaultPath = fs::current_path();
	fs::current_path(DIRECTORY_PATH);

	auto audioDatum = make_unique<AudioData>();
	
	//既に読み込まれているか
	auto result = ranges::find_if(data,
		[&](auto& datum) {
			if (!datum)	return false;
			return datum->filePath == filePath;
		});
	//読み込まれている場合共有
	if (result != data.end()) {

		audioDatum->wave = (*result)->wave;
	}
	//新たに開く
	else {
		
		//ファイルを開く
		MMIOINFO mmioInfo{};
		auto hMmio = mmioOpen(filePath.string().data(), &mmioInfo, MMIO_READ);
		//開けなかった
		if (!hMmio) {

			audioDatum.reset();
			fs::current_path(defaultPath);
			return UNALLOCATED_HANDLE;
		}
		
		//wavチェック
		MMCKINFO riffChunk;
		riffChunk.fccType = mmioFOURCC('W', 'A', 'V', 'E');
		auto mResult = mmioDescend(hMmio, &riffChunk, nullptr, MMIO_FINDRIFF);
		//wavではない
		if (mResult) {

			audioDatum.reset();
			fs::current_path(defaultPath);
			mmioClose(hMmio, MMIO_FHOPEN);
			return UNALLOCATED_HANDLE;
		}

		//formatチャンクを探す
		MMCKINFO chunk;
		chunk.ckid = mmioFOURCC('f', 'm', 't', ' ');
		mResult = mmioDescend(hMmio, &chunk, &riffChunk, MMIO_FINDCHUNK);
		//見つからなかった
		if (mResult) {

			audioDatum.reset();
			fs::current_path(defaultPath);
			mmioClose(hMmio, MMIO_FHOPEN);
			return UNALLOCATED_HANDLE;
		}

		//フォーマット取得
		WAVEFORMATEX format;
		auto size = mmioRead(hMmio, reinterpret_cast<HPSTR>(&format), chunk.cksize);
		//サイズが不適合
		if (size != chunk.cksize) {

			audioDatum.reset();
			fs::current_path(defaultPath);
			mmioClose(hMmio, MMIO_FHOPEN);
			return UNALLOCATED_HANDLE;
		}
		mResult = mmioAscend(hMmio, &chunk, 0);
		if (mResult) {

			audioDatum.reset();
			fs::current_path(defaultPath);
			mmioClose(hMmio, MMIO_FHOPEN);
			return UNALLOCATED_HANDLE;
		}

		//dataチャンクを探す
		chunk.ckid = mmioFOURCC('d', 'a', 't', 'a');
		mResult = mmioDescend(hMmio, &chunk, &riffChunk, MMIO_FINDCHUNK);
		//見つからなかった
		if (mResult) {

			audioDatum.reset();
			fs::current_path(defaultPath);
			mmioClose(hMmio, MMIO_FHOPEN);
			return UNALLOCATED_HANDLE;
		}

		//data読み込み
		auto buffer = make_unique<BYTE[]>(chunk.cksize);
		size = mmioRead(hMmio, reinterpret_cast<HPSTR>(buffer.get()), chunk.cksize);
		//サイズが不適合
		if (size != chunk.cksize) {

			audioDatum.reset();
			fs::current_path(defaultPath);
			mmioClose(hMmio, MMIO_FHOPEN);
			buffer.reset();
			return UNALLOCATED_HANDLE;
		}
		mResult = mmioAscend(hMmio, &chunk, 0);
		mmioClose(hMmio, MMIO_FHOPEN);
		if (mResult) {

			audioDatum.reset();
			fs::current_path(defaultPath);
			return UNALLOCATED_HANDLE;
		}
		audioDatum->wave = make_unique<WaveData>(format, move(buffer), chunk.cksize);
	}
	
	//XAudio2で使えるように
	audioDatum->buffer.pAudioData = audioDatum->wave->data.get();
	audioDatum->buffer.AudioBytes = audioDatum->wave->dataSize;

	//ソースボイスを作成
	IXAudio2SourceVoice* sourceVoice;
	auto hr = xAudio->CreateSourceVoice(&sourceVoice, &audioDatum->wave->format);

	if (FAILED(hr)) {

		fs::current_path(defaultPath);
		return UNALLOCATED_HANDLE;
	}
	
	audioDatum->sourceVoices.emplace_back(sourceVoice);
	
	//配列内に空きがあった場合そこに移動
	for (auto i = 0u; i < data.size(); ++i) {

		if (!data[i]) {

			data[i] = move(audioDatum);
			fs::current_path(defaultPath);
			return i;
		}
	}
	
	data.emplace_back(move(audioDatum));
	
	fs::current_path(defaultPath);
	
	return static_cast<int>(data.size()) - 1;
}

void Play(int handle, float volume) {
	
	if (!HasHandle(handle)) return;

	auto& datum = data[handle];
	
	for (auto* sourceVoice : datum->sourceVoices) {

		XAUDIO2_VOICE_STATE state;
		sourceVoice->GetState(&state);

		//再生されていないとき
		if (state.BuffersQueued == 0) {

			sourceVoice->SubmitSourceBuffer(&datum->buffer);
			sourceVoice->SetVolume(volume);
			sourceVoice->Start();
			return;
		}
	}
	
	//ソースボイスが足りなかったとき追加
	IXAudio2SourceVoice* sourceVoice;
	xAudio->CreateSourceVoice(&sourceVoice, &datum->wave->format);
	datum->sourceVoices.emplace_back(sourceVoice);

	sourceVoice->SubmitSourceBuffer(&datum->buffer);
	sourceVoice->SetVolume(volume);
	sourceVoice->Start();
}

void Stop(int handle) {
	
	if (!HasHandle(handle)) return;

	auto& datum = data[handle];

	for (auto* sourceVoice : datum->sourceVoices) {

		XAUDIO2_VOICE_STATE state;
		sourceVoice->GetState(&state);

		//再生されていないとき
		if (state.BuffersQueued != 0) {
			
			sourceVoice->Stop();
		}
	}
}

void IsLoop(int handle, bool isLoop) {
	
	if (!HasHandle(handle)) return;

	auto& datum = data[handle];
	datum->buffer.LoopCount = XAUDIO2_LOOP_INFINITE;
}

void SetVolume(int handle, float volume) {

	if (!HasHandle(handle)) return;

	volume = clamp(volume, 0.f, 1.f);

	for (auto* sourceVoice : data[handle]->sourceVoices) {
		
		sourceVoice->SetVolume(volume);
	}
}

void SetResident(int handle, bool resident) {
	
	if (!HasHandle(handle))	return;

	data[handle]->resident = resident;
}

void Release(int handle) {
	
	if (!HasHandle(handle))	return;

	auto& datum = data[handle];
	if(datum) {
		if (datum->wave) {

			datum->wave->data.reset();
		}
		for (auto* sourceVoice : datum->sourceVoices) {

			sourceVoice->DestroyVoice();
		}
		datum.reset();
	}
}

void Release(bool isAll) {

	for (auto& datum : data) {

		if (datum) {
			if (isAll) {

				if (datum->wave)	datum->wave.reset();
				for (auto* sourceVoice : datum->sourceVoices) {

					sourceVoice->DestroyVoice();
				}
				datum.reset();
			}
			else if (!datum->resident) {
				
				if (datum->wave)	datum->wave.reset();
				for (auto* sourceVoice : datum->sourceVoices) {

					sourceVoice->DestroyVoice();
				}
				datum.reset();
			}
		}
	}
}


void Finalize() {

	Release(true);
	if (masteringVoice) masteringVoice->DestroyVoice();
	xAudio.Reset();
}

auto HasHandle(int handle) -> bool {

	if (handle < 0)									return false;
	if (handle >= static_cast<int>(data.size()))	return false;
	if (!data[handle])								return false;

	return true;
}

}
