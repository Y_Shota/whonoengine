#include "FbxModel.hpp"

#define NOMINMAX
#include <d3d11.h>
#include <filesystem>
#include <fbxsdk.h>
#include <iostream>
#include <limits>

#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")

#include "DirectXException.hpp"
#include "FbxMaterialData.hpp"
#include "FbxMeshData.hpp"
#include "Camera.hpp"
#include "Direct3DHelper.hpp"
#include "DirectX11.hpp"
#include "FbxHelper.hpp"
#include "FbxSkeletonData.hpp"
#include "GraphicsPipeline.hpp"
#include "LightPlain.hpp"
#include "SceneManager.hpp"
#include "SceneObject.hpp"
#include "Transform.hpp"
#include "WinException.hpp"

using namespace std;
using namespace DirectX;

using Whono::DirectX11::DirectXException;
using Whono::Window::WinException;
using Whono::GameOperation::Camera;
using Whono::GameOperation::Transform;

namespace fs = filesystem;

template<class T>
using FbxPtr = FbxAutoDestroyPtr<T>;
using MeshPtr = unique_ptr<Whono::RenderObject::FbxMeshData>;
using MaterialPtr = unique_ptr<Whono::RenderObject::FbxMaterialData>;
using SkeletonPtr = unique_ptr<Whono::RenderObject::FbxSkeletonData>;




namespace Whono::RenderObject {

namespace {
constexpr auto ALLOW_QUAD = false;
constexpr auto VS_CONSTANT_SHADOW_WORLD_SLOT = 0u;
constexpr auto VS_CONSTANT_SHADOW_SKIN_SLOT = 1u;
}
/**
 * \brief コンスタントバッファのリソース用
 */
struct Constant {

	XMFLOAT4X4	matrixWVP;				//変換行列
	XMFLOAT4X4	matrixWorld;			//ワールド行列
	XMFLOAT4X4	matrixNormalTransform;	//法線変換行列
	XMFLOAT4	camera;					//カメラ座標
	XMFLOAT4	lightDir;				//光源の方向
	XMFLOAT4	lightColor;				//光源の色
	XMFLOAT4	diffuse;				//ディフューズ
	XMFLOAT4	ambient;				//アンビエント
	XMFLOAT4	emissive;				//エミッシブ
	XMFLOAT4	specular;				//スペキュラー
	FLOAT		transparent;			//トランスペアレント
	FLOAT		shininess;				//シャイネス
	FLOAT		reflection;				//リフレクション
	// 0 ディフューズ
	// 1 バンプマップテクスチャ
	// 2 ノーマルマップテクスチャ
	// 3 アニメーション
	// 8 影の描写
	INT			optionFlg;				//各種フラグ
};

struct ShadowMapConstant {

	XMMATRIX worldMatrix;
	int isAnimation;
};

struct FbxModel::Impl {

	/**
	 * \brief ファイルを読み込み各種情報を作成する
	 * \param filePath ファイルパス
	 * \return 生成出来たら真
	 */
	auto Load(const fs::path& filePath) -> bool;
	
	/**
	 * \brief メッシュのデータを作成する
	 * \param scene 読み込むシーン
	 */
	void CreateMeshData(FbxScene* scene);
	/**
	 * \brief マテリアルデータを作成する
	 * \param scene 読み込むシーン
	 */
	void CreateMaterialData(FbxScene* scene);
	/**
	 * \brief スケルトンデータを作成する
	 * \param scene 読み込むシーン
	 */
	void CreateSkeletonData(FbxScene* scene);
	/**
	 * \brief アニメーションデータを作成する
	 * \param scene 読み込むシーン
	 */
	void CreateAnimationData(FbxScene* scene);
	/**
	 * \brief コンスタントバッファを作成する
	 * \throw DirectX11Exception
	 */
	void CreateConstantBuffer() noexcept(false);

	/**
	 * \brief 描画処理
	 * \param transform 変形情報
	 * \param frame フレーム数
	 */
	void Render(const Transform* transform, int frame);
	/**
	 * \brief シャドウマップ描画処理
	 * \param transform 変形情報
	 * \param frame フレーム数
	 */
	void RenderShadowMap(const Transform* transform, int frame);

	void SetSkinConstantBuffer(const MeshPtr& mesh, UINT slot, int frame);
	
	/**
	 * \brief ボーンの座標を取得する
	 * \param boneName ボーンの名前
	 * \param frame アニメーションのフレーム数の指定
	 * \return ボーンの座標　ない場合無効値
	 */
	auto GetBonePosition(const string& boneName, int frame) const -> optional<XMVECTOR>;
	/**
	 * \brief ボーンの変形情報を取得する
	 * \param boneName ボーンの名前
	 * \param frame アニメーションのフレーム数の指定
	 * \return ボーンの変形情報　ない場合は無効値
	 */
	auto GetBoneTransform(const string& boneName, int frame) const -> unique_ptr<Transform>;
	
	/**
	 * \brief モデル空間でのサイズを取得する
	 * \return サイズ
	 */
	auto GetExtents() -> XMVECTOR;
	/**
	 * \brief モデル座標系での中心座標を取得する
	 * \return 中心座標
	 */
	auto GetCenterPosition() -> XMVECTOR;
	/**
	 * \brief レイキャストを行う
	 * \param origin レイの始点
	 * \param direction レイの方向
	 * \param[out] dist 衝突したときの距離
	 * \param[out] normal 衝突したポリゴンの法線
	 * \return 衝突したなら真
	 */
	auto RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool;
	
	FbxPtr<FbxManager>		manager;
	ComPtr<ID3D11Buffer>	constantBuffer;				//パイプライン実行時定数
	vector<MeshPtr>			meshes;						//メッシュデータ
	vector<MaterialPtr>		materials;					//マテリアルデータ
	vector<SkeletonPtr>		skeletons;					//スケルトンデータ
	ComPtr<ID3D11Buffer>	skinMatrixConstantBuffer;	//スキン行列用定数バッファ
	ComPtr<ID3D11Buffer>	shadowWorldMatrixConstant;	//シャドウマップ用ワールド行列定数バッファ
	int						frameCount;					//アニメーションが存在するときのフレーム数
	bool					hasAnimation;				//アニメーションの有無
};

FbxModel::FbxModel() {

	pImpl_ = make_unique<Impl>();
}

FbxModel::~FbxModel() {

	Release();
}

auto FbxModel::Load(const fs::path& filePath) -> bool {

	bool result;
	try {
		result = pImpl_->Load(filePath);
	}
	catch (FbxException& e) {

		cout << e.what() << endl;
		return false;
	}
	return result;
}

void FbxModel::Render(const Transform* transform, int frame) {

	pImpl_->Render(transform, frame);
}

void FbxModel::RenderShadowMap(const Transform* transform, int frame) {

	pImpl_->RenderShadowMap(transform, frame);
}

auto FbxModel::HasAnimation() -> bool {

	return pImpl_->hasAnimation;
}

auto FbxModel::GetBonePosition(const string& boneName, int frame) -> optional<XMVECTOR> {

	return pImpl_->GetBonePosition(boneName, frame);
}

auto FbxModel::GetBoneTransform(const string& boneName, int frame) -> unique_ptr<Transform> {

	return pImpl_->GetBoneTransform(boneName, frame);
}

auto FbxModel::GetExtents() -> XMVECTOR {

	return pImpl_->GetExtents();
}

auto FbxModel::GetCenterPosition() -> XMVECTOR {

	return pImpl_->GetCenterPosition();
}

auto FbxModel::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool {

	return pImpl_->RayCast(origin, direction, dist, normal);
}

void FbxModel::Release() {

	pImpl_->constantBuffer.Reset();
	pImpl_->skinMatrixConstantBuffer.Reset();
	for (auto& skeleton : pImpl_->skeletons) {

		skeleton.reset();
	}
	for (auto& mesh : pImpl_->meshes) {

		mesh->Release();
		mesh.reset();
	}
	for (auto& material : pImpl_->materials) {

		material->Release();
		material.reset();
	}
	pImpl_->manager.Reset();
}


auto FbxModel::Impl::Load(const fs::path& filePath) -> bool {

	//fbxのメモリ管理用変数作成
	manager.Reset(FbxManager::Create());
	if (!manager)	throw FbxException("FbxManager::Create()");

	//ファイルに対して適切なリーダを選択するためのインポータ作成
	auto* importer = FbxImporter::Create(manager, "");
	if (!importer) {

		manager.Reset();
		throw FbxException("FbxImporter::Create()");
	}

	//3Dシーン保持用変数作成
	auto* scene = FbxScene::Create(manager, "");
	if (!scene) {

		manager.Reset();
		throw FbxException("FbxScene::Create()");
	}

	//ファイル読み込み
	auto result = importer->Initialize(filePath.string().c_str());
	if (!result) {
		
		auto currentDirectory = std::filesystem::current_path();
		fs::path tmp;
		for (auto element : currentDirectory) {

			tmp /= element;
			if (element == "Assets") {

				tmp /= "Debug/null_box.fbx";
				break;
			}
		}
		result = importer->Initialize(tmp.string().c_str());
		if (!result)	return false;
	}

	//読み込んだファイルからシーンを読み込み
	importer->Import(scene);

	//データ変換用
	FbxGeometryConverter converter(manager);

	//四角形ポリゴンの使用が許可されている時三角化
	if constexpr (ALLOW_QUAD)	converter.Triangulate(scene, true);

	//マテリアルごとにメッシュを分割
	converter.SplitMeshesPerMaterial(scene, true);

	//不完全なポリゴンを除去
	converter.RemoveBadPolygonsFromMeshes(scene);

	//座標系をDirectXに合わせる
	auto axisType = scene->GetGlobalSettings().GetAxisSystem();
	if (axisType != FbxAxisSystem::DirectX) {

		scene->GetGlobalSettings().SetAxisSystem(FbxAxisSystem::DirectX);
	}

	//カレントディレクトリを読み込むファイルのあるディレクトリにする
	auto defaultPath = fs::current_path();
	if (filePath.has_parent_path())	fs::current_path(filePath.parent_path());

	CreateMeshData(scene);		//メッシュデータをcpuに落とし込む
	CreateMaterialData(scene);	//マテリアルデータをcpuに落とし込む
	CreateSkeletonData(scene);	//スケルトンデータを作成
	CreateAnimationData(scene);	//アニメーション情報作成
	CreateConstantBuffer();		//コンスタントバッファ作成

	//カレントディレクトリを戻す
	fs::current_path(defaultPath);

	return true;
}

void FbxModel::Impl::CreateMeshData(FbxScene* scene) {

	auto meshNum = scene->GetSrcObjectCount<FbxMesh>();
	for (auto i = 0; i < meshNum; ++i) {

		auto& meshData = meshes.emplace_back(make_unique<FbxMeshData>());
		auto* mesh = scene->GetSrcObject<FbxMesh>(i);
		meshData->Initialize(mesh);
	}
}

void FbxModel::Impl::CreateMaterialData(FbxScene* scene) {

	auto materialNum = scene->GetSrcObjectCount<FbxSurfaceMaterial>();
	for (auto i = 0; i < materialNum; ++i) {

		auto& materialData = materials.emplace_back(make_unique<FbxMaterialData>());
		auto* surfaceMaterial = scene->GetSrcObject<FbxSurfaceMaterial>(i);
		materialData->Initialize(surfaceMaterial);
	}
}

void FbxModel::Impl::CreateSkeletonData(FbxScene* scene) {
	
	//スケルトンの数を取得し、ない場合は無視
	auto skeletonNum = scene->GetSrcObjectCount<FbxSkeleton>();
	if (skeletonNum == 0)	return;
	
	
	for (auto i = 0; i < skeletonNum; ++i) {

		auto* skeleton = scene->GetSrcObject<FbxSkeleton>(i);
		auto* skeletonNode = skeleton->GetNode();
		auto& skeletonData = skeletons.emplace_back(make_unique<FbxSkeletonData>());
		skeletonData->Initialize(skeletonNode);
	}
}

void FbxModel::Impl::CreateAnimationData(FbxScene* scene) {

	frameCount = 0;
	
	//アニメーションの名前を取得
	FbxArray<FbxString*> animationNames;
	scene->FillAnimStackNameArray(animationNames);

	hasAnimation = animationNames.Size() > 0;

	//アニメーションがない場合無視
	if(!hasAnimation) return;
	
	//fpsの種類を取得
	auto timeMode = scene->GetGlobalSettings().GetTimeMode();

	//設定されてるフレームあたりの時間と1秒あたりのフレーム数を取得
	auto oneFrameValue = FbxTime::GetOneFrameValue(timeMode);
	auto fps = FbxTime::GetFrameRate(timeMode);
	auto fps60 = FbxTime::GetFrameRate(FbxTime::eFrames60);

	//1つ目のアニメーションの情報を取得
	auto* takeInfo = scene->GetTakeInfo(animationNames[0]->Buffer());
	auto startTime = takeInfo->mLocalTimeSpan.GetStart();
	auto endTime = takeInfo->mLocalTimeSpan.GetStop();

	//60fpsでのフレーム数を取得
	frameCount = static_cast<int>((endTime.Get() - startTime.Get()) / oneFrameValue * (fps60 / fps));
}

void FbxModel::Impl::CreateConstantBuffer() noexcept(false) {

	constantBuffer = Direct3DHelper::CreateConstantBuffer(sizeof(Constant));

	skinMatrixConstantBuffer = Direct3DHelper::CreateConstantBuffer(sizeof(XMMATRIX) * static_cast<UINT>(skeletons.size()));

	shadowWorldMatrixConstant = Direct3DHelper::CreateConstantBuffer(sizeof(ShadowMapConstant));
}

void FbxModel::Impl::Render(const Transform* transform, int frame) {

	//コンテキスト取得
	const auto& d3dContext = DirectX11::GetD3DDeviceContext();

	//コンスタントバッファセット
	d3dContext->VSSetConstantBuffers(0, 1, constantBuffer.GetAddressOf());
	d3dContext->PSSetConstantBuffers(0, 1, constantBuffer.GetAddressOf());

	//存在しうるフレームを超えないようにする
	if (hasAnimation) frame %= frameCount;
	
	//メッシュごとに変化のない情報をセット
	Constant constantBufferResource{};
	auto* camera = Camera::GetCurrentCamera();
	XMStoreFloat4x4(&constantBufferResource.matrixWVP, XMMatrixTranspose(transform->GetWorldMatrix() * camera->GetViewMatrix() * camera->GetProjectionMatrix()));
	XMStoreFloat4x4(&constantBufferResource.matrixWorld, XMMatrixTranspose(transform->GetWorldMatrix()));
	XMStoreFloat4x4(&constantBufferResource.matrixNormalTransform, XMMatrixTranspose(transform->GetWorldRotationMatrix() * XMMatrixInverse(nullptr, transform->GetWorldScalingMatrix())));
	XMStoreFloat4(&constantBufferResource.camera, camera->GetPosition());
	constantBufferResource.optionFlg |= (hasAnimation && frame != 0 ? 1 : 0) << 3;

	auto* sceneManager = GameOperation::SceneManager::GetInstance();
	if (auto* currentScene = sceneManager->GetCurrentScene()) {

		constantBufferResource.optionFlg |= (currentScene->IsShadowRendering() ? 1 : 0) << 0x0F;
		
		if (auto* light = dynamic_cast<GameOperation::LightPlain*>(currentScene->GetLight())) {

			XMStoreFloat4(&constantBufferResource.lightDir, light->GetDirection());
			XMStoreFloat4(&constantBufferResource.lightColor, light->GetColor());
		}
	}
	
	
	//全メッシュを描画
	for (const auto& mesh : meshes) {

		//頂点バッファ、インデックスバッファを取得しセット
		auto* vertexBuffer = mesh->GetVertexBuffer();
		auto* indexBuffer = mesh->GetIndexBuffer();
		
		auto stride = FbxMeshData::GetVertexDataSize();
		UINT offset = 0;
		
		d3dContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		d3dContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

		//メッシュのマテリアルを取得
		auto materialUniqueId = mesh->GetMaterialId();
		auto materialResult = ranges::find_if(materials, [&materialUniqueId](auto&& a) { return a->GetUniqueId() == materialUniqueId; });
		auto* material = (*materialResult).get();
		
		//各種シェーダリソースをセット
		vector materialDataNames{ {
			FbxSurfaceMaterial::sDiffuse,
			FbxSurfaceMaterial::sBump,
			FbxSurfaceMaterial::sNormalMap
		} };
		for (auto i = 0u; i < materialDataNames.size(); ++i) {

			constantBufferResource.optionFlg &= ~(1 << i);
			
			if (auto* texture = material->GetTexture(materialDataNames[i])) {

				constantBufferResource.optionFlg |= 1 << i;
				
				const auto& samplerState = texture->GetSamplerState();
				const auto& shaderResourceView = texture->GetShaderResourceView();
				d3dContext->PSSetSamplers(i, 1, samplerState.GetAddressOf());
				d3dContext->PSSetShaderResources(i, 1, shaderResourceView.GetAddressOf());
			}
		}
		
		//マテリアル情報をコンスタントバッファにセット
		XMStoreFloat4(&constantBufferResource.diffuse, material->GetDiffuse());
		XMStoreFloat4(&constantBufferResource.ambient, material->GetAmbient());
		XMStoreFloat4(&constantBufferResource.emissive, material->GetEmissive());
		XMStoreFloat4(&constantBufferResource.specular, material->GetSpecular());
		constantBufferResource.transparent = material->GetTransparent();
		constantBufferResource.shininess = material->GetShininess();
		constantBufferResource.reflection = material->GetReflection();

		//GPUへ送信
		D3D11_MAPPED_SUBRESOURCE mappedSubResource;
		d3dContext->Map(constantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
		memcpy_s(mappedSubResource.pData, mappedSubResource.RowPitch, &constantBufferResource, sizeof(Constant));

		d3dContext->Unmap(constantBuffer.Get(), 0);

		//アニメーションがある場合
		if (constantBufferResource.optionFlg & 1 << 3) {

			SetSkinConstantBuffer(mesh, 1, frame);
		}
		
		//描画
		d3dContext->DrawIndexed(mesh->GetIndexCount(), 0, 0);
	}
}

void FbxModel::Impl::RenderShadowMap(const Transform* transform, int frame) {

	SetPipeline(GraphicsPipeline::PIPELINE_TYPE::SHADOW_MAP);
	
	//コンテキスト取得
	const auto& d3dContext = DirectX11::GetD3DDeviceContext();

	//コンスタントバッファセット
	d3dContext->VSSetConstantBuffers(VS_CONSTANT_SHADOW_WORLD_SLOT, 1, shadowWorldMatrixConstant.GetAddressOf());

	//定数バッファリソース
	ShadowMapConstant cbResource;
	cbResource.worldMatrix = XMMatrixTranspose(transform->GetWorldMatrix());
	cbResource.isAnimation = hasAnimation && frame != 0 ? 1 : 0;
	
	//GPUへ送信
	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	d3dContext->Map(shadowWorldMatrixConstant.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy_s(mappedSubResource.pData, mappedSubResource.RowPitch, &cbResource, sizeof(ShadowMapConstant));
	d3dContext->Unmap(shadowWorldMatrixConstant.Get(), 0);
	
	//全メッシュを描画
	for (const auto& mesh : meshes) {

		//頂点バッファ、インデックスバッファを取得しセット
		auto* vertexBuffer = mesh->GetVertexBuffer();
		auto* indexBuffer = mesh->GetIndexBuffer();

		auto stride = FbxMeshData::GetVertexDataSize();
		UINT offset = 0;

		d3dContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		d3dContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

		//アニメーションがある場合
		if (cbResource.isAnimation) {

			SetSkinConstantBuffer(mesh, VS_CONSTANT_SHADOW_SKIN_SLOT, frame);
		}

		//描画
		d3dContext->DrawIndexed(mesh->GetIndexCount(), 0, 0);
	}
}

void FbxModel::Impl::SetSkinConstantBuffer(const MeshPtr& mesh, UINT slot, int frame) {

	const auto& d3dContext = DirectX11::GetD3DDeviceContext();
	
	d3dContext->VSSetConstantBuffers(slot, 1, skinMatrixConstantBuffer.GetAddressOf());

	//バッファ内に情報を詰める
	vector<XMMATRIX> skinMatrices(mesh->GetClusterCount() * 2);
	for (auto i = 0u; i < skinMatrices.size(); i += 2) {

		auto uniqueId = mesh->GetClusterLinkUniqueId(i / 2);
		auto result = ranges::find_if(skeletons, [&uniqueId](auto&& a) { return a->GetUniqueId() == uniqueId; });

		skinMatrices[i] = XMMatrixTranspose(mesh->GetClusterBindPose(i / 2));
		skinMatrices[i + 1] = XMMatrixTranspose((*result)->GetGlobalTransform(frame));
	}
	//GPUへ送信
	D3D11_MAPPED_SUBRESOURCE mappedSubResource1;
	d3dContext->Map(skinMatrixConstantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource1);
	memcpy_s(mappedSubResource1.pData, mappedSubResource1.RowPitch, skinMatrices.data(), sizeof(XMMATRIX) * skinMatrices.size());

	d3dContext->Unmap(skinMatrixConstantBuffer.Get(), 0);
}

auto FbxModel::Impl::GetBonePosition(const string& boneName, int frame) const -> optional<XMVECTOR> {

	auto result = ranges::find_if(skeletons, [&boneName](auto&& a){ return a->GetName() == boneName; } );

	if (result == skeletons.cend())	return nullopt;

	auto position = (*result)->GetGlobalPosition(frame);
	auto select = XMVectorGreater(XMVectorAbs(position), XMVectorSplatEpsilon());
	position = XMVectorSelect(g_XMZero, position, select);
	return position;
}

auto FbxModel::Impl::GetBoneTransform(const string& boneName, int frame) const -> unique_ptr<Transform> {

	auto result = ranges::find_if(skeletons, [&boneName](auto&& a) { return a->GetName() == boneName; });

	if (result == skeletons.cend())	return nullptr;

	const auto& fbxMat = (*result)->GetGlobalFbxTransform(frame);

	auto transform = make_unique<Transform>();
	transform
		->SetWorldPosition(FbxHelper::ConvertToXmvector(fbxMat.GetT()))
		->SetWorldOriented(FbxHelper::ConvertToXmvector(fbxMat.GetQ()))
		->SetWorldScale(FbxHelper::ConvertToXmvector(fbxMat.GetS()));
	
	return move(transform);
}

auto FbxModel::Impl::GetExtents() -> XMVECTOR {

	auto min = XMVectorReplicate(numeric_limits<float>::max());
	auto max = XMVectorReplicate(numeric_limits<float>::min());

	for (const auto& mesh : meshes) {

		mesh->GetMinMaxPosition(&min, &max);
	}
	auto extents = (max - min) / 2;
	auto select = XMVectorGreater(XMVectorAbs(extents), XMVectorSplatEpsilon());
	extents = XMVectorSelect(g_XMZero, extents, select);
	return extents;
}

auto FbxModel::Impl::GetCenterPosition() -> XMVECTOR {
	
	auto min = XMVectorReplicate(numeric_limits<float>::max());
	auto max = XMVectorReplicate(numeric_limits<float>::min());

	for (const auto& mesh : meshes) {

		mesh->GetMinMaxPosition(&min, &max);
	}
	auto center = (min + max) * 0.5f;

	auto select = XMVectorGreater(XMVectorAbs(center), XMVectorSplatEpsilon());
	center = XMVectorSelect(g_XMZero, center, select);
	return XMVectorSelect(g_XMZero, center, g_XMSelect1110);
}

auto FbxModel::Impl::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool {

	auto bufNormal = XMVectorZero();
	auto isHitEvenOnce = false;
	auto distance = FLT_MAX;

	for (const auto& mesh : meshes) {

		auto isHit = mesh->RayCast(origin, direction, &distance, &bufNormal);
		if (isHit) isHitEvenOnce = true;
	}
	if (isHitEvenOnce) {

		if (dist)	*dist = distance;
		if (normal)	*normal = bufNormal;
	}
	return isHitEvenOnce;
}

}
