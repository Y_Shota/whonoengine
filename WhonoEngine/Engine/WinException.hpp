#pragma once
#include <exception>


namespace Whono::Window {

/**
* \brief WindowsAPIに関する例外
*/
class WinException final : public std::exception {

public:
	explicit WinException(const char* const msg) : std::exception(msg) {}
};

}