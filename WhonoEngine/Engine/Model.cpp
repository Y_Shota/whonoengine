#include "Model.hpp"

#include <functional>
#include <filesystem>
#include <vector>
#include <unordered_map>


#include "FbxModel.hpp"
#include "GraphicsPipeline.hpp"
#include "ImageModel.hpp"
#include "IRenderer3D.hpp"
#include "SceneObject.hpp"
#include "Time.hpp"
#include "Transform.hpp"

using namespace std;

using Whono::GameOperation::Transform;
using Whono::RenderObject::IRenderer3D;
using Whono::RenderObject::ImageModel;
using Whono::RenderObject::FbxModel;

namespace fs = filesystem;


namespace Whono::Model {


constexpr auto DIRECTORY_PATH = "Assets/";

const unordered_map<string, function<unique_ptr<IRenderer3D>()>> EXTENSION_INSTANCE_MAP{{
	{ ".png", make_unique<ImageModel> },
	{ ".jpg", make_unique<ImageModel> },
	{ ".bmp", make_unique<ImageModel> },
	{ ".fbx", make_unique<FbxModel> }
}};

const unordered_map<string, PIPELINE_TYPE> DEFAULT_PIPELINE_MAP {{
	{ ".png", PIPELINE_TYPE::SIMPLE_2D },
	{ ".jpg", PIPELINE_TYPE::SIMPLE_2D },
	{ ".bmp", PIPELINE_TYPE::SIMPLE_2D },
	{ ".fbx", PIPELINE_TYPE::SIMPLE_3D }
}};

struct ModelData {

	unique_ptr<Transform> transform;
	fs::path filePath;
	shared_ptr<IRenderer3D> model;
	int startFrame = 0;
	int endFrame = 0;
	float currentFrame = 0;
	float animationSpeed = 0;
	bool resident = false;
	PIPELINE_TYPE pipeline;
};

vector<unique_ptr<ModelData>> data;

[[nodiscard]]
auto HasHandle(int handle) -> bool;


auto Load(const std::filesystem::path& filePath) -> int {

	auto defaultPath = fs::current_path();
	fs::current_path(DIRECTORY_PATH);
	
	//モデルデータインスタンス生成
	auto modelDatum = make_unique<ModelData>();

	//既に読み込まれているか
	auto result = ranges::find_if(data,
	                              [&](auto& data) {
		                              if (!data) return false;
		                              return data->filePath == filePath;
	                              });

	//読み込まれている場合共有
	if (result != data.end()) {

		modelDatum->model = (*result)->model;
	}
	//新たに開く
	else {
		try {

			//拡張子から生成するインスタンスを変更
			auto&& extension = filePath.extension().string();
			modelDatum->model = EXTENSION_INSTANCE_MAP.at(extension)();
			modelDatum->pipeline = DEFAULT_PIPELINE_MAP.at(extension);
		}
		//対応してない場合-1を返す
		catch (out_of_range) {

			modelDatum.reset();

			fs::current_path(defaultPath);
			return UNALLOCATED_HANDLE;
		}
		auto ret = modelDatum->model->Load(filePath);
		if (!ret) {

			fs::current_path(defaultPath);
			return UNALLOCATED_HANDLE;
		}
	}
	//いろいろデータ書き込み
	modelDatum->filePath = filePath;
	modelDatum->transform = make_unique<Transform>();

	//配列内に空きがあった場合そこに移動
	for (auto i = 0u; i < data.size(); ++i) {

		if (!data[i]) {

			data[i] = move(modelDatum);
			fs::current_path(defaultPath);
			return i;
		}
	}

	//最後尾に追加
	data.emplace_back(move(modelDatum));
	fs::current_path(defaultPath);
	return static_cast<int>(data.size()) - 1;
}

void Update() {

	for (auto& modelDatum : data) {

		if (!modelDatum)	continue;
		
		//アニメーションが動作中ならフレーム更新
		if (modelDatum->animationSpeed != 0.f) {

			modelDatum->currentFrame += modelDatum->animationSpeed * Time::GetFrameErrorRatio();

			//終了フレームを超えた場合超えた差分を開始フレームに足したフレームにする
			if (modelDatum->currentFrame >= modelDatum->endFrame) {

				modelDatum->currentFrame = fmod(modelDatum->currentFrame, static_cast<float>(modelDatum->endFrame)) + modelDatum->startFrame;
			}
			//開始フレーム未満になった場合超えた差分を終了フレームから引いたフレームにする
			else if (modelDatum->currentFrame < modelDatum->startFrame) {

				auto subFrame = modelDatum->currentFrame - modelDatum->startFrame;
				modelDatum->currentFrame = subFrame - floor(subFrame / modelDatum->endFrame) * modelDatum->endFrame;
			}
		}
	}
}

void Render(int handle) {

	if (!HasHandle(handle))	return;

	auto& modelDatum = data[handle];
	
	switch (SceneObject::GetRenderingPhase()) {
	using enum SceneObject::RENDERING_PHASE;
	case NORMAL:
		SetPipeline(modelDatum->pipeline);
		modelDatum->model->Render(modelDatum->transform.get(), static_cast<int>(modelDatum->currentFrame));
		break;
	case SHADOW:
		modelDatum->model->RenderShadowMap(modelDatum->transform.get(), static_cast<int>(modelDatum->currentFrame));
		break;
	}
}

void SetTransform(int handle, const Transform* transform) {

	if (!HasHandle(handle))	return;
	if (!transform)			return;

	*data[handle]->transform = *transform;
}

void SetLinkedTransform(int handle, Transform* transform) {

	if (!HasHandle(handle))	return;
	if (!transform)			return;

	data[handle]->transform->SetParent(transform);
}

void SetAnimation(int handle, int startFrame, int endFrame, float speed) {

	if (!HasHandle(handle))		return;
	if (endFrame < startFrame)	return;

	auto& modelDatum = data[handle];
	modelDatum->startFrame = startFrame;
	modelDatum->endFrame = endFrame;
	modelDatum->animationSpeed = speed;
}

void SetAnimationFrame(int handle, float frame) {

	if (!HasHandle(handle))	return;

	data[handle]->currentFrame = frame;
}

void SetAnimationSpeed(int handle, float speed) {

	if (!HasHandle(handle))	return;

	data[handle]->animationSpeed = speed;
}

void SetPipeline(int handle, PIPELINE_TYPE pipeline) {

	if (!HasHandle(handle))	return;

	data[handle]->pipeline = pipeline;
}

auto GetTransform(int handle) -> Transform* {

	if (!HasHandle(handle))	return nullptr;

	return data[handle]->transform.get();
}

auto GetBonePosition(int handle, const std::string& boneName) -> optional<XMVECTOR> {

	if (!HasHandle(handle))	return nullopt;

	auto& modelDatum = data[handle];
	
	return modelDatum->model->GetBonePosition(boneName, static_cast<int>(modelDatum->currentFrame));
}

auto GetBoneTransform(int handle, const string& boneName) -> unique_ptr<Transform> {

	if (!HasHandle(handle))	return nullptr;

	auto& modelDatum = data[handle];
	
	auto boneTransform = modelDatum->model->GetBoneTransform(boneName, static_cast<int>(modelDatum->currentFrame));
	if (!boneTransform)	return nullptr;

	boneTransform->SetParent(modelDatum->transform.get(), false);
	return move(boneTransform);
}

auto GetCenterPosition(int handle) -> XMVECTOR {

	if (!HasHandle(handle))	return g_XMZero;

	auto localCenter = data[handle]->model->GetCenterPosition();
	return XMVector3Transform(localCenter, data[handle]->transform->GetWorldMatrix());
}

auto GetWorldSpatialExtents(int handle) -> XMVECTOR {

	if (!HasHandle(handle))	return g_XMZero;

	auto localSize = data[handle]->model->GetExtents();
	return XMVector3Transform(localSize, data[handle]->transform->GetWorldScalingMatrix());
}

auto RayCast(int handle, CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) -> bool {

	if (!HasHandle(handle))	return false;

	const auto& datum = data[handle];
	const auto& transform = datum->transform;
	auto inverseWorldMatrix = XMMatrixInverse(nullptr, transform->GetWorldMatrix());
	auto tmpVec = origin + direction;
	auto deformedOrigin = XMVector3Transform(origin, inverseWorldMatrix);
	auto deformedDirection = XMVector3Transform(tmpVec, inverseWorldMatrix) - deformedOrigin;
	deformedDirection = XMVector3Normalize(deformedDirection);

	if (XMVector3IsNaN(deformedOrigin))		return false;
	if (XMVector3IsNaN(deformedDirection))	return false;
	
	auto result = datum->model->RayCast(deformedOrigin, deformedDirection, dist, normal);

	//距離は法線はモデル空間のものなのでワールド空間で扱うものに変換
	if (result) {
		if (dist) {
			
			auto toIntersect = deformedDirection * (*dist);
			toIntersect = toIntersect * transform->GetWorldScale();
			*dist = XMVector3Length(toIntersect).m128_f32[0];
		}
		if (normal) {

			auto normalTransformMatrix = XMMatrixInverse(nullptr, transform->GetWorldScalingMatrix()) * transform->GetWorldRotationMatrix();
			*normal = XMVector3Transform(*normal, normalTransformMatrix);
			*normal = XMVector3Normalize(*normal);
		}
	}
	
	return result;
}

void SetResident(int handle, bool resident) {

	if (!HasHandle(handle))	return;

	data[handle]->resident = resident;
}

void Release(int handle) {

	if (!HasHandle(handle))	return;

	data[handle]->model.reset();
	data[handle].reset();
}

void Release(bool isAll) {
	
	for (auto& datum : data) {

		if (isAll && datum) {

			if (datum->model)	datum->model.reset();
			datum.reset();
		}
		else if (datum && !datum->resident) {

			if (datum->model)	datum->model.reset();
			datum.reset();
		}
	} 
}

auto HasHandle(int handle) -> bool {

	if (handle < 0 || handle >= static_cast<int>(data.size()))	return false;
	if (!data[handle])											return false;

	return true;
}


}
