#pragma once
#include "IRenderer3D.hpp"

#include <memory>


namespace Whono::RenderObject {


/**
 * \brief fbxファイルを読み込み3D空間にレンダリングする
 */
class FbxModel final : public IRenderer3D {

public:
	/**
	 * \brief コンストラクタ
	 */
	FbxModel();
	/**
	 * \brief デストラクタ
	 */
	~FbxModel();

	/**
	 * \brief リソースを読み込みレンダリングに必要な情報を生成する
	 * \param filePath リソースのファイスパス
	 * \return 生成できたなら真
	 */
	auto Load(const std::filesystem::path& filePath) -> bool override;
	/**
	 * \brief 変形情報をもとに描画する
	 * \param transform 変形情報
	 * \param frame アニメーションのフレーム数の指定
	 */
	void Render(const GameOperation::Transform* transform, int frame = 0) override;
	/**
	 * \brief 変形情報をもとにシャドウマップへ描画する
	 * \param transform 変形情報
	 * \param frame = 0 アニメーションのフレーム数の指定
	 */
	void RenderShadowMap(const GameOperation::Transform* transform, int frame = 0) override;
	/**
	 * \brief アニメーションを持っているかどうか
	 * \return 持っている場合真
	 */
	auto HasAnimation() -> bool override;
	/**
	 * \brief ボーンの座標を取得する
	 * \param boneName ボーンの名前
	 * \param frame アニメーションのフレーム数の指定
	 * \return ボーンの座標　ない場合無効値
	 */
	auto GetBonePosition(const std::string& boneName, int frame = 0) -> std::optional<DirectX::XMVECTOR> override;
	/**
	 * \brief ボーンの変形情報を取得する
	 * \param boneName ボーンの名前
	 * \param frame アニメーションのフレーム数の指定
	 * \return ボーンの変形情報　ない場合は無効値
	 */
	auto GetBoneTransform(const std::string& boneName, int frame = 0) -> std::unique_ptr<GameOperation::Transform> override;
	/**
	 * \brief モデル空間でのサイズを取得する
	 * \return サイズ
	 */
	auto GetExtents( )-> DirectX::XMVECTOR override;
	/**
	* \brief 無変形状態のサイズを取得する
	* \return 無変形状態のサイズ
	*/
	auto GetCenterPosition() -> DirectX::XMVECTOR override;
	/**
	 * \brief レイキャストを行う
	 * \param origin レイの始点
	 * \param direction レイの方向
	 * \param[out] dist 衝突したときの距離
	 * \param[out] normal 衝突したポリゴンの法線
	 * \return 衝突したなら真
	 */
	auto RayCast(DirectX::CXMVECTOR origin, DirectX::CXMVECTOR direction, float* dist, DirectX::XMVECTOR* normal) const -> bool override;
	/**
	 * \brief 解放処理
	 */
	void Release() override;

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽
};

/**
* \brief FBX SDKに関する例外
*/
class FbxException final : public std::exception {
public:
	explicit FbxException(const char* const msg) : std::exception(msg) {}
};

}