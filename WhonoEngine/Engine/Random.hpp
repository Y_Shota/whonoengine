#pragma once

/**
* \brief 乱数に関する処理
* \author whono
* \version 1.0
* \since 1.0
*/
namespace Whono::Random {


/**
 * \brief 乱数生成に用いるシード値を設定
 * \param seed シード値
 */
void SetSeed(int seed);

/**
 * \brief 乱数生成に用いるシード値を予測不可能な値で設定する
 */
void SetRandomSeed();

/**
 * \brief ランダムなバイト配列を生成する
 * \param[out] bytes 先頭アドレス
 * \param size サイズ
 */
void NextBytes(unsigned char* bytes, size_t size);

/**
 * \brief 0から指定された範囲の乱数を生成する
 * \param bound = INT_MAX 範囲
 * \return int乱数
 */
auto NextInt(int bound = 0x7FFFFFFF) -> int;

/**
 * \brief 0からlong long型がとりうる最大値の乱数を生成する
 * \return long long乱数
 */
auto NextLongLong() -> long long;

/**
 * \brief 同確率でtrueかfalseを生成する
 * \return bool乱数
 */
auto NextBool() -> bool;

/**
 * \brief 0から1の範囲の実数を生成する
 * \return float乱数
 */
auto NextFloat() -> float;

/**
 * \brief 0から1の範囲で倍精度の実数を生成する
 * \return double乱数
 */
auto NextDouble() -> double;

/**
 * \brief 平均0の標準偏差1ガウス分布から乱数を生成する
 * \return double乱数
 */
auto NextGaussian() -> double;

/**
 * \brief 入力された平均値のポアソン分布から乱数を生成する
 * \return double乱数
 */
auto NextPoisson(double lambda) -> double;


}