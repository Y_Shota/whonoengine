#pragma once

/**
 * \brief 時間に関する処理
 * \author whono
 * \version 1.0
 * \since 1.0
 */
namespace Whono::Time {

constexpr float FRAME_RATE = 60.f;			//フレームレート

/**
 * \brief 1フレームの処理速度を1/60secにする
 */
void CalculationSleep();

/**
 * \brief FPSの取得
 * \return FPS
 */
[[nodiscard]]
auto GetFps() noexcept -> int;
/**
 * \brief 1フレーム当たりの時間の取得	単位:ms
 * \return 1フレーム当たりの時間
 */
[[nodiscard]]
auto GetFrameTimeMs() noexcept -> float;
/**
 * \brief 1フレーム当たりの時間の取得	単位:s
 * \return 1フレーム当たりの時間
 */
[[nodiscard]]
auto GetFrameTimeS() noexcept -> float;

/**
 * \brief 経過時間の計測をリセットする
 */
void ResetSceneTimer() noexcept;

/**
 * \brief シーン開始からの経過時間を取得	単位:ms
 * \return 経過時間
 */
[[nodiscard]]
auto GetElapsedTimeMs() noexcept -> float;
/**
* \brief シーン開始からの経過時間を取得		単位:s
* \return 経過時間
*/
[[nodiscard]]
auto GetElapsedTimeS() noexcept -> float;
/**
 * \brief 1フレーム当たりの誤差の比率を返す
 * 60FPSの場合1.6667sからの比率を返す
 * \return 誤差の比率
 */
[[nodiscard]]
auto GetFrameErrorRatio() noexcept -> float;

/**
 * \brief 時間に関するデータを出力する
 */
void PrintDebugData();

/**
 * \brief 更新時間かどうか
 * \return 更新すべきなら真
 */
[[nodiscard]]
auto IsUpdateTime() -> bool;
/**
 * \brief 時間計測に関するデータを更新する
 */
void UpdateData();


};

