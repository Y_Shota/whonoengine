#pragma once

#include <filesystem>

#include "Math/Vector2.hpp"

namespace Whono {
namespace GameOperation {
class Transform;
}
}

enum class BASE_POSITION : unsigned char {

	CENTER,
	TOP_LEFT,
	TOP_RIGHT,
	BOTTOM_LEFT,
	BOTTOM_RIGHT,
	TOP_CENTER,
	BOTTOM_CENTER,
	LEFT_CENTER,
	RIGHT_CENTER
};

namespace Whono::Image {

constexpr int UNALLOCATED_HANDLE = -1;

auto Load(const std::filesystem::path& filePath) -> int;

void Render(int handle);

void AllowAntiAliasing(int handle, bool antiAliasing = true);
void SetTransform(int handle, const GameOperation::Transform* transform, BASE_POSITION baseType = BASE_POSITION::CENTER);
void SetPosition(int handle, float x, float y, BASE_POSITION baseType = BASE_POSITION::CENTER);
void SetPosition(int handle, const Vector2& position, BASE_POSITION baseType = BASE_POSITION::CENTER);
void SetRotate(int handle, float angle);
void SetScale(int handle, float x, float y);
void SetScale(int handle, const Vector2& scaling);
void SetFullSize(int handle);
void SetAlpha(int handle, float alpha);
void SetRect(int handle, float left, float top, float width, float height);
void SetBasePosition(int handle, BASE_POSITION baseType = BASE_POSITION::CENTER);
void SetResident(int handle, bool resident = false);

[[nodiscard]]
auto GetImageSize(int handle) -> Vector2;

void Release(int handle);
void Release(bool isAll = true);

}
