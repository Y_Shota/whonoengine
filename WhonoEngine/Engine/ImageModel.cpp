#include "ImageModel.hpp"

#include <array>
#include <d3d11_1.h>
#include <dxgiformat.h>
#include <DirectXMath.h>
#include <iostream>

#include "Camera.hpp"
#include "DirectX11.hpp"
#include "DirectXException.hpp"
#include "Texture.hpp"
#include "Transform.hpp"
#include "WinException.hpp"

using namespace std;
using namespace DirectX;

using Whono::DirectX11::DirectXException;
using Whono::Window::WinException;
using Whono::GameOperation::Transform;

namespace fs = filesystem;


namespace Whono::RenderObject {


/**
 * \brief 頂点情報
 */
struct Vertex {

	static constexpr int NUMBER = 4;	//画像の頂点数

	XMFLOAT4 position;					//座標
	XMFLOAT4 texcoord;					//UV座標
};

/**
 * \brief コンスタントバッファ
 */
struct ConstantBuffer {

	XMMATRIX matrixWVP;					//変換用行列
	float lightLevel;					//レンダリング時の明るさ
};

/**
 * \brief 内部実装隠蔽用
 */
struct ImageModel::Impl {

	/**
	 * \brief 頂点バッファを作成する
	 * \throw DirectXException
	 */
	void CreateVertexBuffer() noexcept(false);
	
	/**
	 * \brief インデックスバッファを作成する
	 * \throw DirectXException
	 */
	void CreateIndexBuffer() noexcept(false);

	/**
	* \brief コンスタントバッファを作成する
	* \throw DirectXException
	*/
	void CreateConstantBuffer() noexcept(false);

	/**
	 * \brief テクスチャを作成する
	 * \param filePath ファイルパス
	 * \throw WinException
	 * \throw DirectXException
	 */
	void CreateTexture(const fs::path& filePath) noexcept(false);

	void SetVertexBuffer(CComPtrRef<ID3D11DeviceContext> d3dContext);
	void SetIndexBuffer(CComPtrRef<ID3D11DeviceContext> d3dContext) const;
	void SetConstantBuffer(CComPtrRef<ID3D11DeviceContext> d3dContext, const Transform* transform);
	void SetShaderResource(CComPtrRef<ID3D11DeviceContext> d3dContext) const;


	static constexpr int INDEX_SIZE = 6;
	
	ComPtr<ID3D11Buffer> vertexBuffer;				//頂点シェーダに渡す情報
	ComPtr<ID3D11Buffer> indexBuffer;				//頂点シェーダへ渡す順番
	ComPtr<ID3D11Buffer> constantBuffer;			//パイプライン実行時定数
	unique_ptr<Texture> texture;					//テクスチャ
};


ImageModel::ImageModel() {

	pImpl_ = make_unique<Impl>();
}

ImageModel::~ImageModel() = default;

auto ImageModel::Load(const fs::path& filePath) -> bool {

	pImpl_->CreateVertexBuffer();
	pImpl_->CreateIndexBuffer();
	pImpl_->CreateConstantBuffer();

	//ファイルが見つからなかったときのみキャッチしfalseを返す
	try {
		pImpl_->CreateTexture(filePath);
	}
	catch (WinException& e) {

		cout << e.what() << endl;
		return false;
	}

	return true;
}

void ImageModel::Render(const Transform* transform, int frame) {

	const auto& d3dContext = DirectX11::GetD3DDeviceContext();
	
	//シェーダ実行に必要な情報をセット
	pImpl_->SetVertexBuffer(d3dContext);
	pImpl_->SetIndexBuffer(d3dContext);
	pImpl_->SetConstantBuffer(d3dContext, transform);
	pImpl_->SetShaderResource(d3dContext);

	//描画
	d3dContext->DrawIndexed(Impl::INDEX_SIZE, 0, 0);
}

auto ImageModel::HasAnimation() -> bool {

	//todo gifならアニメーション対応
	return false;
}

auto ImageModel::GetBonePosition(const string& boneName, int frame) -> optional<XMVECTOR> {

	return nullopt;
}

auto ImageModel::GetBoneTransform(const string& boneName, int frame) -> unique_ptr<Transform> {

	return nullptr;
}

auto ImageModel::GetExtents() -> XMVECTOR {

	//todo 画像サイズ測る
	return g_XMZero;
}

auto ImageModel::GetCenterPosition() -> XMVECTOR {

	//todo 画像サイズ測る
	return g_XMZero;
}

auto ImageModel::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool {

	return false;
}

void ImageModel::Release() {
	
	pImpl_->texture.reset();
	pImpl_->vertexBuffer.Reset();
	pImpl_->indexBuffer.Reset();
	pImpl_->constantBuffer.Reset();
}


void ImageModel::Impl::CreateVertexBuffer() noexcept(false) {

	//四角形ポリゴンなので固定
	array<Vertex, Vertex::NUMBER> vertices{ {
		{ XMFLOAT4(-1.f, 1.f, 0, 0),	XMFLOAT4(0, 0, 0, 0) },
		{ XMFLOAT4(1.f, 1.f, 0, 0),		XMFLOAT4(1, 0, 0, 0) },
		{ XMFLOAT4(1.f, -1.f, 0, 0),	XMFLOAT4(1, 1, 0, 0) },
		{ XMFLOAT4(-1.f, -1.f, 0, 0),	XMFLOAT4(0, 1, 0, 0) },
	} };

	//頂点バッファの設定
	D3D11_BUFFER_DESC vertexBufferDesc{};
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * static_cast<UINT>(vertices.size());
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	D3D11_SUBRESOURCE_DATA vertexData;
	vertexData.pSysMem = vertices.data();

	const auto& d3dDevice = DirectX11::GetD3DDevice();

	//頂点バッファの作成
	auto hr = d3dDevice->CreateBuffer(&vertexBufferDesc, &vertexData, &vertexBuffer);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateBuffer()");
}

void ImageModel::Impl::CreateIndexBuffer() noexcept(false) {

	//四角形ポリゴンなので固定
	array<int, INDEX_SIZE> index{ {
			0, 2, 3,  0, 1, 2
	} };

	//インデックスバッファの設定
	D3D11_BUFFER_DESC indexBufferDesc{};
	indexBufferDesc.ByteWidth = sizeof(int) * static_cast<UINT>(index.size());
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = index.data();

	const auto& d3dDevice = DirectX11::GetD3DDevice();

	//インデックスバッファの設定
	auto hr = d3dDevice->CreateBuffer(&indexBufferDesc, &InitData, &indexBuffer);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateBuffer()");
}

void ImageModel::Impl::CreateConstantBuffer() noexcept(false) {

	//コンスタントバッファの設定
	D3D11_BUFFER_DESC constantBufferDesc{};
	constantBufferDesc.ByteWidth = sizeof(ConstantBuffer);
	constantBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	const auto& d3dDevice = DirectX11::GetD3DDevice();

	//コンスタントバッファの作成
	auto hr = d3dDevice->CreateBuffer(&constantBufferDesc, nullptr, &constantBuffer);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateBuffer()");
}

void ImageModel::Impl::CreateTexture(const fs::path& filePath) noexcept(false) {

	texture = make_unique<Texture>();
	texture->Load(filePath);
	texture->CreateCustomSampler(D3D11_FILTER_MIN_MAG_MIP_LINEAR);
}

void ImageModel::Impl::SetVertexBuffer(CComPtrRef<ID3D11DeviceContext> d3dContext) {

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	d3dContext->IASetVertexBuffers(0, 1, vertexBuffer.GetAddressOf(), &stride, &offset);
}

void ImageModel::Impl::SetIndexBuffer(CComPtrRef<ID3D11DeviceContext> d3dContext) const {

	d3dContext->IASetIndexBuffer(indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
}

void ImageModel::Impl::SetConstantBuffer(CComPtrRef<ID3D11DeviceContext> d3dContext, const Transform* transform) {

	d3dContext->VSSetConstantBuffers(0, 1, constantBuffer.GetAddressOf());
	d3dContext->PSSetConstantBuffers(0, 1, constantBuffer.GetAddressOf());

	//座標返還に必要な情報を取得
	auto* camera = GameOperation::Camera::GetCurrentCamera();
	auto aspectRatio = texture->GetAspectRatio();

	//ローカル->クリップ空間に変換する行列
	auto&& matrixWVP = 
		transform->GetWorldMatrix() *								//ワールド行列
		XMMatrixScaling(1.f, aspectRatio, 1.f) *	//画像比適応用行列
		camera->GetViewMatrix() *									//ビュー行列
		camera->GetProjectionMatrix();								//プロジェクション行列
	
	ConstantBuffer cbResource;
	cbResource.matrixWVP = XMMatrixTranspose(matrixWVP);
	cbResource.lightLevel = 1.f; //todo lightクラス作成後ここに追加

	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	d3dContext->Map(constantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
	memcpy_s(mappedSubResource.pData, mappedSubResource.RowPitch, &cbResource, sizeof(ConstantBuffer));

	d3dContext->Unmap(constantBuffer.Get(), 0);
}

void ImageModel::Impl::SetShaderResource(CComPtrRef<ID3D11DeviceContext> d3dContext) const {

	const auto& samplerState = texture->GetSamplerState();
	d3dContext->PSSetSamplers(0, 1, samplerState.GetAddressOf());

	const auto& shaderResourceView = texture->GetShaderResourceView();
	d3dContext->PSSetShaderResources(0, 1, shaderResourceView.GetAddressOf());
}

}