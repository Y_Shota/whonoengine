#pragma once

#include <memory>


namespace Whono::DesignPattern {

template<class T>
class IFactory {

public:
	virtual ~IFactory() = default;

	template<class... Args>
	static auto Create(Args&&... args) -> std::unique_ptr<T> {

		return std::make_unique<T>(std::forward<Args>(args)...);
	}
};

}