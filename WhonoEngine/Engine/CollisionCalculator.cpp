#include "CollisionCalculator.hpp"

#include <array>
#include <DirectXCollision.h>

#include "Math/Math.hpp"

using namespace std;
using namespace DirectX;


namespace Whono::GameOperation {


namespace CollisionCalculator {

auto IsHit(const CapsuleCollider* capsule, const SphereCollider* sphere) -> bool {

	//カプセルの成分取得
	XMVECTOR segment1, segment2;
	auto capsuleRadius = capsule->GetRadius();
	capsule->GetSegment(&segment1, &segment2);

	//スフィアの成分を取得
	auto boundingSphere = sphere->GetBounding();
	auto sphereCenter = XMLoadFloat3(&boundingSphere.Center);
	
	auto distance = Math::SegmentToPointDistance(segment1, segment2, sphereCenter);

	//線分と点の距離がそれぞれの半径を足した距離以下なら衝突
	return distance <= capsuleRadius + boundingSphere.Radius;
}

auto IsHit(const SphereCollider* sphere, const CapsuleCollider* capsule) -> bool {

	return IsHit(capsule, sphere);
}

auto IsHit(const CapsuleCollider* capsule, const BoxCollider* box) -> bool {

	//カプセルの成分を取得
	XMVECTOR segment1, segment2;
	capsule->GetSegment(&segment1, &segment2);
	auto radius = capsule->GetRadius();

	//ボックスの成分を取得
	auto boundingBox = box->GetBounding();

	//境界内に線分の中に端点がある場合衝突
	if (boundingBox.Contains(segment1) == CONTAINS)	return true;
	if (boundingBox.Contains(segment2) == CONTAINS)	return true;

	BoundingSphere sphere(XMFLOAT3(segment1.m128_f32), radius);
	if (boundingBox.Intersects(sphere))	return true;
	sphere.Center = XMFLOAT3(segment2.m128_f32);
	if (boundingBox.Intersects(sphere))	return true;
	
	//ボックスの頂点座標を取得
	array<XMVECTOR, BoundingBox::CORNER_COUNT> corners;
	{
		array<XMFLOAT3, BoundingBox::CORNER_COUNT> cornersBuffer;
		boundingBox.GetCorners(cornersBuffer.data());
		for (auto i = 0u; i < cornersBuffer.size(); ++i) {

			corners[i] = XMLoadFloat3(&cornersBuffer[i]);
		}
	}

	//カプセルの半径より近かったら交差
	for (auto i = 0; i < corners.size(); i += 2) {

		auto distance = Math::SegmentsDistance(segment1, segment2, corners[i], corners[i + 1]);
		if (distance <= radius)	return true;
	}
	for (auto i = 0; i < corners.size() / 2; ++i) {

		auto distance = Math::SegmentsDistance(segment1, segment2, corners[i], corners[i + 4]);
		if (distance <= radius)	return true;
	}
	return false;
}

auto IsHit(const BoxCollider* box, const CapsuleCollider* capsule) -> bool {

	return IsHit(capsule, box);
}

auto IsHit(const CapsuleCollider* capsule, const ObbCollider* obb) -> bool {

	//カプセルの成分を取得
	XMVECTOR segment1, segment2;
	capsule->GetSegment(&segment1, &segment2);
	auto radius = capsule->GetRadius();

	//ボックスの成分を取得
	auto boundingOrientedBox = obb->GetBounding();
	
	//境界内に線分の中に端点がある場合衝突
	if (boundingOrientedBox.Contains(segment1) == CONTAINS)	return true;
	if (boundingOrientedBox.Contains(segment2) == CONTAINS)	return true;

	BoundingSphere sphere(XMFLOAT3(segment1.m128_f32), radius);
	if (boundingOrientedBox.Intersects(sphere))	return true;
	sphere.Center = XMFLOAT3(segment2.m128_f32);
	if (boundingOrientedBox.Intersects(sphere))	return true;
	
	//ボックスの頂点座標を取得
	array<XMVECTOR, BoundingOrientedBox::CORNER_COUNT> corners;
	{
		array<XMFLOAT3, BoundingOrientedBox::CORNER_COUNT> cornersBuffer;
		boundingOrientedBox.GetCorners(cornersBuffer.data());
		for (auto i = 0u; i < cornersBuffer.size(); ++i) {

			corners[i] = XMLoadFloat3(&cornersBuffer[i]);
		}
	}
	
	//カプセルの半径より近かったら交差
	for (auto i = 0; i < corners.size(); i += 2) {

		auto distance = Math::SegmentsDistance(segment1, segment2, corners[i], corners[i + 1]);
		if (distance <= radius)	return true;
	}
	for (auto i = 0; i < corners.size() / 2; ++i) {

		auto distance = Math::SegmentsDistance(segment1, segment2, corners[i], corners[i + 4]);
		if (distance <= radius)	return true;
	}
	return false;
}

auto IsHit(const ObbCollider* obb, const CapsuleCollider* capsule) -> bool {

	return IsHit(capsule, obb);
}

auto IsHit(const CapsuleCollider* capsule, const FrustumCollider* frustum) -> bool {

	//カプセルの成分を取得
	XMVECTOR segment1, segment2;
	capsule->GetSegment(&segment1, &segment2);
	auto radius = capsule->GetRadius();

	//ボックスの成分を取得
	auto boundingFrustum = frustum->GetBounding();
	
	//境界内に線分の中に端点がある場合衝突
	if (boundingFrustum.Contains(segment1) == CONTAINS)	return true;
	if (boundingFrustum.Contains(segment2) == CONTAINS)	return true;

	BoundingSphere sphere(XMFLOAT3(segment1.m128_f32), radius);
	if (boundingFrustum.Intersects(sphere))	return true;
	sphere.Center = XMFLOAT3(segment2.m128_f32);
	if (boundingFrustum.Intersects(sphere))	return true;

	//ボックスの頂点座標を取得
	array<XMVECTOR, BoundingFrustum::CORNER_COUNT> corners;
	{
		array<XMFLOAT3, BoundingFrustum::CORNER_COUNT> cornersBuffer;
		boundingFrustum.GetCorners(cornersBuffer.data());
		for (auto i = 0u; i < cornersBuffer.size(); ++i) {

			corners[i] = XMLoadFloat3(&cornersBuffer[i]);
		}
	}
	
	//カプセルの半径より近かったら交差
	for (auto i = 0; i < corners.size(); i += 2) {

		auto distance = Math::SegmentsDistance(segment1, segment2, corners[i], corners[i + 1]);
		if (distance <= radius)	return true;
	}
	for (auto i = 0; i < corners.size() / 2; ++i) {

		auto distance = Math::SegmentsDistance(segment1, segment2, corners[i], corners[i + 4]);
		if (distance <= radius)	return true;
	}
	return false;
}
auto IsHit(const FrustumCollider* frustum, const CapsuleCollider* capsule) -> bool {

	return IsHit(capsule, frustum);
}

auto IsHit(const CapsuleCollider* capsule1, const CapsuleCollider* capsule2) -> bool {

	//カプセルの各種成分を取得
	XMVECTOR segmentA1, segmentA2, segmentB1, segmentB2;
	capsule1->GetSegment(&segmentA1, &segmentA2);
	capsule2->GetSegment(&segmentB1, &segmentB2);
	auto radiusA = capsule1->GetRadius();
	auto radiusB = capsule2->GetRadius();

	//線分間の距離を算出
	auto distance = Math::SegmentsDistance(segmentA1, segmentA2, segmentB1, segmentB2);

	return distance <= radiusA + radiusB;
}

}

}
