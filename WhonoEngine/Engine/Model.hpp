#pragma once

#include <DirectXMath.h>
#include <filesystem>
#include <optional>

namespace Whono {
namespace GraphicsPipeline {
enum class PIPELINE_TYPE : unsigned char;
}
namespace GameOperation {
class Transform;
}
}
using Whono::GraphicsPipeline::PIPELINE_TYPE;


namespace Whono::Model {

constexpr int UNALLOCATED_HANDLE = -1;

/**
 * \brief ファイルからモデルデータを作成する
 * \param filePath 読み込むファイルのパス
 * \return モデルハンドル
 */
[[nodiscard]]
auto Load(const std::filesystem::path& filePath) -> int;

/**
 * \brief 更新処理
 */
void Update();

/**
 * \brief 描画する
 * \param handle モデルハンドル
 */
void Render(int handle);

/**
 * \brief 変形情報を設定する
 * \param handle モデルハンドル
 * \param transform 変形情報
 */
void SetTransform(int handle, const GameOperation::Transform* transform);
/**
 * \brief 受けとった変形情報を参照して変形し続けるようにする
 * \param handle モデルハンドル
 * \param transform 参照したい変形情報
 */
void SetLinkedTransform(int handle, GameOperation::Transform* transform);

/**
 * \brief アニメーションの情報を設定する
 * \param handle モデルハンドル
 * \param startFrame 開始フレーム
 * \param endFrame 終了フレーム
 * \param speed 再生速度
 */
void SetAnimation(int handle, int startFrame = 0, int endFrame = 0, float speed = 1);

/**
 * \brief フレームをセットする
 * \param handle モデルハンドル
 * \param frame 指定フレーム
 */
void SetAnimationFrame(int handle, float frame);

/**
 * \brief アニメーションの再生速度を設定する
 * \param handle モデルハンドル
 * \param speed 再生速度
 */
void SetAnimationSpeed(int handle, float speed = 1);

/**
 * \brief 使用するパイプラインを設定する
 * \param handle モデルハンドル
 * \param pipeline 使用するパイプライン
 */
void SetPipeline(int handle, PIPELINE_TYPE pipeline);

/**
 * \brief 変形情報を取得する
 * リンクしている場合書き換え注意
 * \param handle モデルハンドル
 * \return 変形情報
 */
[[nodiscard]]
auto GetTransform(int handle) -> GameOperation::Transform*;

/**
 * \brief ボーンの座標を取得する
 * \param handle モデルハンドル
 * \param boneName ボーンの名前
 * \return 座標オプショナル
 */
[[nodiscard]]
auto GetBonePosition(int handle, const std::string& boneName) -> std::optional<DirectX::XMVECTOR>;

/**
 * \brief ボーンの変形情報を取得する
 * \param handle モデルハンドル
 * \param boneName ボーンに名前
 * \return 変形情報
 */
[[nodiscard]]
auto GetBoneTransform(int handle, const std::string& boneName) -> std::unique_ptr<GameOperation::Transform>;

/**
 * \brief 中心座標を取得する
 * \param handle モデルハンドル
 * \return 中心座標
 */
[[nodiscard]]
auto GetCenterPosition(int handle) -> DirectX::XMVECTOR;

/**
 * \brief ワールド空間内でのサイズを取得する
 * \param handle モデルハンドル
 * \return サイズ
 */
[[nodiscard]]
auto GetWorldSpatialExtents(int handle) -> DirectX::XMVECTOR;

/**
 * \brief レイキャストを行う
 * \param handle モデルハンドル
 * \param origin 発射地点
 * \param direction 方向
 * \param[out] dist 当たった時の距離
 * \param[out] normal 当たったポリゴンの法線
 * \return 当たったなら真
 */
auto RayCast(int handle, DirectX::CXMVECTOR origin, DirectX::CXMVECTOR direction, float* dist = nullptr, DirectX::XMVECTOR* normal = nullptr) -> bool;

/**
 * \brief シーン遷移時に解放するかどうかを指定する
 * \param handle モデルハンドル
 * \param resident シーン遷移時に解放するか
 */
void SetResident(int handle, bool resident = false);

/**
 * \brief 指定したモデル情報を開放する
 * \param handle モデルハンドル
 */
void Release(int handle);

/**
 * \brief モデルデータを解放する
 * \param isAll すべて開放するか
 */
void Release(bool isAll = true);

}
