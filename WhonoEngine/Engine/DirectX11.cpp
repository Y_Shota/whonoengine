#include "DirectX11.hpp"

#include <vector>
#include <d3d11_1.h>
#include <d2d1_1.h>
#include <dwrite.h>
#include <DirectXColors.h>

#include "Audio.hpp"
#include "DirectXException.hpp"
#include "GraphicsPipeline.hpp"
#include "Image.hpp"
#include "Input.hpp"
#include "Model.hpp"
#include "SceneManager.hpp"
#include "Time.hpp"
#include "Window.hpp"
#include "WinException.hpp"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"d2d1.lib")
#pragma comment(lib,"dwrite.lib")

using namespace std;


namespace Whono::DirectX11 {


//DirectX Graphics Infrastructure
ComPtr<IDXGIDevice1> dxgiDevice;	//DXGI本体
ComPtr<IDXGISwapChain1> swapChain;	//描画先のバックバッファチェイン

//Direct3D
ComPtr<ID3D11Device>				d3dDevice;					//Direct3D本体
ComPtr<ID3D11DeviceContext>			d3dImmediateContext;		//グラフィックパイプラインに従い描画する
ComPtr<ID3D11Texture2D>				backBuffer;					//スワップチェインのバックバッファ
ComPtr<ID3D11RenderTargetView>		renderTargetView;			//描画先
ComPtr<ID3D11Texture2D>				depthStencil;
ComPtr<ID3D11DepthStencilView>		depthStencilView;			//奥行情報
ComPtr<ID3D11ShaderResourceView>	depthShaderResourceView;
ComPtr<ID3D11Debug>					d3dDebug;

//Direct2D
ComPtr<ID2D1Factory1>		d2dFactory;		//Direct2Dファクトリ
ComPtr<ID2D1Device>			d2dDevice;		//Direct2D本体
ComPtr<ID2D1DeviceContext>	d2dContext;		//バックバッファへ直接2D描画する

//DirectWrite
ComPtr<IDWriteFactory>		dwFactory;		//DirectWrite本体

DirectX::XMVECTORF32 clearColor = DirectX::Colors::Coral;	//クリアカラー

bool isInitialized = false;

/**
 * \brief デバイスの作成
 * \throw DirectXException
 */
void PreInitDirect3D() noexcept(false);
/**
 * \brief デバイス、スワップチェインの作成
 * \throw DirectXException
 */
void InitDirectXGraphics() noexcept(false);
/**
 * \brief レンダーターゲットの設定
 * \throw DirectXException
 */
void InitDirect3D() noexcept(false);
/**
 * \brief デバイス、コンテキストの作成　描画先の指定
 * \throw DirectXException
 */
void InitDirect2D() noexcept(false);
/**
 * \brief ファクトリの作成
 * \throw DirectXException
 */
void InitDirectWrite() noexcept(false);
/**
 * \brief グラフィックパイプライン共通部の設定
 * \throw DirectXException
 */
void PostInitDirect3D() noexcept(false);
/**
 * \brief レンダーターゲットビューを作成する
 * \throw DirectXException
 */
void CreateRenderTargetView() noexcept(false);
/**
* \brief デプスステンシビューを作成する
* \throw DirectXException
*/
void CreateDepthStencilView() noexcept(false);
/**
 * \brief D2Dのレンダーターゲットをバックバッファから作成する
 * \throw DirectXException
 */
void CreateBackBufferBitmap() noexcept(false);
/**
 * \brief ゲーム本体の更新処理
 * \throw WinException
 */
void Update() noexcept(false);
/**
 * \brief 描画処理
 * \throw DirectXException
 */
void Render() noexcept(false);

void Initialize() noexcept(false) {
	
	auto hr = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
	if (FAILED(hr))	throw Window::WinException("CoInitialize()");
	
	PreInitDirect3D();

	InitDirectXGraphics();
	InitDirect3D();
	InitDirect2D();
	InitDirectWrite();

	PostInitDirect3D();

	GraphicsPipeline::Initialize();

	Audio::Initialize();
	
	auto* sceneManager = GameOperation::SceneManager::GetInstance();
	sceneManager->Initialize();

	isInitialized = true;
}

auto IsInitialized() noexcept -> bool {

	return isInitialized;
}

void Finalize() noexcept {

	Image::Release();
	Model::Release();
	Audio::Finalize();

	GraphicsPipeline::Finalize();

	auto* sceneManager = GameOperation::SceneManager::GetInstance();
	sceneManager->Finalize();
	GameOperation::SceneManager::Destroy();
	
	depthStencilView.Reset();
	depthStencil.Reset();

	dwFactory.Reset();
	
	d2dContext.Reset();
	d2dDevice.Reset();
	d2dFactory.Reset();
	
	renderTargetView.Reset();
	backBuffer.Reset();	
	
	swapChain.Reset();	
	dxgiDevice.Reset();
	
	d3dImmediateContext.Reset();
	
	d3dDevice.Reset();

#ifdef _DEBUG
	
	d3dDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
	d3dDebug.Reset();

#endif
	
	CoUninitialize();
}

void FrameUpdate() noexcept(false) {

	Time::CalculationSleep();

	Update();

	Render();

	Input::PostProcess();
}

void Resize() {
	
	renderTargetView.Reset();
	backBuffer.Reset();
	d2dContext->SetTarget(nullptr);
	swapChain->ResizeBuffers(
		1,
		Window::GetWindowWidth(),
		Window::GetWindowHeight(),
		DXGI_FORMAT_B8G8R8A8_UNORM,
		DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);
	DXGI_SWAP_CHAIN_DESC1 desc;
	swapChain->GetDesc1(&desc);
	CreateRenderTargetView();
	CreateDepthStencilView();
	CreateBackBufferBitmap();
	d3dImmediateContext->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());

	auto* sceneManager = GameOperation::SceneManager::GetInstance();
	sceneManager->UpdateAllCamera();
	
	Render();
}

auto GetD3DDeviceContext() noexcept -> CComPtrRef<ID3D11DeviceContext> {
	
	return d3dImmediateContext;
}

auto GetD3DDevice() noexcept -> CComPtrRef<ID3D11Device> {

	return d3dDevice;
}

void SetDefaultRenderTarget() noexcept {
	
	d3dImmediateContext->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());
}

auto GetD2DDevice() noexcept -> CComPtrRef<ID2D1Device> {

	return d2dDevice;
}

auto GetD2DeviceContext() noexcept -> CComPtrRef<ID2D1DeviceContext> {

	return d2dContext;
}

auto GetDwFactory() noexcept -> CComPtrRef<IDWriteFactory> {

	return dwFactory;
}

void PreInitDirect3D() noexcept(false) {

	//下位互換API
	std::vector features{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0
	};
	D3D_FEATURE_LEVEL level;

	//フラグ
	UINT d3dFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;	//DirectX11でのDirect2Dの使用を許可
#ifdef _DEBUG
	d3dFlags |= D3D11_CREATE_DEVICE_DEBUG;	//デバッグ時に開放漏れを知らせる
#endif

	//Direct3Dの作成
	auto hr = D3D11CreateDevice(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE, nullptr,
		d3dFlags,
		features.data(), static_cast<UINT>(features.size()),
		D3D11_SDK_VERSION,
		&d3dDevice,
		&level,
		&d3dImmediateContext);
	if (FAILED(hr))	throw DirectXException("D3D11CreateDevice()");

#ifdef _DEBUG
	
	hr = d3dDevice->QueryInterface(IID_PPV_ARGS(&d3dDebug));
	if (FAILED(hr))	throw DirectXException("ID3D11Device::QueryInterface()");

	ComPtr<ID3D11InfoQueue> infoQueue;
	hr = d3dDevice->QueryInterface(IID_PPV_ARGS(&infoQueue));
	if (FAILED(hr))	throw DirectXException("ID3D11Device::QueryInterface()");

	vector messages{
		D3D11_MESSAGE_ID_CREATEINPUTLAYOUT_TYPE_MISMATCH,	//頂点インプットレイアウトの不適切な型を許可
		D3D11_MESSAGE_ID_DEVICE_DRAW_SAMPLER_NOT_SET,		//サンプラがセットされていないのを許可
		D3D11_MESSAGE_ID_DEVICE_DRAW_CONSTANT_BUFFER_TOO_SMALL
	};
	D3D11_INFO_QUEUE_FILTER filter{};
	filter.DenyList.NumIDs = static_cast<UINT>(messages.size());
	filter.DenyList.pIDList = messages.data();

	infoQueue->AddStorageFilterEntries(&filter);
	infoQueue.Reset();
	
#endif
}

void InitDirectXGraphics() noexcept(false) {

	//スワップチェインの設定
	DXGI_SWAP_CHAIN_DESC1 scDesc;
	::ZeroMemory(&scDesc, sizeof(scDesc));
	//画面設定
	scDesc.Width = Window::GetWindowWidth();
	scDesc.Height = Window::GetWindowHeight();
	scDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	scDesc.Scaling = DXGI_SCALING_STRETCH;					//スケーリング方法
	scDesc.Stereo = 0;										//立体視
	scDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;			//半透明モード
	scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	//使用方法
	scDesc.BufferCount = 1;									//バックバッファの数
	scDesc.SwapEffect = DXGI_SWAP_EFFECT_SEQUENTIAL;		//描画先の扱い
	//アンチエイリアスの設定
	scDesc.SampleDesc.Count = 1;
	scDesc.SampleDesc.Quality = 0;
	scDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;	//解像度変更の有効

	//DXGIデバイスの作成
	auto hr = d3dDevice->QueryInterface<IDXGIDevice1>(&dxgiDevice);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::QueryInterface()");

	// キューに格納されていく描画コマンドをスワップ時に全てフラッシュする
	dxgiDevice->SetMaximumFrameLatency(1);

	//GPUの取得
	ComPtr<IDXGIAdapter> adapter;
	hr = dxgiDevice->GetAdapter(&adapter);
	if (FAILED(hr))	throw DirectXException("IDXGIAdapter::GetAdapter()");

	//DXGIのファクトリの作成
	ComPtr<IDXGIFactory2> dxgiFactory;
	hr = adapter->GetParent(IID_PPV_ARGS(&dxgiFactory));
	adapter.Reset();
	if (FAILED(hr))	throw DirectXException("IDXGIAdapter::GetParent()");

	//スワップチェインをHWNDから作成
	hr = dxgiFactory->CreateSwapChainForHwnd(
		d3dDevice.Get(),
		Window::GetWindowHandle(),
		&scDesc,
		nullptr,
		nullptr,
		&swapChain);
	dxgiFactory.Reset();
	if (FAILED(hr))	throw DirectXException("IDXGIFactory2::CreateSwapChainForHwnd()");
}

void InitDirect3D() noexcept(false) {
	
	//レンダーターゲットの取得(D3D11)
	auto hr = swapChain->GetBuffer(0, IID_PPV_ARGS(&backBuffer));
	if (FAILED(hr))	throw DirectXException("IDXGISwapChain1::GetBuffer()");

	//レンダーターゲットビューの作成
	hr = d3dDevice->CreateRenderTargetView(backBuffer.Get(), nullptr, &renderTargetView);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateRenderTargetView()");
	//CreateRenderTargetView();
}

void InitDirect2D() noexcept(false) {

	//Direct2Dのファクトリの作成
	D2D1_FACTORY_OPTIONS d2dOption;
	::ZeroMemory(&d2dOption, sizeof(d2dOption));
	auto hr = D2D1CreateFactory(
		D2D1_FACTORY_TYPE_SINGLE_THREADED,
		__uuidof(ID2D1Factory1),
		&d2dOption,
		&d2dFactory);
	if (FAILED(hr))	throw DirectXException("D2D1CreateFactory()");

	//Direct2Dデバイスの作成
	hr = d2dFactory->CreateDevice(dxgiDevice.Get(), &d2dDevice);
	if (FAILED(hr))	throw DirectXException("ID2DFactory::CreateDevice()");

	//Direct2Dデバイスコンテキストの作成
	hr = d2dDevice->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_NONE, &d2dContext);
	
	if (FAILED(hr))	throw DirectXException("ID2D1Device::CreateDeviceContext()");

	CreateBackBufferBitmap();
}

void InitDirectWrite() noexcept(false) {

	auto hr = DWriteCreateFactory(
		DWRITE_FACTORY_TYPE_SHARED, 
		__uuidof(IDWriteFactory),
		&dwFactory);
	if (FAILED(hr))	throw DirectXException("DWriteCreateFactory()");
}

void PostInitDirect3D() noexcept(false) {

	CreateDepthStencilView();

	//共通パイプラインの構築
	d3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	d3dImmediateContext->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());
}

void CreateRenderTargetView() noexcept(false) {
	
	//レンダーターゲットの取得(D3D11)
	auto hr = swapChain->GetBuffer(0, IID_PPV_ARGS(&backBuffer));
	if (FAILED(hr))	throw DirectXException("IDXGISwapChain1::GetBuffer()");

	//レンダーターゲットビューの作成
	hr = d3dDevice->CreateRenderTargetView(backBuffer.Get(), nullptr, &renderTargetView);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateRenderTargetView()");
}

void CreateDepthStencilView() noexcept(false) {

	depthShaderResourceView.Reset();
	depthStencilView.Reset();
	depthStencil.Reset();
	
	//深度ステンシルビューの作成
	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = Window::GetWindowWidth();
	descDepth.Height = Window::GetWindowHeight();
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_R24G8_TYPELESS;	//シェーダーリソースに使用可能な形式
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;

	auto hr = d3dDevice->CreateTexture2D(&descDepth, nullptr, &depthStencil);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateTexture2D");

	
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc{};
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	
	hr = d3dDevice->CreateDepthStencilView(depthStencil.Get(), &depthStencilViewDesc, &depthStencilView);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateDepthStencilView");

	
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc{};
	shaderResourceViewDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;
	
	hr = d3dDevice->CreateShaderResourceView(depthStencil.Get(), &shaderResourceViewDesc, &depthShaderResourceView);
}

void CreateBackBufferBitmap() noexcept(false) {
	
	//DPIの取得
	//vs2017まで
	//FLOAT dpiX, dpiY;
	//d2dFactory->GetDesktopDpi(&dpiX, &dpiY);
	auto dpi = static_cast<FLOAT>(GetDpiForWindow(Window::GetWindowHandle()));
	
	//レンダーターゲットの取得（DXGI）
	ComPtr<IDXGISurface> surface;
	auto hr = swapChain->GetBuffer(0, IID_PPV_ARGS(&surface));
	if (FAILED(hr))	throw DirectXException("IDXGISwapChain1::GetBuffer()");
	
	//Direct2Dの描画先となるビットマップを作成
	auto d2dProp = D2D1::BitmapProperties1(
		D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
		D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED),
		dpi,
		dpi);

	ComPtr<ID2D1Bitmap1> targetBitmap;
	hr = d2dContext->CreateBitmapFromDxgiSurface(surface.Get(), &d2dProp, &targetBitmap);
	surface.Reset();
	if (FAILED(hr)) throw DirectXException("ID2D1DeviceContext::CreateBitmapFromDxgiSurface()");
	
	// 描画するDirect2Dビットマップの設定
	d2dContext->SetTarget(nullptr);
	d2dContext->SetTarget(targetBitmap.Get());
}

void Update() noexcept(false) {

	Input::PreProcess();

	//Time::PrintDebugData();
	
	auto* sceneManager = GameOperation::SceneManager::GetInstance();
	sceneManager->Update();
}


void Render() noexcept(false) {

	d3dImmediateContext->ClearRenderTargetView(renderTargetView.Get(), clearColor);
	d3dImmediateContext->ClearDepthStencilView(depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.f, 0);

	auto* sceneManager = GameOperation::SceneManager::GetInstance();
	sceneManager->Render();

	auto hr = swapChain->Present(0, 0);
	if (FAILED(hr))	throw DirectXException("IDXGISwapChain1::Present");
}
	
}
