#include "PipelineBundle.hpp"

#include <unordered_map>
#include <utility>
#include <d3dcompiler.h>
#include <wrl/client.h>


#include "DirectX11.hpp"
#include "DirectXException.hpp"

#pragma comment(lib, "d3dcompiler.lib")

using Microsoft::WRL::ComPtr;
using Whono::DirectX11::DirectXException;

using namespace std;


namespace Whono::GraphicsPipeline {

/**
 * \brief シェーダの各ステージ
 */
enum class STAGE : char {
	VS, HS, DS, GS, PS, /*CS,*/
};
 
//各ステージの名前、バージョン
const unordered_map<STAGE, pair<string, string>> STATE_NAME_VERSION = { {
	{ STAGE::VS, { "VS", "vs_5_0" } },
	{ STAGE::HS, { "HS", "hs_5_0" } },
	{ STAGE::DS, { "DS", "ds_5_0" } },
	{ STAGE::GS, { "GS", "gs_5_0" } },
	{ STAGE::PS, { "PS", "ps_5_0" } },
} };

//内部実装隠蔽
struct PipelineBundle::Impl {

	std::wstring srcFile;								//読み込むhlslファイル
	ComPtr<ID3D11InputLayout>		inputLayout;		//GPUへのデータの送り方の指定
	ComPtr<ID3D11VertexShader>		vertexShader;		//頂点の変形
	ComPtr<ID3D11HullShader>		hullShader;			//プリミティブの分割方法の決定
	ComPtr<ID3D11DomainShader>		domainShader;		//プリミティブの分割
	ComPtr<ID3D11GeometryShader>	geometryShader;		//プリミティブを増減
	ComPtr<ID3D11RasterizerState>	rasterizerState;	//書き出すピクセルの決定
	ComPtr<ID3D11PixelShader>		pixelShader;		//ピクセルの色を決定
	ComPtr<ID3D11DepthStencilState> depthStencilState;	//深度バッファへの書き込み方の指定
	ComPtr<ID3D11BlendState>		blendState;			//色の合成方法の指定
};

PipelineBundle::PipelineBundle(const std::filesystem::path& srcFile) {

	pImpl_ = make_unique<Impl>();
	pImpl_->srcFile = srcFile;
};

PipelineBundle::~PipelineBundle() = default;

void PipelineBundle::SetPipeline() const {

	const auto& d3dContext = DirectX11::GetD3DDeviceContext();
	//各種ステージをコンテキストへセット
	d3dContext->IASetInputLayout(pImpl_->inputLayout.Get());
	d3dContext->VSSetShader(pImpl_->vertexShader.Get(), nullptr, 0);
	d3dContext->HSSetShader(pImpl_->hullShader.Get(), nullptr, 0);
	d3dContext->DSSetShader(pImpl_->domainShader.Get(), nullptr, 0);
	d3dContext->GSSetShader(pImpl_->geometryShader.Get(), nullptr, 0);
	d3dContext->RSSetState(pImpl_->rasterizerState.Get());
	d3dContext->PSSetShader(pImpl_->pixelShader.Get(), nullptr, 0);
	d3dContext->OMSetDepthStencilState(pImpl_->depthStencilState.Get(), 0);
	d3dContext->OMSetBlendState(pImpl_->blendState.Get(), nullptr, ULONG_MAX);
}

void PipelineBundle::CreateVertexShader(D3D11_INPUT_ELEMENT_DESC* inputLayouts, size_t size) const {

	HRESULT hr;
	ComPtr<ID3DBlob> blob;
	ComPtr<ID3DBlob> errorMessage;

	//行列を列優先、古い形式の記述を拒否
	UINT flag1 = D3D10_SHADER_PACK_MATRIX_COLUMN_MAJOR | D3D10_SHADER_ENABLE_STRICTNESS;

	//最適化レベルの設定
#ifdef _DEBUG
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL0;
#else
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL3;
#endif
	auto&[entryPoint, version] = STATE_NAME_VERSION.at(STAGE::VS);

	//コンパイル
	hr = D3DCompileFromFile(pImpl_->srcFile.c_str(), nullptr, nullptr, entryPoint.c_str(), version.c_str(), flag1, 0, &blob, &errorMessage);
	if (FAILED(hr))	throw DirectXException(static_cast<char*>(errorMessage->GetBufferPointer()));

	const auto& d3dDevice = DirectX11::GetD3DDevice();

	//生成
	hr = d3dDevice->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &pImpl_->vertexShader);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateVertexShader()");

	hr = d3dDevice->CreateInputLayout(inputLayouts, static_cast<UINT>(size), blob->GetBufferPointer(), blob->GetBufferSize(), &pImpl_->inputLayout);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateInputLayout()");
}

void PipelineBundle::CreateHullShader() const {

	HRESULT hr;
	ComPtr<ID3DBlob> blob;
	ComPtr<ID3DBlob> errorMessage;

	//行列を列優先、古い形式の記述を拒否
	UINT flag1 = D3D10_SHADER_PACK_MATRIX_COLUMN_MAJOR | D3D10_SHADER_ENABLE_STRICTNESS;

	//最適化レベルの設定
#ifdef _DEBUG
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL0;
#else
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL3;
#endif
	auto&[entryPoint, version] = STATE_NAME_VERSION.at(STAGE::HS);

	//コンパイル
	hr = D3DCompileFromFile(pImpl_->srcFile.c_str(), nullptr, nullptr, entryPoint.c_str(), version.c_str(), flag1, 0, &blob, &errorMessage);
	if (FAILED(hr))	throw DirectXException(static_cast<char*>(errorMessage->GetBufferPointer()));

	const auto& d3dDevice = DirectX11::GetD3DDevice();

	//生成
	hr = d3dDevice->CreateHullShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &pImpl_->hullShader);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateHullShader()");
}
	
void PipelineBundle::CreateDomainShader() const {

	HRESULT hr;
	ComPtr<ID3DBlob> blob;
	ComPtr<ID3DBlob> errorMessage;

	//行列を列優先、古い形式の記述を拒否
	UINT flag1 = D3D10_SHADER_PACK_MATRIX_COLUMN_MAJOR | D3D10_SHADER_ENABLE_STRICTNESS;

	//最適化レベルの設定
#ifdef _DEBUG
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL0;
#else
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL3;
#endif
	auto&[entryPoint, version] = STATE_NAME_VERSION.at(STAGE::DS);

	//コンパイル
	hr = D3DCompileFromFile(pImpl_->srcFile.c_str(), nullptr, nullptr, entryPoint.c_str(), version.c_str(), flag1, 0, &blob, &errorMessage);
	if (FAILED(hr))	throw DirectXException(static_cast<char*>(errorMessage->GetBufferPointer()));

	const auto& d3dDevice = DirectX11::GetD3DDevice();
	//生成
	hr = d3dDevice->CreateDomainShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &pImpl_->domainShader);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateDomainShader()");
}

void PipelineBundle::CreateGeometryShader() const {

	HRESULT hr;
	ComPtr<ID3DBlob> blob;
	ComPtr<ID3DBlob> errorMessage;

	//行列を列優先、古い形式の記述を拒否
	UINT flag1 = D3D10_SHADER_PACK_MATRIX_COLUMN_MAJOR | D3D10_SHADER_ENABLE_STRICTNESS;

	//最適化レベルの設定
#ifdef _DEBUG
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL0;
#else
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL3;
#endif
	auto&[entryPoint, version] = STATE_NAME_VERSION.at(STAGE::GS);

	//コンパイル
	hr = D3DCompileFromFile(pImpl_->srcFile.c_str(), nullptr, nullptr, entryPoint.c_str(), version.c_str(), flag1, 0, &blob, &errorMessage);
	if (FAILED(hr))	throw DirectXException(static_cast<char*>(errorMessage->GetBufferPointer()));

	const auto& d3dDevice = DirectX11::GetD3DDevice();
	//生成
	hr = d3dDevice->CreateGeometryShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &pImpl_->geometryShader);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateGeometryShader()");
}

void PipelineBundle::CreateRasterizerState(D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode) const {

	D3D11_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(rasterizerDesc));

	rasterizerDesc.FillMode = fillMode;
	rasterizerDesc.CullMode = cullMode;
	rasterizerDesc.FrontCounterClockwise = TRUE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;

	const auto& d3dDevice = DirectX11::GetD3DDevice();
	HRESULT hr = d3dDevice->CreateRasterizerState(&rasterizerDesc, &pImpl_->rasterizerState);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateRasterizerState()");
}

void PipelineBundle::CreatePixelShader() const {

	HRESULT hr;
	ComPtr<ID3DBlob> blob;
	ComPtr<ID3DBlob> errorMessage;

	//行列を列優先、古い形式の記述を拒否
	UINT flag1 = D3D10_SHADER_PACK_MATRIX_COLUMN_MAJOR | D3D10_SHADER_ENABLE_STRICTNESS;

	//最適化レベルの設定
#ifdef _DEBUG
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL0;
#else
	flag1 |= D3D10_SHADER_OPTIMIZATION_LEVEL3;
#endif
	auto&[entryPoint, version] = STATE_NAME_VERSION.at(STAGE::PS);

	//コンパイル
	hr = D3DCompileFromFile(pImpl_->srcFile.c_str(), nullptr, nullptr, entryPoint.c_str(), version.c_str(), flag1, 0, &blob, &errorMessage);
	if (FAILED(hr))	throw DirectXException(static_cast<char*>(errorMessage->GetBufferPointer()));

	const auto& d3dDevice = DirectX11::GetD3DDevice();
	//生成
	hr = d3dDevice->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), nullptr, &pImpl_->pixelShader);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreatePixelShader()");
}

void PipelineBundle::CreateDepthStencilState(bool depthEnable, D3D11_DEPTH_WRITE_MASK depthWriteEnabled, D3D11_COMPARISON_FUNC depthFunc) const {

	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.DepthEnable = depthEnable;
	depthStencilDesc.DepthWriteMask = depthWriteEnabled;
	depthStencilDesc.DepthFunc = depthFunc;
	depthStencilDesc.StencilEnable = FALSE;

	const auto& d3dDevice = DirectX11::GetD3DDevice();
	HRESULT hr = d3dDevice->CreateDepthStencilState(&depthStencilDesc, &pImpl_->depthStencilState);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateDepthStencilState()");
}

void PipelineBundle::CreateBlendState(BLEND_STATE blendStateType, bool alphaToConvergeEnable) const {

	D3D11_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(blendDesc));

	blendDesc.AlphaToCoverageEnable = alphaToConvergeEnable;
	blendDesc.IndependentBlendEnable = FALSE;

	switch (blendStateType) {

	case BLEND_STATE::NONE:			//合成を行わない
		blendDesc.RenderTarget[0].BlendEnable = FALSE;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		break;

	case BLEND_STATE::ADD:			//加算合成
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		break;

	case BLEND_STATE::ALIGNMENT:	//線形合成
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		break;

	case BLEND_STATE::SUB:			//減算合成
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_REV_SUBTRACT;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		break;

	case BLEND_STATE::MUL:			//積算合成
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_SRC_COLOR;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		break;

	case BLEND_STATE::DEPTH_TEST:	//深度テスト
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_MIN;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MIN;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		break;
	}

	const auto& d3dDevice = DirectX11::GetD3DDevice();
	HRESULT hr = d3dDevice->CreateBlendState(&blendDesc, &pImpl_->blendState);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateBlendState()");
}

void PipelineBundle::Release() const {

	pImpl_->inputLayout.Reset();
	pImpl_->vertexShader.Reset();
	pImpl_->hullShader.Reset();
	pImpl_->domainShader.Reset();
	pImpl_->geometryShader.Reset();
	pImpl_->rasterizerState.Reset();
	pImpl_->pixelShader.Reset();
	pImpl_->depthStencilState.Reset();
	pImpl_->blendState.Reset();
}

}
