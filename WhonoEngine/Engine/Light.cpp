#include "Light.hpp"

#include <DirectXColors.h>


using namespace std;
using namespace DirectX;


namespace Whono::GameOperation {


Light::Light(LIGHT_TYPE type)
	: type_(type)
	, color_(Colors::White)
	, power_(-1) {}

void Light::SetColor(const XMVECTORF32& color) {

	color_ = color;
}

auto Light::GetType() const noexcept -> LIGHT_TYPE {

	return type_;
}

auto Light::GetColor() const noexcept ->  const XMVECTORF32& {

	return color_;
}

auto Light::GetPower() const noexcept -> float {

	return power_;
}
}
