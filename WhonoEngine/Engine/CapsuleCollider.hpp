#pragma once
#include "ICollider.hpp"

#include <memory>


namespace Whono::GameOperation {


/**
 * \brief カプセル（球の直線の軌跡）の衝突判定を検証するためのクラス
 */
class CapsuleCollider final : public ICollider {

public:
	/**
	 * \brief コンストラクタ
	 * \param position 中心座標
	 * \param radius 半径
	 * \param height 高さ
	 * \param oriented 姿勢
	 */
	CapsuleCollider(DirectX::CXMVECTOR position, float radius, float height, DirectX::CXMVECTOR oriented);
	/**
	 * \brief デストラクタ
	 */
	~CapsuleCollider();

	/**
	 * \brief 中心座標を取得
	 * \return 中心座標
	 */
	[[nodiscard]]
	auto GetCenterPosition() const -> DirectX::XMVECTOR override;

	/**
	 * \brief 受け取った座標から領域内でもっとも近いであろう点を返す
	 * \param point 比較対象
	 * \return 領域内の座標
	 */
	[[nodiscard]]
	auto GetNearestControlPoint(DirectX::CXMVECTOR point) const -> DirectX::XMVECTOR override;

	/**
	 * \brief 受け取ったベクトルに射影した長さの最大値を取得
	 * \param vector 射影するベクトル
	 * \return 射影した長さの最大値
	 */
	[[nodiscard]]
	auto GetProjectionLengthMax(DirectX::CXMVECTOR vector) const -> float override;
	
	/**
	 * \brief 所持させるゲームオブジェクトを設定
	 * \param gameObject ゲームオブジェクト
	 */
	void SetGameObject(GameObject* gameObject) override;
	/**
	 * \brief 境界線を描画する
	 */
	void Render() const override;
	/**
	 * \brief 衝突判定を行う
	 * \param target 検証相手
	 * \return 当たったら真
	 */
	[[nodiscard]]
	auto IsHit(ICollider* target) const -> bool override;

	/**
	 * \brief レイキャストを行う
	 * \param origin 発射地点
	 * \param direction 方向
	 * \param[out] dist 当たった時の距離
	 * \param[out] normal 当たったポリゴンの法線
	 * \return 当たったなら真
	 */
	auto RayCast(DirectX::CXMVECTOR origin, DirectX::CXMVECTOR direction, float* dist, DirectX::XMVECTOR* normal) const -> bool override;

	/**
	 * \brief 中心座標の軌跡の線分を取得
	 * \param[out] segment1 線分の端点１
	 * \param[out] segment2 線分の端点２
	 */
	void GetSegment(DirectX::XMVECTOR* segment1, DirectX::XMVECTOR* segment2) const;
	/**
	 * \brief 半径を取得する
	 * \return 半径
	 */
	[[nodiscard]]
	auto GetRadius() const -> float;

	/**
	 * \brief インスタンスを作成する
	 * \param position = g_XMZero 中心座標
	 * \param radius = 1.f 半径
	 * \param height = 1.f 高さ
	 * \param oriented = g_XMIdentityR3.v 姿勢
	 * \return インスタンス
	 */
	[[nodiscard]]
	static auto Create(DirectX::CXMVECTOR position = DirectX::g_XMZero, float radius = 1.f, float height = 1.f, DirectX::CXMVECTOR oriented = DirectX::g_XMIdentityR3.v) -> std::unique_ptr<CapsuleCollider>;
	/**
	 * \brief インスタンスを生成する
	 * \param x 中心X座標
	 * \param y 中心Y座標
	 * \param z 中心Z座標
	 * \param radius = 1.f 半径
	 * \param height = 1.f 高さ
	 * \param oriented = g_XMIdentityR3.v 姿勢
	 * \return インスタンス
	 */
	[[nodiscard]]
	static auto Create(float x, float y, float z, float radius = 1.f, float height = 1.f, DirectX::CXMVECTOR oriented = DirectX::g_XMIdentityR3.v) -> std::unique_ptr<CapsuleCollider>;
	/**
	 * \brief インスタンスを生成する
	 * \param position 中心座標
	 * \param radius 半径
	 * \param height 高さ
	 * \param axis 姿勢の回転軸
	 * \param angle 回転量（ラジアン）
	 * \return インスタンス
	 */
	[[nodiscard]]
	static auto Create(DirectX::CXMVECTOR position, float radius, float height, DirectX::CXMVECTOR axis, float angle) -> std::unique_ptr<CapsuleCollider>;
	/**
	 * \brief インスタンスを生成する
	 * \param x 中心X座標
	 * \param y 中心Y座標
	 * \param z 中心Z座標
	 * \param radius 半径
	 * \param height 高さ
	 * \param axis 姿勢の回転軸
	 * \param angle 回転量（ラジアン）
	 * \return インスタンス
	 */
	[[nodiscard]]
	static auto Create(float x, float y, float z, float radius, float height, DirectX::CXMVECTOR axis, float angle) -> std::unique_ptr<CapsuleCollider>;
	/**
	 * \brief インスタンスを生成する
	 * \param position 中心座標
	 * \param radius 半径
	 * \param height 高さ
	 * \param rotateX X軸の回転量（ラジアン）
	 * \param rotateY Y軸の回転量（ラジアン）
	 * \param rotateZ Z軸の回転量（ラジアン）
	 * \return インスタンス
	 */
	[[nodiscard]]
	static auto Create(DirectX::CXMVECTOR position, float radius, float height, float rotateX, float rotateY, float rotateZ) -> std::unique_ptr<CapsuleCollider>;
	/**
	 * \brief インスタンスを生成する
	 * \param x 中心X座標
	 * \param y 中心Y座標
	 * \param z 中心Z座標
	 * \param radius 半径
	 * \param height 高さ
	 * \param rotateX X軸の回転量（ラジアン）
	 * \param rotateY Y軸の回転量（ラジアン）
	 * \param rotateZ Z軸の回転量（ラジアン）
	 * \return インスタンス
	 */
	[[nodiscard]]
	static auto Create(float x, float y, float z, float radius, float height, float rotateX, float rotateY, float rotateZ) -> std::unique_ptr<CapsuleCollider>;

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽
};

}