#pragma once
#include <type_traits>

#include "SphereCollider.hpp"
#include "BoxCollider.hpp"
#include "ObbCollider.hpp"
#include "FrustumCollider.hpp"
#include "CapsuleCollider.hpp"


namespace Whono::GameOperation {


/**
 * \brief 衝突検知の計算用
 */
namespace CollisionCalculator {


/**
 * \brief DirectXで用意されている衝突判定の検証を行う
 * \tparam T DirectXの当たり判定が用意されているIColliderを継承した衝突判定クラス
 * \tparam U DirectXの当たり判定が用意されているIColliderを継承した衝突判定クラス
 * \tparam value IColliderを継承しているかどうか
 * \param collider1 コリジョン１
 * \param collider2 コリジョン２
 * \return 衝突したなら真
 */
template<class T, class U, bool value = std::is_base_of<ICollider, T>::value && std::is_base_of<ICollider, U>::value>
[[nodiscard]]
auto IsHit(const T* collider1, const U* collider2) -> bool {

	if (!value)	return false;
	
	auto&& bounding1 = collider1->GetBounding();
	auto&& bounding2 = collider2->GetBounding();

	return bounding1.Intersects(bounding2);
}

/**
 * \brief カプセルとスフィアの衝突判定を行う
 * \param capsule カプセルコライダー
 * \param sphere スフィアコライダー
 * \return 衝突したなら真
 */
[[nodiscard]]
auto IsHit(const CapsuleCollider* capsule, const SphereCollider* sphere) -> bool;
/**
 * \brief スフィアとカプセルの衝突判定を行う
 * \param sphere スフィアコライダー
 * \param capsule カプセルコライダー
 * \return 衝突したなら真
 */
[[nodiscard]]
auto IsHit(const SphereCollider* sphere, const CapsuleCollider* capsule) -> bool;
/**
 * \brief カプセルとボックスの衝突判定を行う
 * \param capsule カプセル
 * \param box ボックス
 * \return 衝突したなら真
 */
[[nodiscard]]
auto IsHit(const CapsuleCollider* capsule, const BoxCollider* box) -> bool;
/**
 * \brief ボックスとカプセルの衝突判定を行う
 * \param box ボックス
 * \param capsule カプセル
 * \return 衝突したなら真
 */
[[nodiscard]]
auto IsHit(const BoxCollider* box, const CapsuleCollider* capsule) -> bool;
/**
 * \brief カプセルとOBBの衝突判定を行う
 * \param capsule カプセル
 * \param obb OBB
 * \return 衝突したなら真
 */
[[nodiscard]]
auto IsHit(const CapsuleCollider* capsule, const ObbCollider* obb) -> bool;
/**
 * \brief OBBとカプセルの衝突判定を行う
 * \param obb OBB
 * \param capsule カプセル
 * \return 衝突したなら真
 */
[[nodiscard]]
auto IsHit(const ObbCollider* obb, const CapsuleCollider* capsule) -> bool;
/**
 * \brief カプセルとフラスタムの衝突判定を行う
 * \param capsule カプセル
 * \param frustum 錐台
 * \return 衝突したなら真
 */
[[nodiscard]]
auto IsHit(const CapsuleCollider* capsule, const FrustumCollider* frustum) -> bool;
/**
 * \brief フラスタムとカプセルの衝突判定を行う
 * \param frustum フラスタム
 * \param capsule カプセル
 * \return 衝突したなら真
 */
[[nodiscard]]
auto IsHit(const FrustumCollider* frustum, const CapsuleCollider* capsule) -> bool;
/**
 * \brief カプセル同士の衝突判定を行う
 * \param capsule1 カプセル１
 * \param capsule2 カプセル２
 * \return 衝突したなら真
 */
auto IsHit(const CapsuleCollider* capsule1, const CapsuleCollider* capsule2) -> bool;

}

}
