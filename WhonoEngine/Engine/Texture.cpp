#include "Texture.hpp"

#include <wincodec.h>

#pragma comment(lib, "WindowsCodecs.lib")

#include "DirectX11.hpp"
#include "DirectXException.hpp"
#include "WinException.hpp"

using namespace DirectX;
 
using std::filesystem::path;
using Whono::DirectX11::DirectXException;
using Whono::Window::WinException;


namespace Whono::RenderObject {


/**
 * \brief 内部実装隠蔽
 */
struct Texture::Impl {

	/**
	 * \brief ファイルパスからシェーダリソースビューを作成する
	 * \param filePath ファイルパス
	 * \throw WinException
	 * \throw DirectXException
	 */
	void CreateShaderResourceView(const path& filePath) noexcept(false);
	
	/**
	 * \brief サンプラステートを作成
	 * \param filter フィルタ
	 * \param u U座標
	 * \param v V座標
	 * \param w W座標
	 * \throw DirectXException
	 */
	void CreateSamplerState(
		D3D11_FILTER filter = D3D11_FILTER_MIN_MAG_MIP_POINT,
		D3D11_TEXTURE_ADDRESS_MODE u = D3D11_TEXTURE_ADDRESS_WRAP,
		D3D11_TEXTURE_ADDRESS_MODE v = D3D11_TEXTURE_ADDRESS_WRAP,
		D3D11_TEXTURE_ADDRESS_MODE w = D3D11_TEXTURE_ADDRESS_WRAP) noexcept(false);

	ComPtr<ID3D11ShaderResourceView>	shaderResourceView;	//シェーダリソースビュー
	ComPtr<ID3D11SamplerState>			samplerState;		//テクスチャの取得方法
	ComPtr<ID2D1Bitmap1>				d2d1Bitmap;			//ビットマップ
	XMUINT2								size;				//画像サイズ
};


Texture::Texture() {

	pImpl_ = std::make_unique<Impl>();
}

Texture::~Texture() = default;

void Texture::Load(const path& filePath) const noexcept(false) {

	pImpl_->CreateShaderResourceView(filePath);
	pImpl_->CreateSamplerState();
}

auto Texture::GetShaderResourceView() const noexcept -> CComPtrRef<ID3D11ShaderResourceView> {
	
	return pImpl_->shaderResourceView;
}

auto Texture::GetSamplerState() const noexcept -> CComPtrRef<ID3D11SamplerState> {

	return pImpl_->samplerState;
}

auto Texture::GetD2DBitmap() const noexcept -> CComPtrRef<ID2D1Bitmap1> {

	return pImpl_->d2d1Bitmap;
}

auto Texture::GetSize() const noexcept -> const XMUINT2& {
	
	return pImpl_->size;
}

auto Texture::GetSizeVector() const noexcept -> XMVECTOR {

	return XMLoadUInt2(&pImpl_->size);
}

auto Texture::GetAspectRatio() const noexcept -> float {

	return static_cast<float>(pImpl_->size.x) / pImpl_->size.y;
}

void Texture::CreateCustomSampler(D3D11_FILTER filter, D3D11_TEXTURE_ADDRESS_MODE u, D3D11_TEXTURE_ADDRESS_MODE v, D3D11_TEXTURE_ADDRESS_MODE w) const {

	pImpl_->CreateSamplerState(filter, u, v, w);
}

void Texture::CreateCustomSampler(D3D11_FILTER filter, D3D11_TEXTURE_ADDRESS_MODE all) const {

	CreateCustomSampler(filter, all, all, all);
}

void Texture::Release() const {
	
	pImpl_->shaderResourceView.Reset();
	pImpl_->samplerState.Reset();
}

void Texture::Impl::CreateShaderResourceView(const path& filePath) noexcept(false) {

	//画像を読み込むためにWICの必要なCOM群
	ComPtr<IWICImagingFactory>		wicFactory;			//デコーダの生成に必要
	ComPtr<IWICBitmapDecoder>		wicDecoder;			//ファイルからフレームの取得に必要
	ComPtr<IWICBitmapFrameDecode>	wicFrame;			//画像一枚を格納 サイズの取得に必要
	ComPtr<IWICFormatConverter>		wicFormatConverter;	//扱いやすいように形式を変換する

	
	//WICファクトリ作成
	auto hr = ::CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, &wicFactory);
	if (FAILED(hr))	throw WinException("CoCreateInstance()");
	
	//ファイルからデータを抽出
	hr = wicFactory->CreateDecoderFromFilename(filePath.c_str(), nullptr, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &wicDecoder);
	if (FAILED(hr)) {

		auto currentDirectory = std::filesystem::current_path();
		path tmp;
		for (auto element : currentDirectory) {

			tmp /= element;
			if (element == "Assets") {

				tmp /= "Debug/null_texture.png";
				break;
			}
		}
		hr = wicFactory->CreateDecoderFromFilename(tmp.c_str(), nullptr, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &wicDecoder);
		if (FAILED(hr))	throw WinException("IWICImagingFactory::CreateDecoderFromFilename()");
	}
	
	//指定フレームの取得(0指定でgif未対応)
	hr = wicDecoder->GetFrame(0, &wicFrame);
	if (FAILED(hr)) throw WinException("IWICBitmapDecoder::GetFrame()");

	//D2D用フォーマット作成
	hr = wicFactory->CreateFormatConverter(&wicFormatConverter);
	if (FAILED(hr))	throw WinException("IWICImagingFactory::CreateFormatConverter()");
	//フォーマットを初期化
	hr = wicFormatConverter->Initialize(wicFrame.Get(), GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, nullptr, 1.0f, WICBitmapPaletteTypeMedianCut);
	if (FAILED(hr)) throw WinException("IWICFormatConverter::Initialize()");
	//d2d用ビットマップ作成
	hr = DirectX11::GetD2DeviceContext()->CreateBitmapFromWicBitmap(wicFormatConverter.Get(), &d2d1Bitmap);
	if (FAILED(hr))	throw DirectXException("ID2D1DeviceContext::CreateBitmapFromWicBitmap()");

	wicFormatConverter.Reset();
	//D3D用フォーマット作成
	hr = wicFactory->CreateFormatConverter(&wicFormatConverter);
	if (FAILED(hr))	throw WinException("IWICImagingFactory::CreateFormatConverter()");
	//フォーマットを初期化
	hr = wicFormatConverter->Initialize(wicFrame.Get(), GUID_WICPixelFormat32bppRGBA, WICBitmapDitherTypeNone, nullptr, 1.0f, WICBitmapPaletteTypeMedianCut);
	if (FAILED(hr)) throw WinException("IWICFormatConverter::Initialize()");

	//フォーマットから画像サイズを取得
	hr = wicFormatConverter->GetSize(&size.x, &size.y);
	if (FAILED(hr)) throw WinException("IWICFormatConverter::GetSize()");

	
	//書き込み先の設定
	D3D11_TEXTURE2D_DESC textureDesc;
	textureDesc.Width = size.x;
	textureDesc.Height = size.y;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DYNAMIC;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	textureDesc.MiscFlags = 0;

	const auto& d3dDevice = DirectX11::GetD3DDevice();

	//テクスチャ作成
	ComPtr<ID3D11Texture2D> texture;
	hr = d3dDevice->CreateTexture2D(&textureDesc, nullptr, &texture);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateTexture2D()");

	const auto& d3dContext = DirectX11::GetD3DDeviceContext();

	//コンテキストを使ってGPUで書き込み
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	hr = d3dContext->Map(texture.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(hr))	throw DirectXException("ID3D11DeviceContext::Map()");

	hr = wicFormatConverter->CopyPixels(nullptr, size.x * 4, size.x * size.y * 4, static_cast<BYTE*>(mappedResource.pData));

	d3dContext->Unmap(texture.Get(), 0);
	if (FAILED(hr)) throw WinException("IWICFormatConverter::CopyPixels()");

	
	//シェーダリソースビューの設定
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc{};
	shaderResourceViewDesc.Format = textureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	//シェーダリソースビューの作成
	hr = d3dDevice->CreateShaderResourceView(texture.Get(), &shaderResourceViewDesc, &shaderResourceView);
	if (FAILED(hr)) throw DirectXException("ID3D11Device::CreateShaderResourceView()");
}

void Texture::Impl::CreateSamplerState(D3D11_FILTER filter, D3D11_TEXTURE_ADDRESS_MODE u, D3D11_TEXTURE_ADDRESS_MODE v, D3D11_TEXTURE_ADDRESS_MODE w) noexcept(false) {

	//サンプラステート設定
	D3D11_SAMPLER_DESC  samplerDesc{};
	samplerDesc.Filter = filter;
	samplerDesc.AddressU = u;
	samplerDesc.AddressV = v;
	samplerDesc.AddressW = w;

	const auto& d3dDevice = DirectX11::GetD3DDevice();

	if (samplerState)	samplerState.Reset();
	
	//サンプラステートの作成
	auto hr = d3dDevice->CreateSamplerState(&samplerDesc, &samplerState);
	if (FAILED(hr))	throw DirectXException("ID3D11Device::CreateSamplerState()");
}

}