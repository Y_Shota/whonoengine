#include "SceneObject.hpp"

#include "Camera.hpp"
#include "GameObject.hpp"
#include "Light.hpp"
#include "LightPlain.hpp"
#include "ShadowManager.hpp"

using namespace std;

SceneObject::RENDERING_PHASE SceneObject::renderingPhase_ = RENDERING_PHASE::NORMAL;

SceneObject::SceneObject(const string& name)
	: name_(name)
	, isRenderShadow_(false) {}

SceneObject::~SceneObject() = default;

void SceneObject::InitializeAll() {

	//カメラ追加
	cameras_.emplace_back(Camera::Create());

	//ライト追加
	lights_.emplace_back(LightPlain::Create());

	//シャドウマネージャ作成
	shadowManager = make_unique<ShadowManager>();
	shadowManager->Initialize();
	
	GameObject::sceneInitialize = true;
	PreInitChild();
	InitializeChild();
	PostInitChild();
	GameObject::sceneInitialize = false;
}

void SceneObject::PreInit() {}
void SceneObject::PostInit() {}

void SceneObject::Update() {

	for (auto itr = childList_.begin(); itr != childList_.end();) {

		auto* child = *itr;
		if (child->IsAlive()) {

			child->Update();
			child->UpdateChild();
			for (auto* child1 : childList_) {

				child->CheckCollisionDetection(child1);
			}
			child->collidedColliderList_.clear();
			++itr;
			continue;
		}
		//死んでいる場合開放
		child->FinalizeChild();
		child->Finalize();
		itr = childList_.erase(itr);
		ReleaseObject(child);
	}
}

void SceneObject::Render() {

	//影の描画が有効ならシャドウマップ作成
	if (IsShadowRendering()) {

		RenderShadowMap();
	}
	
	for (auto& camera : cameras_) {

		Camera::SetCurrentCamera(camera.get());

		for (auto* child : childList_) {

			child->PreRender();
			child->PreRenderChild();
		}
		for (auto* child : childList_) {
			
			child->Render();
			child->RenderChild();
		}
		for (auto* child : childList_) {

			child->PostRender();
			child->PostRenderChild();
		}
	}
}

void SceneObject::Finalize() {

	for (auto* child : childList_) {

		child->Finalize();
		child->FinalizeChild();
	}
	managementGameObjectInstanceList_.clear();
	childList_.clear();
	cameras_.clear();
	lights_.clear();
	shadowManager->Finalize();
	shadowManager.reset();
}

auto SceneObject::AddCamera() -> Camera* {

	auto& camera = cameras_.emplace_back(Camera::Create());
	return camera.get();
}

auto SceneObject::GetCamera(unsigned index) const -> Camera* {

	if (auto size = cameras_.size(); index > size) return nullptr;
	return cameras_[index].get();
}

auto SceneObject::GetCameraCount() const -> unsigned {

	return static_cast<unsigned>(cameras_.size());
}

auto SceneObject::ResetCameras() -> Camera* {

	cameras_.clear();
	return AddCamera();
}

auto SceneObject::AddLight(std::unique_ptr<Light>&& light) -> Light* {

	return lights_.emplace_back(move(light)).get();
}

auto SceneObject::AddLight(std::unique_ptr<Light>& light) -> Light* {

	return AddLight(move(light));
}

auto SceneObject::GetLight(unsigned index) const -> Light* {

	if (auto size = lights_.size(); index > size) return nullptr;
	return lights_[index].get();
}

auto SceneObject::GetLightCount() const -> unsigned {

	return static_cast<unsigned>(lights_.size());
}

auto SceneObject::ResetLight() -> Light* {
	
	lights_.clear();
	return AddLight(LightPlain::Create());
}

auto SceneObject::GetSceneName() const noexcept -> const string& {

	return name_;
}

auto SceneObject::FindObject(const string& objectName) const noexcept -> GameObject* {

	auto& gameObjectList = managementGameObjectInstanceList_;
	auto result = ranges::find_if(gameObjectList, [&objectName](auto& a) { return a->GetName() == objectName; });

	if (result == gameObjectList.cend()) return nullptr;
	return (*result).get();
}

auto SceneObject::HasGameObject(const GameObject* gameObject) const -> bool {

	return childList_.cend() != find(childList_.cbegin(), childList_.cend(), gameObject);
}

auto SceneObject::IsManagingInstance(GameObject* gameObject) const -> bool {

	auto& gameObjectList = managementGameObjectInstanceList_;
	return gameObjectList.cend() != ranges::find_if(gameObjectList, [&gameObject](auto& a) { return a.get() == gameObject; });
}

void SceneObject::AddGameObjectInstance(unique_ptr<GameObject>& gameObject) {

	managementGameObjectInstanceList_.emplace_back(move(gameObject));
}

void SceneObject::AllowShadowRendering(bool b) {

	isRenderShadow_ = b;
}

auto SceneObject::IsShadowRendering() const noexcept -> bool {

	return isRenderShadow_;
}

auto SceneObject::GetRenderingPhase() noexcept -> RENDERING_PHASE {

	return renderingPhase_;
}

void SceneObject::UpdateAllCamera() {

	for (auto& camera : cameras_) {

		camera->UpdateViewport();
	}
}

auto SceneObject::AddChild(GameObject* child) -> bool {

	if (!child) return false;
	childList_.emplace_back(child);

	return true;
}

void SceneObject::RemoveChild(GameObject* child) {

	if (!child) return;
	if (childList_.empty()) return;

	childList_.remove(child);
}

void SceneObject::ReleaseObject(GameObject* gameObject) {

	if (!gameObject) return;
	if (managementGameObjectInstanceList_.empty()) return;

	managementGameObjectInstanceList_.remove_if([&gameObject](auto&& a) { return a.get() == gameObject; });
}

void SceneObject::PreInitChild() {

	PreInit();
	for (auto* child : childList_) {

		child->PreInit();
		child->Initialized();
		child->PreInitChild();
	}
}

void SceneObject::InitializeChild() {

	Initialize();
	for (auto* child : childList_) {

		child->Initialize();
		child->InitializeChild();
	}
}

void SceneObject::PostInitChild() {

	PostInit();
	for (auto* child : childList_) {

		child->PostInit();
		child->PostInitChild();
	}
}

void SceneObject::RenderShadowMap() {
	
	renderingPhase_ = RENDERING_PHASE::SHADOW;

	shadowManager->Begin();

	for (auto* child : childList_) {

		child->PreRender();
		child->PreRenderChild();
	}
	for (auto* child : childList_) {

		child->Render();
		child->RenderChild();
	}
	for (auto* child : childList_) {

		child->PostRender();
		child->PostRenderChild();
	}

	shadowManager->End();
	
	renderingPhase_ = RENDERING_PHASE::NORMAL;
}
