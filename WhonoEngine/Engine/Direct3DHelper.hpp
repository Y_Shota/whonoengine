#pragma once
#include <d3d11_1.h>
#include <vector>

#include "ComPtrDefinition.hpp"


namespace Whono::Direct3DHelper {

constexpr unsigned MIN_BUFFER_SIZE = 16u;


/**
 * \brief 頂点バッファーを作成する
 * \param data リソースの先頭アドレス
 * \param dataSize リソースのサイズ
 * \return 作成された頂点バッファ
 * \throw DirectX11Exception
 */
auto CreateVertexBuffer(void* data, UINT dataSize) noexcept(false) -> ComPtr<ID3D11Buffer>;

/**
* \brief インデックスバッファーを作成する
* \param data リソースの先頭アドレス
* \param dataSize リソースのサイズ
* \return 作成されたインデックスバッファ
* \throw DirectX11Exception
 */
auto CreateIndexBuffer(void* data, UINT dataSize) noexcept(false) -> ComPtr<ID3D11Buffer>; 

/**
 * \brief コンスタントバッファを作成する
 * \param dataSize リソースのサイズ
* \return 作成されたコンスタントバッファ
* \throw DirectX11Exception
 */
auto CreateConstantBuffer(UINT dataSize) noexcept(false) -> ComPtr<ID3D11Buffer>;

/**
 * \brief ComPtrのベクタ配列を生のポインタのベクタ配列に変換する
 * \tparam T 扱うCom
 * \param comVector 変換したいベクタ配列
 * \param[out] rawVector 変換された生のポインタベクタ配列
 */
template<class T>
void ConvertToRawPtrVector(const std::vector<ComPtr<T>>& comVector, std::vector<T*>* rawVector) noexcept {

	rawVector->clear();
	rawVector(comVector.size());
	for (auto i = 0u; i < comVector.size(); ++i) {

		(*rawVector)[i] = comVector.at(i).Get();
	}
}

}