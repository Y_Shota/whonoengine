#pragma once

#include <memory>
#include <string>
#include <variant>
#include <list>

#include "Camera.hpp"
#include "EnablerIfType.hpp"
#include "SceneObject.hpp"
#include "Transform.hpp"

//前方宣言
namespace Whono::GameOperation {
class ICollider;
};


namespace Whono::GameOperation {


/**
 * \brief ゲーム内のオブジェクト
 */
class GameObject {

public:
	/**
	 * \brief コンストラクタ
	 * \param name オブジェクト名
	 */
	explicit GameObject(const std::string& name);
	/**
	 * \brief デストラクタ
	 */
	virtual ~GameObject();

	/**
	 * \brief 前初期化処理
	 */
	virtual void PreInit() {}
	/**
	 * \brief 初期化処理
	 */
	virtual void Initialize() = 0;
	/**
	 * \brief 後初期化処理
	 */
	virtual void PostInit() {}
	/**
	 * \brief 更新処理
	 */
	virtual void Update() = 0;
	/**
	 * \brief 前描画
	 */
	virtual void PreRender() {}
	/**
	 * \brief 描画処理
	 */
	virtual void Render() = 0;
	/**
	 * \brief 後描画
	 */
	virtual void PostRender() {}
	/**
	 * \brief 終了処理
	 */
	virtual void Finalize() = 0;
	/**
	 * \brief オブジェクトが衝突したときに呼ばれる
	 * \param target 衝突相手
	 * \param myCollider 衝突した自分の判定
	 * \param targetCollider 衝突した相手の判定
	 */
	virtual void DetectedCollision(GameObject* target, ICollider* myCollider, ICollider* targetCollider) {};

	/**
	 * \brief 自分以下のすべての初期化処理を呼ぶ
	 */
	void InitializeAll();
	
	/**
	 * \brief オブジェクト名を取得
	 * \return オブジェクト名
	 */
	[[nodiscard]]
	auto GetName() const noexcept -> const std::string&;
	/**
	 * \brief 変形情報を取得
	 * \return 変形情報
	 */
	[[nodiscard]]
	auto GetTransform() const noexcept -> Transform*;
	/**
	 * \brief 親オブジェクトを取得
	 * シーン直下の場合nullptrを返す
	 * \return 親オブジェクト
	 */
	[[nodiscard]]
	auto GetParent() const noexcept -> GameObject*;

	/**
	 * \brief 親オブジェクトを設定
	 * \param parent 親ゲームオブジェクト
	 * \return 成功したら真
	 */
	auto SetParent(GameObject* parent) -> bool;
	/**
	 * \brief 親オブジェクトを設定
	 * \param parent 親シーンオブジェクト
	 * \return 成功したら真
	 */
	auto SetParent(SceneObject* parent) -> bool;
	
	/**
	 * \brief 所属シーンを取得
	 * \return 所属シーン
	 */
	[[nodiscard]]
	auto GetAffiliationScene() const noexcept -> SceneObject*;
	
	/**
	 * \brief カメラを取得する
	 * \param index インデックス
	 * \return カメラ
	 */
	[[nodiscard]]
	auto GetCamera(unsigned index = 0) const -> Camera*;
	/**
	 * \brief カメラの状態をリセットする
	 * \return リセット後の0番目のカメラ
	 */
	auto ResetCameras() const -> Camera*;
	/**
	 * \brief カメラとの距離を取得する
	 * \param index インデックス
	 * \return 距離
	 */
	[[nodiscard]]
	auto GetCameraDistance(unsigned index = 0) const -> float;
	
	/**
	 * \brief オブジェクトを検索し取得する
	 * \param objectName オブジェクト名
	 * \return 見つかったオブジェクトのインスタンス　見つからなかった場合nullptr
	 */
	auto FindObject(const std::string& objectName) const noexcept -> GameObject*;
	/**
	 * \brief テンプレートで指定した型のオブジェクトの数を取得
	 * \tparam T 探したい型
	 * \return 見つかったオブジェクトの数
	 */
	template<class T, EnablerIfType<std::is_base_of_v<GameObject, T>> = nullptr>
	[[nodiscard]]
	auto GetSrcObjectCount() const -> int;
	/**
	 * \brief テンプレートで指定した型のオブジェクトの数を取得
	 * \tparam T 探したい型
	 * \param index = 0 取得したいオブジェクトのインデックス
	 * \return 見つかったオブジェクトの数
	 */
	template<class T, EnablerIfType<std::is_base_of_v<GameObject, T>> = nullptr>
	[[nodiscard]]
	auto GetSrcObject(int index = 0) const -> T*;
	/**
	 * \brief 衝突判定を追加する
	 * \param collider 衝突判定
	 */
	void AddCollider(std::unique_ptr<ICollider>& collider);
	/**
	 * \brief 衝突判定を追加する
	 * \param collider 衝突判定
	 */
	void AddCollider(std::unique_ptr<ICollider>&& collider);

	/**
	 * \brief オブジェクト削除フラグを立てる
	 */
	void KillMe();
	/**
	 * \brief オブジェクトが生存しているかを取得
	 * \return 生存してるなら真
	 */
	[[nodiscard]]
	auto IsAlive() const -> bool;
	/**
	 * \brief 初期化済みフラグを立てる
	 */
	void Initialized() noexcept;
	/**
	 * \brief 初期化済みかどうか
	 * \return 初期化済みなら真
	 */
	[[nodiscard]]
	auto IsInitialized() const noexcept -> bool;

	/**
	 * \brief ターゲット指定したオブジェクトのみと当たり判定を行う
	 * \param target 検証相手
	 */
	void CheckCollisionDetectionOnlyTarget(GameObject* target);

	/**
	 * \brief 自身との距離を計算する
	 * \param target 比較対象
	 * \return 距離
	 */
	[[nodiscard]]
	auto CalculateDistance(const GameObject* target) const -> float;
	
	/**
	 * \brief 1フレーム当たりの時間の本来あるべき時間との比率を取得する
	 * \return 比率
	 */
	[[nodiscard]]
	static auto GetFrameRatio() -> float;
	
	/**
	 * \brief オブジェクトを生成する
	 * \tparam T GameObjectを継承したクラス
	 * \param parent 親オブジェクト
	 * \return 生成したインスタンス　失敗したらnullptr
	 */
	template<class T>
	friend auto Instantiate(GameObject* parent) -> T*;
	/**
	 * \brief オブジェクトを生成する
	 * \tparam T GameObjectを継承したクラス
	 * \param parent 親シーン
	 * \return 生成したインスタンス　失敗したらnullptr
	 */
	template<class T>
	friend auto Instantiate(SceneObject* parent) -> T*;

private:
	/**
	 * \brief 生成したゲームオブジェクトの所有権をシーン追加する
	 * \param gameObject 生成したゲームオブジェクト
	 */
	void AddGameObjectInstance(std::unique_ptr<GameObject>& gameObject) const;
	
	/**
	 * \brief 子オブジェクトの前初期化処理を呼び出す
	 */
	void PreInitChild() const;
	/**
	 * \brief 子オブジェクトの初期化処理を呼び出す
	 */
	void InitializeChild() const;
	/**
	 * \brief 子オブジェクトの後初期化処理を呼び出す
	 */
	void PostInitChild() const;

	/**
	 * \brief 子オブジェクトの更新処理を呼び出す
	 */
	void UpdateChild();
	/**
	 * \brief 子オブジェクトの前描画処理を呼び出す
	 */
	void PreRenderChild() const;
	/**
	 * \brief 子オブジェクトの描画処理を呼び出す
	 */
	void RenderChild() const;
	/**
	 * \brief 子オブジェクトの後描画処理を呼び出す
	 */
	void PostRenderChild() const;
	/**
	 * \brief 子オブジェクトの終了処理を呼び出す
	 */
	void FinalizeChild();

	/**
	 * \brief 衝突判定を検証する
	 * \param target 検証相手
	 */
	void CheckCollisionDetection(GameObject* target);
	
	/**
	 * \brief 子オブジェクトを追加する
	 * \param child 追加するゲームオブジェクト
	 * \return 成功したら真
	 */
	auto AddChild(GameObject* child) -> bool;
	/**
	 * \brief 子オブジェクトを除外する
	 * \param child 除外する子オブジェクト
	 */
	void RemoveChild(GameObject* child);
	/**
	 * \brief シーン直下かを取得する
	 * \return シーン直下なら真
	 */
	auto IsSceneChild() const -> bool;
	/**
	 * \brief ゲームオブジェクトを親に持つかどうかを取得する
	 * \return ゲームオブジェクトが親なら真
	 */
	auto IsGameObjectChild() const -> bool;
	/**
	 * \brief 変形情報の親子関係を更新する
	 */
	void UpdateTransformParent();

	//void KillChildren();
private:
	std::string name_;												//オブジェクト名
	std::variant<GameObject*, SceneObject*, nullptr_t> parent_;		//親オブジェクト
	std::unique_ptr<Transform> transform_;							//変形情報
	std::list<GameObject*> childList_;								//子オブジェクトリスト
	std::list<std::unique_ptr<ICollider>> colliderList_;			//衝突判定リスト
	std::list<ICollider*> collidedColliderList_;					//衝突判定済み衝突判定リスト
	
	union State {

		using Type = int16_t;
		
		struct Elem {
			Type survivable : 1;	//生存
			Type drawable : 1;		//描画
			Type updatable : 1;		//更新
			Type initialized : 1;	//初期化済みか
		} elem;		//状態各個
		Type all;	//状態すべて
		
		explicit State(Type data) : all(data) {}
		
	} state_;	//オブジェクトの状態

	static bool sceneInitialize;	//シーン初期化中かどうか
	
	//子オブジェクトの処理を呼び出す関数を呼び出すため
	friend class SceneObject;
};

template <class T, EnablerIfType<std::is_base_of_v<GameObject, T>>>
auto GameObject::GetSrcObjectCount() const -> int {

	auto count = 0;
	for (auto* child : childList_) {

		if (dynamic_cast<T*>(child)) {

			++count;
		}
		count += child->GetSrcObjectCount<T>();
	}
	return count;
}

template <class T, EnablerIfType<std::is_base_of_v<GameObject, T>>>
auto GameObject::GetSrcObject(int index) const -> T* {

	for (auto* child : childList_) {

		if (auto* findObject = dynamic_cast<T*>(child)) {

			if (index == 0)	return findObject;
			--index;
		}
		if (auto* findObject = child->GetSrcObject<T>(index)) {

			return findObject;
		}
		index -= child->GetSrcObjectCount<T>();
	}
	return nullptr;
}

template<class T>
auto Instantiate(GameObject* parent) -> T* {

	if (!std::is_base_of_v<GameObject, T>)	return nullptr;
	
	std::unique_ptr<GameObject> pGameObject = std::make_unique<T>();
	pGameObject->SetParent(parent);
	auto* gameObject = dynamic_cast<T*>(pGameObject.get());
	parent->AddGameObjectInstance(pGameObject);

	if (!GameObject::sceneInitialize) {

		gameObject->PreInit();
		gameObject->Initialize();
		gameObject->PostInit();
	}
	
	return gameObject;
}

template<class T>
auto Instantiate(SceneObject* parent) -> T* {

	if (!std::is_base_of_v<GameObject, T>)	return nullptr;
	
	std::unique_ptr<GameObject> pGameObject = std::make_unique<T>();
	pGameObject->SetParent(parent);
	auto* gameObject = dynamic_cast<T*>(pGameObject.get());
	parent->AddGameObjectInstance(pGameObject);

	if (!GameObject::sceneInitialize) {

		gameObject->PreInit();
		gameObject->Initialize();
		gameObject->PostInit();
	}
	
	return gameObject;
}

}

//ユーザに強要しない
//要件等
using namespace Whono;
using namespace GameOperation;
