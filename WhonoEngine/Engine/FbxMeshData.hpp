#pragma once
#include <DirectXMath.h>
#include <memory>
#include <fbxsdk.h>

struct ID3D11Buffer;


namespace Whono::RenderObject {


class FbxMeshData final {

public:
	FbxMeshData();
	~FbxMeshData();

	void Initialize(FbxMesh* mesh) const;

	auto GetMaterialId() const noexcept -> uint64_t;
	auto GetIndexCount() const noexcept -> unsigned;

	auto GetIndexBuffer() const noexcept -> ID3D11Buffer*;
	auto GetVertexBuffer() const noexcept -> ID3D11Buffer*;

	auto GetClusterCount() const noexcept -> size_t;
	auto GetClusterBindPose(int i = 0) const noexcept -> DirectX::XMMATRIX;
	auto GetClusterLinkUniqueId(int i = 0) const noexcept -> uint64_t;

	auto RayCast(DirectX::CXMVECTOR origin, DirectX::CXMVECTOR direction, float* dist, DirectX::XMVECTOR* normal) const -> bool;
	void GetMinMaxPosition(DirectX::XMVECTOR* min, DirectX::XMVECTOR* max) const;
	
	void Release();

	static auto GetVertexDataSize() noexcept -> unsigned;

private:
	struct Vertex;
	struct Impl;
	std::unique_ptr<Impl> pImpl_;
};

}
