#pragma once

#include <memory>
#include <list>
#include <vector>
#include <string>
#include "EnablerIfType.hpp"

//前方宣言
namespace Whono::GameOperation {
class ShadowManager;
class Camera;
class GameObject;
class Light;
}


namespace Whono::GameOperation {

/**
 * \brief ゲーム内のシーンクラス
 */
class SceneObject {

public:
	/**
	 * \brief 描画フェーズ列挙
	 */
	enum class RENDERING_PHASE {

		SHADOW,	//シャドウマッピング
		NORMAL	//通常描画
	};
	
private:
	std::string name_;
	std::vector<std::unique_ptr<Light>> lights_;		//光源（未実装
	std::vector<std::unique_ptr<Camera>> cameras_;		//カメラ
	std::list<std::shared_ptr<GameObject>> managementGameObjectInstanceList_;	//管理オブジェクト
	std::list<GameObject*> childList_;	//子オブジェクトリスト
	std::unique_ptr<ShadowManager>	shadowManager;	//影に関する処理
	bool isRenderShadow_;
	
	static RENDERING_PHASE renderingPhase_;	//描画フェイズ

public:
	/**
	 * \brief コンストラクタ
	 */
	explicit SceneObject(const std::string& name);
	/**
	 * \brief デストラクタ
	 */
	virtual ~SceneObject();

	/**
	 * \brief すべての初期化処理を呼び出す
	 */
	void InitializeAll();
	/**
	 * \brief 前初期化処理
	 */
	virtual void PreInit();
	/**
	 * \brief 初期化処理
	 */
	virtual void Initialize() = 0;
	/**
	 * \brief 後初期化処理
	 */
	virtual void PostInit();
	/**
	 * \brief 更新処理
	 */
	void Update();
	/**
	 * \brief 描画処理
	 */
	void Render();
	/**
	 * \brief 終了処理
	 */
	void Finalize();

	/**
	 * \brief カメラを追加する
	 * \return 追加されたカメラ
	 */
	auto AddCamera() -> Camera*;
	/**
	 * \brief カメラを取得する
	 * \param index = 0 インデックス
	 * \return カメラ
	 */
	[[nodiscard]]
	auto GetCamera(unsigned index = 0) const -> Camera*;
	/**
	 * \brief カメラの個数を取得する
	 * \return カメラの個数
	 */
	[[nodiscard]]
	auto GetCameraCount() const -> unsigned;
	/**
	 * \brief カメラの状態をリセットする
	 * \return リセット後の0番目のカメラ
	 */
	auto ResetCameras() -> Camera*;

	/**
	 * \brief 光源を追加する
	 * \param light 追加するライト
	 * \return 追加された光源
	 */
	auto AddLight(std::unique_ptr<Light>&& light) -> Light*;
	/**
	 * \brief 光源を追加する
	 * \param light 追加するライト
	 * \return 追加された光源
	 */
	auto AddLight(std::unique_ptr<Light>& light) -> Light*;
	/**
	 * \brief 光源を取得する
	 * \param index = 0 インデックス
	 * \return 光源
	 */
	[[nodiscard]]
	auto GetLight(unsigned index = 0) const -> Light*;
	/**
	 * \brief 光源の数を取得する
	 * \return 光源の数
	 */
	[[nodiscard]]
	auto GetLightCount() const -> unsigned;
	/**
	 * \brief 光源をリセットする
	 * \return リセット後の0番目のカメラ
	 */
	auto ResetLight() -> Light*;
	
	/**
	 * \brief シーン名を取得する
	 * \return シーン名
	 */
	[[nodiscard]]
	auto GetSceneName() const noexcept -> const std::string&;
	
	/**
	 * \brief オブジェクトを検索し取得する
	 * \param objectName オブジェクト名
	 * \return 見つかったオブジェクトのインスタンス　見つからなかった場合nullptr
	 */
	auto FindObject(const std::string& objectName) const noexcept -> GameObject*;
	/**
	 * \brief オブジェクトを所持しているかどうかを取得
	 * \param gameObject 剣書したいオブジェクト
	 * \return 所持してたら真
	 */
	[[nodiscard]]
	auto HasGameObject(const GameObject* gameObject) const -> bool;
	/**
	 * \brief テンプレートで指定した型のオブジェクトの数を取得
	 * \tparam T 探したい型
	 * \return 見つかったオブジェクトの数
	 */
	template<class T, EnablerIfType<std::is_base_of_v<GameObject, T>> = nullptr>
	[[nodiscard]]
	auto GetSrcObjectCount() const -> int;
	/**
	 * \brief テンプレートで指定した型のオブジェクトの数を取得
	 * \tparam T 探したい型
	 * \param index = 0 取得したいオブジェクトのインデックス
	 * \return 見つかったオブジェクト
	 */
	template<class T, EnablerIfType<std::is_base_of_v<GameObject, T>> = nullptr>
	[[nodiscard]]
	auto GetSrcObject(int index = 0) const -> T*;

	/**
	 * \brief 管理しているオブジェクトか
	 * \param gameObject 対象
	 * \return 管理しているなら真
	 */
	[[nodiscard]]
	auto IsManagingInstance(GameObject* gameObject) const -> bool;

	/**
	 * \brief 生成したゲームオブジェクトの所有権をシーン追加する
	 * \param gameObject 生成したゲームオブジェクト
	 */
	void AddGameObjectInstance(std::unique_ptr<GameObject>& gameObject);

	/**
	 * \brief シーン内で影の描画を行うかを設定する
	 * \param b trueで描画
	 */
	void AllowShadowRendering(bool b);
	/**
	 * \brief シーン内で影の描画を行うか
	 * \return 行うなら真
	 */
	[[nodiscard]]
	auto IsShadowRendering() const noexcept -> bool;
	/**
	 * \brief 現在の描画フェイズを取得する
	 * \return 辨官フェイズ
	 */
	[[nodiscard]]
	static auto GetRenderingPhase() noexcept -> RENDERING_PHASE;
	
	/**
	* \brief ウィンドウサイズのずれに合わせてカメラを更新する
	*/
	void UpdateAllCamera();

private:
	/**
	 * \brief 子オブジェクトを追加する
	 * \param child 追加するゲームオブジェクト
	 * \return 成功したら真
	 */
	auto AddChild(GameObject* child) -> bool;
	/**
	 * \brief 子オブジェクトを除外する
	 * \param child 除外する子オブジェクト
	 */
	void RemoveChild(GameObject* child);
	/**
	 * \brief 指定オブジェクトを開放する
	 * \param gameObject 開放したいオブジェクト
	 */
	void ReleaseObject(GameObject* gameObject);

	/**
	 * \brief 子オブジェクトの前初期化処理を呼び出す
	 */
	void PreInitChild();
	/**
	 * \brief 子オブジェクトの初期化処理を呼び出す
	 */
	void InitializeChild();
	/**
	 * \brief 子オブジェクトの後初期化処理を呼び出す
	 */
	void PostInitChild();
	/**
	 * \brief シャドウマップ用描画処理
	 */
	void RenderShadowMap();
	
	friend class GameObject;
};


template <class T, EnablerIfType<std::is_base_of_v<GameObject, T>>>
auto SceneObject::GetSrcObjectCount() const -> int {

	auto count = 0;
	for (auto* child : childList_) {
		
		if (dynamic_cast<T*>(child)) {

			++count;
		}
		count += child->GetSrcObjectCount<T>();	
	}
	return count;
}

template <class T, EnablerIfType<std::is_base_of_v<GameObject, T>>>
auto SceneObject::GetSrcObject(int index) const -> T* {
	
	for (auto* child : childList_) {

		if (auto* findObject = dynamic_cast<T*>(child)) {

			if (index == 0) return findObject;
			--index;
		}
		if (auto* findObject = child->GetSrcObject<T>(index)) {

			return findObject;
		}
		index -= child->GetSrcObjectCount<T>();
	}
	return nullptr;
}

}

//ユーザに強要しない
//要件等
using namespace Whono;
using GameOperation::SceneObject;