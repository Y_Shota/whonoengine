#pragma once

#include <DirectXMath.h>
#include <memory>

using namespace DirectX;


namespace Whono::GameOperation {

/**
* \brief 変形情報を管理
* \author whono
* \version 1.0
* \since 1.0
*/
class Transform final {

public:
	/**
	 * \brief デフォルトコンストラクタ
	 */
	Transform();
	
	/**
	 * \brief デストラクタ
	 */
	~Transform();

	
	/**
	 * \brief コピーコンストラクタ
	 * \param other コピー元
	 */
	Transform(const Transform& other);
	
	/**
	 * \brief コピー演算子
	 * \param other コピー元
	 * \return コピーしたオブジェクト
	 */
	Transform& operator=(const Transform& other);

	//ムーブ禁止
	Transform(Transform&&) = delete;
	Transform& operator=(Transform&&) = delete;

	
	auto Copy() const -> std::unique_ptr<Transform>;

	/**
	 * \brief 親となる変形情報を設定する
	 * \param parent 親のポインタ
	 * \param keepWorldTransform ワールド空間での変形情報を保持するか
	 * \return 設定出来たら真
	 */
	auto SetParent(Transform* parent, bool keepWorldTransform = true) const -> bool;

	
	/**
	 * \brief ワールドから見た座標を取得
	 * \return ワールド座標
	 */
	[[nodiscard]]
	auto GetWorldPosition() const noexcept -> CXMVECTOR;	
	/**
	 * \brief 親から見た座標を取得
	 * \return ローカル座標
	 */
	[[nodiscard]]
	auto GetLocalPosition() const noexcept -> CXMVECTOR;	
	/**
	 * \brief ワールドから見た回転クォータニオンを取得
	 * \return ワールド回転クォータニオン
	 */
	[[nodiscard]]
	auto GetWorldOriented() const noexcept -> CXMVECTOR;	
	/**
	 * \brief 親から見た回転クォータニオンを取得
	 * \return ローカル回転クォータニオン
	 */
	[[nodiscard]]
	auto GetLocalOriented() const noexcept -> CXMVECTOR;	
	/**
	 * \brief ワールドから見た拡大率を取得
	 * \return ワールド拡大率
	 */
	[[nodiscard]]
	auto GetWorldScale() const noexcept -> CXMVECTOR;
	/**
	 * \brief 親から見た拡大率を取得
	 * \return ローカル拡大率
	 */
	[[nodiscard]]
	auto GetLocalScale() const noexcept -> CXMVECTOR;
	
	/**
	 * \brief ワールド行列を取得
	 * \return ワールド行列
	 */
	[[nodiscard]]
	auto GetWorldMatrix() const noexcept -> XMMATRIX;
	/**
	 * \brief ローカル行列の取得
	 * \return ローカル行列
	 */
	[[nodiscard]]
	auto GetLocalMatrix() const noexcept -> XMMATRIX;
	/**
	 * \brief ワールド移動行列の取得
	 * \return ワールド移動行列
	 */
	[[nodiscard]]
	auto GetWorldTranslationMatrix() const noexcept -> XMMATRIX;	
	/**
	 * \brief ローカル移動行列の取得
	 * \return ローカル移動行列
	 */
	[[nodiscard]]
	auto GetLocalTranslationMatrix() const noexcept -> XMMATRIX;
	/**
	 * \brief ワールド拡大縮小行列の取得
	 * \return ワールド拡大縮小行列
	 */
	[[nodiscard]]
	auto GetWorldScalingMatrix() const noexcept -> XMMATRIX;
	/**
	 * \brief ローカル拡大縮小行列の取得
	 * \return ローカル拡大縮小行列
	 */
	[[nodiscard]]
	auto GetLocalScalingMatrix() const noexcept -> XMMATRIX;
	/**
	 * \brief ワールド回転行列の取得
	 * \return ワールド回転行列
	 */
	[[nodiscard]]
	auto GetWorldRotationMatrix() const noexcept -> XMMATRIX;
	/**
	 * \brief ローカル回転行列の取得
	 * \return ローカル回転行列
	 */
	[[nodiscard]]
	auto GetLocalRotationMatrix() const noexcept -> XMMATRIX;
	/**
	 * \brief ワールド上でのY軸の回転量を取得する
	 * \return ワールド上でのY軸の回転量
	 */
	[[nodiscard]]
	auto GetWorldPitch() const -> float;

	/**
	 * \brief ローカルの正面ベクトルを取得
	 * \return 正面ベクトル
	 */
	[[nodiscard]]
	auto GetLocalForward() const -> XMVECTOR;
	/**
	 * \brief ワールドの正面ベクトルを取得
	 * \return 正面ベクトル
	 */
	[[nodiscard]]
	auto GetWorldForward() const -> XMVECTOR;
	
	/**
	 * \brief ワールド座標を設定する
	 * \param position 座標
	 * \return 変更後のインスタンス
	 */
	auto SetWorldPosition(CXMVECTOR position) -> Transform*;
	/**
	 * \brief ワールド座標を設定する
	 * \param x X座標
	 * \param y Y座標
	 * \param z Z座標
	 * \return 変更後のインスタンス
	 */
	auto SetWorldPosition(float x, float y, float z) -> Transform*;
	/**
	 * \brief ワールドX座標を設定する
	 * \param x X座標
	 * \return 変更後のインスタンス
	 */
	auto SetWorldPositionX(float x) -> Transform*;
	/**
	 * \brief ワールドY座標を設定する
	 * \param y Y座標
	 * \return 変更後のインスタンス
	 */
	auto SetWorldPositionY(float y) -> Transform*;
	/**
	 * \brief ワールドZ座標を設定する
	 * \param z Z座標
	 * \return 変更後のインスタンス
	 */
	auto SetWorldPositionZ(float z) -> Transform*;	

	/**
	 * \brief ローカル座標をセットする
	 * \param position 座標
	 * \return 変更後のインスタンス
	 */
	auto SetLocalPosition(CXMVECTOR position) -> Transform*;	
	/**
	 * \brief ローカル座標をセットする
	 * \param x X座標
	 * \param y Y座標
	 * \param z Z座標
	 * \return 変更後のインスタンス
	 */
	auto SetLocalPosition(float x, float y, float z) -> Transform*;	
	/**
	 * \brief ローカルX座標をセットする
	 * \param x X座標
	 * \return 変更後のインスタンス
	 */
	auto SetLocalPositionX(float x) -> Transform*;
	/**
	 * \brief ローカルX座標をセットする
	 * \param y Y座標
	 * \return 変更後のインスタンス
	 */
	auto SetLocalPositionY(float y) -> Transform*;
	/**
	 * \brief ローカルX座標をセットする
	 * \param z Z座標
	 * \return 変更後のインスタンス
	 */
	auto SetLocalPositionZ(float z) -> Transform*;
	/**
	 * \brief 現在の座標から引数分移動させる
	 * \param movement 移動量
	 * \return 変更後のインスタンス
	 */
	auto AdjustPosition(CXMVECTOR movement) -> Transform*;
	/**
	 * \brief 現在の座標から引数分移動させる
	 * \param x X移動量
	 * \param y Y移動量
	 * \param z Z移動量
	 * \return 変更後のインスタンス
	 */
	auto AdjustPosition(float x, float y, float z) -> Transform*;
	/**
	 * \brief 現在の座標から引数分X座標を移動させる
	 * \param x X移動量
	 * \return 変更後のインスタンス
	 */
	auto AdjustPositionX(float x) -> Transform*;
	/**
	 * \brief 現在の座標から引数分Y座標を移動させる
	 * \param y Y移動量
	 * \return 変更後のインスタンス
	 */
	auto AdjustPositionY(float y) -> Transform*;
	/**
	 * \brief 現在の座標から引数分Z座標を移動させる
	 * \param z Z移動量
	 * \return 変更後のインスタンス
	 */
	auto AdjustPositionZ(float z) -> Transform*;	

	/**
	 * \brief ワールドスケールをセットする
	 * \param scale 拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetWorldScale(CXMVECTOR scale) -> Transform*;	
	/**
	 * \brief ワールドスケールをセットする
	 * \param x X拡大率
	 * \param y Y拡大率
	 * \param z Z拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetWorldScale(float x, float y, float z) -> Transform*;	
	/**
	 * \brief ワールドXスケールをセットする
	 * \param x X拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetWorldScaleX(float x) -> Transform*;
	/**
	 * \brief ワールドYスケールをセットする
	 * \param y Y拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetWorldScaleY(float y) -> Transform*;
	/**
	 * \brief ワールドZスケールをセットする
	 * \param z Z拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetWorldScaleZ(float z) -> Transform*;

	/**
	 * \brief ローカルスケールをセットする
	 * \param scale 拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetLocalScale(CXMVECTOR scale) -> Transform*;
	/**
	 * \brief ローカルスケールをセットする
	 * \param x X拡大率
	 * \param y Y拡大率
	 * \param z Z拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetLocalScale(float x, float y, float z) -> Transform*;	
	/**
	 * \brief ローカルXスケールをセットする
	 * \param x X拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetLocalScaleX(float x) -> Transform*;
	/**
	 * \brief ローカルYスケールをセットする
	 * \param y Y拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetLocalScaleY(float y) -> Transform*;
	/**
	 * \brief ローカルZスケールをセットする
	 * \param z Z拡大率
	 * \return 変更後のインスタンス
	 */
	auto SetLocalScaleZ(float z) -> Transform*;

	/**
	 * \brief 拡大率を増加させる
	 * \param scaling 増加量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleAdd(CXMVECTOR scaling) -> Transform*;
	/**
	 * \brief 拡大率を増加させる
	 * \param x X増加量
	 * \param y Y増加量
	 * \param z Z増加量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleAdd(float x, float y, float z) -> Transform*;
	/**
	 * \brief X拡大率を増加させる
	 * \param x X増加量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleXAdd(float x) -> Transform*;
	/**
	 * \brief Y拡大率を増加させる
	 * \param y Y増加量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleYAdd(float y) -> Transform*;
	/**
	 * \brief Z拡大率を増加させる
	 * \param z Z増加量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleZAdd(float z) -> Transform*;
	/**
	 * \brief XYZ拡大率を同じ量だけ増加させる
	 * \param scaling 増加量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleAllAdd(float scaling) -> Transform*;
	/**
	 * \brief 拡大率を倍増させる
	 * \param scaling 倍増量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleMultiply(CXMVECTOR scaling) -> Transform*;
	/**
	 * \brief 拡大率を倍増させる
	 * \param x X倍増量
	 * \param y Y倍増量
	 * \param z Z倍増量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleMultiply(float x, float y, float z) -> Transform*;
	/**
	 * \brief X拡大率を倍増させる
	 * \param x X倍増量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleXMultiply(float x) -> Transform*;
	/**
	 * \brief Y拡大率を倍増させる
	 * \param y Y倍増量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleYMultiply(float y) -> Transform*;
	/**
	 * \brief Z拡大率を倍増させる
	 * \param z Z倍増量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleZMultiply(float z) -> Transform*;
	/**
	 * \brief XYZ拡大率を同じ量だけ倍増させる
	 * \param scaling 倍増量
	 * \return 変更後のインスタンス
	 */
	auto AdjustScaleAllMultiply(float scaling) -> Transform*;	

	/**
	 * \brief ワールドオリエントをセットする
	 * \param oriented 回転クォータニオン
	 * \return 変更後のインスタンス
	 */
	auto SetWorldOriented(CXMVECTOR oriented) -> Transform*;
	/**
	 * \brief ワールドオリエントをセットする
	 * \param axis 回転軸
	 * \param angleRadians 回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetWorldOrientedAxisRadians(CXMVECTOR axis, float angleRadians) -> Transform*;	
	/**
	 * \brief ワールドオリエントをセットする
	 * \param axis 回転軸
	 * \param angleDegree 回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetWorldOrientedAxis(CXMVECTOR axis, float angleDegree) -> Transform*;
	/**
	 * \brief ワールドローテートをセットする
	 * \param rotation 回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetWorldRotationRadians(CXMVECTOR rotation) -> Transform*;
	/**
	 * \brief ワールドローテートをセットする
	 * \param x X回転量 (ラジアン)
	 * \param y Y回転量 (ラジアン)
	 * \param z Z回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetWorldRotationRadians(float x, float y, float z ) -> Transform*;
	/**
	 * \brief ワールドローテートをセットする
	 * \param rotation 回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetWorldRotation(CXMVECTOR rotation) -> Transform*;
	/**
	 * \brief ワールドローテートをセットする
	 * \param x X回転量 (度)
	 * \param y Y回転量 (度)
	 * \param z Z回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetWorldRotation(float x, float y, float z ) -> Transform*;
	
	/**
	 * \brief ローカルオリエントをセットする
	 * \param oriented 回転クォータニオン
	 * \return 変更後のインスタンス
	 */
	auto SetLocalOriented(CXMVECTOR oriented) -> Transform*;
	/**
	 * \brief ローカルオリエントをセットする
	 * \param axis 回転軸
	 * \param angleRadians 回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetLocalOrientedAxisRadians(CXMVECTOR axis, float angleRadians) -> Transform*;
	/**
	 * \brief ローカルオリエントをセットする
	 * \param axis 回転軸
	 * \param angleDegree 回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetLocalOrientedAxis(CXMVECTOR axis, float angleDegree) -> Transform*;
	/**
	 * \brief ローカルローテートをセットする
	 * \param rotation 回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetLocalRotationRadians(CXMVECTOR rotation) -> Transform*;
	/**
	 * \brief ローカルローテートをセットする
	 * \param x X回転量 (ラジアン)
	 * \param y Y回転量 (ラジアン)
	 * \param z Z回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetLocalRotationRadians(float x, float y, float z ) -> Transform*;
	/**
	 * \brief ローカルローテートをセットする
	 * \param rotation 回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetLocalRotation(CXMVECTOR rotation) -> Transform*;
	/**
	 * \brief ローカルローテートをセットする
	 * \param x X回転量 (度)
	 * \param y Y回転量 (度)
	 * \param z Z回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetLocalRotation(float x, float y, float z ) -> Transform*;	

	/**
	 * \brief 引数分だけ回転させる
	 * \param oriented 回転クォータニオン
	 * \return 変更後のインスタンス
	 */
	auto AdjustOrientedMultiply(CXMVECTOR oriented) -> Transform*;
	/**
	 * \brief 引数分だけ回転させる
	 * \param axis 回転軸
	 * \param angleRadians 回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto AdjustOrientedMultiplyAxisRadians(CXMVECTOR axis, float angleRadians) -> Transform*;
	/**
	 * \brief 引数分だけ回転させる
	 * \param axis 回転軸
	 * \param angleDegree 回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto AdjustOrientedMultiplyAxis(CXMVECTOR axis, float angleDegree) -> Transform*;
	/**
	 * \brief 引数分だけ回転させる
	 * \param rotation 回転量 (ラジアン) 
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationRadians(CXMVECTOR rotation) -> Transform*;
	/**
	 * \brief 引数分だけ回転させる
	 * \param x X回転量 (ラジアン) 
	 * \param y Y回転量 (ラジアン) 
	 * \param z Z回転量 (ラジアン) 
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationRadians(float x, float y, float z) -> Transform*;
	/**
	 * \brief 引数分だけX軸回転させる
	 * \param x X回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationXRadians(float x) -> Transform*;
	/**
	 * \brief 引数分だけY軸回転させる
	 * \param y Y回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationYRadians(float y) -> Transform*;
	/**
	 * \brief 引数分だけZ軸回転させる
	 * \param z Z回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationZRadians(float z) -> Transform*;
	/**
	 * \brief 引数分だけ回転させる
	 * \param rotation 回転量 (度) 
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotation(CXMVECTOR rotation) -> Transform*;
	/**
	 * \brief 引数分だけ回転させる
	 * \param x X回転量 (度) 
	 * \param y Y回転量 (度) 
	 * \param z Z回転量 (度) 
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotation(float x, float y, float z) -> Transform*;
	/**
	 * \brief 引数分だけX軸回転させる
	 * \param x X回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationX(float x) -> Transform*;
	/**
	 * \brief 引数分だけY軸回転させる
	 * \param y Y回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationY(float y) -> Transform*;
	/**
	 * \brief 引数分だけZ軸回転させる
	 * \param z Z回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationZ(float z) -> Transform*;

	/**
	 * \brief 解放処理
	 */
	void Release();
	
private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;
};

}
