#pragma once
#include <fbxsdk.h>
#include <DirectXMath.h>

namespace Whono::FbxHelper {

/**
 * \brief FBX_SDKのベクトルをDirectXMathのベクトルに変換する
 * \param fbxVector 変換したいベクトル
 * \return 変換されたベクタル
 */
auto ConvertToXmvector(const FbxDouble4& fbxVector) -> DirectX::XMVECTOR;
/**
 * \brief FBX_SDKのベクトルをDirectXMathのベクトルに変換する
 * \param fbxVector 変換したいベクトル
 * \return 変換されたベクタル
 */
auto ConvertToXmvector(const FbxDouble3& fbxVector) -> DirectX::XMVECTOR;
/**
 * \brief FBX_SDKのベクトルをDirectXMathのベクトルに変換する
 * \param fbxVector 変換したいベクトル
 * \return 変換されたベクタル
 */
auto ConvertToXmvector(const FbxDouble2& fbxVector) -> DirectX::XMVECTOR;
/**
 * \brief FBX_SDKの行列をDirectXMathの行列に変換する
 * \param fbxMatrix 変換したい行列
 * \return 変換された行列
 */
auto ConvertToXmmatrix(const FbxDouble4x4& fbxMatrix) -> DirectX::XMMATRIX;
/**
 * \brief DirectXMathのベクトルをFBX_SDKのベクトルに変換する
 * \param xmvector 変換したいベクトル
 * \return 変換されたベクタル
 */
auto ConvertToFbxVector4(DirectX::CXMVECTOR xmvector) -> FbxVector4;
/**
 * \brief DirectXMathの行列をFBX_SDKの行列に変換する
 * \param xmmatrix 変換したい行列
 * \return 変換された行列
 */
auto ConvertToFbxAMatrix(DirectX::CXMMATRIX xmmatrix) -> FbxAMatrix;

/**
 * \brief ノードの名前を出力する
 * \param node ノード
 */
void PrintNodeName(const FbxNode* node);
/**
 * \brief ノードの名前を出力し改行する
 * \param node ノード
 */
void PrintlnNodeName(const FbxNode* node);
/**
 * \brief ベクトルの内容を出力する
 * \param fbxVector ベクトル
 */
void PrintFbxVector(const FbxDouble4& fbxVector);
/**
 * \brief ベクトルの内容を出力し改行する
 * \param fbxVector ベクトル
 */
void PrintlnFbxVector(const FbxDouble4& fbxVector);
/**
 * \brief 行列の内容を出力し改行する
 * \param fbxMatrix 行列
 */
void PrintFbxMatrix(const FbxDouble4x4& fbxMatrix);
/**
 * \brief 行列の内容を出力し改行する
 * \param fbxMatrix 行列
 */
void PrintlnFbxMatrix(const FbxDouble4x4& fbxMatrix);

}

