#include "FbxSkeletonData.hpp"

#include <unordered_map>

#include "FbxHelper.hpp"
#include "Transform.hpp"

using namespace std;

using Whono::GameOperation::Transform;


namespace Whono::RenderObject {


struct FbxSkeletonData::Impl {

	void Initialize(FbxNode* skeletonNode);
	auto GetGlobalTransform(int frame) -> XMMATRIX;
	auto GetGlobalFbxTransform(int frame) -> const FbxAMatrix&;
	auto GetGlobalPosition(int frame) -> XMVECTOR;
	
	string name;
	uint64_t id;
	FbxNode* node;
	FbxAMatrix defaultTransform;
	int currentFrame;
	unordered_map<int, FbxAMatrix> evaluateGlobalTransform;
};

FbxSkeletonData::FbxSkeletonData() {

	pImpl_ = make_unique<Impl>();
}

FbxSkeletonData::~FbxSkeletonData() = default;

void FbxSkeletonData::Initialize(FbxNode* skeletonNode) const {

	pImpl_->Initialize(skeletonNode);
}

auto FbxSkeletonData::GetName() const noexcept -> const std::string& {

	return pImpl_->name;
}

auto FbxSkeletonData::GetGlobalTransform(int frame) const -> XMMATRIX {

	return pImpl_->GetGlobalTransform(frame);
}

auto FbxSkeletonData::GetGlobalFbxTransform(int frame) const -> const FbxAMatrix& {

	return pImpl_->GetGlobalFbxTransform(frame);
}

auto FbxSkeletonData::GetGlobalPosition(int frame) const -> XMVECTOR {

	return pImpl_->GetGlobalPosition(frame);
}

auto FbxSkeletonData::GetUniqueId() const -> uint64_t {

	return pImpl_->id;
}

void FbxSkeletonData::Impl::Initialize(FbxNode* skeletonNode) {

	name = skeletonNode->GetName();
	id = skeletonNode->GetUniqueID();
	node = skeletonNode;
	currentFrame = 0;
}

auto FbxSkeletonData::Impl::GetGlobalTransform(int frame) -> XMMATRIX {

	if (frame != -1)	currentFrame = frame;

	if (evaluateGlobalTransform.contains(currentFrame)) {

		return FbxHelper::ConvertToXmmatrix(evaluateGlobalTransform.at(frame));
	}
	FbxTime frameTime;
	frameTime.SetTime(0, 0, 0, frame, 0, FbxTime::eFrames60);
	evaluateGlobalTransform[frame] = node->EvaluateGlobalTransform(frameTime);
	return FbxHelper::ConvertToXmmatrix(evaluateGlobalTransform.at(frame));
}

auto FbxSkeletonData::Impl::GetGlobalFbxTransform(int frame) -> const FbxAMatrix& {

	if (frame != -1)	currentFrame = frame;

	if (evaluateGlobalTransform.contains(currentFrame)) {

		return evaluateGlobalTransform.at(currentFrame);
	}
	FbxTime frameTime;
	frameTime.SetTime(0, 0, 0, currentFrame, 0, FbxTime::eFrames60);
	evaluateGlobalTransform[currentFrame] = node->EvaluateGlobalTransform(frameTime);
	return evaluateGlobalTransform.at(currentFrame);
}

auto FbxSkeletonData::Impl::GetGlobalPosition(int frame) -> XMVECTOR {

	if (frame != -1)	currentFrame = frame;

	if (evaluateGlobalTransform.contains(currentFrame)) {
		
		return FbxHelper::ConvertToXmvector(evaluateGlobalTransform.at(currentFrame).GetT());
	}
	FbxTime frameTime;
	frameTime.SetTime(0, 0, 0, currentFrame, 0, FbxTime::eFrames60);
	evaluateGlobalTransform[currentFrame] = node->EvaluateGlobalTransform(frameTime);
	return FbxHelper::ConvertToXmvector(evaluateGlobalTransform.at(currentFrame).GetT());
}

}
