#pragma once
#include "UdpSocket.hpp"


namespace Whono::Network {

class UdpClientSocket final : public UdpSocket {
	
public:
	void Open(u_short port) override;
	void Connect(const char* destination) override;
};

}
