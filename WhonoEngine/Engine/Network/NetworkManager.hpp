#pragma once
#include <memory>
#include <string>

//前方宣言
namespace Whono::Network {
class IPacket;
class Socket;
}


/**
 * \brief ネットワークに関する処理
 * \author whono
 * \version 1.0
 * \since 1.0
 */
namespace Whono::Network {


/**
 * \brief 作成するソケットの種類の指定に使用
 */
enum class SOCKET_MODE : unsigned char {

	UDP_SERVER,	//アクセスしてきた相手を送信先に自動設定され、複数人に送信を行う
	UDP_CLIENT,	//任意に設定した相手一人のみに送信を行う
	TCP_SERVER,	//未実装
	TCP_CLIENT	//未実装
};


/**
 * \brief ネットワークに関する処理を補佐する
 */
namespace NetworkManager {

/**
 * \brief 初期化処理
 */
void Initialize();

/**
 * \brief 終了処理
 */
void Finalize();


/**
 * \brief 更新処理
 */
void Update();


/**
 * \brief ソケットを作成する
 * \param mode 作成するソケットの種類
 * \param port 使用するポート　UDPの場合隣のポートも使用される
 * \return 作成したソケットのID
 */
auto CreateSocket(SOCKET_MODE mode, unsigned short port) -> int;

/**
 * \brief 登録されているソケットを削除する
 * \param socketId ソケットID
 */
void RemoveSocket(int socketId);


/**
 * \brief 接続先を登録する
 * Lockされている状態だと変化しない
 * serverの場合複数追加可
 * clientの場合上書きされる
 * \param socketId ソケットID
 * \param destination 接続先IPアドレス
 */
void Connect(int socketId, const char* destination);


/**
 * \brief データの送信を行う
 * \param socketId ソケットID
 * \param packet 送信するデータ
 * \return 成功したら真
 */
auto Send(int socketId, std::shared_ptr<IPacket> packet) -> bool;

/**
 * \brief 受信したデータリストの先頭のデータを取得　ない場合はnullptr
 * \param socketId ソケットID
 * \return 受信したデータ
 */
auto PopReceivePacket(int socketId) -> std::shared_ptr<IPacket>;

/**
 * \brief 受信したデータパケットの数を取得
 * \param socketId ソケットID
 * \return 受信したデータパケットの数
 */
auto GetReceivePacketCount(int socketId) -> int;

/**
 * \brief ブロッキングモードの設定をする
 * \param socketId ソケットID
 * \param isBlocking ブロッキングを行うか
 */
void SetBlocking(int socketId, bool isBlocking);

/**
 * \brief 送受信するデータをバイナリ扱うかどうか
 * バイナリで扱う場合ユーザーが型変換行う必要がある
 * バイナリで扱わない場合PacketIdを継承したID管理パケットを使用
 * \param socketId ソケットID
 * \param isBinary バイナリで送受信を行うか
 */
void SetBinaryProtocol(int socketId, bool isBinary);


/**
 * \brief 通信相手を固定する
 * \param socketId ソケットID
 */
void Lock(int socketId);

/**
 * \brief 通信相手を変更できるようにする
 * \param socketId ソケットID
 */
void Unlock(int socketId);


/**
 * \brief IDに対応したパケットデータインスタンスを生成する
 * \param packetId パケットID
 * \return 生成したインスタンス
 */
auto CreateIdPacket(int packetId) -> std::shared_ptr<IPacket>;

/**
 * \brief ローカルのホスト名を取得する
 * \return ローカルのホスト名
 */
auto GetLocalHostName() -> std::string;

};


}
