#pragma once
#include "Socket.hpp"

#include <unordered_set>


namespace Whono::Network {

class UdpSocket : public Socket {

public:
	UdpSocket();

	void Close() final;
	void SetBlocking(bool isBlocking) const final;
	
protected:
	void ReceiveAll() final;
	void SendAll() final;
	
	void CreateLocalSocket(u_short port);
	void CreateRemoteSocket(u_short port);

	std::unordered_set<u_long> destinationSet_;
	std::unordered_set<u_long> recipientSet_;
	
private:
	SOCKET localSocket_;
	u_short localPort_;
	SOCKET remoteSocket_;
	u_short remotePort_;
};

}

