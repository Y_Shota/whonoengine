#include "Socket.hpp"

#include "IPacket.hpp"

using namespace std;



namespace Whono::Network {


Socket::Socket()
	: isLock_(false), isBinary_(false) {
}

Socket::~Socket() = default;

void Socket::Update() {

	ReceiveAll();
	SendAll();
	OtherProcess();
}

void Socket::Lock() {

	isLock_ = true;
}

void Socket::Unlock() {

	isLock_ = false;
}

void Socket::Send(shared_ptr<IPacket> packet) {

	outList_.emplace_back(packet);
}

auto Socket::FrontReceivePacket() -> shared_ptr<IPacket> {

	if (inList_.empty())	return nullptr;

	return inList_.front();
}

void Socket::PopFrontReceivePacket() {

	if (inList_.empty())	return;

	inList_.pop_front();
}

auto Socket::GetReceivePacketCount() const -> int {

	return static_cast<int>(inList_.size());
}

void Socket::SetBinaryProtocol(bool isBinary) {

	isBinary_ = isBinary;
}


}
