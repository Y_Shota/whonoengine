#pragma once
#include "UdpSocket.hpp"


namespace Whono::Network {

class UdpServerSocket final : public UdpSocket {
	
public:
	void Open(u_short port) override;
	void Connect(const char* destination) override;
	
protected:
	void OtherProcess() override;
};

}
