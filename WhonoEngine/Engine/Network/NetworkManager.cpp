#include "NetworkManager.hpp"

#include <algorithm>
#include <functional>
#include <unordered_map>
#include <WS2tcpip.h>


#include "../EnablerIfType.hpp"
#include "IPacketId.hpp"
#include "Socket.hpp"
#include "UdpClientSocket.hpp"
#include "UdpServerSocket.hpp"
#include "NetworkException.hpp"

//#include "../TestPacket.hpp"

#pragma comment(lib, "ws2_32.lib")

using namespace std;


namespace Whono::Network {

namespace NetworkManager {


WSADATA wsaData;	//Windowsでソケット通信を行うために必要

vector<unique_ptr<Socket>> sockets;	//現在登録されているソケット

//ソケットの種類によってインスタンス生成関数を切り替えるためのマップ
const unordered_map<SOCKET_MODE, function<unique_ptr<Socket>()>> SOCKET_INSTANTIATE_MAP{{
	{ SOCKET_MODE::UDP_SERVER,	make_unique<UdpServerSocket> },
	{ SOCKET_MODE::UDP_CLIENT,	make_unique<UdpClientSocket> },
}};

//ID管理パケットの種類によってインスタンス生成関数を切り替えるためのマップ
unordered_map<int, function<shared_ptr<IPacketId>()>> instantiateMap;


/**
 * \brief ユーザー定義ID管理パケットを登録する
 * \tparam PacketType ユーザー定義ID管理パケットクラス
 * \param packetId 紐づけるID 通信先と統一する必要あり
 */
template<class PacketType, EnablerIfType<is_base_of_v<IPacketId, PacketType>> = nullptr>
void RegisterIdPacket(int packetId);


void Initialize() {

	auto result = WSAStartup(MAKEWORD(2, 0), &wsaData);
	if (result != 0)	throw NetworkException("WSAStartup() : ErrorCode", result);

	//-------------------------------------------------------------------------
	//	ここにユーザー定義ID管理パケットクラスを追加
	//
	//	RegisterIdPacket<PacketType>(packetId)
	//	PacketType	PacketIDを継承したクラス
	//	packetId	紐づけるID　重複不可　通信先と統一必須
	//
	//	Example: RegisterIdPacket<TestPacket>(0);
	//
	//	紐づけしたIDは PacketType::SGetId() または object::getId() で取得可能
	//-------------------------------------------------------------------------

}

void Finalize() {

	for (auto& socket : sockets) {

		socket->Close();
		socket.reset();
	}

	auto result = WSACleanup();
	if (result != 0)	throw NetworkException("WSACleanup() : ErrorCode", WSAGetLastError());
}

void Update() {

	for (const auto& socket : sockets) {

		if (!socket)	continue;

		socket->Update();
	}
}

auto CreateSocket(SOCKET_MODE mode, unsigned short port) -> int {

	auto socket = SOCKET_INSTANTIATE_MAP.at(mode)();
	socket->Open(port);

	auto result = ranges::find_if(sockets, [](auto&& socket) { return !socket; });
	if (result != sockets.end()) {

		*result = move(socket);
		return static_cast<int>(distance(sockets.begin(), result));
	}

	sockets.emplace_back(move(socket));
	return static_cast<int>(sockets.size()) - 1;
}

void RemoveSocket(int socketId) {

	if (socketId < 0 || socketId >= static_cast<int>(sockets.size()))	return;

	sockets[socketId]->Close();
	sockets[socketId].reset();
}

void Connect(int socketId, const char* destination) {

	if (socketId < 0 || socketId >= static_cast<int>(sockets.size()))	return;
	if (!sockets[socketId])												return;

	sockets[socketId]->Connect(destination);
}

auto Send(int socketId, std::shared_ptr<IPacket> packet) -> bool {

	if (socketId < 0 || socketId >= static_cast<int>(sockets.size()))	return false;
	if (!sockets[socketId])												return false;

	sockets[socketId]->Send(packet);

	return true;
}

auto PopReceivePacket(int socketId) -> std::shared_ptr<IPacket> {

	if (socketId < 0 || socketId >= static_cast<int>(sockets.size()))	return nullptr;
	if (!sockets[socketId])												return nullptr;

	auto packet = sockets[socketId]->FrontReceivePacket();
	sockets[socketId]->PopFrontReceivePacket();

	return packet;
}

auto GetReceivePacketCount(int socketId) -> int {

	if (socketId < 0 || socketId >= static_cast<int>(sockets.size()))	return 0;
	if (!sockets[socketId])												return 0;

	return sockets[socketId]->GetReceivePacketCount();
}

void SetBlocking(int socketId, bool isBlocking) {

	if (socketId < 0 || socketId >= static_cast<int>(sockets.size()))	return;
	if (!sockets[socketId])												return;

	sockets[socketId]->SetBlocking(isBlocking);
}

void SetBinaryProtocol(int socketId, bool isBinary) {

	if (socketId < 0 || socketId >= static_cast<int>(sockets.size()))	return;
	if (!sockets[socketId])												return;

	sockets[socketId]->SetBinaryProtocol(isBinary);
}

void Lock(int socketId) {

	if (socketId < 0 || socketId >= static_cast<int>(sockets.size()))	return;
	if (!sockets[socketId])												return;

	sockets[socketId]->Lock();
}

void Unlock(int socketId) {

	if (socketId < 0 || socketId >= static_cast<int>(sockets.size()))	return;
	if (!sockets[socketId])												return;

	sockets[socketId]->Unlock();
}

auto CreateIdPacket(int packetId) -> std::shared_ptr<IPacket> {

	return instantiateMap.at(packetId)();
}

auto GetLocalHostName() -> std::string {

	string buffer;
	buffer.resize(INET_ADDRSTRLEN);
	gethostname(buffer.data(), static_cast<int>(buffer.size()));

	addrinfo hints;
	addrinfo* res;
	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_family = AF_INET;
	hints.ai_protocol = IPPROTO_IPV4;

	if (getaddrinfo(buffer.c_str(), nullptr, &hints, &res) != 0) {

		return ""s;
	}

	auto* a = res;
	while (true) {

		buffer.clear();
		buffer.resize(INET_ADDRSTRLEN);
		inet_ntop(AF_INET, &reinterpret_cast<sockaddr_in*>(a->ai_addr)->sin_addr, buffer.data(), INET_ADDRSTRLEN);

		if (a->ai_next == nullptr)	break;
		a = a->ai_next;
	}

	freeaddrinfo(res);

	return buffer;
}

template<class PacketType, EnablerIfType<is_base_of_v<IPacketId, PacketType>>>
void RegisterIdPacket(int packetId) {

	if (instantiateMap.find(packetId) != instantiateMap.end())	return;

	PacketType::SetId(packetId);
	instantiateMap[packetId] = make_shared<PacketType>;
}
	
}

}
