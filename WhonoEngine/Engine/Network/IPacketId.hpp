#pragma once
#include "IPacket.hpp"


namespace Whono::Network {

class IPacketId : public IPacket {

public:
	virtual ~IPacketId() = default;

	virtual auto GetId() -> int = 0;
};

}
