#pragma once
#include <memory>

#include "Light.hpp"


namespace Whono::GameOperation {

class LightPlain final : public Light {

private:
	DirectX::XMVECTOR direction_;
	
public:
	LightPlain();
	
	auto GetViewMatrix() -> DirectX::XMMATRIX override;
	auto GetProjectionMatrix() -> DirectX::XMMATRIX override;

	void SetDirection(DirectX::CXMVECTOR direction) noexcept;
	auto GetDirection() const noexcept -> DirectX::CXMVECTOR;

	static auto Create() -> std::unique_ptr<LightPlain>;
};

}

