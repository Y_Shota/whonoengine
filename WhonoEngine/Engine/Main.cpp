#include <iostream>

#include "DirectX11.hpp"
#include "Window.hpp"

//自作の名前空間のスコープなしでの使用を許可
using namespace Whono;


//エントリポイント
//デバッグ時のみコンソールを表示
#ifdef _DEBUG
int main() {

#else
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {

#endif

	//メッセージループ用変数
	MSG msg;
	::ZeroMemory(&msg, sizeof(msg));
	
	try {

		//ウィンドウを初期化
		Window::InitWindow(::GetModuleHandle(nullptr), SW_SHOW);

		//メインループ
		while (WM_QUIT != msg.message) {

			if (::PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {

				TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
			else {
				
				DirectX11::FrameUpdate();
			}
		}	
	}
	//何かしらの不都合が起きた時にキャッチし内容を出力
	catch (std::exception& e) {

		std::cout << e.what() << std::endl;
		std::cin.ignore();
	}
	return static_cast<int>(msg.wParam);
}