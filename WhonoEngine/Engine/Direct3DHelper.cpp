#include "Direct3DHelper.hpp"

#include "DirectX11.hpp"
#include "DirectXException.hpp"


namespace Whono::Direct3DHelper {


auto CreateVertexBuffer(void* data, UINT dataSize) noexcept(false) -> ComPtr<ID3D11Buffer> {

	ComPtr<ID3D11Buffer> vertexBuffer;

	if (dataSize % 16 || dataSize == 0)	(dataSize += 0x10u) &= ~0x0Fu;

	CD3D11_BUFFER_DESC bufferDesc(
		dataSize,
		D3D11_BIND_VERTEX_BUFFER
	);

	D3D11_SUBRESOURCE_DATA subresource{};
	subresource.pSysMem = data;
	auto hResult = DirectX11::GetD3DDevice()->CreateBuffer(&bufferDesc, &subresource, &vertexBuffer);

	if (FAILED(hResult))	throw DirectX11::DirectXException("ID3D11Device::CreateBuffer");

	return vertexBuffer;
}

auto CreateIndexBuffer(void* data, UINT dataSize) noexcept(false) -> ComPtr<ID3D11Buffer> {

	ComPtr<ID3D11Buffer> indexBuffer;

	if (dataSize % 16 || dataSize == 0)	(dataSize += 0x10u) &= ~0x0Fu;
	
	CD3D11_BUFFER_DESC bufferDesc(
		dataSize,
		D3D11_BIND_INDEX_BUFFER
	);
	
	D3D11_SUBRESOURCE_DATA subresource{};
	subresource.pSysMem = data;

	auto hResult = DirectX11::GetD3DDevice()->CreateBuffer(&bufferDesc, &subresource, &indexBuffer);
	if (FAILED(hResult))	throw DirectX11::DirectXException("ID3D11Device::CreateBuffer");
	
	return indexBuffer;
}

auto CreateConstantBuffer(UINT dataSize) noexcept(false) -> ComPtr<ID3D11Buffer> {

	ComPtr<ID3D11Buffer> constantBuffer;

	if (dataSize % 16 || dataSize == 0)	(dataSize += 0x10u) &= ~0x0Fu;
	
	CD3D11_BUFFER_DESC bufferDesc(
		dataSize,
		D3D11_BIND_CONSTANT_BUFFER,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_WRITE
	);

	auto hResult = DirectX11::GetD3DDevice()->CreateBuffer(&bufferDesc, nullptr, &constantBuffer);
	if (FAILED(hResult))	throw DirectX11::DirectXException("ID3D11Device::CreateBuffer");

	return constantBuffer;
}


}
