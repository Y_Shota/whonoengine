#pragma once
#include "IFactory.hpp"

#include <DirectXMath.h>
#include <memory>

using namespace DirectX;


namespace Whono::GameOperation {


/**
* \brief カメラの動きを管理するクラス
* \author whono
* \version 1.0
* \since 1.0
*/
class Camera final : public DesignPattern::IFactory<Camera> {

public:
	/**
	 * \brief コンストラクタ
	 */
	Camera();
	/**
	 * \brief デストラクタ
	 */
	~Camera();
	
	/**
	 * \brief カメラの状態を初期状態にリセットする
	 */
	void Reset() const;
	
	/**
	 * \brief ビュー変換行列を取得
	 * \return ビュー変換行列
	 */
	[[nodiscard]]
	auto GetViewMatrix() const noexcept -> CXMMATRIX;
	/**
	 * \brief プロジェクション変換行列を取得
	 * \return プロジェクション行列
	 */
	[[nodiscard]]
	auto GetProjectionMatrix() const noexcept -> CXMMATRIX;
	
	/**
	 * \brief カメラの座標を取得
	 * \return 座標
	 */
	[[nodiscard]]
	auto GetPosition() const noexcept -> CXMVECTOR;
	/**
	 * \brief カメラの注視座標を取得
	 * \return 座標
	 */
	[[nodiscard]]
	auto GetTargetPosition() const noexcept -> CXMVECTOR;
	/**
	 * \brief カメラの姿勢を取得
	 * \return 姿勢
	 */
	[[nodiscard]]
	auto GetOriented() const noexcept -> CXMVECTOR;
	/**
	 * \brief y軸回転を取得する
	 * \return y軸回転量
	 */
	[[nodiscard]]
	auto GetPitch() const -> float;
	
	/**
	 * \brief カメラの座標を設定
	 * \param position 座標
	 * \return 変更後のインスタンス
	 */
	auto SetPosition(CXMVECTOR position) -> Camera*;
	/**
	 * \brief カメラの座標を設定
	 * \param x X座標
	 * \param y Y座標
	 * \param z Z座標
	 * \return 変更後のインスタンス
	 */
	auto SetPosition(float x, float y, float z) -> Camera*;
	/**
	 * \brief カメラの注視点を設定
	 * \param target 座標
	 * \return 変更後のインスタンス
	 */
	auto SetTargetPosition(CXMVECTOR target) -> Camera*;
	/**
	 * \brief カメラの注視点を設定
	 * \param x X座標
	 * \param y Y座標
	 * \param z Z座標
	 * \return 変更後のインスタンス変更後のインスタンス
	 */
	auto SetTargetPosition(float x, float y, float z) -> Camera*;
	/**
	 * \brief カメラの姿勢を設定
	 * \param oriented 姿勢クォータニオン
	 * \return 変更後のインスタンス
	 */
	auto SetOriented(CXMVECTOR oriented) -> Camera*;
	/**
	 * \brief カメラの姿勢を設定
	 * \param axis 回転軸
	 * \param angleRadians 回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetOrientedAxisRadians(CXMVECTOR axis, float angleRadians) -> Camera*;
	/**
	 * \brief カメラの姿勢を設定
	 * \param axis 回転軸
	 * \param angleDegree 回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetOrientedAxis(CXMVECTOR axis, float angleDegree) -> Camera*;
	/**
	 * \brief カメラの回転情報を設定
	 * \param rotation オイラー角 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetRotationRadians(CXMVECTOR rotation) -> Camera*;
	/**
	 * \brief カメラの回転情報を設定
	 * \param x X軸回転量 (ラジアン)
	 * \param y Y軸回転量 (ラジアン)
	 * \param z Z軸回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetRotationRadians(float x, float y, float z) -> Camera*;
	/**
	 * \brief カメラの回転情報を設定
	 * \param rotation オイラー角 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetRotation(CXMVECTOR rotation) -> Camera*;
	/**
	 * \brief カメラの回転情報を設定
	 * \param x X軸回転量 (度)
	 * \param y Y軸回転量 (度)
	 * \param z Z軸回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetRotation(float x, float y, float z) -> Camera*;
	/**
	 * \brief 公転の状態を設定
	 * \param quaternion 姿勢
	 * \return 変更後のインスタンス
	 */
	auto SetRevolutionQuaternion(CXMVECTOR quaternion) -> Camera*;
	/**
	 * \brief 公転の状態を設定
	 * \param axis 姿勢の回転軸
	 * \param angleRadians 回転量（ラジアン）
	 * \return 変更後のインスタンス
	 */
	auto SetRevolutionQuaternionAxisRadians(CXMVECTOR axis, float angleRadians) -> Camera*;
	/**
	 * \brief 公転の状態を設定
	 * \param axis 姿勢の回転軸
	 * \param angleDegrees 回転軸（度）
	 * \return 変更後のインスタンス
	 */
	auto SetRevolutionQuaternionAxis(CXMVECTOR axis, float angleDegrees) -> Camera*;
	/**
	 * \brief 公転の状態を設定
	 * \param revolution オイラー角（ラジアン）
	 * \return 変更後のインスタンス
	 */
	auto SetRevolutionRadians(CXMVECTOR revolution) -> Camera*;
	/**
	 * \brief 公転の状態を設定
	 * \param x X軸回転量 (ラジアン)
	 * \param y Y軸回転量 (ラジアン)
	 * \param z Z軸回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto SetRevolutionRadians(float x, float y, float z) -> Camera*;
	/**
	 * \brief 公転の状態を設定
	 * \param revolution オイラー角（度）
	 * \return 変更後のインスタンス
	 */
	auto SetRevolution(CXMVECTOR revolution) -> Camera*;
	/**
	 * \brief 公転の状態を設定
	 * \param x X軸回転量 (度)
	 * \param y Y軸回転量 (度)
	 * \param z Z軸回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto SetRevolution(float x, float y, float z) -> Camera*;
	/**
	 * \brief 視野角を設定	
	 * \param fovAngleRadians 視野角（ラジアン）
	 * \return 変更後のインスタンス
	 */
	auto SetFovAngleRadians(float fovAngleRadians) -> Camera*;
	/**
	 * \brief 視野角を設定
	 * \param fovAngleDegrees 視野角（度）
	 * \return 変更後のインスタンス変更後のインスタンス
	 */
	auto SetFovAngle(float fovAngleDegrees) -> Camera*;
	/**
	 * \brief 描画距離の設定
	 * \param nearZ 最小距離
	 * \param farZ 最大距離
	 * \return 変更後のインスタンス
	 */
	auto SetClippingPlanes(float nearZ, float farZ) -> Camera*;	
	/**
	 * \brief 画角（ビューポート）の設定
	 * \param topLeftX ウィンドウのX開始地点
	 * \param topLeftY ウィンドウのY開始地点
	 * \param width 幅
	 * \param height 高さ
	 * \return 変更後のインスタンス
	 */
	auto SetViewport(float topLeftX, float topLeftY, float width, float height) -> Camera*;
	
	/**
	 * \brief カメラの座標を調整
	 * \param position 座標
	 * \return 変更後のインスタンス
	 */
	auto AdjustPosition(CXMVECTOR position) -> Camera*;
	/**
	 * \brief カメラの座標を調整
	 * \param x X座標
	 * \param y Y座標
	 * \param z Z座標
	 * \return 変更後のインスタンス
	 */
	auto AdjustPosition(float x, float y, float z) -> Camera*;
	/**
	 * \brief カメラの注視点を調整
	 * \param target 座標
	 * \return 変更後のインスタンス
	 */
	auto AdjustTargetPosition(CXMVECTOR target) -> Camera*;
	/**
	 * \brief カメラの注視点を調整
	 * \param x X座標
	 * \param y Y座標
	 * \param z Z座標
	 * \return 変更後のインスタンス
	 */
	auto AdjustTargetPosition(float x, float y, float z) -> Camera*;
	/**
	 * \brief カメラの姿勢を調整
	 * \param oriented 姿勢
	 * \return 変更後のインスタンス
	 */
	auto AdjustOriented(CXMVECTOR oriented) -> Camera*;
	/**
	 * \brief カメラの姿勢を調整
	 * \param axis 姿勢の回転軸
	 * \param angleRadians 回転量（ラジアン）
	 * \return 変更後のインスタンス
	 */
	auto AdjustOrientedAxisRadians(CXMVECTOR axis, float angleRadians) -> Camera*;
	/**
	 * \brief カメラの姿勢を調整
	 * \param axis 姿勢の回転軸
	 * \param angleDegree 回転量（度）
	 * \return 変更後のインスタンス
	 */
	auto AdjustOrientedAxis(CXMVECTOR axis, float angleDegree) -> Camera*;
	/**
	 * \brief カメラの姿勢を調整
	 * \param rotation オイラー角（ラジアン）
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationRadians(CXMVECTOR rotation) -> Camera*;
	/**
	 * \brief カメラの姿勢を調整
	 * \param x X軸回転量 (ラジアン)
	 * \param y Y軸回転量 (ラジアン)
	 * \param z Z軸回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotationRadians(float x, float y, float z) -> Camera*;
	/**
	 * \brief カメラの姿勢を調整
	 * \param rotation オイラー角（度）
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotation(CXMVECTOR rotation) -> Camera*;
	/**
	 * \brief カメラの姿勢を調整
	 * \param x X軸回転量 (度)
	 * \param y Y軸回転量 (度)
	 * \param z Z軸回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRotation(float x, float y, float z) -> Camera*;
	/**
	 * \brief 公転の状態を調整
	 * \param quaternion 姿勢
	 * \return 変更後のインスタンス
	 */
	auto AdjustRevolutionQuaternion(CXMVECTOR quaternion) -> Camera*;
	/**
	 * \brief 公転の状態を調整
	 * \param axis 姿勢の回転軸
	 * \param angleRadians 回転量（ラジアン）
	 * \return 変更後のインスタンス
	 */
	auto AdjustRevolutionQuaternionAxisRadians(CXMVECTOR axis, float angleRadians) -> Camera*;
	/**
	 * \brief 公転の状態を調整
	 * \param axis 姿勢の回転軸
	 * \param angleDegrees 回転量（度）
	 * \return 
	 */
	auto AdjustRevolutionQuaternionAxis(CXMVECTOR axis, float angleDegrees) -> Camera*;
	/**
	 * \brief 公転の状態を調整
	 * \param revolution オイラー角（ラジアン）
	 * \return 変更後のインスタンス
	 */
	auto AdjustRevolutionRadians(CXMVECTOR revolution) -> Camera*;
	/**
	 * \brief 公転の状態を調整
	 * \param x X軸回転量 (ラジアン)
	 * \param y Y軸回転量 (ラジアン)
	 * \param z Z軸回転量 (ラジアン)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRevolutionRadians(float x, float y, float z) -> Camera*;
	/**
	 * \brief 公転の状態を調整
	 * \param revolution オイラー角（度）
	 * \return 変更後のインスタンス
	 */
	auto AdjustRevolution(CXMVECTOR revolution) -> Camera*;
	/**
	 * \brief 公転の状態を調整
	 * \param x X軸回転量 (度)
	 * \param y Y軸回転量 (度)
	 * \param z Z軸回転量 (度)
	 * \return 変更後のインスタンス
	 */
	auto AdjustRevolution(float x, float y, float z) -> Camera*;
	/**
	 * \brief 視野角を調整
	 * \param fovAngleRadians 視野角（ラジアン）
	 * \return 変更後のインスタンス
	 */
	auto AdjustFovAngleRadians(float fovAngleRadians) -> Camera*;
	/**
	 * \brief 視野角を調整
	 * \param fovAngleDegrees 視野角（度）
	 * \return 変更後のインスタンス
	 */
	auto AdjustFovAngle(float fovAngleDegrees) -> Camera*;
	/**
	 * \brief 描画距離を調整
	 * \param nearZ 動かす量
	 * \param farZ 動かす量
	 * \return 変更後のインスタンス
	 */
	auto AdjustClippingPlanes(float nearZ, float farZ) -> Camera*;

	/**
	 * \brief パラメータと自身の座標でラープする
	 * \param target ターゲット
	 * \param rate レート
	 * \return 変更後のインスタンス
	 */
	auto LerpPosition(CXMVECTOR target, float rate) -> Camera*;

	/**
	 * \brief パラーメータと自身の姿勢でスラープする
	 * \param target ターゲット
	 * \param rate レート
	 * \return 変更後のインスタンス
	 */
	auto SlerpOriented(CXMVECTOR target, float rate) -> Camera*;
	/**
	 * \brief パラーメータと自身の姿勢で公転スラープする
	 * \param target ターゲット
	 * \param rate レート
	 * \return 変更後のインスタンス
	 */
	auto SlerpRevolution(CXMVECTOR target, float rate) -> Camera*;

	/**
	 * \brief デバイスコンテキストへビューポートを設定
	 */
	void SetContextViewport() const;
	
	/**
	 * \brief 現在処理中のカメラを切り替える
	 * \param camera 処理したいカメラ
	 */
	static void SetCurrentCamera(Camera* camera) noexcept;
	/**
	 * \brief 現在処理中のカメラを取得
	 */
	static auto GetCurrentCamera() noexcept -> Camera*;

	/**
	 * \brief ウィンドウサイズのずれに合わせてビューポートを更新する
	 */
	void UpdateViewport() const;
	
private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_; //内部実装隠蔽
};

using CameraPtr = std::unique_ptr<Camera>;

}


