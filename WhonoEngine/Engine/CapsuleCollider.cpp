#include "CapsuleCollider.hpp"

#include <array>

#include "CollisionCalculator.hpp"
#include "GameObject.hpp"
#include "GraphicsPipeline.hpp"
#include "Model.hpp"

using namespace std;
using namespace DirectX;


namespace Whono::GameOperation {


struct CapsuleCollider::Impl {

	/**
	 * \brief 境界線を描画する
	 */
	void Render() const;
	/**
	 * \brief 線分を取得する
	 * \param segment1 線分の端点１
	 * \param segment2 線分の端点２
	 */
	void GetSegment(XMVECTOR* segment1, XMVECTOR* segment2) const;
	
	XMVECTOR center;		//中心座標
	float radius;			//半径
	float height;			//高さ
	XMVECTOR oriented;		//姿勢
	GameObject* gameObject;	//所有者
	array<int, 3> hModels;	//モデルハンドル
};

CapsuleCollider::CapsuleCollider(CXMVECTOR position, float radius, float height, CXMVECTOR oriented) {

	pImpl_ = make_unique<Impl>();
	pImpl_->center = position;
	pImpl_->radius = radius;
	pImpl_->height = height < radius * 2 ? radius * 2 : height;
	pImpl_->oriented = oriented;
	pImpl_->gameObject = nullptr;
	pImpl_->hModels.fill(-1);

	pImpl_->hModels[0] = Model::Load("Debug/Collision/half_sphere_top.fbx");
	Model::SetPipeline(pImpl_->hModels[0], PIPELINE_TYPE::DEBUG_3D);
	pImpl_->hModels[1] = Model::Load("Debug/Collision/half_sphere_bottom.fbx");
	Model::SetPipeline(pImpl_->hModels[1], PIPELINE_TYPE::DEBUG_3D);
	pImpl_->hModels[2] = Model::Load("Debug/Collision/bottomless_cylinder.fbx");
	Model::SetPipeline(pImpl_->hModels[2], PIPELINE_TYPE::DEBUG_3D);
}

CapsuleCollider::~CapsuleCollider() = default;

auto CapsuleCollider::GetCenterPosition() const -> XMVECTOR {

	return Model::GetCenterPosition(pImpl_->hModels[2]);
}

auto CapsuleCollider::GetNearestControlPoint(CXMVECTOR point) const -> XMVECTOR {

	array<XMVECTOR, 3> controlPoints;
	controlPoints[0] = GetCenterPosition();
	GetSegment(&controlPoints[1], &controlPoints[2]);

	auto compare = [&point](auto&& a, auto&& b) {

		return XMVector3Length(a - point).m128_f32[0] < XMVector3Length(b - point).m128_f32[0];
	};
	auto result = ranges::min_element(controlPoints, compare);

	return *result;
}

auto CapsuleCollider::GetProjectionLengthMax(CXMVECTOR vector) const -> float {

	//todo 
	return 0.f;
}

void CapsuleCollider::SetGameObject(GameObject* gameObject) {

	pImpl_->gameObject = gameObject;

	for (auto hModel : pImpl_->hModels) {

		Model::SetLinkedTransform(hModel, gameObject->GetTransform());
	}
}

void CapsuleCollider::Render() const {

	pImpl_->Render();
}

auto CapsuleCollider::IsHit(ICollider* target) const -> bool {
	
	if (auto* sphere = dynamic_cast<SphereCollider*>(target)) {

		return CollisionCalculator::IsHit(this, sphere);
	}
	if (auto* box = dynamic_cast<BoxCollider*>(target)) {

		return CollisionCalculator::IsHit(this, box);
	}
	if (auto* obb = dynamic_cast<ObbCollider*>(target)) {

		return CollisionCalculator::IsHit(this, obb);
	}
	if (auto* frustum = dynamic_cast<FrustumCollider*>(target)) {

		return CollisionCalculator::IsHit(this, frustum);
	}
	if (auto* capsule = dynamic_cast<CapsuleCollider*>(target)) {

		return CollisionCalculator::IsHit(this, capsule);
	}
	return false;
}

auto CapsuleCollider::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool {

	auto isHitEvenOnce = false;

	for (auto i = 0u; i < pImpl_->hModels.size(); ++i) {

		auto distanceBuffer = numeric_limits<float>::max();
		auto normalBuffer = XMVectorZero();
		
		if (Model::RayCast(pImpl_->hModels[i], origin, direction, &distanceBuffer, &normalBuffer)) {

			isHitEvenOnce = true;
			if (*dist > distanceBuffer) {

				*dist = distanceBuffer;
				*normal = normalBuffer;
			}
		}
	}
	return isHitEvenOnce;
}

void CapsuleCollider::GetSegment(XMVECTOR* segment1, XMVECTOR* segment2) const {

	pImpl_->GetSegment(segment1, segment2);
}

auto CapsuleCollider::GetRadius() const -> float {

	auto scale = pImpl_->gameObject->GetTransform()->GetWorldScale();
	auto scaling = scale.m128_f32[0];
	
	//半径は所有者のXのスケールの影響を受ける
	return pImpl_->radius * scaling;
}

auto CapsuleCollider::Create(CXMVECTOR position, float radius, float height, CXMVECTOR oriented) -> unique_ptr<CapsuleCollider> {

	return make_unique<CapsuleCollider>(position, radius, height, oriented);
}

auto CapsuleCollider::Create(float x, float y, float z, float radius, float height, CXMVECTOR oriented) -> unique_ptr<CapsuleCollider> {

	return Create(XMVectorSet(x, y, z, 0), radius, height, oriented);
}

auto CapsuleCollider::Create(CXMVECTOR position, float radius, float height, CXMVECTOR axis, float angle) -> unique_ptr<CapsuleCollider> {

	return Create(position, radius, height, XMQuaternionRotationAxis(axis, angle));
}

auto CapsuleCollider::Create(float x, float y, float z, float radius, float height, CXMVECTOR axis, float angle) -> unique_ptr<CapsuleCollider> {

	return Create(XMVectorSet(x, y, z, 0), radius, height, axis, angle);
}

auto CapsuleCollider::Create(CXMVECTOR position, float radius, float height, float rotateX, float rotateY, float rotateZ) -> unique_ptr<CapsuleCollider> {

	return Create(position, radius, height, XMQuaternionRotationRollPitchYaw(rotateX, rotateY, rotateZ));
}

auto CapsuleCollider::Create(float x, float y, float z, float radius, float height, float rotateX, float rotateY, float rotateZ) -> unique_ptr<CapsuleCollider> {

	return Create(XMVectorSet(x, y, z, 0), radius, height, rotateX, rotateY, rotateZ);
}

void CapsuleCollider::Impl::Render() const {

	//必要な変形情報の取得
	auto objectScale = gameObject->GetTransform()->GetWorldScale();
	auto* modelTransform0 = Model::GetTransform(hModels[0]);
	auto* modelTransform1 = Model::GetTransform(hModels[1]);
	auto* modelTransform2 = Model::GetTransform(hModels[2]);

	//中心座標から球の軌跡の端までの距離
	auto offset = height / 2 - radius;

	auto segment1 = XMVectorZero();
	auto segment2 = XMVectorZero();

	GetSegment(&segment1, &segment2);
	
	//各種変形情報を更新
	modelTransform0
		->SetWorldPosition(segment1)
		->SetLocalOriented(oriented)
		->SetWorldScale(XMVectorReplicate(radius * objectScale.m128_f32[0]));

	modelTransform1
		->SetWorldPosition(segment2)
		->SetLocalOriented(oriented)
		->SetWorldScale(XMVectorReplicate(radius * objectScale.m128_f32[0]));
	
	modelTransform2
		->SetLocalPosition(center)
		->SetLocalOriented(oriented)
		->SetWorldScale(
			radius * objectScale.m128_f32[0],
			offset * objectScale.m128_f32[0],
			radius * objectScale.m128_f32[0]);

	//描画
#ifdef _DEBUG

	for (auto hModel : hModels) {

		Model::Render(hModel);
	}
	
#endif
}

void CapsuleCollider::Impl::GetSegment(XMVECTOR* segment1, XMVECTOR* segment2) const {
	
	auto y = height / 2 - radius;
	auto* objectTransform = gameObject->GetTransform();
	
	auto offset = XMVectorSet(0, y, 0, 0) * objectTransform->GetWorldScale().m128_f32[0];
	offset = XMVector3Rotate(offset, objectTransform->GetWorldOriented());
	offset = XMVector3Rotate(offset, oriented);
	auto center = XMVector3Transform(this->center, objectTransform->GetWorldMatrix());

	*segment1 = center + offset;
	*segment2 = center - offset;
}

}
