#pragma once
#include <memory>
#include <fbxsdk.h>

#include "Texture.hpp"


namespace Whono::RenderObject {

class FbxMaterialData final {

public:
	FbxMaterialData();
	~FbxMaterialData();

	void Initialize(FbxSurfaceMaterial* surfaceMaterial) const;

	auto GetName() const noexcept -> const std::string&;
	auto GetUniqueId() const noexcept -> uint64_t;
	auto GetAmbient() const noexcept -> DirectX::CXMVECTOR;
	auto GetDiffuse() const noexcept -> DirectX::CXMVECTOR;
	auto GetEmissive() const noexcept -> DirectX::CXMVECTOR;
	auto GetTransparent() const noexcept -> float;
	auto GetSpecular() const noexcept -> DirectX::CXMVECTOR;
	auto GetShininess() const noexcept -> float;
	auto GetReflection() const noexcept -> float;

	auto GetTexture(const std::string& key) const -> Texture*;

	void Release();

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;
};


}
