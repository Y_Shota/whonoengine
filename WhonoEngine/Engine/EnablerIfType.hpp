#pragma once
#include <type_traits>

namespace Whono {

template<bool Condition>
using EnablerIfType = std::enable_if_t<Condition, nullptr_t>;

}
