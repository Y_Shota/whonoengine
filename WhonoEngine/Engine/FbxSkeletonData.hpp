#pragma once
#include <DirectXMath.h>
#include <memory>
#include <fbxsdk.h>


namespace Whono::RenderObject {


class FbxSkeletonData {

public:
	FbxSkeletonData();
	~FbxSkeletonData();

	void Initialize(FbxNode* skeletonNode) const;

	auto GetName() const noexcept -> const std::string&;
	auto GetGlobalTransform(int frame = -1) const -> DirectX::XMMATRIX;
	auto GetGlobalFbxTransform(int frame = -1) const -> const FbxAMatrix&;
	auto GetGlobalPosition(int frame = -1) const -> DirectX::XMVECTOR;
	auto GetUniqueId() const -> uint64_t;

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;
};
	
}



