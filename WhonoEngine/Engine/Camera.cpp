#include "Camera.hpp"


#include <algorithm>
#include <DirectXMath.h>
#include <d3d11_1.h>
#include <iostream>


#include "DirectX11.hpp"
#include "Math/Math.hpp"
#include "Window.hpp"

using namespace std;
using namespace DirectX;


namespace Whono::GameOperation {

//基本となる方向
constexpr XMVECTOR DEFAULT_RIGHT	= { 1.f, 0, 0, 0 };
constexpr XMVECTOR DEFAULT_UP		= { 0, 1.f, 0, 0 };
constexpr XMVECTOR DEFAULT_FORWARD	= { 0, 0, 1.f, 0 };

//処理中のカメラ
Camera* currentCamera = nullptr;

struct Camera::Impl {

	/**
	 * \brief 初期化処理
	 */
	void Initialize();
	/**
	 * \brief ビュー行列を更新する
	 */
	void UpdateViewMatrix();
	/**
	 * \brief プロジェクション行列を更新する
	 */
	void UpdateProjectionMatrix();
	/**
	 * \brief 注視点を更新する
	 */
	void UpdateTargetPosition();
	/**
	 * \brief 姿勢を更新する
	 */
	void UpdateOriented();
	/**
	 * \brief ウィンドウサイズのずれに合わせてビューポートを更新する
	 */
	void UpdateViewport();

	XMVECTOR		position	= DEFAULT_FORWARD * -10.f;	//座標
	XMVECTOR		target		= XMVectorZero();			//注視点
	XMVECTOR		oriented	= XMQuaternionIdentity();	//姿勢
	float			fovAngle	= XM_PIDIV4;				//視野角
	float			aspectRatio	= Window::GetAspectRatio();	//アスペクト比
	D3D11_VIEWPORT	viewport	= { 0, 0, Window::GetWindowWidthF(), Window::GetWindowHeightF(), 0, 1.f };	//描画範囲
	float			nearZ		= 0.1f;						//最小描画距離
	float			farZ		= 1000.f;					//最大描画距離
	
	XMMATRIX matrixView			= XMMatrixIdentity();		//ビュー行列
	XMMATRIX matrixProjection	= XMMatrixIdentity();		//プロジェクション行列
};


Camera::Camera() {
	
	pImpl_ = make_unique<Impl>();
	Reset();
}

Camera::~Camera() = default;

void Camera::Reset() const {

	pImpl_->Initialize();
}

auto Camera::GetViewMatrix() const noexcept -> CXMMATRIX {

	return pImpl_->matrixView;
}

auto Camera::GetProjectionMatrix() const noexcept -> CXMMATRIX {

	return pImpl_->matrixProjection;
}

auto Camera::GetPosition() const noexcept -> CXMVECTOR {

	return pImpl_->position;
}

auto Camera::GetTargetPosition() const noexcept -> CXMVECTOR {

	return pImpl_->target;
}

auto Camera::GetOriented() const noexcept -> CXMVECTOR {

	return pImpl_->oriented;
}

auto Camera::GetPitch() const -> float {

	const auto& q = pImpl_->oriented;
	
	//よくわからないけどy軸に関するΘが取れる
	auto sinTheta = 2 * (q.m128_f32[3] * q.m128_f32[1] - q.m128_f32[0] * q.m128_f32[2]);
	auto targetToEyeVec = pImpl_->target - pImpl_->position;

	if (targetToEyeVec.m128_f32[2] >= 0) {

		return asin(sinTheta);
	}
	return copysign(XM_PI, sinTheta) - asin(sinTheta);
}

auto Camera::SetPosition(CXMVECTOR position) -> Camera* {

	if (XMVector3NotEqual(pImpl_->position, position)) {

		//カメラが移動したら注視点も同期して移動
		pImpl_->target += position - pImpl_->position;
		pImpl_->position = position;

		//ビュー行列を更新
		pImpl_->UpdateViewMatrix();
	}
	return this;
}

auto Camera::SetPosition(float x, float y, float z) -> Camera* {

	return SetPosition(XMVectorSet(x, y, z, 0));
}

auto Camera::SetTargetPosition(CXMVECTOR target) -> Camera* {

	if (XMVector3NotEqual(pImpl_->target, target)) {
		
		pImpl_->target = target;

		//注視点が変わると姿勢も変化するので更新
		pImpl_->UpdateOriented();

		//ビュー行列を更新
		pImpl_->UpdateViewMatrix();
	}
	return this;
}

auto Camera::SetTargetPosition(float x, float y, float z) -> Camera* {

	return SetTargetPosition(XMVectorSet(x, y, z, 0));
}

auto Camera::SetOriented(CXMVECTOR oriented) -> Camera* {

	if (XMQuaternionNotEqual(pImpl_->oriented, oriented)) {

		pImpl_->oriented = oriented;

		//姿勢が変化すると注視点も変化するので更新
		pImpl_->UpdateTargetPosition();

		//ビュー行列を更新
		pImpl_->UpdateViewMatrix();
	}
	return this;
}

auto Camera::SetOrientedAxisRadians(CXMVECTOR axis, float angleRadians) -> Camera* {

	return SetOriented(XMQuaternionRotationAxis(axis, angleRadians));
}

auto Camera::SetOrientedAxis(CXMVECTOR axis, float angleDegree) -> Camera* {

	return SetOrientedAxisRadians(axis, XMConvertToRadians(angleDegree));
}

auto Camera::SetRotationRadians(CXMVECTOR rotation) -> Camera* {

	return SetOriented(XMQuaternionRotationRollPitchYawFromVector(rotation));	
}

auto Camera::SetRotationRadians(float x, float y, float z) -> Camera* {

	return SetRotationRadians(XMVectorSet(x, y, z, 0));
}

auto Camera::SetRotation(CXMVECTOR rotation) -> Camera* {

	return SetRotationRadians(Math::ConvertToRadiansVector(rotation));
}

auto Camera::SetRotation(float x, float y, float z) -> Camera* {

	return SetRotation(XMVectorSet(x, y, z, 0));
}

auto Camera::SetRevolutionQuaternion(CXMVECTOR quaternion) -> Camera* {

	if (XMQuaternionNotEqual(pImpl_->oriented, quaternion)) {

		//現在の注視点から視点へのベクトルを作成
		auto targetToEyeVec = pImpl_->position - pImpl_->target;
		//長さを抽出し回転させ新たな注視点から視点へのベクトルを作成
		targetToEyeVec = XMVector3Rotate(-DEFAULT_FORWARD * XMVector3Length(targetToEyeVec), quaternion);

		//視点の場所と姿勢を更新
		pImpl_->position = targetToEyeVec + pImpl_->target;
		pImpl_->oriented = quaternion;

		//ビュー行列を更新
		pImpl_->UpdateViewMatrix();
	}
	return this;
}

auto Camera::SetRevolutionQuaternionAxisRadians(CXMVECTOR axis, float angleRadians) -> Camera* {

	return SetRevolutionQuaternion(XMQuaternionRotationAxis(axis, angleRadians));
}

auto Camera::SetRevolutionQuaternionAxis(CXMVECTOR axis, float angleDegrees) -> Camera* {

	return SetRevolutionQuaternionAxisRadians(axis, XMConvertToRadians(angleDegrees));
}

auto Camera::SetRevolutionRadians(CXMVECTOR revolution) -> Camera* {

	return SetRevolutionQuaternion(XMQuaternionRotationRollPitchYawFromVector(revolution));
}

auto Camera::SetRevolutionRadians(float x, float y, float z) -> Camera* {

	return SetRevolutionRadians(XMVectorSet(x, y, z, 0));
}

auto Camera::SetRevolution(CXMVECTOR revolution) -> Camera* {

	return SetRevolutionRadians(Math::ConvertToRadiansVector(revolution));
}

auto Camera::SetRevolution(float x, float y, float z) -> Camera* {

	return SetRevolution(XMVectorSet(x, y, z, 0));
}

auto Camera::SetFovAngleRadians(float fovAngleRadians) -> Camera* {

	if (pImpl_->fovAngle != fovAngleRadians) {

		pImpl_->fovAngle = fovAngleRadians;

		//プロジェクション行列を更新
		pImpl_->UpdateProjectionMatrix();
	}
	return this;
}

auto Camera::SetFovAngle(float fovAngleDegrees) -> Camera* {

	return SetFovAngleRadians(XMConvertToRadians(fovAngleDegrees));
}

auto Camera::SetClippingPlanes(float nearZ, float farZ) -> Camera* {

	pImpl_->nearZ = nearZ;
	pImpl_->farZ = farZ;

	//プロジェクション行列を更新
	pImpl_->UpdateProjectionMatrix();

	return this;
}

auto Camera::SetViewport(float topLeftX, float topLeftY, float width, float height) -> Camera* {

	pImpl_->aspectRatio = width / height;
	pImpl_->viewport = { topLeftX, topLeftY, width, height, 0, 1.f };

	//プロジェクション行列を更新
	pImpl_->UpdateProjectionMatrix();

	return this;
}

auto Camera::AdjustPosition(CXMVECTOR position) -> Camera* {

	if (XMVector3NotEqual(position, g_XMZero)) {

		pImpl_->position += position;
		pImpl_->target += position;

		//ビュー行列を更新
		pImpl_->UpdateViewMatrix();
	}
	return this;
}

auto Camera::AdjustPosition(float x, float y, float z) -> Camera* {

	return AdjustPosition(XMVectorSet(x, y, z, 0));
}

auto Camera::AdjustTargetPosition(CXMVECTOR target) -> Camera* {

	if (XMVector3NotEqual(target, g_XMZero)) {

		pImpl_->target += target;

		//注視点が変化すると姿勢も変化するので更新
		pImpl_->UpdateOriented();

		//ビュー行列を更新
		pImpl_->UpdateViewMatrix();
	}
	return this;
}

auto Camera::AdjustTargetPosition(float x, float y, float z)->Camera* {

	return AdjustTargetPosition(XMVectorSet(x, y, z, 0));
}

auto Camera::AdjustOriented(CXMVECTOR oriented) -> Camera* {

	if (!XMQuaternionIsIdentity(oriented)) {

		//姿勢はクォータニオンの掛け算
		pImpl_->oriented = XMQuaternionMultiply(pImpl_->oriented, oriented);

		//姿勢が変化すると注視点も変化するので更新
		pImpl_->UpdateTargetPosition();

		//ビュー行列を更新
		pImpl_->UpdateViewMatrix();
	}
	return this;
}

auto Camera::AdjustOrientedAxisRadians(CXMVECTOR axis, float angleRadians) -> Camera* {

	return AdjustOriented(XMQuaternionRotationAxis(axis, angleRadians));	
}

auto Camera::AdjustOrientedAxis(CXMVECTOR axis, float angleDegree) -> Camera* {

	return AdjustOrientedAxisRadians(axis, XMConvertToRadians(angleDegree));
}

auto Camera::AdjustRotationRadians(CXMVECTOR rotation) -> Camera* {

	auto axisX = XMVector3Rotate(DEFAULT_RIGHT, pImpl_->oriented);
	auto axisZ = XMVector3Rotate(DEFAULT_FORWARD, pImpl_->oriented);

	auto quaternion = XMQuaternionRotationAxis(axisX, rotation.m128_f32[0]);
	quaternion = XMQuaternionMultiply(quaternion, XMQuaternionRotationAxis(axisZ, rotation.m128_f32[2]));
	quaternion = XMQuaternionMultiply(quaternion, XMQuaternionRotationAxis(DEFAULT_UP, rotation.m128_f32[1]));

	return AdjustOriented(quaternion);
}

auto Camera::AdjustRotationRadians(float x, float y, float z) -> Camera* {

	return AdjustRotationRadians(XMVectorSet(x, y, z, 0));
}

auto Camera::AdjustRotation(CXMVECTOR rotation) -> Camera* {

	return AdjustRotationRadians(Math::ConvertToRadiansVector(rotation));
}

auto Camera::AdjustRotation(float x, float y, float z) -> Camera* {

	return AdjustRotation(XMVectorSet(x, y, z, 0));
}

auto Camera::AdjustRevolutionQuaternion(CXMVECTOR quaternion) -> Camera* {

	if (!XMQuaternionIsIdentity(quaternion)) {

		//現在の注視点から視点へのベクトルを作成し回転し視点を移動させる
		auto targetToEyeVec = pImpl_->position - pImpl_->target;
		targetToEyeVec = XMVector3Rotate(targetToEyeVec, quaternion);
		pImpl_->position = targetToEyeVec + pImpl_->target;
		pImpl_->oriented = XMQuaternionMultiply(pImpl_->oriented, quaternion);

		//ビュー行列を更新
		pImpl_->UpdateViewMatrix();
	}
	return this;
}
auto Camera::AdjustRevolutionQuaternionAxisRadians(CXMVECTOR axis, float angleRadians) -> Camera* {

	return AdjustRevolutionQuaternion(XMQuaternionRotationAxis(axis, angleRadians));
}

auto Camera::AdjustRevolutionQuaternionAxis(CXMVECTOR axis, float angleDegrees) -> Camera* {

	return AdjustRevolutionQuaternionAxisRadians(axis, XMConvertToRadians(angleDegrees));
}

auto Camera::AdjustRevolutionRadians(CXMVECTOR revolution) -> Camera* {

	return AdjustRevolutionQuaternion(XMQuaternionRotationRollPitchYawFromVector(revolution));
}

auto Camera::AdjustRevolutionRadians(float x, float y, float z) -> Camera* {

	return AdjustRevolutionRadians(XMVectorSet(x, y, z, 0));
}

auto Camera::AdjustRevolution(CXMVECTOR revolution) -> Camera* {

	return  AdjustRevolutionRadians(Math::ConvertToRadiansVector(revolution));
}

auto Camera::AdjustRevolution(float x, float y, float z) -> Camera* {

	return AdjustRevolution(XMVectorSet(x, y, z, 0));
}

auto Camera::AdjustFovAngleRadians(float fovAngleRadians) -> Camera* {

	if (fovAngleRadians == 0.f) {

		pImpl_->fovAngle += fovAngleRadians;

		//プロジェクション行列を更新
		pImpl_->UpdateProjectionMatrix();
	}
	return this;
}

auto Camera::AdjustFovAngle(float fovAngleDegrees) -> Camera* {

	return AdjustFovAngleRadians(XMConvertToRadians(fovAngleDegrees));
}

auto Camera::AdjustClippingPlanes(float nearZ, float farZ) -> Camera* {

	pImpl_->nearZ = nearZ;
	pImpl_->farZ = farZ;

	//プロジェクション行列を更新
	pImpl_->UpdateProjectionMatrix();

	return this;
}

auto Camera::LerpPosition(CXMVECTOR target, float rate) -> Camera* {

	rate = clamp(rate, 0.f, 1.f);
	auto v = XMVectorLerp(pImpl_->position, target, rate);

	return SetPosition(v);
}

auto Camera::SlerpOriented(CXMVECTOR target, float rate) -> Camera* {

	rate = clamp(rate, 0.f, 1.f);
	auto q = XMQuaternionSlerp(pImpl_->oriented, target, rate);

	return SetOriented(q);
}

auto Camera::SlerpRevolution(CXMVECTOR target, float rate) -> Camera* {
	
	rate = clamp(rate, 0.f, 1.f);
	auto q = XMQuaternionSlerp(pImpl_->oriented, target, rate);

	return SetRevolutionQuaternion(q);
}

void Camera::SetContextViewport() const {

	const auto& d3dContext = DirectX11::GetD3DDeviceContext();
	d3dContext->RSSetViewports(1, &pImpl_->viewport);
}

void Camera::SetCurrentCamera(Camera* camera) noexcept {

	currentCamera = camera;
	camera->SetContextViewport();
}

auto Camera::GetCurrentCamera() noexcept -> Camera* {

	return currentCamera;
}

void Camera::UpdateViewport() const {

	pImpl_->UpdateViewport();
}


void Camera::Impl::Initialize() {

	position	= DEFAULT_FORWARD * -10.f;
	target		= XMVectorZero();
	oriented	= XMQuaternionIdentity();
	fovAngle	= XM_PIDIV4;
	aspectRatio	= Window::GetAspectRatio();
	viewport	= { 0, 0, Window::GetWindowWidthF(), Window::GetWindowHeightF(), 0, 1.f };
	nearZ		= 0.1f;
	farZ		= 1000.f;

	UpdateProjectionMatrix();
	UpdateViewMatrix();
}

void Camera::Impl::UpdateViewMatrix() {

	auto up = XMVector3Rotate(DEFAULT_UP, oriented);
	
	matrixView = XMMatrixLookAtLH(position, target, up);
}

void Camera::Impl::UpdateProjectionMatrix() {

	matrixProjection = XMMatrixPerspectiveFovLH(fovAngle, aspectRatio, nearZ, farZ);
}

void Camera::Impl::UpdateTargetPosition() {

	auto targetVec = DEFAULT_FORWARD * XMVector3Length(target - position);
	targetVec = XMVector3Rotate(targetVec, oriented);
	target = targetVec + position;
}

void Camera::Impl::UpdateOriented() {

	auto targetVec = target - position;
	auto normal = XMVector3Cross(DEFAULT_FORWARD, targetVec);
	if (XMVector3Equal(normal, g_XMZero))	return;
	
	auto angle = XMVector3AngleBetweenVectors(DEFAULT_FORWARD, targetVec);
	oriented = XMQuaternionRotationAxis(normal, angle.m128_f32[0]);
}

void Camera::Impl::UpdateViewport() {

	aspectRatio = Window::GetAspectRatio();
	UpdateProjectionMatrix();
	auto widthRatio = Window::GetWindowWidth() / Window::GetWindowPrevWidthF();
	auto heightRatio = Window::GetWindowHeight() / Window::GetWindowPrevHeightF();

	if (widthRatio == 0.f ||
		heightRatio == 0.f ||
		widthRatio == numeric_limits<float>::infinity() ||
		heightRatio == numeric_limits<float>::infinity())	return;
	
	viewport.TopLeftX *= widthRatio;
	viewport.Width *= widthRatio;
	viewport.TopLeftY *= heightRatio;
	viewport.Height *= heightRatio;
}


}
