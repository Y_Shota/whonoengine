#pragma once
#include <filesystem>


namespace Whono {

//前方宣言
class ConfigElem;

/**
 * \brief ファイルからデータを読み書きするためのクラス
 */
class ConfigFile {

public:
	/**
	 * \brief コンストラクタ
	 * \param path 開くファイルのパス
	 */
	explicit ConfigFile(const std::filesystem::path& path);
	/**
	 * \brief デストラクタ
	 */
	~ConfigFile();

	/**
	 * \brief ファイルを読み込む
	 */
	void Load() const;
	/**
	 * \brief ファイルを保存する
	 */
	void Save() const;
	/**
	 * \brief 既に読み込んであるデータをリセット
	 */
	void Reset() const;
	
	/**
	 * \brief int値を取得する
	 * \param key キー
	 * \param defaultValue デフォルト値
	 * \param comment コメント
	 * \return キーにあったint値
	 */
	auto GetInteger(const std::string& key, int defaultValue, const std::string& comment = "") const -> int;
	/**
	 * \brief long値を取得する
	 * \param key キー
	 * \param defaultValue デフォルト値
	 * \param comment コメント
	 * \return キーにあったlong値
	 */
	auto GetLong(const std::string& key, long defaultValue, const std::string& comment = "") const -> long;
	/**
	 * \brief float値を取得する
	 * \param key キー
	 * \param defaultValue デフォルト値
	 * \param comment コメント
	 * \return キーにあったfloat値
	 */
	auto GetFloat(const std::string& key, float defaultValue, const std::string& comment = "") const -> float;
	/**
	 * \brief double値を取得する
	 * \param key キー
	 * \param defaultValue デフォルト値
	 * \param comment コメント
	 * \return キーにあったdouble値
	 */
	auto GetDouble(const std::string& key, double defaultValue, const std::string& comment = "") const -> double;
	/**
	 * \brief char値を取得する
	 * \param key キー
	 * \param defaultValue デフォルト値
	 * \param comment コメント
	 * \return キーにあったchar値
	 */
	auto GetCharacter(const std::string& key, char defaultValue, const std::string& comment = "") const -> char;
	/**
	 * \brief std::string値を取得する
	 * \param key キー
	 * \param defaultValue デフォルト値
	 * \param comment コメント
	 * \return キーにあったstd::string値
	 */
	auto GetString(const std::string& key, const std::string& defaultValue, const std::string& comment = "") const->std::string;
	/**
	 * \brief bool値を取得する
	 * \param key キー
	 * \param defaultValue デフォルト値
	 * \param comment コメント
	 * \return キーにあったbool値
	 */
	auto GetBoolean(const std::string& key, bool defaultValue, const std::string& comment = "") const -> bool;

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽
};

}