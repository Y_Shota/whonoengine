#pragma once
#include <string>


namespace Whono {

class ConfigElem {

public:
	virtual ~ConfigElem() = default;

	virtual void SetElement(const std::string& elem) = 0;

	virtual auto GetKeyword() const noexcept -> char = 0;
	virtual auto GetElemString() const noexcept -> std::string = 0;

	virtual auto GetInteger() const noexcept -> int { return 0; }
	virtual auto GetLong() const noexcept -> long { return 0L; }
	virtual auto GetFloat() const noexcept -> float { return 0.f; }
	virtual auto GetDouble() const noexcept -> double { return 0.; }
	virtual auto GetCharacter() const noexcept -> char { return '\0'; }
	virtual auto GetString() const noexcept -> std::string { return ""; }
	virtual auto GetBoolean() const noexcept -> bool { return false; }

	virtual auto IsInteger() const noexcept -> bool { return false; }
	virtual auto IsLong() const noexcept -> bool { return false; }
	virtual auto IsFloat() const noexcept -> bool { return false; }
	virtual auto IsDouble() const noexcept -> bool { return false; }
	virtual auto IsCharacter() const noexcept -> bool { return false; }
	virtual auto IsString() const noexcept -> bool { return false; }
	virtual auto IsBoolean() const noexcept -> bool { return false; }


};

}