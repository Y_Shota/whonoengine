#include "ConfigElemDouble.hpp"


namespace Whono {

ConfigElemDouble::ConfigElemDouble()
	: elem_(0.) {}

void ConfigElemDouble::SetElement(const std::string& elem) {

	elem_ = std::stod(elem);
}

auto ConfigElemDouble::GetKeyword() const noexcept -> char {

	return KEYWORD;
}

auto ConfigElemDouble::GetElemString() const noexcept -> std::string {

	return std::to_string(elem_);
}

auto ConfigElemDouble::GetDouble() const noexcept -> double {

	return elem_;
}

auto ConfigElemDouble::IsDouble() const noexcept -> bool {

	return true;
}

}