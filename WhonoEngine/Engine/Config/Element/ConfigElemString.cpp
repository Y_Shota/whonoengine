#include "ConfigElemString.hpp"


namespace Whono {

ConfigElemString::ConfigElemString()
	: elem_("") {}

void ConfigElemString::SetElement(const std::string& elem) {

	elem_ = elem;
}

auto ConfigElemString::GetKeyword() const noexcept -> char {

	return KEYWORD;
}

auto ConfigElemString::GetElemString() const noexcept -> std::string {

	return elem_;
}

auto ConfigElemString::GetString() const noexcept -> std::string {

	return elem_;
}

auto ConfigElemString::IsString() const noexcept -> bool {

	return true;
}

}