#include "ConfigElemFloat.hpp"


namespace Whono {

ConfigElemFloat::ConfigElemFloat()
	: elem_(0.f) {}

void ConfigElemFloat::SetElement(const std::string& elem) {

	elem_ = std::stof(elem);
}

auto ConfigElemFloat::GetKeyword() const noexcept -> char {

	return KEYWORD;
}

auto ConfigElemFloat::GetElemString() const noexcept -> std::string {

	return std::to_string(elem_);
}

auto ConfigElemFloat::GetFloat() const noexcept -> float {

	return elem_;
}

auto ConfigElemFloat::IsFloat() const noexcept -> bool {

	return true;
}

}