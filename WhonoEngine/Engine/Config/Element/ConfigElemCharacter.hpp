#pragma once
#include "ConfigElem.hpp"


namespace Whono {

class ConfigElemCharacter final : public ConfigElem {

public:
	static constexpr auto KEYWORD = 'C';

private:
	char elem_;

public:
	ConfigElemCharacter();

	void SetElement(const std::string& elem) override;

	auto GetKeyword() const noexcept -> char override;
	auto GetElemString() const noexcept -> std::string override;

	auto GetCharacter() const noexcept -> char override;

	auto IsCharacter() const noexcept -> bool override;
};

}