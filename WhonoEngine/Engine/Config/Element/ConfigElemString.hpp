#pragma once
#include "ConfigElem.hpp"


namespace Whono {

class ConfigElemString final : public ConfigElem {

public:
	static constexpr auto KEYWORD = 'S';

private:
	std::string elem_;

public:
	ConfigElemString();

	void SetElement(const std::string& elem) override;

	auto GetKeyword() const noexcept -> char override;
	auto GetElemString() const noexcept -> std::string override;

	auto GetString() const noexcept -> std::string override;

	auto IsString() const noexcept -> bool override;
};

}