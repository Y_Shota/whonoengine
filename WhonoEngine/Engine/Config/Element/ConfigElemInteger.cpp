#include "ConfigElemInteger.hpp"


namespace Whono {

ConfigElemInteger::ConfigElemInteger()
	: elem_(0) {}

void ConfigElemInteger::SetElement(const std::string& elem) {

	elem_ = std::stoi(elem);
}

auto ConfigElemInteger::GetKeyword() const noexcept -> char {

	return KEYWORD;
}

auto ConfigElemInteger::GetElemString() const noexcept -> std::string {

	return std::to_string(elem_);
}

auto ConfigElemInteger::GetInteger() const noexcept -> int {

	return elem_;
}

auto ConfigElemInteger::IsInteger() const noexcept -> bool {

	return true;
}

}