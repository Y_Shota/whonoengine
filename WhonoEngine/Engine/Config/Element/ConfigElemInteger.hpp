#pragma once
#include "ConfigElem.hpp"


namespace Whono {

class ConfigElemInteger final : public ConfigElem {

public:
	static constexpr auto KEYWORD = 'I';

private:
	int elem_;

public:
	ConfigElemInteger();

	void SetElement(const std::string& elem) override;

	auto GetKeyword() const noexcept -> char override;
	auto GetElemString() const noexcept -> std::string override;

	auto GetInteger() const noexcept -> int override;

	auto IsInteger() const noexcept -> bool override;
};

}