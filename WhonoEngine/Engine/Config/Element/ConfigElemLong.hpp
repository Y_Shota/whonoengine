#pragma once
#include "ConfigElem.hpp"


namespace Whono {

class ConfigElemLong final : public ConfigElem {

public:
	static constexpr auto KEYWORD = 'L';

private:
	long elem_;

public:
	ConfigElemLong();

	void SetElement(const std::string& elem) override;

	auto GetKeyword() const noexcept -> char override;
	auto GetElemString() const noexcept -> std::string override;

	auto GetLong() const noexcept -> long override;

	auto IsLong() const noexcept -> bool override;
};

}