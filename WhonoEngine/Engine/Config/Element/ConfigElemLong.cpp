#include "ConfigElemLong.hpp"


namespace Whono {

ConfigElemLong::ConfigElemLong()
	: elem_(0L) {}

void ConfigElemLong::SetElement(const std::string& elem) {

	elem_ = std::stol(elem);
}

auto ConfigElemLong::GetKeyword() const noexcept -> char {

	return KEYWORD;
}

auto ConfigElemLong::GetElemString() const noexcept -> std::string {

	return std::to_string(elem_);
}

auto ConfigElemLong::GetLong() const noexcept -> long {

	return elem_;
}

auto ConfigElemLong::IsLong() const noexcept -> bool {

	return true;
}

}