#include "ConfigElemBoolean.hpp"


namespace Whono {

ConfigElemBoolean::ConfigElemBoolean()
	: elem_(false) {}

void ConfigElemBoolean::SetElement(const std::string& elem) {

	elem_ = elem == "true";
}

auto ConfigElemBoolean::GetKeyword() const noexcept -> char {

	return KEYWORD;
}

auto ConfigElemBoolean::GetElemString() const noexcept -> std::string {

	return  elem_ ? "true" : "false";
}

auto ConfigElemBoolean::GetBoolean() const noexcept -> bool {

	return elem_;
}

auto ConfigElemBoolean::IsBoolean() const noexcept -> bool {

	return true;
}

}