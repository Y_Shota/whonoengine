#include "ConfigElemCharacter.hpp"


namespace Whono {

ConfigElemCharacter::ConfigElemCharacter()
	: elem_('\0') {}

void ConfigElemCharacter::SetElement(const std::string& elem) {

	elem_ = elem[0];
}

auto ConfigElemCharacter::GetKeyword() const noexcept -> char {

	return KEYWORD;
}

auto ConfigElemCharacter::GetElemString() const noexcept -> std::string {

	return { elem_ };
}

auto ConfigElemCharacter::GetCharacter() const noexcept -> char {

	return elem_;
}

auto ConfigElemCharacter::IsCharacter() const noexcept -> bool {

	return true;
}

}