#pragma once
#include "ConfigElem.hpp"


namespace Whono {

class ConfigElemFloat final : public ConfigElem {

public:
	static constexpr auto KEYWORD = 'F';

private:
	float elem_;

public:
	ConfigElemFloat();

	void SetElement(const std::string& elem) override;

	auto GetKeyword() const noexcept -> char override;
	auto GetElemString() const noexcept -> std::string override;

	auto GetFloat() const noexcept -> float override;

	auto IsFloat() const noexcept -> bool override;
};

}