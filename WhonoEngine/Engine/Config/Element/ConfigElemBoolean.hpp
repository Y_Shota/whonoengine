#pragma once
#include "ConfigElem.hpp"


namespace Whono {

class ConfigElemBoolean final : public ConfigElem {

public:
	static constexpr auto KEYWORD = 'B';

private:
	bool elem_;

public:
	ConfigElemBoolean();

	void SetElement(const std::string& elem) override;

	auto GetKeyword() const noexcept -> char override;
	auto GetElemString() const noexcept -> std::string override;

	auto GetBoolean() const noexcept -> bool override;

	auto IsBoolean() const noexcept -> bool override;
};

}