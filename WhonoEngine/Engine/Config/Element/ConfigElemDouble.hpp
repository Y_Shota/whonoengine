#pragma once
#include "ConfigElem.hpp"


namespace Whono {

class ConfigElemDouble final : public ConfigElem {

public:
	static constexpr auto KEYWORD = 'D';

private:
	double elem_;

public:
	ConfigElemDouble();

	void SetElement(const std::string& elem) override;

	auto GetKeyword() const noexcept -> char override;
	auto GetElemString() const noexcept -> std::string override;

	auto GetDouble() const noexcept -> double override;

	auto IsDouble() const noexcept -> bool override;
};

}