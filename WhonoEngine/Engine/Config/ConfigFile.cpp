#include "ConfigFile.hpp"

#include <fstream>
#include <functional>
#include <algorithm>
#include <regex>
#include <unordered_map>

#include "Element/ConfigElem.hpp"
#include "Element/ConfigElemBoolean.hpp"
#include "Element/ConfigElemCharacter.hpp"
#include "Element/ConfigElemDouble.hpp"
#include "Element/ConfigElemFloat.hpp"
#include "Element/ConfigElemInteger.hpp"
#include "Element/ConfigElemLong.hpp"
#include "Element/ConfigElemString.hpp"

using namespace std;


namespace Whono {

namespace {

const unordered_map<char, function<unique_ptr<ConfigElem>()>> INSTANCE_FUNCTION_MAP{ {
	{ ConfigElemInteger::KEYWORD, make_unique<ConfigElemInteger> },
	{ ConfigElemLong::KEYWORD, make_unique<ConfigElemLong> },
	{ ConfigElemFloat::KEYWORD, make_unique<ConfigElemFloat> },
	{ ConfigElemDouble::KEYWORD, make_unique<ConfigElemDouble> },
	{ ConfigElemCharacter::KEYWORD, make_unique<ConfigElemCharacter> },
	{ ConfigElemString::KEYWORD, make_unique<ConfigElemString> },
	{ ConfigElemBoolean::KEYWORD, make_unique<ConfigElemBoolean> },
} };
}

struct ElemData {

	string key;
	string document;
	unique_ptr<ConfigElem> elem;
};

/**
 * \brief 文字列ストリームに変換可能な型
 */
template<class T>
concept AbleToString = requires (const T& a) {

	stringstream() << a;
};

/**
 * \brief 内部実装用構造体
 */
struct ConfigFile::Impl {

	filesystem::path path;										//開くファイル
	unordered_map<string, unique_ptr<ElemData>> elementMap;		//属性マップ
	vector<ElemData*> elements;									//順番保持用属性配列

	void Load();
	void Save();
	void Reset();

	auto GetInteger(const string& key, int defaultValue, const string& comment) -> int;
	auto GetLong(const string& key, long defaultValue, const string& comment) -> long;
	auto GetFloat(const string& key, float defaultValue, const string& comment) -> float;
	auto GetDouble(const string& key, double defaultValue, const string& comment) -> double;
	auto GetCharacter(const string& key, char defaultValue, const string& comment) -> char;
	auto GetString(const string& key, const string& defaultValue, const string& comment) -> string;
	auto GetBoolean(const string& key, bool defaultValue, const string& comment) -> bool;

	/**
	 * \brief ドキュメントを生成する
	 * \tparam T string変換可能な変換元の型
	 * \param defaultValue デフォルト値
	 * \param comment コメント
	 * \return 生成したドキュメント文字列
	 */
	template<AbleToString T>
	[[nodiscard]]
	static auto GetDocument(const T& defaultValue, const string& comment) -> string;
};


ConfigFile::ConfigFile(const filesystem::path& path) {

	pImpl_ = make_unique<Impl>();
	pImpl_->path = path;
}

ConfigFile::~ConfigFile() {}

void ConfigFile::Load() const {

	pImpl_->Load();
}

void ConfigFile::Save() const {

	pImpl_->Save();
}

void ConfigFile::Reset() const {

	pImpl_->Reset();
}

auto ConfigFile::GetInteger(const string& key, int defaultValue, const string& comment) const -> int {

	return pImpl_->GetInteger(key, defaultValue, comment);
}

auto ConfigFile::GetLong(const string& key, long defaultValue, const string& comment) const -> long {

	return pImpl_->GetLong(key, defaultValue, comment);
}

auto ConfigFile::GetFloat(const string& key, float defaultValue, const string& comment) const -> float {

	return pImpl_->GetFloat(key, defaultValue, comment);
}

auto ConfigFile::GetDouble(const string& key, double defaultValue, const string& comment) const -> double {

	return pImpl_->GetDouble(key, defaultValue, comment);
}

auto ConfigFile::GetCharacter(const string& key, char defaultValue, const string& comment) const -> char {

	return pImpl_->GetCharacter(key, defaultValue, comment);
}

auto ConfigFile::GetString(const string& key, const string& defaultValue, const string& comment) const -> string {

	return pImpl_->GetString(key, defaultValue, comment);
}

auto ConfigFile::GetBoolean(const string& key, bool defaultValue, const string& comment) const -> bool {

	return pImpl_->GetBoolean(key, defaultValue, comment);
}

void ConfigFile::Impl::Load() {

	auto ifs = ifstream(path.string());

	string document;
	string line;
	while (getline(ifs, line)) {

		//コメントまたは空行ならドキュメントを保存し、次の行へ
		if (line[0] == '#' || line[0] == '\0' || line[0] == '\n' || line[0] == '\r') {

			document += line + '\n';
			continue;
		}

		//キーの存在を確認
		regex regex(":.+=");
		smatch match;
		if (regex_search(line, match, regex)) {

			//要素オブジェクト生成、設定
			auto configElem = INSTANCE_FUNCTION_MAP.at(match.prefix().str()[0])();
			configElem->SetElement(match.suffix());

			//キーを取得
			auto key = match.str();
			key.erase(key.begin());
			key.pop_back();

			//キー重複防止
			if (!elementMap.contains(key)) {

				//オブジェクト生成、保持
				auto elemDatum = make_unique<ElemData>();
				elemDatum->key = key;
				elemDatum->document = document;
				elemDatum->elem = move(configElem);

				elements.emplace_back(elemDatum.get());
				elementMap[key] = move(elemDatum);

				document.clear();
			}
		}
	}

	ifs.close();
}

void ConfigFile::Impl::Save() {

	auto ofs = ofstream(path.string());

	for (const auto& element : elements) {

		ofs << element->document
			<< element->elem->GetKeyword() << ':'
			<< element->key << '='
			<< element->elem->GetElemString() << endl;
	}
}

void ConfigFile::Impl::Reset() {

	elements.clear();
	elementMap.clear();
}

auto ConfigFile::Impl::GetInteger(const string& key, int defaultValue, const string& comment) -> int {

	//ファイル内にない場合
	if (!elementMap.contains(key)) {

		//オブジェクト生成
		auto elemDatum = make_unique<ElemData>();

		//ドキュメント生成
		elemDatum->document = GetDocument(defaultValue, comment);

		//デフォルト値設定
		auto configElem = INSTANCE_FUNCTION_MAP.at(ConfigElemInteger::KEYWORD)();
		configElem->SetElement(to_string(defaultValue));
		elemDatum->elem = move(configElem);
		elemDatum->key = key;

		elements.emplace_back(elemDatum.get());
		elementMap[key] = move(elemDatum);

		return defaultValue;
	}

	auto& elemDatum = elementMap.at(key);
	if (elemDatum->elem->IsInteger()) {

		//ドキュメントがなかった場合追加
		auto doc = elemDatum->document;
		doc.erase(ranges::remove(doc, '\n').begin(), doc.end());
		if (doc == "") {

			elemDatum->document = GetDocument(defaultValue, comment);
		}
		//要素返還
		return elemDatum->elem->GetInteger();
	}
	//指定の型ではない
	return 0;
}

auto ConfigFile::Impl::GetLong(const string& key, long defaultValue, const string& comment) -> long {

	//ファイル内にない場合
	if (!elementMap.contains(key)) {

		//オブジェクト生成
		auto elemDatum = make_unique<ElemData>();

		//ドキュメント生成
		elemDatum->document = GetDocument(defaultValue, comment);

		//デフォルト値設定
		auto configElem = INSTANCE_FUNCTION_MAP.at(ConfigElemLong::KEYWORD)();
		configElem->SetElement(to_string(defaultValue));
		elemDatum->elem = move(configElem);
		elemDatum->key = key;

		elements.emplace_back(elemDatum.get());
		elementMap[key] = move(elemDatum);

		return defaultValue;
	}

	auto& elemDatum = elementMap.at(key);
	if (elemDatum->elem->IsLong()) {

		//ドキュメントがなかった場合追加
		auto doc = elemDatum->document;
		doc.erase(ranges::remove(doc, '\n').begin(), doc.end());
		if (doc == "") {

			elemDatum->document = GetDocument(defaultValue, comment);
		}
		//要素返還
		return elemDatum->elem->GetLong();
	}
	return 0L;
}

auto ConfigFile::Impl::GetFloat(const string& key, float defaultValue, const string& comment) -> float {

	//ファイル内にない場合
	if (!elementMap.contains(key)) {

		//オブジェクト生成
		auto elemDatum = make_unique<ElemData>();

		//ドキュメント生成
		elemDatum->document = GetDocument(defaultValue, comment);

		//デフォルト値設定
		auto configElem = INSTANCE_FUNCTION_MAP.at(ConfigElemFloat::KEYWORD)();
		configElem->SetElement(to_string(defaultValue));
		elemDatum->elem = move(configElem);
		elemDatum->key = key;

		elements.emplace_back(elemDatum.get());
		elementMap[key] = move(elemDatum);

		return defaultValue;
	}

	auto& elemDatum = elementMap.at(key);
	if (elemDatum->elem->IsFloat()) {

		//ドキュメントがなかった場合追加
		auto doc = elemDatum->document;
		doc.erase(ranges::remove(doc, '\n').begin(), doc.end());
		if (doc == "") {

			elemDatum->document = GetDocument(defaultValue, comment);
		}
		//要素返還
		return elemDatum->elem->GetFloat();
	}
	return 0.f;
}

auto ConfigFile::Impl::GetDouble(const string& key, double defaultValue, const string& comment) -> double {

	//ファイル内にない場合
	if (!elementMap.contains(key)) {

		//オブジェクト生成
		auto elemDatum = make_unique<ElemData>();

		//ドキュメント生成
		elemDatum->document = GetDocument(defaultValue, comment);

		//デフォルト値設定
		auto configElem = INSTANCE_FUNCTION_MAP.at(ConfigElemDouble::KEYWORD)();
		configElem->SetElement(to_string(defaultValue));
		elemDatum->elem = move(configElem);
		elemDatum->key = key;

		elements.emplace_back(elemDatum.get());
		elementMap[key] = move(elemDatum);

		return defaultValue;
	}

	auto& elemDatum = elementMap.at(key);
	if (elemDatum->elem->IsDouble()) {

		//ドキュメントがなかった場合追加
		auto doc = elemDatum->document;
		doc.erase(ranges::remove(doc, '\n').begin(), doc.end());
		if (doc == "") {

			elemDatum->document = GetDocument(defaultValue, comment);
		}
		//要素返還
		return elemDatum->elem->GetDouble();
	}
	return 0.;
}

auto ConfigFile::Impl::GetCharacter(const string& key, char defaultValue, const string& comment) -> char {

	//ファイル内にない場合
	if (!elementMap.contains(key)) {

		//オブジェクト生成
		auto elemDatum = make_unique<ElemData>();

		//ドキュメント生成
		elemDatum->document = GetDocument(defaultValue, comment);

		//デフォルト値設定
		auto configElem = INSTANCE_FUNCTION_MAP.at(ConfigElemCharacter::KEYWORD)();
		configElem->SetElement(to_string(defaultValue));
		elemDatum->elem = move(configElem);
		elemDatum->key = key;

		elements.emplace_back(elemDatum.get());
		elementMap[key] = move(elemDatum);

		return defaultValue;
	}

	auto& elemDatum = elementMap.at(key);
	if (elemDatum->elem->IsCharacter()) {

		//ドキュメントがなかった場合追加
		auto doc = elemDatum->document;
		doc.erase(ranges::remove(doc, '\n').begin(), doc.end());
		if (doc == "") {

			elemDatum->document = GetDocument(defaultValue, comment);
		}
		//要素返還
		return elemDatum->elem->GetCharacter();
	}
	return '\0';
}

auto ConfigFile::Impl::GetString(const string& key, const string& defaultValue, const string& comment) -> string {

	//ファイル内にない場合
	if (!elementMap.contains(key)) {

		//オブジェクト生成
		auto elemDatum = make_unique<ElemData>();

		//ドキュメント生成
		elemDatum->document = GetDocument(defaultValue, comment);

		//デフォルト値設定
		auto configElem = INSTANCE_FUNCTION_MAP.at(ConfigElemString::KEYWORD)();
		configElem->SetElement(defaultValue);
		elemDatum->elem = move(configElem);
		elemDatum->key = key;

		elements.emplace_back(elemDatum.get());
		elementMap[key] = move(elemDatum);

		return defaultValue;
	}

	auto& elemDatum = elementMap.at(key);
	if (elemDatum->elem->IsString()) {

		//ドキュメントがなかった場合追加
		auto doc = elemDatum->document;
		doc.erase(ranges::remove(doc, '\n').begin(), doc.end());
		if (doc == "") {

			elemDatum->document = GetDocument(defaultValue, comment);
		}
		//要素返還
		return elemDatum->elem->GetString();
	}
	return "";
}

auto ConfigFile::Impl::GetBoolean(const string& key, bool defaultValue, const string& comment) -> bool {

	//ファイル内にない場合
	if (!elementMap.contains(key)) {

		//オブジェクト生成
		auto elemDatum = make_unique<ElemData>();

		//ドキュメント生成
		elemDatum->document = GetDocument(defaultValue, comment);

		//デフォルト値設定
		auto configElem = INSTANCE_FUNCTION_MAP.at(ConfigElemBoolean::KEYWORD)();
		configElem->SetElement(defaultValue ? "true" : "false");
		elemDatum->elem = move(configElem);
		elemDatum->key = key;

		elements.emplace_back(elemDatum.get());
		elementMap[key] = move(elemDatum);

		return defaultValue;
	}

	auto& elemDatum = elementMap.at(key);
	if (elemDatum->elem->IsBoolean()) {

		//ドキュメントがなかった場合追加
		auto doc = elemDatum->document;
		doc.erase(ranges::remove(doc, '\n').begin(), doc.end());
		if (doc == "") {

			elemDatum->document = GetDocument(defaultValue, comment);
		}
		//要素返還
		return elemDatum->elem->GetBoolean();
	}
	return false;
}

template <AbleToString T>
auto ConfigFile::Impl::GetDocument(const T& defaultValue, const string& comment) -> string {

	//ドキュメント生成
	auto documentSs = stringstream();
	documentSs << '#' << comment << "[default: " << defaultValue << ']' << endl;
	return documentSs.str();
}

}