#pragma once
#include <DirectXMath.h>


namespace Whono::GameOperation {

enum class LIGHT_TYPE : unsigned char {

	PLAIN,
	POINT,
};

class Light {

private:
	LIGHT_TYPE type_;
	DirectX::XMVECTORF32 color_;
	float power_;
	
public:
	explicit Light(LIGHT_TYPE type);
	virtual ~Light() = default;

	void SetColor(const DirectX::XMVECTORF32& color);

	auto GetType() const noexcept -> LIGHT_TYPE;
	auto GetColor() const noexcept -> const DirectX::XMVECTORF32&;
	auto GetPower() const noexcept -> float;

	virtual auto GetViewMatrix() -> DirectX::XMMATRIX = 0;
	virtual auto GetProjectionMatrix() -> DirectX::XMMATRIX = 0;
};

}
