Texture2D g_texture : register(t0);
SamplerState g_sampler0 : register(s0);
Texture2D g_bumpMap : register(t1);
SamplerState g_sampler1 : register(s1);
Texture2D g_normalMap : register(t2);
SamplerState g_sampler2 : register(s2);

//シャドウマップ
Texture2D g_shadowMap : register(t10);
SamplerComparisonState g_shadowSampler : register(s10);
//SamplerState g_shadowSampler : register(s10);

cbuffer global : register(b0) {
	
    float4x4 g_matrixWVP;
    float4x4 g_matrixWorld;
    float4x4 g_matrixNormalTransform;
    float4 g_camera;
    float4 g_lightDir;
    float4 g_lightColor;
    float4 g_diffuse;
    float4 g_ambient;
    float4 g_emissive;
    float4 g_specular;
    float g_transparent;
    float g_shininess;
    float g_reflection;
    int g_optionFlg;
};

struct SkinMatrices {

    float4x4 bindPoseMatrix;
    float4x4 boneMatrix;
};

cbuffer skinMatrix : register(b1) {
	
    SkinMatrices g_skinMatrix[128];
}

cbuffer shadow : register(b2) {

    float4x4 g_shadowMatrix;
}


struct VS_IN {
	
    float4 position     : POSITION;
    float4 normal       : NORMAL;
    float4 tangent      : TANGENT;
    float4 binormal     : BINORMAL;
    float2 uv           : TEXCOORD;
    float4 blendWeights : BLENDWEIGHT;
    int4   blendIndices : BLENDINDICES;
};

struct VS_OUT {
	
	float4 pos		: SV_POSITION;
    float2 uv       : TEXCOORD;
    float4 normal   : TEXCOORD1;
    float4 eye      : TEXCOORD2;
    float4 light    : TEXCOORD3;
    float4 shadowPos : TEXCOORD4;
};

VS_OUT VS(VS_IN input) {

	//出力用変数
    VS_OUT output;
    
    //一時保存
    float4 position = input.position;
    float4 normal = input.normal;
    float4 tangent = input.tangent;
    float4 binormal = input.binormal;
	
	//アニメーションがあるときスキン行列適用
    if (g_optionFlg & 0x08 && input.blendIndices[0] >= 0) {
    	
        float4x4 skinMatrix = 0;
        for (int i = 0; i < 4; ++i) {
        	
            if (input.blendIndices[i] < 0)  break;
        	
            skinMatrix += mul(g_skinMatrix[input.blendIndices[i]].bindPoseMatrix, g_skinMatrix[input.blendIndices[i]].boneMatrix) * input.blendWeights[i];
        }
    	
        position = mul(position, skinMatrix);
        normal = mul(normal, skinMatrix);

        if (g_optionFlg & 0x02 || g_optionFlg & 0x04) {
        	
            tangent = mul(tangent, skinMatrix);
            binormal = mul(binormal, skinMatrix);
        }
    }
    
    output.pos = mul(position, g_matrixWVP);
    output.shadowPos = mul(mul(position, g_matrixWorld), g_shadowMatrix);
    
    //テックス座標
    output.uv = input.uv;
    
    //法線変形
    normal.w = 0;
    binormal.w = 0;
    tangent.w = 0;
    output.normal = mul(normal, g_matrixNormalTransform);
    
    //光源方向と視点ベクトル
    output.light = normalize(g_lightDir);
    output.eye = normalize(g_camera - mul(input.position, g_matrixWorld));
	
    if (g_optionFlg & 0x02 || g_optionFlg & 0x04) {        //法線をテクスチャからとるときのみ計算
    
        binormal = mul(binormal, g_matrixNormalTransform);
        tangent = mul(tangent, g_matrixNormalTransform);

        float4 light = output.light;
        output.light.x = dot(light, tangent);
        output.light.y = dot(light, binormal);
        output.light.z = dot(light, output.normal);

        float4 eye = output.eye;
        output.eye.x = dot(eye, tangent);
        output.eye.y = dot(eye, binormal);
        output.eye.z = dot(eye, output.normal);
    }
    return output;
}

typedef VS_OUT PS_IN;

struct PS_OUT {
	
    float4 color : SV_TARGET;
};

PS_OUT PS(PS_IN input) {
	
    PS_OUT output;
    
    //正規化
    input.light = normalize(input.light);
    input.eye = normalize(input.eye);
	
    //テクスチャの有無によって切り替える
    float4 id = g_optionFlg & 0x01 ? g_texture.Sample(g_sampler0, input.uv) : g_diffuse;

    float4 bumpMap = g_bumpMap.Sample(g_sampler1, input.uv);
    float4 normalMap = g_normalMap.Sample(g_sampler2, input.uv);

	//法線テクスチャ、バンプテクスチャから法線を計算
    float4 normal = 0;
    if (g_optionFlg & 0x02)
        normal += bumpMap * 2.f - 1.f;
    if (g_optionFlg & 0x04)
        normal += normalMap * 2.f - 1.f;
    if (length(normal) == 0)
        normal = input.normal;
	
    normal.w = 0;
    normal = normalize(normal);
    float shade = saturate(dot(-input.light, normal));
    shade = (shade + 1) * 0.5f;
    
    float4 diffuse = shade * id; //最終的なディフューズカラー
	
    //視線やら反射ベクトルとかからハイライトを算出
    float4 r = reflect(input.light, normal);
    float ks = 2;
    float4 specular = ks * pow(saturate(dot(r, input.eye)), g_shininess) * g_specular;
	

    float shadow = 1;
    if (g_optionFlg & 0x8000) {
    	
		// シャドウマップの深度値と比較する.
        float3 shadowCoord = input.shadowPos.xyz / input.shadowPos.w;

		// 最大深度傾斜を求める.
        float maxDepthSlope = max(abs(ddx(shadowCoord.z)), abs(ddy(shadowCoord.z)));
        
        float bias = 0.01f;             // 固定バイアス
        float slopeScaledBias = 0.01f;  // 深度傾斜
        float depthBiasClamp = 0.1f;    // バイアスクランプ値

        float shadowBias = bias + slopeScaledBias * maxDepthSlope;
        shadowBias = min(shadowBias, depthBiasClamp);
        
        float shadowThreshold = g_shadowMap.SampleCmpLevelZero(g_shadowSampler, shadowCoord.xy, shadowCoord.z - shadowBias);
        shadow = lerp(0.5f, 1, shadowThreshold);
    }
	
    output.color = (g_ambient + diffuse + g_emissive) * g_lightColor;
    output.color.a = 1.f - g_transparent;
    if (shadow >= 1)     output.color += specular;
    if (shade > 0.5)    output.color.rgb *= shadow;
    
    return output;
}