
struct SkinMatrices {
	
    float4x4 bindPoseMatrix;
    float4x4 boneMatrix;
};

cbuffer worldMatrix : register(b0) {
    
    float4x4 g_matrixWorld; //描画対象のワールド変換行列
    int g_isAnimation;
}

cbuffer skinMatrix : register(b1) {
	
    SkinMatrices g_skinMatrix[128];
}

cbuffer lightMatrix : register(b2) {

    float4x4 g_matrixView;          //ライト視点でのビュー変換行列
    float4x4 g_matrixProjection;    //ライト視点での射影変換行列
}

struct VS_IN {
	
    float4 position : POSITION;
    float4 normal : NORMAL;
    float4 tangent : TANGENT;
    float4 binormal : BINORMAL;
    float2 uv : TEXCOORD;
    float4 blendWeights : BLENDWEIGHT;
    int4 blendIndices : BLENDINDICES;
};

float4 VS(VS_IN input) : SV_POSITION {


    float4 position = input.position;
    position.w = 1;
	
	//アニメーションがあるときスキン行列適用
    if (g_isAnimation && input.blendIndices[0] >= 0) {
    	
        float4x4 skinMatrix = 0;
        for (int i = 0; i < 4; ++i) {
        	
            if (input.blendIndices[i] < 0)
                break;
        	
            skinMatrix += mul(g_skinMatrix[input.blendIndices[i]].bindPoseMatrix, g_skinMatrix[input.blendIndices[i]].boneMatrix) * input.blendWeights[i];
        }
        position = mul(position, skinMatrix);
    }
    position = mul(position, g_matrixWorld);
    position = mul(position, g_matrixView);
    position = mul(position, g_matrixProjection);
	
    return position;
}