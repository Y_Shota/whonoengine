
Texture2D g_texture : register(t0);
SamplerState g_sampler : register(s0);

cbuffer global : register(b0)
{
    float4x4 matWVP;
    float lightLevel;
};

struct VS_IN {

    float4 position : POSITION;
    float4 uv : TEXCOORD;
};

struct VS_OUT
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD;
};

VS_OUT VS(VS_IN input)
{

    VS_OUT output;

    output.position = mul(input.position, matWVP);
    output.uv = input.uv.xy;

    return output;
}

typedef VS_OUT PS_IN;

struct PS_OUT {

    float4 color : SV_Target;
};

PS_OUT PS(PS_IN input)
{
    PS_OUT output;

    output.color = g_texture.Sample(g_sampler, input.uv);
    output.color.rgb *= lightLevel;
    //output.color = float4(1, 1, 1, 1);
	
    return output;
}