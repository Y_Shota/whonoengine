cbuffer global : register(b0)
{
    float4x4 g_matrixWVP;
    float4x4 g_matrixWorld;
    float4x4 g_matrixNormalTransform;
    float4 g_camera;
    float4 g_lightDir;
    float4 g_diffuse;
    float4 g_ambient;
    float4 g_emissive;
    float4 g_specular;
    float g_transparent;
    float g_shininess;
    float g_reflection;
    int g_optionFlg;
};

cbuffer skinMatrix : register(b1)
{
    float4x4 g_skinMatrix[128];
}

struct VS_IN
{
    float4 position : POSITION;
    float4 normal : NORMAL;
    float4 tangent : TANGENT;
    float4 binormal : BINORMAL;
    float2 uv : TEXCOORD;
    float4 blendWeights : BLENDWEIGHT;
    int4 blendIndices : BLENDINDICES;
};

struct VS_OUT
{
    float4 position : SV_POSITION;
};

VS_OUT VS(VS_IN input)
{

	//出力用変数
    VS_OUT output;
    
    float4 position = input.position;
	
	//アニメーションがあるときスキン行列適用
    if (g_optionFlg & 0x08)
    {
        float4x4 skinMatrix = 0;
        for (int i = 0; i < 4; ++i)
        {
            if (input.blendIndices[i] < 0)
                break;
            skinMatrix += g_skinMatrix[input.blendIndices[i]] * input.blendWeights[i];
        }
        position = mul(position, skinMatrix);
    }
    output.position = mul(position, g_matrixWVP);
	
    return output;
}

typedef VS_OUT PS_IN;

struct PS_OUT
{
    float4 color : SV_TARGET;
};

PS_OUT PS(VS_OUT input)
{
	
    PS_OUT output;

    output.color = float4(0.5f, 1.f, 1.f, 0.5f);
   
    return output;
}