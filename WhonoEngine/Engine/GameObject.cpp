﻿#include "GameObject.hpp"

#include <list>

#include "ICollider.hpp"
#include "Time.hpp"

using namespace std;



namespace Whono::GameOperation {




GameObject::GameObject(const std::string& name)
	: name_(name), parent_(nullptr)
	, state_{ 0xFF } {

	transform_ = make_unique<Transform>();
	state_.elem.initialized = false;
}

GameObject::~GameObject() = default;

void GameObject::InitializeAll() {

	PreInit();
	Initialized();
	PreInitChild();
	Initialize();
	InitializeChild();
	PostInit();
	PostInitChild();
}

auto GameObject::GetName() const noexcept -> const std::string& {

	return name_;
}

auto GameObject::GetTransform() const noexcept -> Transform* {

	return transform_.get();
}

auto GameObject::GetParent() const noexcept -> GameObject* {

	if (IsGameObjectChild()) {

		return get<GameObject*>(parent_);
	}
	return nullptr;
}

auto GameObject::SetParent(GameObject* parent) -> bool {

	if (!parent)					return false;
	if (!parent->AddChild(this))	return false;

	//親が変わると変形情報の親子も更新
	UpdateTransformParent();

	return true;
}

auto GameObject::SetParent(SceneObject* parent) -> bool {

	if (!parent)					return false;
	if (!parent->AddChild(this))	return false;

	parent_ = parent;

	//親が変わると変形情報の親子も更新
	UpdateTransformParent();

	return true;
}

auto GameObject::GetAffiliationScene() const noexcept -> SceneObject* {

	//シーン直下なら親を返す
	if (IsSceneChild()) {

		return get<SceneObject*>(parent_);
	}
	//そうでないなら再起
	if (IsGameObjectChild()) {

		return get<GameObject*>(parent_)->GetAffiliationScene();
	}
	return nullptr;
}

auto GameObject::GetCamera(unsigned index) const -> Camera* {

	return GetAffiliationScene()->GetCamera(index);
}

auto GameObject::ResetCameras() const -> Camera* {

	return GetAffiliationScene()->ResetCameras();
}

auto GameObject::GetCameraDistance(unsigned index) const -> float {

	auto* camera = GetCamera(index);
	auto cameraPosition = camera->GetPosition();
	auto position = GetTransform()->GetWorldPosition();

	return XMVector3Length(cameraPosition - position).m128_f32[0];
}

auto GameObject::FindObject(const std::string& objectName) const noexcept -> GameObject* {

	return GetAffiliationScene()->FindObject(objectName);
}

void GameObject::AddCollider(std::unique_ptr<ICollider>& collider) {

	AddCollider(move(collider));
}

void GameObject::AddCollider(std::unique_ptr<ICollider>&& collider) {

	collider->SetGameObject(this);
	colliderList_.emplace_back(move(collider));
}

void GameObject::KillMe() {

	state_.elem.survivable = false;
}

auto GameObject::IsAlive() const -> bool {

	return state_.elem.survivable;
}

void GameObject::Initialized() noexcept {

	state_.elem.initialized = true;
}

auto GameObject::IsInitialized() const noexcept -> bool {

	return state_.elem.initialized;
}

void GameObject::CheckCollisionDetectionOnlyTarget(GameObject* target) {

	for (const auto& collider : colliderList_) {

		auto detectedFlg = false;

		for (const auto& targetCollider : target->colliderList_) {

			//既に検証済みなら無視
			auto* targetColliderRaw = targetCollider.get();
			auto result = find(collidedColliderList_.cbegin(), collidedColliderList_.cend(), targetColliderRaw);
			if (result != collidedColliderList_.cend()) {

				continue;
			}


			if (collider->IsHit(targetColliderRaw)) {

				detectedFlg = true;
				collidedColliderList_.emplace_back(targetColliderRaw);
				DetectedCollision(target, collider.get(), targetColliderRaw);
				target->DetectedCollision(this, targetColliderRaw, collider.get());
				break;
			}
		}
		if (detectedFlg)	break;
	}
}

auto GameObject::CalculateDistance(const GameObject* target) const -> float {

	return XMVector3Length(transform_->GetWorldPosition() - target->transform_->GetWorldPosition()).m128_f32[0];
}

auto GameObject::GetFrameRatio() -> float {

	return Time::GetFrameErrorRatio();
}

void GameObject::AddGameObjectInstance(std::unique_ptr<GameObject>& gameObject) const {

	GetAffiliationScene()->AddGameObjectInstance(gameObject);
}

void GameObject::PreInitChild() const {

	for (auto* child : childList_) {

		child->PreInit();
		child->Initialized();
		child->PreInitChild();
	}
}

void GameObject::InitializeChild() const {

	for (auto* child : childList_) {

		child->Initialize();
		child->InitializeChild();
	}
}

void GameObject::PostInitChild() const {

	for (auto* child : childList_) {

		child->PostInit();
		child->PostInitChild();
	}
}

void GameObject::UpdateChild() {

	for (auto itr = childList_.begin(); itr != childList_.end();) {

		auto* child = *itr;
		//生きている場合通常更新処理
		if (child->IsAlive()) {

			if (!child->IsInitialized()) {

				child->InitializeAll();
			}
			child->Update();
			child->UpdateChild();
			child->CheckCollisionDetection(this);
			child->collidedColliderList_.clear();
			++itr;
			continue;
		}
		//死んでいる場合開放
		child->FinalizeChild();
		child->Finalize();
		itr = childList_.erase(itr);
		GetAffiliationScene()->ReleaseObject(child);
	}
}

void GameObject::PreRenderChild() const {

	for (auto* child : childList_) {

		child->PreRender();
		child->PreRenderChild();
	}
}

void GameObject::RenderChild() const {

	//デバッグ時のみコリジョンを表示
	for (const auto& collider : colliderList_) {

		collider->Render();
	}

	for (auto* child : childList_) {

		child->Render();
		child->RenderChild();
	}
}

void GameObject::PostRenderChild() const {

	for (auto* child : childList_) {

		child->PostRender();
		child->PostRenderChild();
	}
}

void GameObject::FinalizeChild() {

	colliderList_.clear();
	auto* scene = GetAffiliationScene();

	for (auto* child : childList_) {

		child->FinalizeChild();
		child->Finalize();
		scene->ReleaseObject(child);
	}
}

void GameObject::CheckCollisionDetection(GameObject* target) {

	//相手がいない、相手が自分の時無視
	if (!target)		return;
	if (target == this)	return;

	//自分と相手のすべてのコライダーを総当たり検証
	CheckCollisionDetectionOnlyTarget(target);

	//相手に子がいないなら終了
	if (target->childList_.empty())	return;

	//相手の子も検証
	for (auto* targetChild : target->childList_) {

		CheckCollisionDetection(targetChild);
	}
}

auto GameObject::AddChild(GameObject* child) -> bool {

	//nullなら失敗
	if (!child)	return false;

	//追加したいオブジェクトが先祖にいないかチェック
	auto* check = GetParent();
	while (check) {

		if (check == child)	return false;

		check = check->GetParent();
	}

	//既に親がいた場合、子のリストから外させる
	if (auto* prevParent = child->GetParent()) {

		prevParent->RemoveChild(child);
	}
	//シーン直下だった場合
	else if (child->IsSceneChild()) {

		auto* scene = child->GetAffiliationScene();
		if (scene->HasGameObject(child)) {

			scene->RemoveChild(child);
		}
	}

	//親子関係を築く
	child->parent_ = this;
	childList_.emplace_back(child);

	return true;
}

void GameObject::RemoveChild(GameObject* child) {

	if (!child)				return;
	if (childList_.empty())	return;

	childList_.remove(child);
}

auto GameObject::IsSceneChild() const -> bool {

	return holds_alternative<SceneObject*>(parent_);
}

auto GameObject::IsGameObjectChild() const -> bool {

	return holds_alternative<GameObject*>(parent_);
}

void GameObject::UpdateTransformParent() {

	if (IsGameObjectChild()) {

		const auto& parentTransform = get<GameObject*>(parent_)->GetTransform();
		transform_->SetParent(parentTransform);
	}
	else {

		transform_->SetParent(nullptr);
	}
}

bool GameObject::sceneInitialize = false;

}