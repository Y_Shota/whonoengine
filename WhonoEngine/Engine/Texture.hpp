#pragma once

#include <d2d1_1.h>
#include <d3d11_1.h>
#include <DirectXMath.h>
#include <filesystem>
#include <memory>

#include "ComPtrDefinition.hpp"


namespace Whono::RenderObject {


/**
* \brief 画像を読み込みDirectXで扱える形で保持
* \author whono
* \version 1.0
* \since 1.0
*/
class Texture {

public:
	/**
	 * \brief コンストラクタ
	 */
	Texture();	
	/**
	 * \brief デストラクタ
	 */
	~Texture();
	
	/**
	 * \brief ファイルパスからファイルを読み込む
	 * \param filePath ファイルパス
	 * \throw WinException
	 * \throw DirectXException
	 */
	void Load(const std::filesystem::path& filePath) const noexcept(false);
	
	/**
	 * \brief シェーダリソースビューを取得
	 * \return シェーダリソースビュー
	 */
	[[nodiscard]]
	auto GetShaderResourceView() const noexcept -> CComPtrRef<ID3D11ShaderResourceView>;
	/**
	 * \brief サンプラステートを取得
	 * \return サンプラステート
	 */
	[[nodiscard]]
	auto GetSamplerState() const noexcept -> CComPtrRef<ID3D11SamplerState>;
	/**
	 * \brief Direct2Dのビットマップを取得
	 * \return d2d1Bitmap
	 */
	[[nodiscard]]
	auto GetD2DBitmap() const noexcept -> CComPtrRef<ID2D1Bitmap1>;
	
	/**
	 * \brief 画像サイズの取得
	 * \return サイズ
	 */
	[[nodiscard]]
	auto GetSize() const noexcept -> const DirectX::XMUINT2&;
	/**
	 * \brief 画像サイズの取得
	 * \return サイズ (XMVECTOR)
	 */
	[[nodiscard]]
	auto GetSizeVector() const noexcept -> DirectX::XMVECTOR;

	/**
	 * \brief 画像のアスペクト比の取得
	 * \return アスペクト比
	 */
	[[nodiscard]]
	auto GetAspectRatio() const noexcept -> float;

	/**
	 * \brief カスタム可能なサンプラを作成する
	 * \param filter フィルタ
	 * \param u U座標
	 * \param v V座標
	 * \param w W座標
	 */
	void CreateCustomSampler(D3D11_FILTER filter, D3D11_TEXTURE_ADDRESS_MODE u, D3D11_TEXTURE_ADDRESS_MODE v, D3D11_TEXTURE_ADDRESS_MODE w) const;
	/**
	 * \brief カスタム可能なサンプラを作成する
	 * \param filter フィルタ
	 * \param all = D3D11_TEXTURE_ADDRESS_WRAP UVWすべて
	 */
	void CreateCustomSampler(D3D11_FILTER filter, D3D11_TEXTURE_ADDRESS_MODE all = D3D11_TEXTURE_ADDRESS_WRAP) const;

	/**
	 * \brief 解放処理
	 */
	void Release() const;

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽
};

}

