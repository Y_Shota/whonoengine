#include "FbxHelper.hpp"

#include <iostream>
#include <iomanip>

using namespace std;
using namespace DirectX;


namespace Whono::FbxHelper {


auto ConvertToXmvector(const FbxDouble4& fbxVector) -> XMVECTOR {

	XMVECTOR vector = g_XMZero;

	for (auto i = 0; i < 4; ++i) {

		vector.m128_f32[i] = static_cast<float>(fbxVector[i]);
	}
	return vector;
}

auto ConvertToXmvector(const FbxDouble3& fbxVector) -> XMVECTOR {

	XMVECTOR vector = g_XMZero;

	for (auto i = 0; i < 3; ++i) {

		vector.m128_f32[i] = static_cast<float>(fbxVector[i]);
	}
	return vector;
}

auto ConvertToXmvector(const FbxDouble2& fbxVector) -> XMVECTOR {

	XMVECTOR vector = g_XMZero;

	for (auto i = 0; i < 2; ++i) {

		vector.m128_f32[i] = static_cast<float>(fbxVector[i]);
	}
	return vector;
}

auto ConvertToXmmatrix(const FbxDouble4x4& fbxMatrix) -> XMMATRIX {

	auto matrix = XMMatrixIdentity();

	for (auto i = 0; i < 4; ++i) {

		matrix.r[i] = ConvertToXmvector(fbxMatrix[i]);
	}
	return matrix;
}

auto ConvertToFbxVector4(CXMVECTOR xmvector) -> FbxVector4 {

	FbxVector4 vector;

	for (auto i = 0; i < 4; ++i) {

		vector[i] = xmvector.m128_f32[i];
	}
	return vector;
}

auto ConvertToFbxAMatrix(CXMMATRIX xmmatrix) -> FbxAMatrix {

	FbxAMatrix matrix;
	
	for (auto i = 0; i < 4; ++i) {

		matrix[i] = ConvertToFbxVector4(xmmatrix.r[i]);
	}
	return matrix;
}

void PrintNodeName(const FbxNode* node) {

	cout << node->GetName();
}

void PrintlnNodeName(const FbxNode* node) {

	PrintNodeName(node);
	cout << endl;
}

void PrintFbxVector(const FbxDouble4& fbxVector) {

	cout << '(';
	for (auto i = 0; i < 4; ++i) {

		cout << setw(10) << setprecision(3) << fbxVector[i] ;
	}
	cout << ')';
}

void PrintlnFbxVector(const FbxDouble4& fbxVector) {

	PrintFbxVector(fbxVector);
	cout << endl;
}

void PrintFbxMatrix(const FbxDouble4x4& fbxMatrix) {

	for (auto i = 0; i < 4; ++i) {

		cout << (i == 0 ? " ( " : "   ");
		PrintFbxVector(fbxMatrix[i]);
		if (i == 3) cout << " ) ";
		else		cout << endl;
	}
}
void PrintlnFbxMatrix(const FbxDouble4x4& fbxMatrix) {

	PrintFbxMatrix(fbxMatrix);
	cout << endl;
}

	
}
