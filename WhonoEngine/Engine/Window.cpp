#include "Window.hpp"

#include <tchar.h>

#include "DirectX11.hpp"
#include "Input.hpp"
#include "Time.hpp"
#include "WinException.hpp"


namespace Whono::Window {

HWND mainWindowHandle;

constexpr auto MIN_WIDTH = 200u;
constexpr auto MIN_HEIGHT = 150u;

UINT width = 800;				//ウィンドウ幅
UINT height = 600;				//ウィンドウ高
UINT prevWidth = width;			//変更前のウィンドウ幅
UINT prevHeight = height;		//変更前のウィンドウ高

void ResizeWindow();
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	
void InitWindow(HINSTANCE hInstance, int nCmdShow) {

	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = nullptr;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = _T(CLASS_NAME);
	wcex.hIconSm = nullptr;

	if (!RegisterClassEx(&wcex))	throw WinException("RegisterClassEX()");

	RECT rect = { 0, 0, static_cast<LONG>(width), static_cast<LONG>(height) };
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE);
	const int windowWidth = rect.right - rect.left;
	const int windowHeight = rect.bottom - rect.top;

	mainWindowHandle = CreateWindow(
		_T(CLASS_NAME),
		_T(WINDOW_NAME),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		windowWidth, windowHeight,
		nullptr,
		nullptr,
		hInstance,
		nullptr
	);
	if (!mainWindowHandle)	throw WinException("CreateWindow()");

	ShowWindow(mainWindowHandle, nCmdShow);
}

auto GetWindowHandle() noexcept -> HWND {

	return mainWindowHandle;
}

auto IsActive( )-> bool {

	auto hWnd = GetActiveWindow();
	return hWnd == mainWindowHandle;
}

auto GetWindowSize() -> RECT {

	RECT client;
	GetClientRect(mainWindowHandle, &client);
	
	return client;
}

auto GetWindowWidth() -> UINT {

	return width;
}

auto GetWindowHeight() -> UINT {

	return height;
}

auto GetWindowWidthF() -> float {

	return static_cast<float>(width);
}

auto GetWindowHeightF() -> float {

	return static_cast<float>(height);
}

auto GetWindowPrevWidthF() -> float {

	return static_cast<float>(prevWidth);
}

auto GetWindowPrevHeightF() -> float {

	return static_cast<float>(prevHeight);
}

auto GetAspectRatio() -> float {

	return static_cast<float>(width) / height;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

	auto result = Input::MouseProc(hWnd, message, wParam, lParam);
	if (result)	return 0;

	switch (message) {
	//ウィンドウの最小サイズの設定
	case WM_GETMINMAXINFO: {
		auto minMaxInfo = reinterpret_cast<LPMINMAXINFO>(lParam);
		minMaxInfo->ptMinTrackSize.x = MIN_WIDTH;
		minMaxInfo->ptMinTrackSize.y = MIN_HEIGHT;
		break;
	}
		
	case WM_CREATE:
		Input::Initialize();
		break;
		
	case WM_SHOWWINDOW:
		DirectX11::Initialize();
		break;

	//ウィンドウ操作中でも描画更新を行うようにする　FPSは下がる
	case WM_ENTERSIZEMOVE:
		SetTimer(hWnd, 1, 1, nullptr);
		break;
	case WM_EXITSIZEMOVE:
		KillTimer(hWnd, 1);
		break;
	case WM_TIMER:
		if (Time::IsUpdateTime()) {

			if (DirectX11::IsInitialized())
				DirectX11::FrameUpdate();
		}
		break;

	//ウィンドウサイズが変更されたらバッファなどもリサイズ
	case WM_SIZE:
		ResizeWindow();
		break;

	case WM_DESTROY:
		DirectX11::Finalize();
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void ResizeWindow() {

	prevWidth = width;
	prevHeight = height;
	auto rect = GetWindowSize();
	width = rect.right;
	height = rect.bottom;
	
	DirectX11::Resize();
}
	
}






