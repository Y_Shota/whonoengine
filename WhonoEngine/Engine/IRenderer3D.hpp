#pragma once
#include <filesystem>
#include <DirectXMath.h>
#include <optional>
#include <string>

//前方宣言
namespace Whono::GameOperation { class Transform; }

namespace Whono::RenderObject {


/**
 * \brief 3D空間レンダリング共通
 * \author whono
 * \version 1.0
 * \since 1.0
 */
class IRenderer3D {

public:
	/**
	 * \brief 仮想デストラクタ
	 */
	virtual ~IRenderer3D() = default;

	/**
	 * \brief リソースを読み込みレンダリングに必要な情報を生成する
	 * \param filePath リソースのファイスパス
	 * \return 生成できたなら真
	 */
	virtual auto Load(const std::filesystem::path& filePath) -> bool = 0;
	
	/**
	 * \brief 変形情報をもとに描画する
	 * \param transform 変形情報
	 * \param frame アニメーションのフレーム数の指定
	 */
	virtual void Render(const GameOperation::Transform* transform, int frame = 0) = 0;

	/**
	 * \brief 変形情報をもとにシャドウマップへ描画する
	 * \param transform 変形情報
	 * \param frame = 0 アニメーションのフレーム数の指定
	 */
	virtual void RenderShadowMap(const GameOperation::Transform* transform, int frame = 0) {}

	/**
	 * \brief アニメーションを持っているかどうか
	 * \return 持っている場合真
	 */
	virtual auto HasAnimation() -> bool = 0;

	/**
	 * \brief ボーンの座標を取得する
	 * \param boneName ボーンの名前
	 * \param frame アニメーションのフレーム数の指定
	 * \return ボーンの座標　ない場合無効値
	 */
	virtual auto GetBonePosition(const std::string& boneName, int frame = 0) -> std::optional<DirectX::XMVECTOR> = 0;

	/**
	 * \brief ボーンの変形情報を取得する
	 * \param boneName ボーンの名前
	 * \param frame アニメーションのフレーム数の指定
	 * \return ボーンの変形情報　ない場合は無効値
	 */
	virtual auto GetBoneTransform(const std::string& boneName, int frame = 0) -> std::unique_ptr<GameOperation::Transform> = 0;
	
	/**
	 * \brief モデル空間でのサイズを取得する
	 * \return サイズ
	 */
	virtual auto GetExtents() -> DirectX::XMVECTOR = 0;
	
	/**
	 * \brief モデル座標系での中心座標を取得する
	 * \return 中心座標
	 */
	virtual auto GetCenterPosition() -> DirectX::XMVECTOR = 0;

	/**
	 * \brief レイキャストを行う
	 * \param origin レイの始点
	 * \param direction レイの方向
	 * \param[out] dist 衝突したときの距離
	 * \param[out] normal 衝突したポリゴンの法線
	 * \return 衝突したなら真
	 */
	virtual auto RayCast(DirectX::CXMVECTOR origin, DirectX::CXMVECTOR direction, float* dist, DirectX::XMVECTOR* normal) const -> bool = 0;
	
	/**
	 * \brief 解放処理
	 */
	virtual void Release() = 0;
};

}