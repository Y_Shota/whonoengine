#pragma once
#include "ISingleton.hpp"

#include <memory>
#include <string>

//前方宣言
namespace Whono::GameOperation { class SceneObject; }


namespace Whono::GameOperation {


/**
 * \brief シーンを管理するクラス
 * ゲーム内のすべての処理はここから呼ばれる
 */
class SceneManager : public DesignPattern::ISingleton<SceneManager> {

public:
	/**
	 * \brief 初期化処理
	 */
	void Initialize() const;
	/**
	 * \brief 更新処理
	 */
	void Update() const;
	/**
	 * \brief 描画処理
	 */
	void Render() const;
	/**
	 * \brief 終了処理
	 */
	void Finalize();
	
	/**
	 * \brief シーンを切り替える
	 * \param sceneName 切り替え先のシーン名
	 */
	void ChangeScene(const std::string& sceneName) const;
	/**
	 * \brief 現在のシーンを取得する
	 * \return 現在のシーン
	 */
	[[nodiscard]]
	auto GetCurrentScene() const noexcept -> SceneObject*;

	/**
	 * \brief ウィンドウサイズのずれに合わせてカメラを更新する
	 */
	void UpdateAllCamera() const;
	
protected:
	/**
	 * \brief コンストラクタ
	 */
	SceneManager();

public:
	/**
	 * \brief デストラクタ
	 */
	~SceneManager();

	//コピー禁止
	SceneManager(const SceneManager&) = delete;
	SceneManager& operator=(const SceneManager&) = delete;

	//ムーブ禁止
	SceneManager(SceneManager&&) = delete;
	SceneManager& operator=(SceneManager&&) = delete;

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽
};

}
