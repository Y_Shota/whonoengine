#include "BoxCollider.hpp"

#include <array>

#include "CollisionCalculator.hpp"
#include "GameObject.hpp"
#include "GraphicsPipeline.hpp"
#include "Model.hpp"

using namespace std;
using namespace DirectX;


namespace Whono::GameOperation {

struct BoxCollider::Impl {

	/**
	 * \brief バウンディングを設定する
	 * \param position 中心座標
	 * \param extents 大きさ
	 */
	void SetBounding(CXMVECTOR position, CXMVECTOR extents) noexcept;
	/**
	 * \brief 境界線を描画する
	 */
	void Render() const;

	BoundingBox bounding;	//計算用バウンディング
	XMVECTOR position;		//中心座標
	XMVECTOR extents;		//大きさ
	GameObject* gameObject;	//所有者
	int hModel;				//モデルハンドル
};

BoxCollider::BoxCollider(CXMVECTOR position, CXMVECTOR extents) {

	pImpl_ = make_unique<Impl>();
	pImpl_->gameObject = nullptr;
	pImpl_->hModel = -1;

	pImpl_->SetBounding(position, extents);

	pImpl_->hModel = Model::Load("Debug/Collision/box.fbx");
	Model::SetPipeline(pImpl_->hModel, PIPELINE_TYPE::DEBUG_3D);
}

BoxCollider::~BoxCollider() = default;

auto BoxCollider::GetCenterPosition() const -> XMVECTOR {

	return Model::GetCenterPosition(pImpl_->hModel);
}

auto BoxCollider::GetNearestControlPoint(CXMVECTOR point) const -> XMVECTOR {

	static constexpr auto PLAIN_COUNT = 6;
	static constexpr auto EDGE_COUNT = 12;
	
	array<XMVECTOR, PLAIN_COUNT + EDGE_COUNT> controlPoints;

	//面の中心と辺の中心方向へのオフセット
	const array<XMVECTOR, PLAIN_COUNT + EDGE_COUNT> OFFSET{ {
		g_XMIdentityR0, g_XMIdentityR1, g_XMIdentityR2,
		g_XMNegIdentityR0, g_XMNegIdentityR1, g_XMNegIdentityR2,
		XMVectorSet(1, 1, 0, 0), XMVectorSet(-1, 1, 0, 0), XMVectorSet(1, -1, 0, 0), XMVectorSet(-1, -1, 0, 0),
		XMVectorSet(1, 0, 1, 0), XMVectorSet(-1, 0, 1, 0), XMVectorSet(1, 0, -1, 0), XMVectorSet(-1, 0, -1, 0),
		XMVectorSet(0, 1, 1, 0), XMVectorSet(0, -1, 1, 0), XMVectorSet(0, 1, -1, 0), XMVectorSet(0, -1, -1, 0),
	} };
	auto* objectTransform = pImpl_->gameObject->GetTransform();
	auto matrix = objectTransform->GetWorldScalingMatrix() * objectTransform->GetWorldTranslationMatrix();
	for (auto i = 0; i < PLAIN_COUNT + EDGE_COUNT; ++i) {

		//少し内側
		auto ratio = i < PLAIN_COUNT ? 0.9f : 0.7f;
		auto localPosition = pImpl_->extents * OFFSET[i] * ratio + pImpl_->position;
		controlPoints[i] = XMVector3Transform(localPosition, matrix);
	}

	//算出した中で受け取った座標と最も近い点を見つける
	auto compare = [&point](auto&& a, auto&& b) {

		return XMVector3Length(a - point).m128_f32[0] < XMVector3Length(b - point).m128_f32[0];
	};
	auto result = ranges::min_element(controlPoints, compare);

	return *result;
}

auto BoxCollider::GetProjectionLengthMax(CXMVECTOR vector) const -> float {

	//ボックスの頂点座標を取得
	array<XMVECTOR, BoundingBox::CORNER_COUNT> corners;
	{
		array<XMFLOAT3, BoundingBox::CORNER_COUNT> cornersBuffer;
		GetBounding().GetCorners(cornersBuffer.data());
		for (auto i = 0u; i < cornersBuffer.size(); ++i) {

			corners[i] = XMLoadFloat3(&cornersBuffer[i]);
		}
	}
	auto center = GetCenterPosition();
	auto length = 0.f;

	for (const auto& corner : corners) {

		auto dot = XMVector3Dot(vector, corner - center).m128_f32[0];
		length = length < dot ? dot : length;
	}
	return length;
}

void BoxCollider::SetGameObject(GameObject* gameObject) {

	pImpl_->gameObject = gameObject;
	
	Model::SetLinkedTransform(pImpl_->hModel, gameObject->GetTransform());
}

void BoxCollider::Render() const {

	pImpl_->Render();
}

auto BoxCollider::IsHit(ICollider* target) const -> bool {
	
	if (auto* sphere = dynamic_cast<SphereCollider*>(target)) {

		return CollisionCalculator::IsHit(this, sphere);
	}
	if (auto* box = dynamic_cast<BoxCollider*>(target)) {

		return CollisionCalculator::IsHit(this, box);
	}
	if (auto* obb = dynamic_cast<ObbCollider*>(target)) {

		return CollisionCalculator::IsHit(this, obb);
	}
	if (auto* frustum = dynamic_cast<FrustumCollider*>(target)) {

		return CollisionCalculator::IsHit(this, frustum);
	}
	if (auto* capsule = dynamic_cast<CapsuleCollider*>(target)) {

		return CollisionCalculator::IsHit(this, capsule);
	}
	return false;
}

auto BoxCollider::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool {

	return Model::RayCast(pImpl_->hModel, origin, direction, dist, normal);
}

auto BoxCollider::GetBounding() const noexcept -> BoundingBox {

	BoundingBox bounding;

	auto matrix = pImpl_->gameObject->GetTransform()->GetWorldMatrix();//= Model::GetTransform(pImpl_->hModel)->GetWorldMatrix();
	pImpl_->bounding.Transform(bounding, matrix);
	
	return bounding;
}

auto BoxCollider::Create(CXMVECTOR position, CXMVECTOR extents) -> unique_ptr<BoxCollider> {

	return make_unique<BoxCollider>(position, extents);
}

auto BoxCollider::Create(CXMVECTOR position, float extentX, float extentY, float extentZ) -> unique_ptr<BoxCollider> {

	return Create(position, XMVectorSet(extentX, extentY, extentZ, 0));
}

auto BoxCollider::Create(float x, float y, float z, CXMVECTOR extents) -> unique_ptr<BoxCollider> {

	return Create(XMVectorSet(x, y, z, 0), extents);
}

auto BoxCollider::Create(float x, float y, float z, float extentX, float extentY, float extentZ) -> unique_ptr<BoxCollider> {

	return Create(XMVectorSet(x, y, z, 0), XMVectorSet(extentX, extentY, extentZ, 0));
}

void BoxCollider::Impl::SetBounding(CXMVECTOR position, CXMVECTOR extents) noexcept {

	this->position = position;
	this->extents = extents;
	XMStoreFloat3(&bounding.Center, position);
	XMStoreFloat3(&bounding.Extents, extents);
}

void BoxCollider::Impl::Render() const {

	auto* modelTransform = Model::GetTransform(hModel);
	//auto* objectTransform = gameObject->GetTransform();

	modelTransform
		->SetLocalPosition(position)
		->SetLocalScale(extents)
		->SetWorldOriented(XMQuaternionIdentity());

#ifdef _DEBUG
	
	Model::Render(hModel);
	
#endif
}

}
