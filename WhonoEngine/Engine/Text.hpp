#pragma once
#include <d2d1helper.h>
#include <dwrite.h>
#include <string>

#include "Image.hpp"
#include "Math/Vector2.hpp"

namespace Whono::Text {

constexpr int UNALLOCATED_HANDLE = -1;

[[nodiscard]]
auto CreateFormat(
	const std::string& fontName,
	float fontSize,
	DWRITE_FONT_WEIGHT weight = DWRITE_FONT_WEIGHT_NORMAL,
	DWRITE_FONT_STYLE style = DWRITE_FONT_STYLE_NORMAL,
	DWRITE_FONT_STRETCH stretch = DWRITE_FONT_STRETCH_NORMAL) -> int;

[[nodiscard]]
auto CreateFormat(
	const std::wstring& fontName,
	float fontSize,
	DWRITE_FONT_WEIGHT weight = DWRITE_FONT_WEIGHT_NORMAL,
	DWRITE_FONT_STYLE style = DWRITE_FONT_STYLE_NORMAL,
	DWRITE_FONT_STRETCH stretch = DWRITE_FONT_STRETCH_NORMAL) -> int;

void Render(int handle, const std::string& text);
void Render(int handle, const std::wstring& text);

void SetPosition(int handle, const Vector2& position, BASE_POSITION baseType);
void SetBasePosition(int handle, BASE_POSITION baseType);
void SetAlignment(int handle, DWRITE_TEXT_ALIGNMENT alignment);
void SetColor(int handle, const D2D1::ColorF& color);
void SetColor(int handle, float r, float g, float b);
void SerColor(int handle, const XMVECTORF32& color);

}

