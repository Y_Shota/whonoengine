#pragma once

struct ID3D11Device;
struct ID3D11DeviceContext;

namespace Whono {

/**
 * \brief グラフィックスパイプラインに関する処理を管理する
 * \author whono
 * \version 1.0
 * \since 1.0
 */
namespace GraphicsPipeline {

class PipelineBundle;

/**
 * \brief シェーダの種類列挙
 */
enum class PIPELINE_TYPE : unsigned char {
	SIMPLE_3D,
	DEBUG_3D,
	SIMPLE_2D,
	SHADOW_MAP,
};


/**
 * \brief 初期化処理
 */
void Initialize();

/**
 * \brief 終了処理
 */
void Finalize();

/**
 * \brief パイプラインのインスタンスを取得する
 * \param type 取得したいパイプラインの列挙型
 * \return パイプライン
 */
[[nodiscard]]
auto GetPipeline(PIPELINE_TYPE type) -> PipelineBundle*;

/**
 * \brief パイプラインをコンテキストへセットする
 * \param type セットしたいパイプラインの列挙型
 */
void SetPipeline(PIPELINE_TYPE type);

}

}