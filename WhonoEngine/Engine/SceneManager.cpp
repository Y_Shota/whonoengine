#include "SceneManager.hpp"

#include <ranges>
#include <unordered_map>
#include <string>
#include <type_traits>

#include "Audio.hpp"
#include "EnablerIfType.hpp"
#include "Image.hpp"
#include "Model.hpp"
#include "SceneObject.hpp"
#include "Time.hpp"

#include "../PlayScene/PlayScene.hpp"
#include "../ResultScene/ResultScene.hpp"
#include "../TitleScene/TitleScene.hpp"

using namespace std;


namespace Whono::GameOperation {



struct SceneManager::Impl {

	/**
	 * \brief 初期化処理
	 */
	void Initialize();
	/**
	 * \brief 更新処理
	 */
	void Update();
	/**
	 * \brief 描画処理
	 */
	void Render();
	/**
	 * \brief 指定シーンを初期シーンとし、準備する
	 * \param sceneName 初期シーン名
	 */
	void SetupFirstScene(const string& sceneName);
	/**
	 * \brief シーンを登録する
	 * \tparam SceneType 登録したいSceneObjectを継承したクラス
	 * \return 生成したシーンインスタンス
	 */
	template<class SceneType, EnablerIfType<is_base_of_v<SceneObject, SceneType>> = nullptr>
	auto RegisterScene() -> SceneType*;
	/**
	 * \brief 現在のシーンを取得する
	 * \return 現在にシーン
	 */
	auto GetCurrentScene() const noexcept -> SceneObject*;

	string currentScene;	//現在のシーン名
	string nextScene;		//次のフレームのシーン名
	unordered_map<string, unique_ptr<SceneObject>> sceneMap;	//シーンインスタンスマップ
};

void SceneManager::Impl::Initialize() {

	//------------------------------------------------------------
	//	ここにシーンを追加
	//
	//	RegisterScene<SceneType>(sceneName)
	//	SceneType	SceneObjectを継承したクラス
	//	sceneName	シーンの名前　重複不可
	//------------------------------------------------------------

	RegisterScene<TitleScene>();
	RegisterScene<PlayScene>();
	RegisterScene<ResultScene>();

	SetupFirstScene(TitleScene::SCENE_NAME);
}


SceneManager::SceneManager() {

	pImpl_ = make_unique<Impl>();
}

SceneManager::~SceneManager() = default;

void SceneManager::Initialize() const {
	
	pImpl_->Initialize();
}

void SceneManager::Update() const {

	pImpl_->Update();
}

void SceneManager::Render() const {

	pImpl_->Render();
}

void SceneManager::Finalize() {

	for (auto& scene : pImpl_->sceneMap | views::values) {
		
		scene.reset();
	}
	pImpl_.reset();
}

void SceneManager::ChangeScene(const string& sceneName) const {

	pImpl_->nextScene = sceneName;
}

auto SceneManager::GetCurrentScene() const noexcept -> SceneObject* {

	return pImpl_->GetCurrentScene();
}

void SceneManager::UpdateAllCamera() const {

	pImpl_->sceneMap.at(pImpl_->currentScene)->UpdateAllCamera();
}

void SceneManager::Impl::Update() {

	//次フレームのシーンが同じ場合更新処理
	if (currentScene == nextScene) {

		auto& scene = sceneMap.at(currentScene);
		scene->Update();
		Model::Update();
	}
	//違う場合
	else {

		auto& scene = sceneMap.at(currentScene);
		auto& next = sceneMap.at(nextScene);

		//常駐リソース以外開放
		Model::Release(false);
		Image::Release(false);
		Audio::Release(false);
		
		//現在のシーンを終了し、次シーンの初期化をする
		scene->Finalize();
		currentScene = nextScene;
		
		next->InitializeAll();
		
		//シーンタイマーをリセット
		Time::ResetSceneTimer();
		
		next->Update();
	}
}

void SceneManager::Impl::Render() {

	auto& scene = sceneMap.at(currentScene);
	scene->Render();
}

void SceneManager::Impl::SetupFirstScene(const string& sceneName) {

	currentScene = nextScene = sceneName;
	sceneMap.at(currentScene)->InitializeAll();
	Time::ResetSceneTimer();
}

template<class SceneType, EnablerIfType<is_base_of_v<SceneObject, SceneType>>>
auto SceneManager::Impl::RegisterScene() -> SceneType* {
	
	auto pScene = make_unique<SceneType>();
	auto* scene = pScene.get();
	sceneMap.emplace(pScene->GetSceneName(), move(pScene));
	
	return scene;
}

auto SceneManager::Impl::GetCurrentScene() const noexcept -> SceneObject* {

	return sceneMap.at(currentScene).get();
}


}