#pragma once

#include <memory>


namespace Whono::DesignPattern {

template<class T>
class ISingleton {

private:
	static std::unique_ptr<T> instance_;

protected:
	ISingleton() = default;

public:
	virtual ~ISingleton() = default;

	ISingleton(const ISingleton&) = delete;
	ISingleton& operator=(const ISingleton&) = delete;

	ISingleton(ISingleton&&) = delete;
	ISingleton& operator=(ISingleton&&) = delete;

	static auto GetInstance() noexcept -> T* {
	
		if (!instance_) {

			struct A : T {};
			instance_ = std::make_unique<A>();
		}
		return instance_.get();
	}

	static void Destroy() noexcept {

		instance_.reset();
	}
};

template<class T>
std::unique_ptr<T> ISingleton<T>::instance_;

}