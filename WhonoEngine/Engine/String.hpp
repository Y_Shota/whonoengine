#pragma once
#include <string>

namespace Whono::String {

[[nodiscard]]
auto ConvertToWString(const std::string& source) -> std::wstring;

[[nodiscard]]
auto ConvertToString(const std::wstring& source) -> std::string;

}