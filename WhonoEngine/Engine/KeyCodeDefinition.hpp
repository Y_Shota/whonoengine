#pragma once

namespace Whono {

//文字コードで対応の仮想キーを統一化

constexpr unsigned VK_A = 'A';
constexpr unsigned VK_B = 'B';
constexpr unsigned VK_C = 'C';
constexpr unsigned VK_D = 'D';
constexpr unsigned VK_E = 'E';
constexpr unsigned VK_F = 'F';
constexpr unsigned VK_G = 'G';
constexpr unsigned VK_H = 'H';
constexpr unsigned VK_I = 'I';
constexpr unsigned VK_J = 'J';
constexpr unsigned VK_K = 'K';
constexpr unsigned VK_L = 'L';
constexpr unsigned VK_M = 'M';
constexpr unsigned VK_N = 'N';
constexpr unsigned VK_O = 'O';
constexpr unsigned VK_P = 'P';
constexpr unsigned VK_Q = 'Q';
constexpr unsigned VK_R = 'R';
constexpr unsigned VK_S = 'S';
constexpr unsigned VK_T = 'T';
constexpr unsigned VK_U = 'U';
constexpr unsigned VK_V = 'V';
constexpr unsigned VK_W = 'W';
constexpr unsigned VK_X = 'X';
constexpr unsigned VK_Y = 'Y';
constexpr unsigned VK_Z = 'Z';

constexpr unsigned VK_0 = '0';
constexpr unsigned VK_1 = '1';
constexpr unsigned VK_2 = '2';
constexpr unsigned VK_3 = '3';
constexpr unsigned VK_4 = '4';
constexpr unsigned VK_5 = '5';
constexpr unsigned VK_6 = '6';
constexpr unsigned VK_7 = '7';
constexpr unsigned VK_8 = '8';
constexpr unsigned VK_9 = '9';


//元のが長いためゲームパッド用マクロ代替

constexpr unsigned PAD_UP			= 0x0001;
constexpr unsigned PAD_DOWN			= 0x0002;
constexpr unsigned PAD_LEFT			= 0x0004;
constexpr unsigned PAD_RIGHT		= 0x0008;
constexpr unsigned PAD_START		= 0x0010;
constexpr unsigned PAD_BACK			= 0x0020;
constexpr unsigned PAD_LTHUMB		= 0x0040;
constexpr unsigned PAD_RTHUMB		= 0x0080;
constexpr unsigned PAD_LSHOULDER	= 0x0100;
constexpr unsigned PAD_RSHOULDER	= 0x0200;
constexpr unsigned PAD_A			= 0x1000;
constexpr unsigned PAD_B			= 0x2000;
constexpr unsigned PAD_X			= 0x4000;
constexpr unsigned PAD_Y			= 0x8000;

}