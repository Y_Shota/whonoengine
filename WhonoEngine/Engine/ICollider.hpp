#pragma once

#include <DirectXMath.h>

//前方宣言
namespace Whono::GameOperation {
class GameObject;
}


namespace Whono::GameOperation {


/**
 * \brief 衝突判定オブジェクトに必要なインターフェース
 */
class ICollider {

public:
	/**
	 * \brief デストラクタ
	 */
	virtual ~ICollider() = default;

	/**
	 * \brief 中心座標を取得
	 * \return 中心座標
	 */
	[[nodiscard]]
	virtual auto GetCenterPosition() const -> DirectX::XMVECTOR = 0;

	/**
	 * \brief 受け取った座標から領域内でもっとも近いであろう点を返す
	 * \param point 比較対象
	 * \return 領域内の座標
	 */
	[[nodiscard]]
	virtual auto GetNearestControlPoint(DirectX::CXMVECTOR point) const -> DirectX::XMVECTOR = 0;
	
	/**
	 * \brief 受け取ったベクトルに射影した長さの最大値を取得
	 * \param vector 射影するベクトル
	 * \return 射影した長さの最大値
	 */
	[[nodiscard]]
	virtual auto GetProjectionLengthMax(DirectX::CXMVECTOR vector) const -> float = 0;
	
	/**
	 * \brief 所持させるゲームオブジェクトを設定
	 * \param gameObject ゲームオブジェクト
	 */
	virtual void SetGameObject(GameObject* gameObject) = 0;
	
	/**
	 * \brief 境界線を描画する
	 */
	virtual void Render() const = 0;
	
	/**
	 * \brief 衝突判定を行う
	 * \param target 検証相手
	 * \return 当たったら真
	 */
	[[nodiscard]]
	virtual auto IsHit(ICollider* target) const -> bool = 0;
	
	/**
	 * \brief レイキャストを行う
	 * \param origin 発射地点
	 * \param direction 方向
	 * \param[out] dist 当たった時の距離
	 * \param[out] normal 当たったポリゴンの法線
	 * \return 当たったなら真
	 */
	[[nodiscard]]
	virtual auto RayCast(DirectX::CXMVECTOR origin, DirectX::CXMVECTOR direction, float* dist, DirectX::XMVECTOR* normal) const -> bool = 0;
};

}
