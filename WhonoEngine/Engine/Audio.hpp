#pragma once
#include <filesystem>

namespace Whono::Audio {

constexpr int UNALLOCATED_HANDLE = -1;

void Initialize();

[[nodiscard]]
auto Load(const std::filesystem::path& filePath) -> int;

void Play(int handle, float volume = 1.f);
void Stop(int handle);

void IsLoop(int handle, bool isLoop);
void SetVolume(int handle, float volume);
void SetResident(int handle, bool resident = false);

void Release(int handle);
void Release(bool isAll = true);

void Finalize();

}
