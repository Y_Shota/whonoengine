#include "Image.hpp"

#include <iostream>

#include "DirectX11.hpp"
#include "DirectXException.hpp"
#include "SceneObject.hpp"
#include "Texture.hpp"
#include "Transform.hpp"
#include "Window.hpp"
#include "WinException.hpp"

using namespace std;
using namespace DirectX;

using RenderObject::Texture;
using GameOperation::Transform;

namespace fs = filesystem;

namespace Whono::Image {

constexpr auto DIRECTORY_PATH = "Assets/";

struct ImageData {

	shared_ptr<Texture> texture;
	fs::path filePath;
	BASE_POSITION baseType = BASE_POSITION::CENTER;
	D2D1_POINT_2F position = { 0, 0 };
	D2D1_POINT_2F scale = { 1, 1 };
	float alpha = 1;
	float angle = 0.f;
	D2D1_RECT_F rect{};
	D2D1_BITMAP_INTERPOLATION_MODE mode = D2D1_BITMAP_INTERPOLATION_MODE_LINEAR;
	int startFrame = 0;
	int endFrame = 0;
	float currentFrame = 0;
	float animationSpeed = 0;
	bool resident = false;
};

vector<unique_ptr<ImageData>> data;

auto CalculationScreenPosition(BASE_POSITION baseType, const D2D1_POINT_2F& position, const D2D1_POINT_2F& scale, const XMUINT2& size) -> D2D1_POINT_2F;
auto HasHandle(int handle) -> bool;


auto Load(const filesystem::path& filePath) -> int {

	auto defaultPath = fs::current_path();
	fs::current_path(DIRECTORY_PATH);

	//画像データインスタンス生成
	auto imageDatum = make_unique<ImageData>();

	//既に読み込まれているか
	auto result = ranges::find_if(data, 
		[&](auto& datum) {
			if (!datum)	return false;
			return datum->filePath == filePath;
		});

	//読み込まれている場合共有
	if (result != data.end()) {

		imageDatum->texture = (*result)->texture;
	}
	//新たに開く
	else {

		imageDatum->texture = make_unique<Texture>();
		try {
			imageDatum->texture->Load(filePath);
		}
		catch (Window::WinException& e) {

			cout << e.what() << endl;
			imageDatum.reset();
			fs::current_path(defaultPath);
			return UNALLOCATED_HANDLE;
		}
		catch (DirectX11::DirectXException& e) {

			cout << e.what() << endl;
			imageDatum.reset();
			fs::current_path(defaultPath);
			return UNALLOCATED_HANDLE;
		}
	}
	//いろいろデータ書き込み
	imageDatum->filePath = filePath;

	auto size = imageDatum->texture->GetSizeVector();
	imageDatum->rect = D2D1::RectF(0, 0, size.m128_f32[0], size.m128_f32[1]);

	//配列内に空きがあった場合そこに移動
	for (auto i = 0u; i < data.size(); ++i) {

		if (!data[i]) {

			data[i] = move(imageDatum);
			fs::current_path(defaultPath);
			return i;
		}
	}

	//最後尾に追加
	data.emplace_back(move(imageDatum));
	fs::current_path(defaultPath);
	return static_cast<int>(data.size()) - 1;
}


void Render(int handle) {

	if (!HasHandle(handle))	return;
	if (SceneObject::GetRenderingPhase() == SceneObject::RENDERING_PHASE::SHADOW)	return;
	
	const auto& d2dContext = DirectX11::GetD2DeviceContext();
	const auto& datum = data[handle];

	const auto& texSize = datum->texture->GetSize();
	auto position = CalculationScreenPosition(datum->baseType, datum->position, datum->scale, texSize);
	auto translation = D2D1::Matrix3x2F::Translation(position.x, position.y);
	auto scale = D2D1::Matrix3x2F::Scale(datum->scale.x, datum->scale.y);
	auto rotatePoint = D2D1::Point2F(texSize.x * 0.5f, texSize.y * 0.5f);
	auto rotation = D2D1::Matrix3x2F::Rotation(datum->angle, rotatePoint);
	auto mat = rotation * scale * translation;
	d2dContext->SetTransform(mat);

	d2dContext->BeginDraw();
	d2dContext->DrawBitmap(datum->texture->GetD2DBitmap().Get(), nullptr, datum->alpha, datum->mode, &datum->rect);
	d2dContext->EndDraw();
	
	d2dContext->SetTransform(D2D1::Matrix3x2F::Identity());
}

void AllowAntiAliasing(int handle, bool antiAliasing) {

	if (!HasHandle(handle))	return;

	data[handle]->mode = antiAliasing ? D2D1_BITMAP_INTERPOLATION_MODE_LINEAR : D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR;
}

void SetTransform(int handle, const Transform* transform, BASE_POSITION baseType) {

	if (!HasHandle(handle))	return;

	const auto& position = transform->GetWorldPosition();
	SetPosition(handle, position.m128_f32[0], position.m128_f32[1], baseType);
	XMVECTOR axis;
	float angle;
	XMQuaternionToAxisAngle(&axis, &angle, transform->GetWorldOriented());
	SetRotate(handle, angle);
	const auto& scale = transform->GetWorldScale();
	SetScale(handle, scale.m128_f32[0], scale.m128_f32[1]);
}

void SetPosition(int handle, float x, float y, BASE_POSITION baseType) {

	if (!HasHandle(handle))	return;

	data[handle]->position = D2D1::Point2F(x, y);
	data[handle]->baseType = baseType;
}

void SetPosition(int handle, const Vector2& position, BASE_POSITION baseType) {

	if (!HasHandle(handle))	return;

	data[handle]->position = D2D1::Point2F(position.X(), position.Y());
	data[handle]->baseType = baseType;
}

void SetRotate(int handle, float angle) {

	if (!HasHandle(handle))	return;

	data[handle]->angle = angle;
}

void SetScale(int handle, float x, float y) {

	SetScale(handle, Vector2::Set(x, y));
}

void SetScale(int handle, const Vector2& scaling) {
	
	if (!HasHandle(handle))	return;

	data[handle]->scale = D2D1::Point2F(scaling.X(), scaling.Y());
}

void SetFullSize(int handle) {

	if (!HasHandle(handle))	return;

	auto& textureSize = data[handle]->texture->GetSize();
	auto windowSize = Window::GetWindowSize();

	data[handle]->scale = { static_cast<FLOAT>(windowSize.right) / textureSize.x, static_cast<FLOAT>(windowSize.bottom) / textureSize.y };
}

void SetAlpha(int handle, float alpha) {

	if (!HasHandle(handle))	return;

	data[handle]->alpha = clamp(alpha, 0.f, 1.f);
}

void SetRect(int handle, float left, float top, float width, float height) {

	if (!HasHandle(handle))	return;

	data[handle]->rect = D2D1::RectF(left, top, left + width, top + height);
}

void SetBasePosition(int handle, BASE_POSITION baseType) {

	if (!HasHandle(handle))	return;


	data[handle]->baseType = baseType;
}

void SetResident(int handle, bool resident) {

	if (!HasHandle(handle))	return;

	data[handle]->resident = resident;
}

auto GetImageSize(int handle) -> Vector2 {

	if (!HasHandle(handle)) return Math::ZERO_VECTOR_2D;

	auto size = data[handle]->texture->GetSize();

	return Vector2::Set(static_cast<float>(size.x), static_cast<float>(size.y));
}

void Release(int handle) {

	if (!HasHandle(handle)) return;

	data[handle]->texture.reset();
	data[handle].reset();
}

void Release(bool isAll) {

	for (auto& datum : data) {

		if (isAll && datum) {

			if (datum->texture)	datum->texture.reset();
			datum.reset();
		}
		else if (datum && !datum->resident) {
				
			if (datum->texture)	datum->texture.reset();
			datum.reset();
		}
	} 
}

auto CalculationScreenPosition(BASE_POSITION baseType, const D2D1_POINT_2F& position, const D2D1_POINT_2F& scale, const XMUINT2& size) -> D2D1_POINT_2F {

	auto width = Window::GetWindowWidthF();
	auto height = Window::GetWindowHeightF();

	auto pos = position;
	auto texSize = D2D1::Point2F(size.x * scale.x * 0.5f, size.y * scale.y * 0.5f);
	
	switch (baseType) {
	using enum BASE_POSITION;
	case CENTER:
		pos.x += width * 0.5f - texSize.x;
		pos.y += height * 0.5f - texSize.y;
		break;
	case TOP_LEFT:
		pos.x -= texSize.x;
		pos.y -= texSize.y;
		break;
	case TOP_RIGHT:
		pos.x += width - texSize.x;
		pos.y -= texSize.y;
		break;
	case BOTTOM_LEFT:
		pos.x -= texSize.x;
		pos.y += height - texSize.y;
		break;
	case BOTTOM_RIGHT:
		pos.x += width - texSize.x;
		pos.y += height - texSize.y;
		break;
	case TOP_CENTER:
		pos.x += width * 0.5f - texSize.x;
		pos.y -= texSize.y;
		break;
	case BOTTOM_CENTER:
		pos.x += width * 0.5f - texSize.x;
		pos.y += height - texSize.y;
		break;
	case LEFT_CENTER:
		pos.x -= texSize.x;
		pos.y += height * 0.5f - texSize.y;
		break;
	case RIGHT_CENTER:
		pos.x += width - texSize.x;
		pos.y += height * 0.5f - texSize.y;
		break;
	default:
		break;
	}
	return pos;
}

auto HasHandle(int handle) -> bool {

	if (handle < 0)									return false;
	if (handle >= static_cast<int>(data.size()))	return false;
	if (!data[handle])								return false;

	return true;
}


}
