#include "ObbCollider.hpp"

#include <array>

#include "CollisionCalculator.hpp"
#include "GameObject.hpp"
#include "Math/Math.hpp"
#include "GraphicsPipeline.hpp"
#include "Model.hpp"

using namespace std;
using namespace DirectX;


namespace Whono::GameOperation {


struct ObbCollider::Impl {

	/**
	 * \brief バウンディングを設定する
	 * \param position 中心座標
	 * \param extents 大きさ
	 * \param oriented 姿勢
	 */
	void SetBounding(CXMVECTOR position, CXMVECTOR extents, CXMVECTOR oriented) noexcept;
	/**
	 * \brief 境界線を描画する
	 */
	void Render() const;

	BoundingOrientedBox bounding;	//計算用バウンディング
	XMVECTOR position;				//中心座標
	XMVECTOR extents;				//大きさ
	XMVECTOR oriented;				//姿勢
	GameObject* gameObject;			//所有者
	int hModel;						//モデルハンドル
};


ObbCollider::ObbCollider(CXMVECTOR position, CXMVECTOR extents, CXMVECTOR oriented) {

	pImpl_ = make_unique<Impl>();
	pImpl_->gameObject = nullptr;
	pImpl_->hModel = -1;

	pImpl_->SetBounding(position, extents, oriented);
	
	pImpl_->hModel = Model::Load("Debug/Collision/box.fbx");
	Model::SetPipeline(pImpl_->hModel, PIPELINE_TYPE::DEBUG_3D);
	
}

ObbCollider::~ObbCollider() = default;

auto ObbCollider::GetCenterPosition() const -> XMVECTOR {

	return Model::GetCenterPosition(pImpl_->hModel);
}

auto ObbCollider::GetNearestControlPoint(CXMVECTOR point) const -> XMVECTOR {

	static constexpr auto PLAIN_COUNT = 6;
	static constexpr auto EDGE_COUNT = 12;

	array<XMVECTOR, PLAIN_COUNT + EDGE_COUNT> controlPoints;

	//面の中心と辺の中心方向へのオフセット
	const array<XMVECTOR, PLAIN_COUNT + EDGE_COUNT> OFFSET{ {
		g_XMIdentityR0, g_XMIdentityR1, g_XMIdentityR2,
		g_XMNegIdentityR0, g_XMNegIdentityR1, g_XMNegIdentityR2,
		XMVectorSet(1, 1, 0, 0), XMVectorSet(-1, 1, 0, 0), XMVectorSet(1, -1, 0, 0), XMVectorSet(-1, -1, 0, 0),
		XMVectorSet(1, 0, 1, 0), XMVectorSet(-1, 0, 1, 0), XMVectorSet(1, 0, -1, 0), XMVectorSet(-1, 0, -1, 0),
		XMVectorSet(0, 1, 1, 0), XMVectorSet(0, -1, 1, 0), XMVectorSet(0, 1, -1, 0), XMVectorSet(0, -1, -1, 0),
	} };
	auto* objectTransform = pImpl_->gameObject->GetTransform();
	auto matrix = objectTransform->GetWorldScalingMatrix() * objectTransform->GetWorldTranslationMatrix();
	for (auto i = 0; i < PLAIN_COUNT + EDGE_COUNT; ++i) {

		//少し内側
		auto ratio = i < PLAIN_COUNT ? 0.9f : 0.7f;
		auto localPosition = XMVector3Rotate(pImpl_->extents * OFFSET[i] * ratio, pImpl_->oriented) + pImpl_->position;
		controlPoints[i] = XMVector3Transform(localPosition, matrix);
	}

	//算出した中で受け取った座標と最も近い点を見つける
	auto compare = [&point](auto&& a, auto&& b) {

		return XMVector3Length(a - point).m128_f32[0] < XMVector3Length(b - point).m128_f32[0];
	};
	auto result = ranges::min_element(controlPoints, compare);

	return *result;
}

auto ObbCollider::GetProjectionLengthMax(CXMVECTOR vector) const -> float {

	//todo
	return 0.f;
}

void ObbCollider::SetGameObject(GameObject* gameObject) {

	pImpl_->gameObject = gameObject;

	Model::SetLinkedTransform(pImpl_->hModel, gameObject->GetTransform());
}

void ObbCollider::Render() const {

	pImpl_->Render();
}

auto ObbCollider::IsHit(ICollider* target) const -> bool {

	if (auto* sphere = dynamic_cast<SphereCollider*>(target)) {

		return CollisionCalculator::IsHit(this, sphere);
	}
	if (auto* box = dynamic_cast<BoxCollider*>(target)) {

		return CollisionCalculator::IsHit(this, box);
	}
	if (auto* obb = dynamic_cast<ObbCollider*>(target)) {

		return CollisionCalculator::IsHit(this, obb);
	}
	if (auto* frustum = dynamic_cast<FrustumCollider*>(target)) {

		return CollisionCalculator::IsHit(this, frustum);
	}
	if (auto* capsule = dynamic_cast<CapsuleCollider*>(target)) {

		return CollisionCalculator::IsHit(this, capsule);
	}
	return false;
}

auto ObbCollider::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool {

	return Model::RayCast(pImpl_->hModel, origin, direction, dist, normal);
}

auto ObbCollider::GetBounding() const noexcept -> BoundingOrientedBox {

	BoundingOrientedBox bounding;
	
	auto matrix = pImpl_->gameObject->GetTransform()->GetWorldMatrix();
	pImpl_->bounding.Transform(bounding, matrix);

	return bounding;
}

auto ObbCollider::Create(CXMVECTOR position, CXMVECTOR extents, CXMVECTOR oriented) -> unique_ptr<ObbCollider> {

	return make_unique<ObbCollider>(position, extents, oriented);
}

auto ObbCollider::Create(CXMVECTOR position, float extentX, float extentY, float extentZ, CXMVECTOR oriented) -> unique_ptr<ObbCollider> {

	return Create(position, XMVectorSet(extentX, extentY, extentZ, 0), oriented);
}

auto ObbCollider::Create(float x, float y, float z, CXMVECTOR extents, CXMVECTOR oriented) -> unique_ptr<ObbCollider> {

	return Create(XMVectorSet(x, y, z, 0), extents, oriented);
}

auto ObbCollider::Create(float x, float y, float z, float extentX, float extentY, float extentZ, CXMVECTOR oriented) -> unique_ptr<ObbCollider> {

	return Create(XMVectorSet(x, y, z, 0), XMVectorSet(extentX, extentY, extentZ, 0), oriented);
}

auto ObbCollider::Create(CXMVECTOR position, CXMVECTOR extents, CXMVECTOR axis, float angle) -> unique_ptr<ObbCollider> {

	return Create(position, extents, XMQuaternionRotationAxis(axis, XMConvertToRadians(angle)));
}

auto ObbCollider::Create(CXMVECTOR position, float extentX, float extentY, float extentZ, CXMVECTOR axis, float angle) -> unique_ptr<ObbCollider> {

	return Create(position, XMVectorSet(extentX, extentY, extentZ, 0), axis, angle);
}

auto ObbCollider::Create(float x, float y, float z, CXMVECTOR extents, CXMVECTOR axis, float angle) -> unique_ptr<ObbCollider> {

	return Create(XMVectorSet(x, y, z, 0), extents, axis, angle);
}

auto ObbCollider::Create(float x, float y, float z, float extentX, float extentY, float extentZ, CXMVECTOR axis, float angle) -> unique_ptr<ObbCollider> {

	return Create(x, y, z, XMVectorSet(extentX, extentY, extentZ, 0), axis, angle);
}

auto ObbCollider::Create(CXMVECTOR position, CXMVECTOR extents, float rotateX, float rotateY, float rotateZ) -> unique_ptr<ObbCollider> {

	auto rotate = XMVectorSet(rotateX, rotateY, rotateZ, 0);
	rotate = Math::ConvertToRadiansVector(rotate);
	
	return Create(position, extents, XMQuaternionRotationRollPitchYawFromVector(rotate));
}

auto ObbCollider::Create(CXMVECTOR position, float extentX, float extentY, float extentZ, float rotateX, float rotateY, float rotateZ) -> unique_ptr<ObbCollider> {

	return Create(position, XMVectorSet(extentX, extentY, extentZ, 0), rotateX, rotateY, rotateZ);
}

auto ObbCollider::Create(float x, float y, float z, CXMVECTOR extents, float rotateX, float rotateY, float rotateZ) -> unique_ptr<ObbCollider> {

	return Create(XMVectorSet(z, y, z, 0), extents, rotateX, rotateY, rotateZ);
}

auto ObbCollider::Create(float x, float y, float z, float extentX, float extentY, float extentZ, float rotateX, float rotateY, float rotateZ) -> unique_ptr<ObbCollider> {

	return Create(XMVectorSet(z, y, z, 0), extentX, extentY, extentZ, rotateX, rotateY, rotateZ);
}

void ObbCollider::Impl::SetBounding(CXMVECTOR position, CXMVECTOR extents, CXMVECTOR oriented) noexcept {

	this->position = position;
	this->extents = extents;
	this->oriented = oriented;

	XMStoreFloat3(&bounding.Center, position);
	XMStoreFloat3(&bounding.Extents, extents);
	XMStoreFloat4(&bounding.Orientation, oriented);
}

void ObbCollider::Impl::Render() const {

	auto* modelTransform = Model::GetTransform(hModel);

	modelTransform
		->SetLocalPosition(position)
		->SetLocalScale(extents)
		->SetLocalOriented(oriented);

#ifdef _DEBUG

	Model::Render(hModel);

#endif
}

}
