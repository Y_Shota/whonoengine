#pragma once
#include <ostream>


#include "../EnablerIfType.hpp"
#include "Math.hpp"


namespace Whono {
namespace Math {

class Vector3;

class Vector2 final {

public:
	/**
	 * \brief コンストラクタ
	 */
	constexpr Vector2();
	/**
	 * \brief コンストラクタ
	 * \param x X成分
	 * \param y Y成分
	 */
	constexpr Vector2(float x, float y);

	//コピームーブを許可
	constexpr Vector2(const Vector2&) = default;
	constexpr auto operator=(const Vector2&)->Vector2& = default;
	constexpr Vector2(Vector2&&) = default;
	constexpr auto operator=(Vector2&&)->Vector2& = default;

	explicit Vector2(const Vector3& rhs);
	auto operator=(const Vector3& rhs)->Vector2&;

	/**
	 * \brief X成分を取得
	 * \return X成分
	 */
	constexpr auto X() const noexcept -> float;
	/**
	 * \brief Y成分を取得
	 * \return Y成分
	 */
	constexpr auto Y() const noexcept -> float;

	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XX() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YY() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YX() const noexcept -> Vector2;

	/**
	 * \brief X成分を設定
	 * \param x 設定する値
	 * \return メソッドチェイン
	 */
	auto X(float x) noexcept -> Vector2&;
	/**
	 * \brief Y成分を設定
	 * \param y 設定する値
	 * \return メソッドチェイン
	 */
	auto Y(float y) noexcept -> Vector2&;

	/**
	 * \brief 正規化する
	 * \return メソッドチェイン
	 */
	auto Normalize() noexcept -> Vector2&;

	/**
	 * \brief コンポーネントを配列と見た時の先頭アドレスの取得
	 * \return 先頭アドレス
	 */
	constexpr auto Data() const noexcept -> float const*;

	/**
	 * \brief 長さを取得
	 * \return 長さ
	 */
	constexpr auto Length() const noexcept -> float;

	/**
	 * \brief 零ベクトルかどうか
	 * \return 零ベクトルなら真
	 */
	constexpr auto IsZero() const noexcept -> bool;

	/**
	 * \brief ベクトルを作成する
	 * \param x X成分
	 * \param y Y成分
	 * \return 生成したベクトル
	 */
	static constexpr auto Set(float x, float y) noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに値を複製したベクトルを作成する
	 * \param value 複製する値
	 * \return 生成したベクトル
	 */
	static constexpr auto Replicate(float value) noexcept -> Vector2;
	/**
	 * \brief 正規化したベクトルを取得
	 * \return 正規化したベクトル
	 */
	static constexpr auto Normalize(const Vector2& vector) noexcept -> Vector2;
	/**
	 * \brief 逆ベクトルを取得
	 * \return 逆ベクトル
	 */
	static constexpr auto Inverse(const Vector2& vector) noexcept -> Vector2;
	/**
	 * \brief 逆数ベクトルを取得
	 * \return 逆数ベクトル
	 */
	static constexpr auto Reciprocal(const Vector2& vector) noexcept -> Vector2;
	/**
	 * \brief 内積を行う
	 * \param lhs 左辺
	 * \param rhs 右辺
	 * \return 内積
	 */
	static constexpr auto Dot(const Vector2& lhs, const Vector2& rhs) noexcept -> float;
	/**
	 * \brief 外積を行う
	 * \param lhs 左辺
	 * \param rhs 右辺
	 * \return ベクトル
	 */
	static constexpr auto Cross(const Vector2& lhs, const Vector2& rhs) noexcept -> float;

	explicit constexpr operator bool() const noexcept;
	constexpr auto operator!() const noexcept -> bool;

	constexpr auto operator+() const noexcept -> Vector2;
	constexpr auto operator-() const noexcept -> Vector2;
	constexpr auto operator~() const noexcept -> Vector2;

	auto operator+=(const Vector2& rhs) noexcept -> Vector2&;
	auto operator-=(const Vector2& rhs) noexcept -> Vector2&;
	auto operator*=(const Vector2& rhs) noexcept -> Vector2&;
	auto operator/=(const Vector2& rhs) noexcept -> Vector2&;
	template<class T, EnablerIfType<std::is_arithmetic_v<T>> = nullptr>
	auto operator+=(T rhs) noexcept -> Vector2&;
	template<class T, EnablerIfType<std::is_arithmetic_v<T>> = nullptr>
	auto operator-=(T rhs) noexcept -> Vector2&;
	template<class T, EnablerIfType<std::is_arithmetic_v<T>> = nullptr>
	auto operator*=(T rhs) noexcept -> Vector2&;
	template<class T, EnablerIfType<std::is_arithmetic_v<T>> = nullptr>
	auto operator/=(T rhs) noexcept -> Vector2&;

	static constexpr int DIMENSION = 2;

private:
	float v[DIMENSION];
};

constexpr auto operator+(const Vector2& lhs, const Vector2& rhs) noexcept -> Vector2;
constexpr auto operator-(const Vector2& lhs, const Vector2& rhs) noexcept -> Vector2;
constexpr auto operator*(const Vector2& lhs, const Vector2& rhs) noexcept -> Vector2;
constexpr auto operator/(const Vector2& lhs, const Vector2& rhs) noexcept -> Vector2;
template<class T, EnablerIfType<std::is_arithmetic_v<T>> = nullptr>
constexpr auto operator+(const Vector2& lhs, T rhs) noexcept -> Vector2;
template<class T, EnablerIfType<std::is_arithmetic_v<T>> = nullptr>
constexpr auto operator-(const Vector2& lhs, T rhs) noexcept -> Vector2;
template<class T, EnablerIfType<std::is_arithmetic_v<T>> = nullptr>
constexpr auto operator*(const Vector2& lhs, T rhs) noexcept -> Vector2;
template<class T, EnablerIfType<std::is_arithmetic_v<T>> = nullptr>
constexpr auto operator/(const Vector2& lhs, T rhs) noexcept -> Vector2;

constexpr auto operator>(const Vector2& lhs, const Vector2& rhs) noexcept -> bool;
constexpr auto operator>=(const Vector2& lhs, const Vector2& rhs) noexcept -> bool;
constexpr auto operator<(const Vector2& lhs, const Vector2& rhs) noexcept -> bool;
constexpr auto operator<=(const Vector2& lhs, const Vector2& rhs) noexcept -> bool;

constexpr auto operator==(const Vector2& lhs, const Vector2& rhs) noexcept -> bool;
constexpr auto operator!=(const Vector2& lhs, const Vector2& rhs) noexcept -> bool;

auto operator<<(std::ostream& os, const Vector2& rhs) noexcept -> std::ostream&;


constexpr Vector2::Vector2()
	: Vector2(0, 0) {}

constexpr Vector2::Vector2(float x, float y)
	: v{ x, y } {}

constexpr auto Vector2::X() const noexcept -> float {

	return v[0];
}

constexpr auto Vector2::Y() const noexcept -> float {

	return v[1];
}

constexpr auto Vector2::XX() const noexcept -> Vector2 {

	return Replicate(X());
}

constexpr auto Vector2::YY() const noexcept -> Vector2 {

	return Replicate(Y());
}

constexpr auto Vector2::YX() const noexcept -> Vector2 {

	return Set(Y(), X());
}

constexpr auto Vector2::Data() const noexcept -> float const* {

	return v;
}

constexpr auto Vector2::Length() const noexcept -> float {

	auto&& square = *this * *this;
	return SquareRoot(square.X() + square.Y());
}

constexpr auto Vector2::IsZero() const noexcept -> bool {

	return *this == Set(0, 0);
}
constexpr auto Vector2::Set(float x, float y) noexcept -> Vector2 {

	return Vector2(x, y);
}

constexpr auto Vector2::Replicate(float value) noexcept -> Vector2 {

	return Vector2(value, value);
}

constexpr auto Vector2::Normalize(const Vector2& vector) noexcept -> Vector2 {

	return vector / vector.Length();
}

constexpr auto Vector2::Inverse(const Vector2& vector) noexcept -> Vector2 {

	return Set(-vector.X(), -vector.Y());
}

constexpr auto Vector2::Reciprocal(const Vector2& vector) noexcept -> Vector2 {

	return Set(1 / vector.X(), 1 / vector.Y());
}
constexpr auto Vector2::Dot(const Vector2& lhs, const Vector2& rhs) noexcept -> float {

	auto&& tmp = lhs * rhs;
	return tmp.X() + tmp.Y();
}

constexpr auto Vector2::Cross(const Vector2& lhs, const Vector2& rhs) noexcept -> float {

	auto&& tmp = lhs * rhs.YX();
	return tmp.X() - tmp.Y();
}

constexpr Vector2::operator bool() const noexcept {

	return !IsZero();
}

constexpr auto Vector2::operator!() const noexcept -> bool {

	return IsZero();
}

constexpr auto Vector2::operator+() const noexcept -> Vector2 {

	return *this;
}

constexpr auto Vector2::operator-() const noexcept -> Vector2 {

	return Inverse(*this);
}

constexpr auto Vector2::operator~() const noexcept -> Vector2 {

	return Reciprocal(*this);
}

template <class T, EnablerIfType<std::is_arithmetic_v<T>>>
auto Vector2::operator+=(T rhs) noexcept -> Vector2& {

	return *this += Replicate(static_cast<float>(rhs));
}

template <class T, EnablerIfType<std::is_arithmetic_v<T>>>
auto Vector2::operator-=(T rhs) noexcept -> Vector2& {

	return *this -= Replicate(static_cast<float>(rhs));
}

template <class T, EnablerIfType<std::is_arithmetic_v<T>>>
auto Vector2::operator*=(T rhs) noexcept -> Vector2& {

	return *this *= Replicate(static_cast<float>(rhs));
}

template <class T, EnablerIfType<std::is_arithmetic_v<T>>>
auto Vector2::operator/=(T rhs) noexcept -> Vector2& {

	return *this /= Replicate(static_cast<float>(rhs));
}

constexpr auto operator+(const Vector2& lhs, const Vector2& rhs) noexcept -> Vector2 {

	return Vector2::Set(lhs.X() + rhs.X(), lhs.Y() + rhs.Y());
}

constexpr auto operator-(const Vector2& lhs, const Vector2& rhs) noexcept -> Vector2 {

	return Vector2::Set(lhs.X() - rhs.X(), lhs.Y() - rhs.Y());
}

constexpr auto operator*(const Vector2& lhs, const Vector2& rhs) noexcept -> Vector2 {

	return Vector2::Set(lhs.X() * rhs.X(), lhs.Y() * rhs.Y());
}

constexpr auto operator/(const Vector2& lhs, const Vector2& rhs) noexcept -> Vector2 {

	return Vector2::Set(lhs.X() / rhs.X(), lhs.Y() / rhs.Y());
}

template<class T, EnablerIfType<std::is_arithmetic_v<T>>>
constexpr auto operator+(const Vector2& lhs, T rhs) noexcept -> Vector2 {

	return lhs + Vector2::Replicate(static_cast<float>(rhs));
}

template<class T, EnablerIfType<std::is_arithmetic_v<T>>>
constexpr auto operator-(const Vector2& lhs, T rhs) noexcept -> Vector2 {

	return lhs - Vector2::Replicate(static_cast<float>(rhs));
}

template<class T, EnablerIfType<std::is_arithmetic_v<T>>>
constexpr auto operator*(const Vector2& lhs, T rhs) noexcept -> Vector2 {

	return lhs * Vector2::Replicate(static_cast<float>(rhs));
}

template<class T, EnablerIfType<std::is_arithmetic_v<T>>>
constexpr auto operator/(const Vector2& lhs, T rhs) noexcept -> Vector2 {

	return lhs / Vector2::Replicate(static_cast<float>(rhs));
}

constexpr auto operator>(const Vector2& lhs, const Vector2& rhs) noexcept -> bool {

	return lhs.X() > rhs.X() && lhs.Y() > rhs.Y();
}

constexpr auto operator>=(const Vector2& lhs, const Vector2& rhs) noexcept -> bool {

	return lhs.X() >= rhs.X() && lhs.Y() >= rhs.Y();
}

constexpr auto operator<(const Vector2& lhs, const Vector2& rhs) noexcept -> bool {

	return lhs.X() < rhs.X() && lhs.Y() < rhs.Y();
}

constexpr auto operator<=(const Vector2& lhs, const Vector2& rhs) noexcept -> bool {

	return lhs.X() <= rhs.X() && lhs.Y() <= rhs.Y();
}

constexpr auto operator==(const Vector2& lhs, const Vector2& rhs) noexcept -> bool {

	return lhs.X() == rhs.X() && lhs.Y() == rhs.Y();
}

constexpr auto operator!=(const Vector2& lhs, const Vector2& rhs) noexcept -> bool {

	return lhs.X() != rhs.X() || lhs.Y() != rhs.Y();
}

constexpr Vector2 X_AXIS_2D = Vector2::Set(1, 0);
constexpr Vector2 Y_AXIS_2D = Vector2::Set(0, 1);
constexpr Vector2 X_AXIS_INV_2D = -X_AXIS_2D;
constexpr Vector2 Y_AXIS_INV_2D = -Y_AXIS_2D;
constexpr Vector2 ZERO_VECTOR_2D = Vector2::Set(0, 0);

}

using Math::Vector2;

}
