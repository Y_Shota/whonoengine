#pragma once
#include <ostream>

#include "Math.hpp"
#include "Vector2.hpp"


namespace Whono {

namespace Math {

class Quaternion;

class Vector3 final {

public:
	/**
	 * \brief コンストラクタ
	 */
	constexpr Vector3();
	/**
	 * \brief コンストラクタ
	 * \param x X成分
	 * \param y Y成分
	 * \param z Z成分
	 */
	constexpr Vector3(float x, float y, float z);

	//コピームーブを許可
	constexpr Vector3(const Vector3&) = default;
	constexpr auto operator=(const Vector3&) -> Vector3& = default;
	constexpr Vector3(Vector3&&) = default;
	constexpr auto operator=(Vector3&&) -> Vector3& = default;

	// ReSharper disable once CppNonExplicitConvertingConstructor
	constexpr Vector3(const Vector2& rhs);
	constexpr auto operator=(const Vector2& rhs) -> Vector3&;

	/**
	 * \brief X成分を取得
	 * \return X成分
	 */
	constexpr auto X() const noexcept -> float;
	/**
	 * \brief Y成分を取得
	 * \return Y成分
	 */
	constexpr auto Y() const noexcept -> float;
	/**
	 * \brief Z成分を取得
	 * \return Z成分
	 */
	constexpr auto Z() const noexcept -> float;

	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XX() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XY() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XZ() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YX() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YY() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YZ() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZX() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZY() const noexcept -> Vector2;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZZ() const noexcept -> Vector2;

	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XXX() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XXY() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XXZ() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XYX() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XYY() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XYZ() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XZX() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XZY() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto XZZ() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YXX() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YXY() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YXZ() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YYX() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YYY() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YYZ() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YZX() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YZY() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto YZZ() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZXX() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZXY() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZXZ() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZYX() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZYY() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZYZ() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZZX() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZZY() const noexcept -> Vector3;
	/**
	 * \brief 各コンポーネントに指定成分を設定したベクトルを取得
	 * \return 各コンポーネントに指定成分を設定したベクトル
	 */
	constexpr auto ZZZ() const noexcept -> Vector3;

	/**
	 * \brief X成分を設定
	 * \param x 設定する値
	 * \return メソッドチェイン
	 */
	auto X(float x) noexcept -> Vector3&;
	/**
	 * \brief Y成分を設定
	 * \param y 設定する値
	 * \return メソッドチェイン
	 */
	auto Y(float y) noexcept -> Vector3&;
	/**
	 * \brief Z成分を設定
	 * \param z 設定する値
	 * \return メソッドチェイン
	 */
	auto Z(float z) noexcept -> Vector3&;
	/**
	 * \brief 正規化する
	 * \return メソッドチェイン
	 */
	auto Normalize() noexcept -> Vector3&;

	/**
	 * \brief コンポーネントを配列と見た時の先頭アドレスの取得
	 * \return 先頭アドレス
	 */
	constexpr auto Data() const noexcept -> float const*;

	/**
	 * \brief 長さを取得
	 * \return 長さ
	 */
	constexpr auto Length() const noexcept -> float;
	/**
	 * \brief 零ベクトルかどうか
	 * \return 零ベクトルなら真
	 */
	constexpr auto IsZero() const noexcept -> bool;

	/**
	 * \brief ベクトルを作成する
	 * \param x X成分
	 * \param y Y成分
	 * \param z Z成分
	 * \return 生成したベクトル
	 */
	static constexpr auto Set(float x, float y, float z) noexcept -> Vector3;
	/**
	 * \brief 2次元ベクトルとZ成分から3次元ベクトルを作成する
	 * \param vector2 XYベクトル
	 * \param z Z成分
	 * \return 生成したベクトル
	 */
	static constexpr auto Set(const Vector2& vector2, float z) noexcept -> Vector3;
	/**
	 * \brief 配列から3次元ベクトルを作成する
	 * \param data 配列の戦闘アドレス
	 * \return 生成ベクトル
	 */
	static auto Set(const float* data) -> Vector3;
	/**
	 * \brief 各コンポーネントに値を複製したベクトルを作成する
	 * \param value 複製する値
	 * \return 生成したベクトル
	 */
	static constexpr auto Replicate(float value) -> Vector3;
	/**
	 * \brief 正規化したベクトルを取得
	 * \return 正規化したベクトル
	 */
	static constexpr auto Normalize(const Vector3& vector) noexcept -> Vector3;
	/**
	 * \brief 逆ベクトルを取得
	 * \return 逆ベクトル
	 */
	static constexpr auto Inverse(const Vector3& vector) noexcept -> Vector3;
	/**
	 * \brief 逆数ベクトルを取得
	 * \return 逆数ベクトル
	 */
	static constexpr auto Reciprocal(const Vector3& vector) noexcept -> Vector3;
	/**
	 * \brief 内積を行う
	 * \param lhs 左辺
	 * \param rhs 右辺
	 * \return 内積
	 */
	static constexpr auto Dot(const Vector3& lhs, const Vector3& rhs) noexcept -> float;
	/**
	 * \brief 外積を行う
	 * \param lhs 左辺
	 * \param rhs 右辺
	 * \return ベクトル
	 */
	static constexpr auto Cross(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3;

	explicit constexpr operator Vector2() const noexcept;

	explicit constexpr operator bool() const noexcept;
	constexpr auto operator!() const noexcept -> bool;

	constexpr auto operator[](std::size_t index) const -> const float&;
	auto operator[](std::size_t index) -> float&;

	constexpr auto operator+() const noexcept -> Vector3;
	constexpr auto operator-() const noexcept -> Vector3;
	constexpr auto operator~() const noexcept -> Vector3;

	auto operator+=(const Vector3& rhs) noexcept -> Vector3&;
	auto operator-=(const Vector3& rhs) noexcept -> Vector3&;
	auto operator*=(const Vector3& rhs) noexcept -> Vector3&;
	auto operator/=(const Vector3& rhs) noexcept -> Vector3&;
	auto operator+=(float rhs) noexcept -> Vector3&;
	auto operator-=(float rhs) noexcept -> Vector3&;
	auto operator*=(float rhs) noexcept -> Vector3&;
	auto operator/=(float rhs) noexcept -> Vector3&;
	auto operator*=(const Quaternion& rhs) noexcept -> Vector3&;

	/* todo 行列に対応
	constexpr auto operator*=(Matrix4x4 rhs) noexcept -> Vector3&;
	*/

	static constexpr int DIMENSION = 3;

private:
	float v[DIMENSION];
};

constexpr auto operator+(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3;
constexpr auto operator-(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3;
constexpr auto operator*(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3;
constexpr auto operator/(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3;
constexpr auto operator+(const Vector3& lhs, float rhs) noexcept -> Vector3;
constexpr auto operator-(const Vector3& lhs, float rhs) noexcept -> Vector3;
constexpr auto operator*(const Vector3& lhs, float rhs) noexcept -> Vector3;
constexpr auto operator/(const Vector3& lhs, float rhs) noexcept -> Vector3;

/* todo 行列に対応
constexpr auto operator*(const Vector3& lhs, Matrix4x4 rhs) noexcept -> Vector3&;
*/

constexpr auto operator>(const Vector3& lhs, const Vector3& rhs) noexcept -> bool;
constexpr auto operator>=(const Vector3& lhs, const Vector3& rhs) noexcept -> bool;
constexpr auto operator<(const Vector3& lhs, const Vector3& rhs) noexcept -> bool;
constexpr auto operator<=(const Vector3& lhs, const Vector3& rhs) noexcept -> bool;

constexpr auto operator==(const Vector3& lhs, const Vector3& rhs) noexcept -> bool;
constexpr auto operator!=(const Vector3& lhs, const Vector3& rhs) noexcept -> bool;

auto operator<<(std::ostream& os, const Vector3& rhs) noexcept -> std::ostream&;


constexpr Vector3::Vector3()
	: Vector3(0, 0, 0) {}

constexpr Vector3::Vector3(float x, float y, float z)
	: v{ x, y, z } {}

constexpr Vector3::Vector3(const Vector2& rhs)
	: Vector3(rhs.X(), rhs.Y(), 0) {}

constexpr auto Vector3::operator=(const Vector2& rhs) -> Vector3& {

	*this = Set(rhs, 0);
	return *this;
}

constexpr auto Vector3::X() const noexcept -> float {

	return v[0];
}

constexpr auto Vector3::Y() const noexcept -> float {

	return v[1];
}

constexpr auto Vector3::Z() const noexcept -> float {

	return v[2];
}

constexpr auto Vector3::XX() const noexcept -> Vector2 {

	return Vector2::Set(X(), X());
}

constexpr auto Vector3::XY() const noexcept -> Vector2 {

	return Vector2::Set(X(), Y());
}

constexpr auto Vector3::XZ() const noexcept -> Vector2 {

	return Vector2::Set(X(), Z());
}

constexpr auto Vector3::YX() const noexcept -> Vector2 {

	return Vector2::Set(Y(), X());
}

constexpr auto Vector3::YY() const noexcept -> Vector2 {

	return Vector2::Set(Y(), Y());
}

constexpr auto Vector3::YZ() const noexcept -> Vector2 {

	return Vector2::Set(Y(), Z());
}

constexpr auto Vector3::ZX() const noexcept -> Vector2 {

	return Vector2::Set(Z(), X());
}

constexpr auto Vector3::ZY() const noexcept -> Vector2 {

	return Vector2::Set(Z(), Y());
}

constexpr auto Vector3::ZZ() const noexcept -> Vector2 {

	return Vector2::Set(Z(), Z());
}

constexpr auto Vector3::XXX() const noexcept -> Vector3 {

	return Replicate(X());
}

constexpr auto Vector3::XXY() const noexcept -> Vector3 {

	return Set(X(), X(), Y());
}

constexpr auto Vector3::XXZ() const noexcept -> Vector3 {

	return Set(X(), X(), Z());
}

constexpr auto Vector3::XYX() const noexcept -> Vector3 {

	return Set(X(), Y(), X());
}

constexpr auto Vector3::XYY() const noexcept -> Vector3 {

	return Set(X(), Y(), Y());
}

constexpr auto Vector3::XYZ() const noexcept -> Vector3 {

	return Set(X(), Y(), Z());
}

constexpr auto Vector3::XZX() const noexcept -> Vector3 {

	return Set(X(), Z(), X());
}

constexpr auto Vector3::XZY() const noexcept -> Vector3 {

	return Set(X(), Z(), Y());
}

constexpr auto Vector3::XZZ() const noexcept -> Vector3 {

	return Set(X(), Z(), Z());
}

constexpr auto Vector3::YXX() const noexcept -> Vector3 {

	return Set(Y(), X(), X());
}

constexpr auto Vector3::YXY() const noexcept -> Vector3 {

	return Set(Y(), X(), Y());
}

constexpr auto Vector3::YXZ() const noexcept -> Vector3 {

	return Set(Y(), X(), Z());
}

constexpr auto Vector3::YYX() const noexcept -> Vector3 {

	return Set(Y(), Y(), X());
}

constexpr auto Vector3::YYY() const noexcept -> Vector3 {

	return Replicate(Y());
}

constexpr auto Vector3::YYZ() const noexcept -> Vector3 {

	return Set(Y(), Y(), Z());
}

constexpr auto Vector3::YZX() const noexcept -> Vector3 {

	return Set(Y(), Z(), X());
}

constexpr auto Vector3::YZY() const noexcept -> Vector3 {

	return Set(Y(), Z(), Y());
}

constexpr auto Vector3::YZZ() const noexcept -> Vector3 {

	return Set(Y(), Z(), Z());
}

constexpr auto Vector3::ZXX() const noexcept -> Vector3 {

	return Set(Z(), X(), X());
}

constexpr auto Vector3::ZXY() const noexcept -> Vector3 {

	return Set(Z(), X(), Y());
}

constexpr auto Vector3::ZXZ() const noexcept -> Vector3 {

	return Set(Z(), X(), Z());
}

constexpr auto Vector3::ZYX() const noexcept -> Vector3 {

	return Set(Z(), Y(), X());
}

constexpr auto Vector3::ZYY() const noexcept -> Vector3 {

	return Set(Z(), Y(), Y());
}

constexpr auto Vector3::ZYZ() const noexcept -> Vector3 {

	return Set(Z(), Y(), Z());
}

constexpr auto Vector3::ZZX() const noexcept -> Vector3 {

	return Set(Z(), Z(), X());
}

constexpr auto Vector3::ZZY() const noexcept -> Vector3 {

	return Set(Z(), Z(), Y());
}

constexpr auto Vector3::ZZZ() const noexcept -> Vector3 {

	return Replicate(Z());
}

constexpr auto Vector3::Data() const noexcept -> float const* {

	return v;
}

constexpr auto Vector3::Length() const noexcept -> float {

	auto&& square = *this * *this;
	return SquareRoot(square.X() + square.Y() + square.Z());
}

constexpr auto Vector3::IsZero() const noexcept -> bool {

	return *this == Set(0, 0, 0);
}

constexpr auto Vector3::Set(float x, float y, float z) noexcept -> Vector3 {

	return Vector3(x, y, z);
}

constexpr auto Vector3::Set(const Vector2& vector2, float z) noexcept -> Vector3 {

	return Set(vector2.X(), vector2.Y(), 0);
}

constexpr auto Vector3::Replicate(float value) -> Vector3 {

	return Vector3(value, value, value);
}

constexpr auto Vector3::Normalize(const Vector3& vector) noexcept -> Vector3 {

	return vector / vector.Length();
}

constexpr auto Vector3::Inverse(const Vector3& vector) noexcept -> Vector3 {

	return Set(-vector.X(), -vector.Y(), -vector.Z());
}

constexpr auto Vector3::Reciprocal(const Vector3& vector) noexcept -> Vector3 {

	return Set(1 / vector.X(), 1 / vector.Y(), 1 / vector.Z());
}

constexpr auto Vector3::Dot(const Vector3& lhs, const Vector3& rhs) noexcept -> float {

	auto&& tmp = lhs * rhs;
	return tmp.X() + tmp.Y() + tmp.Z();
}

constexpr auto Vector3::Cross(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3 {

	return lhs.YZX() * rhs.ZXY() - lhs.ZXY() * rhs.YZX();
}

constexpr Vector3::operator Vector2() const noexcept {

	return Vector2::Set(X(), Y());
}

constexpr Vector3::operator bool() const noexcept {

	return !IsZero();
}

constexpr auto Vector3::operator!() const noexcept -> bool {

	return IsZero();
}

constexpr auto Vector3::operator[](std::size_t index) const -> const float& {

	if (index >= DIMENSION)	throw std::out_of_range("");

	return v[index];
}

constexpr auto Vector3::operator+() const noexcept -> Vector3 {

	return *this;
}

constexpr auto Vector3::operator-() const noexcept -> Vector3 {

	return Inverse(*this);
}

constexpr auto Vector3::operator~() const noexcept -> Vector3 {

	return Reciprocal(*this);
}

constexpr auto operator+(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3 {

	return Vector3::Set(lhs.X() + rhs.X(), lhs.Y() + rhs.Y(), lhs.Z() + rhs.Z());
}

constexpr auto operator-(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3 {

	return Vector3::Set(lhs.X() - rhs.X(), lhs.Y() - rhs.Y(), lhs.Z() - rhs.Z());
}

constexpr auto operator*(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3 {

	return Vector3::Set(lhs.X() * rhs.X(), lhs.Y() * rhs.Y(), lhs.Z() * rhs.Z());
}

constexpr auto operator/(const Vector3& lhs, const Vector3& rhs) noexcept -> Vector3 {

	return Vector3::Set(lhs.X() / rhs.X(), lhs.Y() / rhs.Y(), lhs.Z() / rhs.Z());
}

constexpr auto operator+(const Vector3& lhs, float rhs) noexcept -> Vector3 {

	return lhs + Vector3::Replicate(rhs);
}

constexpr auto operator-(const Vector3& lhs, float rhs) noexcept -> Vector3 {

	return lhs - Vector3::Replicate(rhs);
}

constexpr auto operator*(const Vector3& lhs, float rhs) noexcept -> Vector3 {

	return lhs * Vector3::Replicate(rhs);
}

constexpr auto operator/(const Vector3& lhs, float rhs) noexcept -> Vector3 {

	return lhs / Vector3::Replicate(rhs);
}

constexpr auto operator>(const Vector3& lhs, const Vector3& rhs) noexcept -> bool {

	return lhs.X() > rhs.X() && lhs.Y() > rhs.Y() && lhs.Z() > rhs.Z();
}

constexpr auto operator>=(const Vector3& lhs, const Vector3& rhs) noexcept -> bool {

	return lhs.X() >= rhs.X() && lhs.Y() >= rhs.Y() && lhs.Z() >= rhs.Z();
}

constexpr auto operator<(const Vector3& lhs, const Vector3& rhs) noexcept -> bool {

	return lhs.X() < rhs.X() && lhs.Y() < rhs.Y() && lhs.Z() < rhs.Z();
}

constexpr auto operator<=(const Vector3& lhs, const Vector3& rhs) noexcept -> bool {

	return lhs.X() <= rhs.X() && lhs.Y() <= rhs.Y() && lhs.Z() <= rhs.Z();
}

constexpr auto operator==(const Vector3& lhs, const Vector3& rhs) noexcept -> bool {

	return lhs.X() == rhs.X() && lhs.Y() == rhs.Y() && lhs.Z() == rhs.Z();
}

constexpr auto operator!=(const Vector3& lhs, const Vector3& rhs) noexcept -> bool {

	return lhs.X() != rhs.X() || lhs.Y() != rhs.Y() || lhs.Z() != rhs.Z();
}

constexpr Vector3 X_AXIS_3D = X_AXIS_2D;
constexpr Vector3 Y_AXIS_3D = Y_AXIS_2D;
constexpr Vector3 Z_AXIS_3D = Vector3::Set(0, 0, 1);
constexpr Vector3 X_AXIS_INV_3D = -X_AXIS_3D;
constexpr Vector3 Y_AXIS_INV_3D = -Y_AXIS_3D;
constexpr Vector3 Z_AXIS_INV_3D = -Z_AXIS_3D;
constexpr Vector3 ZERO_VECTOR_3D = ZERO_VECTOR_2D;

constexpr Vector3 DEFAULT_UP_3D = Y_AXIS_3D;
constexpr Vector3 DEFAULT_DOWN_3D = Y_AXIS_INV_3D;
constexpr Vector3 DEFAULT_FORWARD_3D = Z_AXIS_3D;
constexpr Vector3 DEFAULT_BACK_3D = Z_AXIS_INV_3D;
constexpr Vector3 DEFAULT_RIGHT_3D = X_AXIS_3D;
constexpr Vector3 DEFAULT_LEFT_3D = X_AXIS_INV_3D;

}

using Math::Vector3;

}
