#include "Math.hpp"

#include <algorithm>
#include <array>

using namespace DirectX;


namespace Whono::Math {


auto PointsDistance(CXMVECTOR point1, CXMVECTOR point2) -> float {

	auto vec = point2 - point1;
	return XMVector3Length(vec).m128_f32[0];
}

auto SegmentToPointDistance(CXMVECTOR segment1, CXMVECTOR segment2, CXMVECTOR point) -> float {

	//直線と点の距離
	//return XMVector3LinePointDistance(segment1, segment2, point).m128_f32[0];
	
	auto vec1 = point - segment1;
	auto vec2 = segment2 - segment1;

	//線分の両端から伸びる垂線間にあるか判別する
	auto discriminant = XMVector3Dot(vec1, vec2).m128_f32[0];

	//0以下なら点と線分の端点１の距離
	if (discriminant <= 0) {

		return PointsDistance(segment1, point);
	}

	auto segmentLengthSquare = XMVector3LengthSq(vec2).m128_f32[0];

	//線分上に点からの垂線との交点があるとき
	if (discriminant < segmentLengthSquare) {

		//三平方から
		return sqrtf(XMVector3LengthSq(vec1).m128_f32[0] - discriminant * discriminant / segmentLengthSquare);
	}

	//線分の端点２を超えた時は端点２と点の距離
	if (discriminant >= segmentLengthSquare) {

		return PointsDistance(segment2, point);
	}
	return 0;
}

auto SegmentsDistance(CXMVECTOR segmentA1, CXMVECTOR segmentA2, CXMVECTOR segmentB1, CXMVECTOR segmentB2) -> float {
	
	//線分同士の端点のいずれかが同じ座標の時
	if (XMVector3Equal(segmentA1, segmentB1) ||
		XMVector3Equal(segmentA1, segmentB2) ||
		XMVector3Equal(segmentA2, segmentB1) ||
		XMVector3Equal(segmentA2, segmentB2)) {

		return 0.f;
	}

	//線分Aが点に縮退しているとき
	if (XMVector3Equal(segmentA1, segmentA2)) {

		return SegmentToPointDistance(segmentB1, segmentB2, segmentA1);
	}
	//線分Bが点に縮退しているとき
	if (XMVector3Equal(segmentA1, segmentA2)) {

		return SegmentToPointDistance(segmentB1, segmentB2, segmentA1);
	}
	
	//同一平面上にあるとき
	if (CheckCoplanarFourPoint(segmentA1, segmentA2, segmentB1, segmentB2)) {

		//線分が交差しているとき
		if (IntersectSegments(segmentA1, segmentA2, segmentB1, segmentB2)) {

			return 0.f;
		}

		//各線分と端点の距離の中で一番近いものが距離
		std::array eachDistance{
			SegmentToPointDistance(segmentA1, segmentA2, segmentB1),
			SegmentToPointDistance(segmentA1, segmentA2, segmentB2),
			SegmentToPointDistance(segmentB1, segmentB2, segmentA1),
			SegmentToPointDistance(segmentB1, segmentB2, segmentA2)
		};
		return *std::ranges::min_element(eachDistance);
	}
	
	//2直線間での距離を取得
	auto lineDistance = LinesDistance(segmentA1, segmentA2, segmentB1, segmentB2);

	auto segmentA = segmentA2 - segmentA1;
	auto segmentB = segmentB2 - segmentB1;
	auto normal = XMVector3Cross(segmentA, segmentB);
	normal = XMVector3Normalize(normal);

	auto delta = normal * lineDistance;
	
	//同一平面上に移動させたとき交差する時
	if (IntersectSegments(segmentA1 - delta, segmentA2 - delta, segmentB1, segmentB2)) {

		return lineDistance;
	}
	
	//各線分と端点の距離の中で一番近いものが距離
	std::array eachDistance{
		SegmentToPointDistance(segmentA1, segmentA2, segmentB1),
		SegmentToPointDistance(segmentA1, segmentA2, segmentB2),
		SegmentToPointDistance(segmentB1, segmentB2, segmentA1),
		SegmentToPointDistance(segmentB1, segmentB2, segmentA2)
	};
	return *std::ranges::min_element(eachDistance);
}

auto LinesDistance(CXMVECTOR lineA1, CXMVECTOR lineA2, CXMVECTOR lineB1, CXMVECTOR lineB2) -> float {

	//直線の方向ベクトルを算出
	auto lineA = lineA2 - lineA1;
	auto lineB = lineB2 - lineB1;

	//方向ベクトルから法線を算出
	auto normal = XMVector3Cross(lineA, lineB);
	normal = XMVector3Normalize(normal);

	//法線と2直線の始点座標のベクトルの内積が距離になる
	return abs(XMVector3Dot(normal, lineB1 - lineA1).m128_f32[0]);
}

auto CheckCoplanarFourPoint(CXMVECTOR point1, CXMVECTOR point2, CXMVECTOR point3, CXMVECTOR point4) -> bool {

	//点１を基準としたベクトルの行列式を求め０の時同一平面上
	auto mat = XMMATRIX{
		point2 - point1,
		point3 - point1,
		point4 - point1,
		g_XMIdentityR3
	};
	return XMMatrixDeterminant(mat).m128_f32[0] == 0;
}

auto IntersectSegments(CXMVECTOR segmentA1, CXMVECTOR segmentA2, CXMVECTOR segmentB1, CXMVECTOR segmentB2) -> bool {

	//同一平面上にない場合交差しない
	if(!CheckCoplanarFourPoint(segmentA1, segmentA2, segmentB1, segmentB2))	return false;

	//必要なベクトルを算出
	auto segmentA = segmentA2 - segmentA1;
	auto segmentB = segmentB2 - segmentB1;
	auto a1b1 = segmentB1 - segmentA1;
	auto a1b2 = segmentB2 - segmentA1;
	auto b1a2 = segmentA2 - segmentB1;

	//線分から見たもう一方の線分の端点の方向を外積で算出
	auto cross1 = XMVector3Cross(segmentA, a1b1);
	auto cross2 = XMVector3Cross(segmentA, a1b2);
	auto cross3 = XMVector3Cross(segmentB, -a1b1);
	auto cross4 = XMVector3Cross(segmentB, b1a2);

	//線分の両側にもう一方の線分の端点があるときそれぞれの法線の内積が負
	return XMVector3Dot(cross1, cross2).m128_f32[0] < 0 &&
		XMVector3Dot(cross3, cross4).m128_f32[0] < 0;
}

auto GetRollFromQuaternion(CXMVECTOR quaternion) -> float {

	XMFLOAT4 q;
	XMStoreFloat4(&q, quaternion);

	auto sinTheta = 2 * (q.y * q.z + q.x * q.w);
	auto cosTheta = powf(q.x, 2) - powf(q.y, 2) - powf(q.z, 2) + powf(q.w, 2);

	sinTheta = std::clamp(sinTheta, -1.f, 1.f);
	cosTheta = std::clamp(cosTheta, -1.f, 1.f);

	/*auto mat = XMMatrixRotationQuaternion(quaternion);
	auto sinTheta = mat.r[1].m128_f32[2];
	auto cosTheta = mat.r[2].m128_f32[2];*/
	
	return atan2f(sinTheta, cosTheta);
}

auto GetPitchFromQuaternion(CXMVECTOR quaternion) -> float {

	XMFLOAT4 q;
	XMStoreFloat4(&q, quaternion);

	auto sinTheta = 2 * (q.y * q.w - q.x * q.z);
	auto forward = XMVector3Rotate(DEFAULT_FORWARD, quaternion);

	sinTheta = std::clamp(sinTheta, -1.f, 1.f);
	
	if (forward.m128_f32[2] >= 0) {

		return asinf(sinTheta);
	}
	return copysignf(XM_PI, sinTheta) - asinf(sinTheta);
}

auto GetYawFromQuaternion(CXMVECTOR quaternion) -> float {

	XMFLOAT4 q;
	XMStoreFloat4(&q, quaternion);

	auto sinTheta = 2 * (q.z * q.w + q.x * q.y);
	auto cosTheta = powf(q.x, 2) + powf(q.y, 2) - powf(q.z, 2) - powf(q.w, 2);

	sinTheta = std::clamp(sinTheta, -1.f, 1.f);
	cosTheta = std::clamp(cosTheta, -1.f, 1.f);

	return atan2f(sinTheta, cosTheta);
}

auto ConvertToEuler(CXMVECTOR quaternion) -> XMVECTOR {

	auto euler = XMVectorZero();
	
	euler.m128_f32[0] = GetYawFromQuaternion(quaternion);
	euler.m128_f32[1] = GetPitchFromQuaternion(quaternion);
	euler.m128_f32[2] = GetYawFromQuaternion(quaternion);

	return euler;
}

auto ConvertToRadiansVector(CXMVECTOR degrees) -> XMVECTOR {

	auto radians = degrees;
	for (auto& elem : radians.m128_f32) {

		elem = XMConvertToRadians(elem);
	}
	return radians;
}

auto ConvertToDegreesVector(CXMVECTOR radians) -> XMVECTOR {

	auto degrees = radians;
	for (auto& elem : degrees.m128_f32) {

		elem = XMConvertToRadians(elem);
	}
	return degrees;
}

}
