#pragma once

#include "Vector4.hpp"
#include "Angle.hpp"

namespace Whono::Math {

class Quaternion final {

private:
	constexpr Quaternion();
	constexpr explicit Quaternion(float x, float y, float z, float w);

public:
	//コピームーブを許可
	constexpr Quaternion(const Quaternion&) = default;
	constexpr auto operator=(const Quaternion&) -> Quaternion& = default;
	constexpr Quaternion(Quaternion&&) = default;
	constexpr auto operator=(Quaternion&&) -> Quaternion& = default;

	// ReSharper disable once CppNonExplicitConvertingConstructor
	constexpr Quaternion(const Vector4& rhs);
	constexpr auto operator=(const Vector4& rhs) -> Quaternion&;
	
	constexpr auto X() const noexcept -> float;
	constexpr auto Y() const noexcept -> float;
	constexpr auto Z() const noexcept -> float;
	constexpr auto W() const noexcept -> float;

	constexpr auto Imaginary() const noexcept -> Vector3;
	constexpr auto Real() const noexcept -> float;

	auto X(float x) noexcept -> Quaternion&;
	auto Y(float y) noexcept -> Quaternion&;
	auto Z(float z) noexcept -> Quaternion&;
	auto W(float w) noexcept -> Quaternion&;

	auto Imaginary(const Vector3& imaginary) noexcept -> Quaternion&;
	auto Real(float real) noexcept -> Quaternion&;

	constexpr auto Length() const noexcept -> float;
	constexpr auto EulerAngles() const noexcept -> Vector3;
	constexpr auto Roll() const noexcept -> Angle;
	constexpr auto Pitch() const noexcept -> Angle;
	constexpr auto Yaw() const noexcept -> Angle;

	void ToAxisAngle(Vector3* axis, Angle* angle) const;

	static constexpr auto Set(float x, float y, float z, float w) -> Quaternion;
	static constexpr auto Set(const Vector3& imaginary, float w) -> Quaternion;
	static constexpr auto Set(const Vector4& vector4) -> Quaternion;
	static constexpr auto AngleAxis(const Vector3& axis, const Angle& angle) -> Quaternion;
	static constexpr auto Euler(const Angle& x, const Angle& y, const Angle& z) -> Quaternion;
	static constexpr auto Normalize(const Quaternion& quaternion) -> Quaternion;
	static constexpr auto Inverse(const Quaternion& quaternion) -> Quaternion;
	static constexpr auto Conjugate(const Quaternion& quaternion) -> Quaternion;
	static constexpr auto Slerp(const Quaternion& from, const Quaternion& to, float ratio) -> Quaternion;

	explicit constexpr operator Vector4() const noexcept;
	
	constexpr auto operator[](std::size_t index) const -> const float&;
	auto operator[](std::size_t index) -> float&;
	
	constexpr auto operator+() const noexcept->Quaternion;
	constexpr auto operator-() const noexcept->Quaternion;
	constexpr auto operator~() const noexcept->Quaternion;

	auto operator+=(const Quaternion& rhs) noexcept -> Quaternion&;
	auto operator-=(const Quaternion& rhs) noexcept -> Quaternion&;
	auto operator*=(const Quaternion& rhs) noexcept -> Quaternion&;
	auto operator/=(const Quaternion& rhs) noexcept -> Quaternion&;
	auto operator+=(float rhs) noexcept -> Quaternion&;
	auto operator-=(float rhs) noexcept -> Quaternion&;
	auto operator*=(float rhs) noexcept -> Quaternion&;
	auto operator/=(float rhs) noexcept -> Quaternion&;
	
	static constexpr int DIMENSION = 4;
	
private:
	float q[DIMENSION];
};

constexpr auto operator+(const Quaternion& lhs, const Quaternion& rhs) noexcept -> Quaternion;
constexpr auto operator-(const Quaternion& lhs, const Quaternion& rhs) noexcept -> Quaternion;
constexpr auto operator*(const Quaternion& lhs, const Quaternion& rhs) noexcept -> Quaternion;
constexpr auto operator/(const Quaternion& lhs, const Quaternion& rhs) noexcept -> Quaternion;
constexpr auto operator+(const Quaternion& lhs, float rhs) noexcept -> Quaternion;
constexpr auto operator-(const Quaternion& lhs, float rhs) noexcept -> Quaternion;
constexpr auto operator*(const Quaternion& lhs, float rhs) noexcept -> Quaternion;
constexpr auto operator/(const Quaternion& lhs, float rhs) noexcept -> Quaternion;

constexpr auto operator*(const Quaternion& lhs, const Vector3& rhs) noexcept -> Vector3;

constexpr auto operator==(const Quaternion& lhs, const Quaternion& rhs) noexcept -> bool;
constexpr auto operator!=(const Quaternion& lhs, const Quaternion& rhs) noexcept -> bool;

auto operator<<(std::ostream& os, const Quaternion& rhs) noexcept -> std::ostream&;

constexpr Quaternion::Quaternion()
	: Quaternion(0, 0, 0, 1) {}

constexpr Quaternion::Quaternion(float x, float y, float z, float w)
	: q{ x, y, z, w } {}

constexpr Quaternion::Quaternion(const Vector4& rhs)
	: Quaternion(rhs.X(), rhs.Y(), rhs.Z(), rhs.W()) {}

constexpr auto Quaternion::operator=(const Vector4& rhs) -> Quaternion& {
	
	*this = Set(rhs);
	return *this;
}

constexpr auto Quaternion::X() const noexcept -> float {

	return q[0];
}

constexpr auto Quaternion::Y() const noexcept -> float {

	return q[1];
}

constexpr auto Quaternion::Z() const noexcept -> float {

	return q[2];
}

constexpr auto Quaternion::W() const noexcept -> float {

	return q[3];
}

constexpr auto Quaternion::Imaginary() const noexcept -> Vector3 {

	return Vector3::Set(q[0], q[1], q[2]);
}

constexpr auto Quaternion::Real() const noexcept -> float {

	return W();
}

constexpr auto Quaternion::Length() const noexcept -> float {

	auto&& tmp = static_cast<Vector4>(*this);
	return tmp.Length();
}

constexpr auto Quaternion::EulerAngles() const noexcept -> Vector3 {

	return Vector3::Set(Roll(), Pitch(), Yaw());
}

constexpr auto Quaternion::Roll() const noexcept -> Angle {

	auto sinTheta = 2 * (Y() * Z() + X() * W());
	auto cosTheta = X() * X() - Y() * Y() - Z() * Z() + W() * W();

	return ArcTangent2(sinTheta, cosTheta);
}

constexpr auto Quaternion::Pitch() const noexcept -> Angle {

	auto sinTheta = 2 * (Y() * W() - X() * Z());
	auto forward = *this * DEFAULT_FORWARD_3D;

	if (forward.Z() >= 0) {

		return ArcSine(sinTheta);
	}
	return CopySign(PI, sinTheta) - ArcSine(sinTheta);
}

constexpr auto Quaternion::Yaw() const noexcept -> Angle {

	auto sinTheta = 2 * (Z() * W() + X() * Y());
	auto cosTheta = X() * X() + Y() * Y() - Z() * Z() - W() * W();

	return ArcTangent2(sinTheta, cosTheta);
}

constexpr auto Quaternion::Set(float x, float y, float z, float w) -> Quaternion {

	return Quaternion(x, y, z, w);
}

constexpr auto Quaternion::Set(const Vector3& imaginary, float w) -> Quaternion {

	return Quaternion(imaginary.X(), imaginary.Y(), imaginary.Z(), w);
}

constexpr auto Quaternion::Set(const Vector4& vector4) -> Quaternion {

	return Quaternion(vector4);
}

constexpr auto Quaternion::AngleAxis(const Vector3& axis, const Angle& angle) -> Quaternion {

	return Set(Vector3::Normalize(axis) * Sine(angle * 0.5f), Cosine(angle * 0.5f));
}

constexpr auto Quaternion::Euler(const Angle& x, const Angle& y, const Angle& z) -> Quaternion {

	auto c = Vector3::Set(Cosine(z * 0.5f), Cosine(x * 0.5f), Cosine(y * 0.5f));
	auto s = Vector3::Set(Sine(z * 0.5f), Sine(x * 0.5f), Sine(y * 0.5f));

	return Set(
		c.X() * c.Y() * s.Z() - s.X() * s.Y() * c.Z(),
		c.X() * s.Y() * c.Z() - s.X() * c.Y() * s.Z(), 
		s.X() * c.Y() * c.Z() - c.X() * s.Y() * s.Z(), 
		c.X() * c.Y() * c.Z() - s.X() * s.Y() * s.Z());
}

constexpr auto Quaternion::Normalize(const Quaternion& quaternion) -> Quaternion {

	return quaternion / quaternion.Length();
}

constexpr auto Quaternion::Inverse(const Quaternion& quaternion) -> Quaternion {

	return Conjugate(quaternion) / quaternion.Length();
}

constexpr auto Quaternion::Conjugate(const Quaternion& quaternion) -> Quaternion {

	return Set(-quaternion.Imaginary(), quaternion.Real());
}

constexpr auto Quaternion::Slerp(const Quaternion& from, const Quaternion& to, float ratio) -> Quaternion {

	auto&& imaginary = from.Imaginary() * to.Imaginary();
	auto&& real = from.Real() * to.Real();
	auto cosHalfTheta = imaginary.X() + imaginary.Y() + imaginary.Z() + real;

	if (Absolute(cosHalfTheta) >= 1.f)	return from;

	auto halfTheta = ArcCosine(cosHalfTheta);
	auto sinHalfTheta = SquareRoot(1.f - Pow(cosHalfTheta, 2));

	if (Absolute(sinHalfTheta) < 1e-10) {

		return from * 0.5f + to * 0.5f;
	}
	auto ratioA = Sine((1 - ratio) * halfTheta) / sinHalfTheta;
	auto ratioB = Sine(ratio * halfTheta) / sinHalfTheta;

	return from * ratioA + to * ratioB;
}

constexpr Quaternion::operator Vector4() const noexcept {

	return Vector4::Set(Imaginary(), Real());
}

constexpr auto Quaternion::operator[](std::size_t index) const -> const float& {

	if (index >= DIMENSION)	throw std::out_of_range("");

	return q[index];
}

constexpr auto Quaternion::operator+() const noexcept -> Quaternion {

	return *this;
}

constexpr auto Quaternion::operator-() const noexcept -> Quaternion {

	return Set(-Imaginary(), -Real());
}

constexpr auto Quaternion::operator~() const noexcept -> Quaternion {

	return Conjugate(*this);
}

constexpr auto operator+(const Quaternion& lhs, const Quaternion& rhs) noexcept -> Quaternion {

	auto&& imaginary = lhs.Imaginary() + rhs.Imaginary();
	auto&& real = lhs.Real() + rhs.Real();
	return Quaternion::Set(imaginary, real);
}

constexpr auto operator-(const Quaternion& lhs, const Quaternion& rhs) noexcept -> Quaternion {
	
	auto&& imaginary = lhs.Imaginary() - rhs.Imaginary();
	auto&& real = lhs.Real() - rhs.Real();
	return Quaternion::Set(imaginary, real);
}

constexpr auto operator*(const Quaternion& lhs, const Quaternion& rhs) noexcept -> Quaternion {

	auto&& vecL = static_cast<Vector4>(lhs);
	auto&& vecR = static_cast<Vector4>(rhs);

	return Quaternion::Set(
		vecL.YZX() * vecR.ZXY() - vecL.ZXY() * vecR.YZX() + vecL.XYZ() * vecR.WWW() + vecL.WWW() * vecR.XYZ(),
		lhs.W() * rhs.W() - lhs.X() * rhs.X() - lhs.Y() * rhs.Y() - lhs.Z() * rhs.Z());
}

constexpr auto operator/(const Quaternion& lhs, const Quaternion& rhs) noexcept -> Quaternion {

	return lhs * Quaternion::Inverse(rhs);
}

constexpr auto operator+(const Quaternion& lhs, float rhs) noexcept -> Quaternion {

	return Quaternion::Set(lhs.Imaginary(), lhs.Real() + rhs);
}

constexpr auto operator-(const Quaternion& lhs, float rhs) noexcept -> Quaternion {

	return Quaternion::Set(lhs.Imaginary(), lhs.Real() - rhs);
}

constexpr auto operator*(const Quaternion& lhs, float rhs) noexcept -> Quaternion {

	return Quaternion::Set(lhs.Imaginary() * rhs, lhs.Real() * rhs);
}

constexpr auto operator/(const Quaternion& lhs, float rhs) noexcept -> Quaternion {

	return Quaternion::Set(lhs.Imaginary() / rhs, lhs.Real() / rhs);
}

constexpr auto operator*(const Quaternion& lhs, const Vector3& rhs) noexcept -> Vector3 {

	return (lhs * Quaternion::Set(rhs, 0) * Quaternion::Conjugate(lhs)).Imaginary();
}

constexpr auto operator==(const Quaternion& lhs, const Quaternion& rhs) noexcept -> bool {

	return lhs.Imaginary() == rhs.Imaginary() && lhs.Real() == rhs.Real();
}

constexpr auto operator!=(const Quaternion& lhs, const Quaternion& rhs) noexcept -> bool {

	return lhs.Imaginary() != rhs.Imaginary() || lhs.Real() != rhs.Real();
}

constexpr auto QUATERNION_IDENTITY = Quaternion::Set(0, 0, 0, 1);

}
