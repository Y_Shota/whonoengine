#include "Vector2.hpp"

#include "Vector3.hpp"

namespace Whono::Math {

Vector2::Vector2(const Vector3& rhs)
	: Vector2(rhs.XY()) {}

auto Vector2::operator=(const Vector3& rhs) -> Vector2& {

	*this = rhs.XY();
	return *this;
}

auto Vector2::X(float x) noexcept -> Vector2& {

	v[0] = x;
	return  *this;
}

auto Vector2::Y(float y) noexcept -> Vector2& {

	v[1] = y;
	return  *this;
}

auto Vector2::Normalize() noexcept -> Vector2& {

	*this /= Length();
	return *this;
}

auto Vector2::operator+=(const Vector2& rhs) noexcept -> Vector2& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] += rhs.v[i];
	}
	return *this;
}

auto Vector2::operator-=(const Vector2& rhs) noexcept -> Vector2& {
	
	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] -= rhs.v[i];
	}
	return *this;
}

auto Vector2::operator*=(const Vector2& rhs) noexcept -> Vector2& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] *= rhs.v[i];
	}
	return *this;
}

auto Vector2::operator/=(const Vector2& rhs) noexcept -> Vector2& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] /= rhs.v[i];
	}
	return *this;
}

auto operator<<(std::ostream& os, const Vector2& rhs) noexcept -> std::ostream& {

	os << '(' << rhs.X() << ", " << rhs.Y() << ')';
	return os;
}

}
