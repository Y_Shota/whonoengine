#pragma once

#include <DirectXMath.h>
#include <limits>

using namespace DirectX;

namespace Whono::Math {

constexpr XMVECTOR DEFAULT_RIGHT = { 1, 0, 0, 0 };
constexpr XMVECTOR DEFAULT_UP = { 0, 1, 0, 0 };
constexpr XMVECTOR DEFAULT_FORWARD = { 0, 0, 1, 0 };

/**
 * \brief 2点間の距離を求める
 * \param point1 点１
 * \param point2 点２
 * \return 2点間の距離
 */
auto PointsDistance(CXMVECTOR point1, CXMVECTOR point2) -> float;
/**
 * \brief 線分と点の距離を求める
 * \param segment1 線分の端点１
 * \param segment2 線分の端点２
 * \param point 点
 * \return 線分と点の距離
 */
auto SegmentToPointDistance(CXMVECTOR segment1, CXMVECTOR segment2, CXMVECTOR point) -> float;
/**
 * \brief 2線分間の距離を求める
 * \param segmentA1 線分Aの端点１
 * \param segmentA2 線分Aの端点２
 * \param segmentB1 線分Bの端点１
 * \param segmentB2 線分Bの端点２
 * \return 2線分間の距離
 */
auto SegmentsDistance(CXMVECTOR segmentA1, CXMVECTOR segmentA2,CXMVECTOR segmentB1, CXMVECTOR segmentB2) -> float;
/**
 * \brief 2直線間の距離を求める
 * \param lineA1 直線Aを通る点１
 * \param lineA2 直線Aを通る点２
 * \param lineB1 直線Bを通る点１
 * \param lineB2 直線Bを通る点２
 * \return 2直線間の距離
 */
auto LinesDistance(CXMVECTOR lineA1, CXMVECTOR lineA2, CXMVECTOR lineB1, CXMVECTOR lineB2) -> float;
/**
 * \brief 4点が同一平面上にあるかを検証する
 * \param point1 点１
 * \param point2 点２
 * \param point3 点３
 * \param point4 点４
 * \return 同一平面状なら真
 */
auto CheckCoplanarFourPoint(CXMVECTOR point1, CXMVECTOR point2, CXMVECTOR point3, CXMVECTOR point4) -> bool;
/**
 * \brief 2線分が交差しているかを検証する
 * \param segmentA1 線分Aの端点１
 * \param segmentA2 線分Aの端点２
 * \param segmentB1 線分Bの端点１
 * \param segmentB2 線分Bの端点２
 * \return 交差しているなら真
 */
auto IntersectSegments(CXMVECTOR segmentA1, CXMVECTOR segmentA2,CXMVECTOR segmentB1, CXMVECTOR segmentB2) -> bool;

/**
 * \brief クォータニオンからX軸回転を抽出する
 * \param quaternion 抽出元クォータニオン
 * \return X軸回転量
 */
auto GetRollFromQuaternion(CXMVECTOR quaternion) -> float;
/**
 * \brief クォータニオンからY軸回転を抽出する
 * \param quaternion 抽出元クォータニオン
 * \return Y軸回転量
 */
auto GetPitchFromQuaternion(CXMVECTOR quaternion) -> float;
/**
 * \brief クォータニオンからZ軸回転を抽出する
 * \param quaternion 抽出元クォータニオン
 * \return Z軸回転量
 */
auto GetYawFromQuaternion(CXMVECTOR quaternion) -> float;
/**
 * \brief クォータニオンをオイラー角に変換する
 * \param quaternion 変換元クォータニオン
 * \return オイラー角
 */
auto ConvertToEuler(CXMVECTOR quaternion) -> XMVECTOR;

/**
 * \brief XMVECTORの全要素をラジアンに変換する
 * \param degrees 単位度のオイラー角
 * \return 単位ラジアンのオイラー角
 */
auto ConvertToRadiansVector(CXMVECTOR degrees) -> XMVECTOR;
/**
 * \brief XMVECTORの全要素を度に変換する
 * \param radians 単位ラジアンのオイラー角
 * \return 単位度のオイラー角
 */
auto ConvertToDegreesVector(CXMVECTOR radians) -> XMVECTOR;


/**
 * \brief 絶対値を求める コンパイル時に実行可能
 * \param x 絶対値を求めたい値
 * \return 絶対値
 */
constexpr auto Absolute(float x) -> float {

	return x >= 0 ? x : -x;
}
/**
 * \brief 非負の平方根を求める コンパイル時に実行可能
 * \param s 平方根を求めたい値
 * \return 平方根
 */
constexpr auto SquareRoot(float s) -> float {

	auto x = s / 2.f;
	auto prev = 0.f;

	while (x != prev) {

		prev = x;
		x = (x + s / x) / 2.f;
	}
	return x;
}
/**
 * \brief 累乗を求める　コンパイル時に実行可能
 * \param x 底
 * \param y 何乗か
 * \return xのy乗
 */
constexpr auto Pow(float x, int y) -> float {

	return y == 0 ? 1 : x * Pow(x, y - 1);
}
/**
 * \brief 階乗を求める　コンパイル時に実行可能
 * \param x 求めたい底
 * \return xの階乗
 */
constexpr auto Factorial(int x) -> int {

	return x == 0 ? 1 : x * Factorial(x - 1);
}
/**
 * \brief 符号をコピーする　コンパイル時に実行可能
 * \param x コピーされる値
 * \param sign コピー元
 * \return 符号をコピーしたx
 */
constexpr auto CopySign(float x, float sign) -> float {

	return Absolute(x) * (sign >= 0 ? 1.f : -1.f);
}
/**
 * \brief アルゴリズムの終了条件として用いる
 * \param x 比較対象１
 * \param y 比較対象２
 * \return 誤差ないなら真
 */
constexpr auto Feq(float x, float y) -> bool {

	return Absolute(x - y) <= 1e-3f;
}
/**
 * \brief 正弦を求める　コンパイル時に実行可能
 * \param x 正弦を求めたい値
 * \return 正弦値
 */
constexpr auto Sine(float x) -> float {

	auto sum = 0.f;
	for (auto i = 0;; ++i) {

		auto t = Pow(-1.f, i) / Factorial(2 * i + 1) * Pow(x, 2 * i + 1);
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}
/**
 * \brief 余弦を求める　コンパイル時に実行可能
 * \param x 余弦を求めたい値
 * \return 余弦値
 */
constexpr auto Cosine(float x) -> float {

	auto sum = 0.f;
	for (auto i = 0;; ++i) {

		auto t = Pow(-1.f, i) / Factorial(2 * i) * Pow(x, 2 * i);
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}
/**
 * \brief 正接を求める　コンパイル時に実行可能
 * \param x 正接を求めたい値
 * \return 正接値
 */
constexpr auto Tangent(float x) -> float {

	return Sine(x) / Cosine(x);
}
/**
 * \brief 逆正弦を求める　コンパイル時に実行可能
 * \param x 逆正弦を求めたい値　定義域(-1...1)
 * \return 逆正弦値　定義域外0
 */
constexpr auto ArcSine(float x) -> float {

	auto absX = Absolute(x);
	
	if (absX > 1.f)		return 0.f;
	if (x == 0.f)		return 0.f;
	if (absX == 1.f)	return XM_PIDIV2 * x;

	if (absX > 1 / SquareRoot(2) && absX < 1 ) {

		return CopySign(XM_PIDIV2 - ArcSine(SquareRoot(1 - x * x)), x);
	}
	
	auto sum = 0.f;
	for (auto i = 0;; ++i) {

		auto t = Factorial(2 * i) / (Pow(4, i) * Pow(static_cast<float>(Factorial(i)), 2) * (2 * i + 1)) * Pow(x, 2 * i + 1);
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}
/**
 * \brief 逆余弦を求める　コンパイル時に実行可能
 * \param x 逆余弦を求めたい値　定義域(-1...1)
 * \return 逆余弦値　定義域外0
 */
constexpr auto ArcCosine(float x) -> float {

	auto absX = Absolute(x);

	if (absX > 1.f)	return 0.f;
	if (x == 0.f)	return XM_PIDIV2;
	if (x == 1.f)	return 0.f;
	if (x == -1.f)	return XM_PI;

	auto sum = 0.f;
	for (auto i = 0;; ++i) {

		auto t = Factorial(2 * i) / (Pow(4, i) * Pow(static_cast<float>(Factorial(i)), 2) * (2 * i + 1)) * Pow(x, 2 * i + 1);
		if (Feq(sum, sum + t))	return XM_PIDIV2 - sum;
		sum += t;
	}
}
/**
 * \brief 逆正接を求める　コンパイル時に実行可能
 * \param x 逆正接を求めたい値
 * \return 逆正接値
 */
constexpr auto ArcTangent(float x) -> float {

	if (Absolute(x) == 1.f)								return XM_PIDIV4 * x;
	if (x == 0)											return 0;
	if (x == std::numeric_limits<double>::infinity())	return XM_PIDIV2;

	auto absX = Absolute(x);

	if (absX > 1) {
		constexpr auto sqrt2 = SquareRoot(2);
		if (absX > sqrt2 + 1) {

			return XM_PIDIV2 - ArcTangent(1 / x);
		}
		if (absX > sqrt2 - 1 && absX <= sqrt2 + 1) {
			
			return XM_PIDIV4 + ArcTangent((x - 1) / (x + 1));
		}
	}
	
	auto sum = 0.f;
	for (auto i = 0;; ++i) {

		auto t = Pow(-1.f, i) / (2 * i + 1) * Pow(x, 2 * i + 1);
		if (Feq(sum, sum + t))	return sum;
		sum += t;
	}
}
/**
 * \brief 対辺と隣辺から逆正接を求める　コンパイル時に実行可能
 * \param y 対辺
 * \param x 隣辺
 * \return 逆正接値
 */
constexpr auto ArcTangent2(float y, float x) -> float {

	if (x == 0)	return CopySign(XM_PIDIV2, y);
	if (x > 0)	return ArcTangent(y / x);
	return ArcTangent(y / x) + CopySign(XM_PI, y);
}

}