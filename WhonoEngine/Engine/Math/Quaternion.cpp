#include "Quaternion.hpp"

namespace Whono::Math {


auto Quaternion::X(float x) noexcept -> Quaternion& {

	q[0] = x;
	return *this;
}

auto Quaternion::Y(float y) noexcept -> Quaternion& {

	q[1] = y;
	return *this;
}

auto Quaternion::Z(float z) noexcept -> Quaternion& {

	q[2] = z;
	return *this;
}

auto Quaternion::W(float w) noexcept -> Quaternion& {

	q[3] = w;
	return *this;
}

auto Quaternion::Imaginary(const Vector3& imaginary) noexcept -> Quaternion& {

	for(auto i = 0; i < Vector3::DIMENSION; ++i) {

		q[i] = imaginary[i];
	}
	return *this;
}

auto Quaternion::Real(float real) noexcept -> Quaternion& {

	return W(real);
}

void Quaternion::ToAxisAngle(Vector3* axis, Angle* angle) const {

	*axis = Imaginary() / sqrtf(1 - powf(Real(), 2));
	*angle = 2 * acosf(Real());
}

auto Quaternion::operator[](std::size_t index) -> float& {

	if (index >= DIMENSION)	throw std::out_of_range("");

	return q[index];
}

auto Quaternion::operator+=(const Quaternion& rhs) noexcept -> Quaternion& {

	for (auto i = 0; i < DIMENSION; ++i) {

		q[i] += rhs.q[i];
	}
	return *this;
}

auto Quaternion::operator-=(const Quaternion& rhs) noexcept -> Quaternion& {

	for (auto i = 0; i < DIMENSION; ++i) {

		q[i] /= rhs.q[i];
	}
	return *this;
}

auto Quaternion::operator*=(const Quaternion& rhs) noexcept -> Quaternion& {

	*this = *this * rhs;
	return *this;
}

auto Quaternion::operator/=(const Quaternion& rhs) noexcept -> Quaternion& {

	*this = *this / rhs;
	return *this;
}

auto Quaternion::operator+=(float rhs) noexcept -> Quaternion& {

	q[3] += rhs;
	return *this;
}

auto Quaternion::operator-=(float rhs) noexcept -> Quaternion& {

	q[3] -= rhs;
	return *this;
}

auto Quaternion::operator*=(float rhs) noexcept -> Quaternion& {

	for (auto i = 0; i < DIMENSION; ++i) {

		q[i] *= rhs;
	}
	return *this;
}

auto Quaternion::operator/=(float rhs) noexcept -> Quaternion& {

	for (auto i = 0; i < DIMENSION; ++i) {

		q[i] /= rhs;
	}
	return *this;
}

auto operator<<(std::ostream& os, const Quaternion& rhs) noexcept -> std::ostream& {

	os << '(' << rhs.X() << ", " << rhs.Y() << ", " << rhs.Z() << ", " << rhs.W() << ')';
	return os;
}

}

