#pragma once
#include <ostream>

#include "Math.hpp"
#include "Vector3.hpp"


namespace Whono::Math {

class Vector4 final {

public:
	/**
	 * \brief コンストラクタ
	 */
	constexpr Vector4();
	/**
	 * \brief コンストラクタ
	 * \param x X成分
	 * \param y Y成分
	 * \param z Z成分
	 * \param w W成分
	 */
	constexpr Vector4(float x, float y, float z, float w);

	//コピームーブを許可
	constexpr Vector4(const Vector4&) = default;
	constexpr auto operator=(const Vector4&) -> Vector4& = default;
	constexpr Vector4(Vector4&&) = default;
	constexpr auto operator=(Vector4&&) -> Vector4& = default;

	// ReSharper disable once CppNonExplicitConvertingConstructor
	constexpr Vector4(const Vector3& rhs);
	constexpr auto operator=(const Vector3& rhs) -> Vector4&;

	/**
	 * \brief X成分を取得
	 * \return X成分
	 */
	constexpr auto X() const noexcept -> float;
	/**
	 * \brief Y成分を取得
	 * \return Y成分
	 */
	constexpr auto Y() const noexcept -> float;
	/**
	 * \brief Z成分を取得
	 * \return Z成分
	 */
	constexpr auto Z() const noexcept -> float;
	/**
	 * \brief W成分を取得
	 * \return W成分
	 */
	constexpr auto W() const noexcept -> float;

	constexpr auto XX() const noexcept -> Vector2;
	constexpr auto XY() const noexcept -> Vector2;
	constexpr auto XZ() const noexcept -> Vector2;
	constexpr auto XW() const noexcept -> Vector2;
	constexpr auto YX() const noexcept -> Vector2;
	constexpr auto YY() const noexcept -> Vector2;
	constexpr auto YZ() const noexcept -> Vector2;
	constexpr auto YW() const noexcept -> Vector2;
	constexpr auto ZX() const noexcept -> Vector2;
	constexpr auto ZY() const noexcept -> Vector2;
	constexpr auto ZZ() const noexcept -> Vector2;
	constexpr auto ZW() const noexcept -> Vector2;
	constexpr auto WX() const noexcept -> Vector2;
	constexpr auto WY() const noexcept -> Vector2;
	constexpr auto WZ() const noexcept -> Vector2;
	constexpr auto WW() const noexcept -> Vector2;

	constexpr auto XXX() const noexcept -> Vector3;
	constexpr auto XXY() const noexcept -> Vector3;
	constexpr auto XXZ() const noexcept -> Vector3;
	constexpr auto XXW() const noexcept -> Vector3;
	constexpr auto XYX() const noexcept -> Vector3;
	constexpr auto XYY() const noexcept -> Vector3;
	constexpr auto XYZ() const noexcept -> Vector3;
	constexpr auto XYW() const noexcept -> Vector3;
	constexpr auto XZX() const noexcept -> Vector3;
	constexpr auto XZY() const noexcept -> Vector3;
	constexpr auto XZZ() const noexcept -> Vector3;
	constexpr auto XZW() const noexcept -> Vector3;
	constexpr auto XWX() const noexcept -> Vector3;
	constexpr auto XWY() const noexcept -> Vector3;
	constexpr auto XWZ() const noexcept -> Vector3;
	constexpr auto XWW() const noexcept -> Vector3;
	constexpr auto YXX() const noexcept -> Vector3;
	constexpr auto YXY() const noexcept -> Vector3;
	constexpr auto YXZ() const noexcept -> Vector3;
	constexpr auto YXW() const noexcept -> Vector3;
	constexpr auto YYX() const noexcept -> Vector3;
	constexpr auto YYY() const noexcept -> Vector3;
	constexpr auto YYZ() const noexcept -> Vector3;
	constexpr auto YYW() const noexcept -> Vector3;
	constexpr auto YZX() const noexcept -> Vector3;
	constexpr auto YZY() const noexcept -> Vector3;
	constexpr auto YZZ() const noexcept -> Vector3;
	constexpr auto YZW() const noexcept -> Vector3;
	constexpr auto YWX() const noexcept -> Vector3;
	constexpr auto YWY() const noexcept -> Vector3;
	constexpr auto YWZ() const noexcept -> Vector3;
	constexpr auto YWW() const noexcept -> Vector3;
	constexpr auto ZXX() const noexcept -> Vector3;
	constexpr auto ZXY() const noexcept -> Vector3;
	constexpr auto ZXZ() const noexcept -> Vector3;
	constexpr auto ZXW() const noexcept -> Vector3;
	constexpr auto ZYX() const noexcept -> Vector3;
	constexpr auto ZYY() const noexcept -> Vector3;
	constexpr auto ZYZ() const noexcept -> Vector3;
	constexpr auto ZYW() const noexcept -> Vector3;
	constexpr auto ZZX() const noexcept -> Vector3;
	constexpr auto ZZY() const noexcept -> Vector3;
	constexpr auto ZZZ() const noexcept -> Vector3;
	constexpr auto ZZW() const noexcept -> Vector3;
	constexpr auto ZWX() const noexcept -> Vector3;
	constexpr auto ZWY() const noexcept -> Vector3;
	constexpr auto ZWZ() const noexcept -> Vector3;
	constexpr auto ZWW() const noexcept -> Vector3;
	constexpr auto WXX() const noexcept -> Vector3;
	constexpr auto WXY() const noexcept -> Vector3;
	constexpr auto WXZ() const noexcept -> Vector3;
	constexpr auto WXW() const noexcept -> Vector3;
	constexpr auto WYX() const noexcept -> Vector3;
	constexpr auto WYY() const noexcept -> Vector3;
	constexpr auto WYZ() const noexcept -> Vector3;
	constexpr auto WYW() const noexcept -> Vector3;
	constexpr auto WZX() const noexcept -> Vector3;
	constexpr auto WZY() const noexcept -> Vector3;
	constexpr auto WZZ() const noexcept -> Vector3;
	constexpr auto WZW() const noexcept -> Vector3;
	constexpr auto WWX() const noexcept -> Vector3;
	constexpr auto WWY() const noexcept -> Vector3;
	constexpr auto WWZ() const noexcept -> Vector3;
	constexpr auto WWW() const noexcept -> Vector3;

	constexpr auto XXXX() const noexcept -> Vector4;
	constexpr auto XXYX() const noexcept -> Vector4;
	constexpr auto XXZX() const noexcept -> Vector4;
	constexpr auto XXWX() const noexcept -> Vector4;
	constexpr auto XYXX() const noexcept -> Vector4;
	constexpr auto XYYX() const noexcept -> Vector4;
	constexpr auto XYZX() const noexcept -> Vector4;
	constexpr auto XYWX() const noexcept -> Vector4;
	constexpr auto XZXX() const noexcept -> Vector4;
	constexpr auto XZYX() const noexcept -> Vector4;
	constexpr auto XZZX() const noexcept -> Vector4;
	constexpr auto XZWX() const noexcept -> Vector4;
	constexpr auto XWXX() const noexcept -> Vector4;
	constexpr auto XWYX() const noexcept -> Vector4;
	constexpr auto XWZX() const noexcept -> Vector4;
	constexpr auto XWWX() const noexcept -> Vector4;
	constexpr auto YXXX() const noexcept -> Vector4;
	constexpr auto YXYX() const noexcept -> Vector4;
	constexpr auto YXZX() const noexcept -> Vector4;
	constexpr auto YXWX() const noexcept -> Vector4;
	constexpr auto YYXX() const noexcept -> Vector4;
	constexpr auto YYYX() const noexcept -> Vector4;
	constexpr auto YYZX() const noexcept -> Vector4;
	constexpr auto YYWX() const noexcept -> Vector4;
	constexpr auto YZXX() const noexcept -> Vector4;
	constexpr auto YZYX() const noexcept -> Vector4;
	constexpr auto YZZX() const noexcept -> Vector4;
	constexpr auto YZWX() const noexcept -> Vector4;
	constexpr auto YWXX() const noexcept -> Vector4;
	constexpr auto YWYX() const noexcept -> Vector4;
	constexpr auto YWZX() const noexcept -> Vector4;
	constexpr auto YWWX() const noexcept -> Vector4;
	constexpr auto ZXXX() const noexcept -> Vector4;
	constexpr auto ZXYX() const noexcept -> Vector4;
	constexpr auto ZXZX() const noexcept -> Vector4;
	constexpr auto ZXWX() const noexcept -> Vector4;
	constexpr auto ZYXX() const noexcept -> Vector4;
	constexpr auto ZYYX() const noexcept -> Vector4;
	constexpr auto ZYZX() const noexcept -> Vector4;
	constexpr auto ZYWX() const noexcept -> Vector4;
	constexpr auto ZZXX() const noexcept -> Vector4;
	constexpr auto ZZYX() const noexcept -> Vector4;
	constexpr auto ZZZX() const noexcept -> Vector4;
	constexpr auto ZZWX() const noexcept -> Vector4;
	constexpr auto ZWXX() const noexcept -> Vector4;
	constexpr auto ZWYX() const noexcept -> Vector4;
	constexpr auto ZWZX() const noexcept -> Vector4;
	constexpr auto ZWWX() const noexcept -> Vector4;
	constexpr auto WXXX() const noexcept -> Vector4;
	constexpr auto WXYX() const noexcept -> Vector4;
	constexpr auto WXZX() const noexcept -> Vector4;
	constexpr auto WXWX() const noexcept -> Vector4;
	constexpr auto WYXX() const noexcept -> Vector4;
	constexpr auto WYYX() const noexcept -> Vector4;
	constexpr auto WYZX() const noexcept -> Vector4;
	constexpr auto WYWX() const noexcept -> Vector4;
	constexpr auto WZXX() const noexcept -> Vector4;
	constexpr auto WZYX() const noexcept -> Vector4;
	constexpr auto WZZX() const noexcept -> Vector4;
	constexpr auto WZWX() const noexcept -> Vector4;
	constexpr auto WWXX() const noexcept -> Vector4;
	constexpr auto WWYX() const noexcept -> Vector4;
	constexpr auto WWZX() const noexcept -> Vector4;
	constexpr auto WWWX() const noexcept -> Vector4;
	constexpr auto XXXY() const noexcept -> Vector4;
	constexpr auto XXYY() const noexcept -> Vector4;
	constexpr auto XXZY() const noexcept -> Vector4;
	constexpr auto XXWY() const noexcept -> Vector4;
	constexpr auto XYXY() const noexcept -> Vector4;
	constexpr auto XYYY() const noexcept -> Vector4;
	constexpr auto XYZY() const noexcept -> Vector4;
	constexpr auto XYWY() const noexcept -> Vector4;
	constexpr auto XZXY() const noexcept -> Vector4;
	constexpr auto XZYY() const noexcept -> Vector4;
	constexpr auto XZZY() const noexcept -> Vector4;
	constexpr auto XZWY() const noexcept -> Vector4;
	constexpr auto XWXY() const noexcept -> Vector4;
	constexpr auto XWYY() const noexcept -> Vector4;
	constexpr auto XWZY() const noexcept -> Vector4;
	constexpr auto XWWY() const noexcept -> Vector4;
	constexpr auto YXXY() const noexcept -> Vector4;
	constexpr auto YXYY() const noexcept -> Vector4;
	constexpr auto YXZY() const noexcept -> Vector4;
	constexpr auto YXWY() const noexcept -> Vector4;
	constexpr auto YYXY() const noexcept -> Vector4;
	constexpr auto YYYY() const noexcept -> Vector4;
	constexpr auto YYZY() const noexcept -> Vector4;
	constexpr auto YYWY() const noexcept -> Vector4;
	constexpr auto YZXY() const noexcept -> Vector4;
	constexpr auto YZYY() const noexcept -> Vector4;
	constexpr auto YZZY() const noexcept -> Vector4;
	constexpr auto YZWY() const noexcept -> Vector4;
	constexpr auto YWXY() const noexcept -> Vector4;
	constexpr auto YWYY() const noexcept -> Vector4;
	constexpr auto YWZY() const noexcept -> Vector4;
	constexpr auto YWWY() const noexcept -> Vector4;
	constexpr auto ZXXY() const noexcept -> Vector4;
	constexpr auto ZXYY() const noexcept -> Vector4;
	constexpr auto ZXZY() const noexcept -> Vector4;
	constexpr auto ZXWY() const noexcept -> Vector4;
	constexpr auto ZYXY() const noexcept -> Vector4;
	constexpr auto ZYYY() const noexcept -> Vector4;
	constexpr auto ZYZY() const noexcept -> Vector4;
	constexpr auto ZYWY() const noexcept -> Vector4;
	constexpr auto ZZXY() const noexcept -> Vector4;
	constexpr auto ZZYY() const noexcept -> Vector4;
	constexpr auto ZZZY() const noexcept -> Vector4;
	constexpr auto ZZWY() const noexcept -> Vector4;
	constexpr auto ZWXY() const noexcept -> Vector4;
	constexpr auto ZWYY() const noexcept -> Vector4;
	constexpr auto ZWZY() const noexcept -> Vector4;
	constexpr auto ZWWY() const noexcept -> Vector4;
	constexpr auto WXXY() const noexcept -> Vector4;
	constexpr auto WXYY() const noexcept -> Vector4;
	constexpr auto WXZY() const noexcept -> Vector4;
	constexpr auto WXWY() const noexcept -> Vector4;
	constexpr auto WYXY() const noexcept -> Vector4;
	constexpr auto WYYY() const noexcept -> Vector4;
	constexpr auto WYZY() const noexcept -> Vector4;
	constexpr auto WYWY() const noexcept -> Vector4;
	constexpr auto WZXY() const noexcept -> Vector4;
	constexpr auto WZYY() const noexcept -> Vector4;
	constexpr auto WZZY() const noexcept -> Vector4;
	constexpr auto WZWY() const noexcept -> Vector4;
	constexpr auto WWXY() const noexcept -> Vector4;
	constexpr auto WWYY() const noexcept -> Vector4;
	constexpr auto WWZY() const noexcept -> Vector4;
	constexpr auto WWWY() const noexcept -> Vector4;
	constexpr auto XXXZ() const noexcept -> Vector4;
	constexpr auto XXYZ() const noexcept -> Vector4;
	constexpr auto XXZZ() const noexcept -> Vector4;
	constexpr auto XXWZ() const noexcept -> Vector4;
	constexpr auto XYXZ() const noexcept -> Vector4;
	constexpr auto XYYZ() const noexcept -> Vector4;
	constexpr auto XYZZ() const noexcept -> Vector4;
	constexpr auto XYWZ() const noexcept -> Vector4;
	constexpr auto XZXZ() const noexcept -> Vector4;
	constexpr auto XZYZ() const noexcept -> Vector4;
	constexpr auto XZZZ() const noexcept -> Vector4;
	constexpr auto XZWZ() const noexcept -> Vector4;
	constexpr auto XWXZ() const noexcept -> Vector4;
	constexpr auto XWYZ() const noexcept -> Vector4;
	constexpr auto XWZZ() const noexcept -> Vector4;
	constexpr auto XWWZ() const noexcept -> Vector4;
	constexpr auto YXXZ() const noexcept -> Vector4;
	constexpr auto YXYZ() const noexcept -> Vector4;
	constexpr auto YXZZ() const noexcept -> Vector4;
	constexpr auto YXWZ() const noexcept -> Vector4;
	constexpr auto YYXZ() const noexcept -> Vector4;
	constexpr auto YYYZ() const noexcept -> Vector4;
	constexpr auto YYZZ() const noexcept -> Vector4;
	constexpr auto YYWZ() const noexcept -> Vector4;
	constexpr auto YZXZ() const noexcept -> Vector4;
	constexpr auto YZYZ() const noexcept -> Vector4;
	constexpr auto YZZZ() const noexcept -> Vector4;
	constexpr auto YZWZ() const noexcept -> Vector4;
	constexpr auto YWXZ() const noexcept -> Vector4;
	constexpr auto YWYZ() const noexcept -> Vector4;
	constexpr auto YWZZ() const noexcept -> Vector4;
	constexpr auto YWWZ() const noexcept -> Vector4;
	constexpr auto ZXXZ() const noexcept -> Vector4;
	constexpr auto ZXYZ() const noexcept -> Vector4;
	constexpr auto ZXZZ() const noexcept -> Vector4;
	constexpr auto ZXWZ() const noexcept -> Vector4;
	constexpr auto ZYXZ() const noexcept -> Vector4;
	constexpr auto ZYYZ() const noexcept -> Vector4;
	constexpr auto ZYZZ() const noexcept -> Vector4;
	constexpr auto ZYWZ() const noexcept -> Vector4;
	constexpr auto ZZXZ() const noexcept -> Vector4;
	constexpr auto ZZYZ() const noexcept -> Vector4;
	constexpr auto ZZZZ() const noexcept -> Vector4;
	constexpr auto ZZWZ() const noexcept -> Vector4;
	constexpr auto ZWXZ() const noexcept -> Vector4;
	constexpr auto ZWYZ() const noexcept -> Vector4;
	constexpr auto ZWZZ() const noexcept -> Vector4;
	constexpr auto ZWWZ() const noexcept -> Vector4;
	constexpr auto WXXZ() const noexcept -> Vector4;
	constexpr auto WXYZ() const noexcept -> Vector4;
	constexpr auto WXZZ() const noexcept -> Vector4;
	constexpr auto WXWZ() const noexcept -> Vector4;
	constexpr auto WYXZ() const noexcept -> Vector4;
	constexpr auto WYYZ() const noexcept -> Vector4;
	constexpr auto WYZZ() const noexcept -> Vector4;
	constexpr auto WYWZ() const noexcept -> Vector4;
	constexpr auto WZXZ() const noexcept -> Vector4;
	constexpr auto WZYZ() const noexcept -> Vector4;
	constexpr auto WZZZ() const noexcept -> Vector4;
	constexpr auto WZWZ() const noexcept -> Vector4;
	constexpr auto WWXZ() const noexcept -> Vector4;
	constexpr auto WWYZ() const noexcept -> Vector4;
	constexpr auto WWZZ() const noexcept -> Vector4;
	constexpr auto WWWZ() const noexcept -> Vector4;
	constexpr auto XXXW() const noexcept -> Vector4;
	constexpr auto XXYW() const noexcept -> Vector4;
	constexpr auto XXZW() const noexcept -> Vector4;
	constexpr auto XXWW() const noexcept -> Vector4;
	constexpr auto XYXW() const noexcept -> Vector4;
	constexpr auto XYYW() const noexcept -> Vector4;
	constexpr auto XYZW() const noexcept -> Vector4;
	constexpr auto XYWW() const noexcept -> Vector4;
	constexpr auto XZXW() const noexcept -> Vector4;
	constexpr auto XZYW() const noexcept -> Vector4;
	constexpr auto XZZW() const noexcept -> Vector4;
	constexpr auto XZWW() const noexcept -> Vector4;
	constexpr auto XWXW() const noexcept -> Vector4;
	constexpr auto XWYW() const noexcept -> Vector4;
	constexpr auto XWZW() const noexcept -> Vector4;
	constexpr auto XWWW() const noexcept -> Vector4;
	constexpr auto YXXW() const noexcept -> Vector4;
	constexpr auto YXYW() const noexcept -> Vector4;
	constexpr auto YXZW() const noexcept -> Vector4;
	constexpr auto YXWW() const noexcept -> Vector4;
	constexpr auto YYXW() const noexcept -> Vector4;
	constexpr auto YYYW() const noexcept -> Vector4;
	constexpr auto YYZW() const noexcept -> Vector4;
	constexpr auto YYWW() const noexcept -> Vector4;
	constexpr auto YZXW() const noexcept -> Vector4;
	constexpr auto YZYW() const noexcept -> Vector4;
	constexpr auto YZZW() const noexcept -> Vector4;
	constexpr auto YZWW() const noexcept -> Vector4;
	constexpr auto YWXW() const noexcept -> Vector4;
	constexpr auto YWYW() const noexcept -> Vector4;
	constexpr auto YWZW() const noexcept -> Vector4;
	constexpr auto YWWW() const noexcept -> Vector4;
	constexpr auto ZXXW() const noexcept -> Vector4;
	constexpr auto ZXYW() const noexcept -> Vector4;
	constexpr auto ZXZW() const noexcept -> Vector4;
	constexpr auto ZXWW() const noexcept -> Vector4;
	constexpr auto ZYXW() const noexcept -> Vector4;
	constexpr auto ZYYW() const noexcept -> Vector4;
	constexpr auto ZYZW() const noexcept -> Vector4;
	constexpr auto ZYWW() const noexcept -> Vector4;
	constexpr auto ZZXW() const noexcept -> Vector4;
	constexpr auto ZZYW() const noexcept -> Vector4;
	constexpr auto ZZZW() const noexcept -> Vector4;
	constexpr auto ZZWW() const noexcept -> Vector4;
	constexpr auto ZWXW() const noexcept -> Vector4;
	constexpr auto ZWYW() const noexcept -> Vector4;
	constexpr auto ZWZW() const noexcept -> Vector4;
	constexpr auto ZWWW() const noexcept -> Vector4;
	constexpr auto WXXW() const noexcept -> Vector4;
	constexpr auto WXYW() const noexcept -> Vector4;
	constexpr auto WXZW() const noexcept -> Vector4;
	constexpr auto WXWW() const noexcept -> Vector4;
	constexpr auto WYXW() const noexcept -> Vector4;
	constexpr auto WYYW() const noexcept -> Vector4;
	constexpr auto WYZW() const noexcept -> Vector4;
	constexpr auto WYWW() const noexcept -> Vector4;
	constexpr auto WZXW() const noexcept -> Vector4;
	constexpr auto WZYW() const noexcept -> Vector4;
	constexpr auto WZZW() const noexcept -> Vector4;
	constexpr auto WZWW() const noexcept -> Vector4;
	constexpr auto WWXW() const noexcept -> Vector4;
	constexpr auto WWYW() const noexcept -> Vector4;
	constexpr auto WWZW() const noexcept -> Vector4;
	constexpr auto WWWW() const noexcept -> Vector4;
	
	/**
	 * \brief X成分を設定
	 * \param x 設定する値
	 * \return メソッドチェイン
	 */
	auto X(float x) noexcept -> Vector4&;
	/**
	 * \brief Y成分を設定
	 * \param y 設定する値
	 * \return メソッドチェイン
	 */
	auto Y(float y) noexcept -> Vector4&;
	/**
	 * \brief Z成分を設定
	 * \param z 設定する値
	 * \return メソッドチェイン
	 */
	auto Z(float z) noexcept -> Vector4&;
	/**
	 * \brief W成分を設定
	 * \param w 設定する値
	 * \return メソッドチェイン
	 */
	auto W(float w) noexcept -> Vector4&;
	/**
	 * \brief 正規化する
	 * \return メソッドチェイン
	 */
	auto Normalize() noexcept -> Vector4&;

	/**
	 * \brief コンポーネントを配列と見た時の先頭アドレスの取得
	 * \return 先頭アドレス
	 */
	constexpr auto Data() const noexcept -> float const*;

	/**
	 * \brief 長さを取得
	 * \return 長さ
	 */
	constexpr auto Length() const noexcept -> float;
	/**
	 * \brief 零ベクトルかどうか
	 * \return 零ベクトルなら真
	 */
	constexpr auto IsZero() const noexcept -> bool;

	/**
	 * \brief ベクトルを作成する
	 * \param x X成分
	 * \param y Y成分
	 * \param z Z成分
	 * \param w W成分
	 * \return 生成したベクトル
	 */
	static constexpr auto Set(float x, float y, float z, float w) noexcept -> Vector4;
	/**
	 * \brief 2次元ベクトルとZ成分から4次元ベクトルを作成する
	 * \param vector2 XYベクトル
	 * \param z Z成分
	 * \param w W成分
	 * \return 生成したベクトル
	 */
	static constexpr auto Set(const Vector2& vector2, float z, float w) noexcept -> Vector4;
	/**
	 * \brief 3次元ベクトルとW成分から4次元ベクトルを作成する
	 * \param vector3 XYZベクトル
	 * \param w W成分
	 * \return 生成したベクトル
	 */
	static constexpr auto Set(const Vector3& vector3, float w) noexcept -> Vector4;
	/**
	 * \brief 各コンポーネントに値を複製したベクトルを作成する
	 * \param value 複製する値
	 * \return 生成したベクトル
	 */
	static constexpr auto Replicate(float value)->Vector4;
	/**
	 * \brief 正規化したベクトルを取得
	 * \return 正規化したベクトル
	 */
	static constexpr auto Normalize(const Vector4& vector) noexcept -> Vector4;
	/**
	 * \brief 逆ベクトルを取得
	 * \return 逆ベクトル
	 */
	static constexpr auto Inverse(const Vector4& vector) noexcept -> Vector4;
	/**
	 * \brief 逆数ベクトルを取得
	 * \return 逆数ベクトル
	 */
	static constexpr auto Reciprocal(const Vector4& vector) noexcept -> Vector4;
	/**
	 * \brief 内積を行う
	 * \param lhs 左辺
	 * \param rhs 右辺
	 * \return 内積
	 */
	static constexpr auto Dot(const Vector4& lhs, const Vector4& rhs) noexcept -> float;
	
	explicit constexpr operator Vector3() const noexcept;

	explicit constexpr operator bool() const noexcept;
	constexpr auto operator!() const noexcept -> bool;

	constexpr auto operator[](std::size_t index) const -> const float&;
	auto operator[](std::size_t index) -> float&;

	constexpr auto operator+() const noexcept -> Vector4;
	constexpr auto operator-() const noexcept -> Vector4;
	constexpr auto operator~() const noexcept -> Vector4;

	auto operator+=(const Vector4& rhs) noexcept -> Vector4&;
	auto operator-=(const Vector4& rhs) noexcept -> Vector4&;
	auto operator*=(const Vector4& rhs) noexcept -> Vector4&;
	auto operator/=(const Vector4& rhs) noexcept -> Vector4&;
	auto operator+=(float rhs) noexcept -> Vector4&;
	auto operator-=(float rhs) noexcept -> Vector4&;
	auto operator*=(float rhs) noexcept -> Vector4&;
	auto operator/=(float rhs) noexcept -> Vector4&;

	static constexpr int DIMENSION = 4;

private:
	float v[DIMENSION];
};

constexpr auto operator+(const Vector4& lhs, const Vector4& rhs) noexcept -> Vector4;
constexpr auto operator-(const Vector4& lhs, const Vector4& rhs) noexcept -> Vector4;
constexpr auto operator*(const Vector4& lhs, const Vector4& rhs) noexcept -> Vector4;
constexpr auto operator/(const Vector4& lhs, const Vector4& rhs) noexcept -> Vector4;
constexpr auto operator+(const Vector4& lhs, float rhs) noexcept -> Vector4;
constexpr auto operator-(const Vector4& lhs, float rhs) noexcept -> Vector4;
constexpr auto operator*(const Vector4& lhs, float rhs) noexcept -> Vector4;
constexpr auto operator/(const Vector4& lhs, float rhs) noexcept -> Vector4;

constexpr auto operator>(const Vector4& lhs, const Vector4& rhs) noexcept -> bool;
constexpr auto operator>=(const Vector4& lhs, const Vector4& rhs) noexcept -> bool;
constexpr auto operator<(const Vector4& lhs, const Vector4& rhs) noexcept -> bool;
constexpr auto operator<=(const Vector4& lhs, const Vector4& rhs) noexcept -> bool;

constexpr auto operator==(const Vector4& lhs, const Vector4& rhs) noexcept -> bool;
constexpr auto operator!=(const Vector4& lhs, const Vector4& rhs) noexcept -> bool;

auto operator<<(std::ostream& os, const Vector4& rhs) noexcept -> std::ostream&;


constexpr Vector4::Vector4()
	: Vector4(0, 0, 0, 0) {}

constexpr Vector4::Vector4(float x, float y, float z, float w)
	: v{ x, y, z, w } {}

constexpr Vector4::Vector4(const Vector3& rhs)
	: Vector4(rhs.X(), rhs.Y(), rhs.Z(), 0) {}

constexpr auto Vector4::operator=(const Vector3& rhs) -> Vector4& {

	*this = Set(rhs, 0);
	return *this;
}

constexpr auto Vector4::X() const noexcept -> float {

	return v[0];
}

constexpr auto Vector4::Y() const noexcept -> float {

	return v[1];
}

constexpr auto Vector4::Z() const noexcept -> float {

	return v[2];
}

constexpr auto Vector4::W() const noexcept -> float {

	return v[3];
}

constexpr auto Vector4::XX() const noexcept -> Vector2 {

	return Vector2::Set(X(), X());
}
constexpr auto Vector4::XY() const noexcept -> Vector2 {

	return Vector2::Set(X(), Y());
}
constexpr auto Vector4::XZ() const noexcept -> Vector2 {

	return Vector2::Set(X(), Z());
}
constexpr auto Vector4::XW() const noexcept -> Vector2 {

	return Vector2::Set(X(), W());
}
constexpr auto Vector4::YX() const noexcept -> Vector2 {

	return Vector2::Set(Y(), X());
}
constexpr auto Vector4::YY() const noexcept -> Vector2 {

	return Vector2::Set(Y(), Y());
}
constexpr auto Vector4::YZ() const noexcept -> Vector2 {

	return Vector2::Set(Y(), Z());
}
constexpr auto Vector4::YW() const noexcept -> Vector2 {

	return Vector2::Set(Y(), W());
}
constexpr auto Vector4::ZX() const noexcept -> Vector2 {

	return Vector2::Set(Z(), X());
}
constexpr auto Vector4::ZY() const noexcept -> Vector2 {

	return Vector2::Set(Z(), Y());
}
constexpr auto Vector4::ZZ() const noexcept -> Vector2 {

	return Vector2::Set(Z(), Z());
}
constexpr auto Vector4::ZW() const noexcept -> Vector2 {

	return Vector2::Set(Z(), W());
}
constexpr auto Vector4::WX() const noexcept -> Vector2 {

	return Vector2::Set(W(), X());
}
constexpr auto Vector4::WY() const noexcept -> Vector2 {

	return Vector2::Set(W(), Y());
}
constexpr auto Vector4::WZ() const noexcept -> Vector2 {

	return Vector2::Set(W(), Z());
}
constexpr auto Vector4::WW() const noexcept -> Vector2 {

	return Vector2::Set(W(), W());
}

constexpr auto Vector4::XXX() const noexcept -> Vector3 {

	return Vector3::Set(XX(), X());
}
constexpr auto Vector4::XXY() const noexcept -> Vector3 {

	return Vector3::Set(XX(), Y());
}
constexpr auto Vector4::XXZ() const noexcept -> Vector3 {

	return Vector3::Set(XX(), Z());
}
constexpr auto Vector4::XXW() const noexcept -> Vector3 {

	return Vector3::Set(XX(), W());
}
constexpr auto Vector4::XYX() const noexcept -> Vector3 {

	return Vector3::Set(XY(), X());
}
constexpr auto Vector4::XYY() const noexcept -> Vector3 {

	return Vector3::Set(XY(), Y());
}
constexpr auto Vector4::XYZ() const noexcept -> Vector3 {

	return Vector3::Set(XY(), Z());
}
constexpr auto Vector4::XYW() const noexcept -> Vector3 {

	return Vector3::Set(XY(), W());
}
constexpr auto Vector4::XZX() const noexcept -> Vector3 {

	return Vector3::Set(XZ(), X());
}
constexpr auto Vector4::XZY() const noexcept -> Vector3 {

	return Vector3::Set(XZ(), Y());
}
constexpr auto Vector4::XZZ() const noexcept -> Vector3 {

	return Vector3::Set(XZ(), Z());
}
constexpr auto Vector4::XZW() const noexcept -> Vector3 {

	return Vector3::Set(XZ(), W());
}
constexpr auto Vector4::XWX() const noexcept -> Vector3 {

	return Vector3::Set(XW(), X());
}
constexpr auto Vector4::XWY() const noexcept -> Vector3 {

	return Vector3::Set(XW(), Y());
}
constexpr auto Vector4::XWZ() const noexcept -> Vector3 {

	return Vector3::Set(XW(), Z());
}
constexpr auto Vector4::XWW() const noexcept -> Vector3 {

	return Vector3::Set(XW(), W());
}
constexpr auto Vector4::YXX() const noexcept -> Vector3 {

	return Vector3::Set(YX(), X());
}
constexpr auto Vector4::YXY() const noexcept -> Vector3 {

	return Vector3::Set(YX(), Y());
}
constexpr auto Vector4::YXZ() const noexcept -> Vector3 {

	return Vector3::Set(YX(), Z());
}
constexpr auto Vector4::YXW() const noexcept -> Vector3 {

	return Vector3::Set(YX(), W());
}
constexpr auto Vector4::YYX() const noexcept -> Vector3 {

	return Vector3::Set(YY(), X());
}
constexpr auto Vector4::YYY() const noexcept -> Vector3 {

	return Vector3::Set(YY(), Y());
}
constexpr auto Vector4::YYZ() const noexcept -> Vector3 {

	return Vector3::Set(YY(), Z());
}
constexpr auto Vector4::YYW() const noexcept -> Vector3 {

	return Vector3::Set(YY(), W());
}
constexpr auto Vector4::YZX() const noexcept -> Vector3 {

	return Vector3::Set(YZ(), X());
}
constexpr auto Vector4::YZY() const noexcept -> Vector3 {

	return Vector3::Set(YZ(), Y());
}
constexpr auto Vector4::YZZ() const noexcept -> Vector3 {

	return Vector3::Set(YZ(), Z());
}
constexpr auto Vector4::YZW() const noexcept -> Vector3 {

	return Vector3::Set(YZ(), W());
}
constexpr auto Vector4::YWX() const noexcept -> Vector3 {

	return Vector3::Set(YW(), X());
}
constexpr auto Vector4::YWY() const noexcept -> Vector3 {

	return Vector3::Set(YW(), Y());
}
constexpr auto Vector4::YWZ() const noexcept -> Vector3 {

	return Vector3::Set(YW(), Z());
}
constexpr auto Vector4::YWW() const noexcept -> Vector3 {

	return Vector3::Set(YW(), W());
}
constexpr auto Vector4::ZXX() const noexcept -> Vector3 {

	return Vector3::Set(ZX(), X());
}
constexpr auto Vector4::ZXY() const noexcept -> Vector3 {

	return Vector3::Set(ZX(), Y());
}
constexpr auto Vector4::ZXZ() const noexcept -> Vector3 {

	return Vector3::Set(ZX(), Z());
}
constexpr auto Vector4::ZXW() const noexcept -> Vector3 {

	return Vector3::Set(ZX(), W());
}
constexpr auto Vector4::ZYX() const noexcept -> Vector3 {

	return Vector3::Set(ZY(), X());
}
constexpr auto Vector4::ZYY() const noexcept -> Vector3 {

	return Vector3::Set(ZY(), Y());
}
constexpr auto Vector4::ZYZ() const noexcept -> Vector3 {

	return Vector3::Set(ZY(), Z());
}
constexpr auto Vector4::ZYW() const noexcept -> Vector3 {

	return Vector3::Set(ZY(), W());
}
constexpr auto Vector4::ZZX() const noexcept -> Vector3 {

	return Vector3::Set(ZZ(), X());
}
constexpr auto Vector4::ZZY() const noexcept -> Vector3 {

	return Vector3::Set(ZZ(), Y());
}
constexpr auto Vector4::ZZZ() const noexcept -> Vector3 {

	return Vector3::Set(ZZ(), Z());
}
constexpr auto Vector4::ZZW() const noexcept -> Vector3 {

	return Vector3::Set(ZZ(), W());
}
constexpr auto Vector4::ZWX() const noexcept -> Vector3 {

	return Vector3::Set(ZW(), X());
}
constexpr auto Vector4::ZWY() const noexcept -> Vector3 {

	return Vector3::Set(ZW(), Y());
}
constexpr auto Vector4::ZWZ() const noexcept -> Vector3 {

	return Vector3::Set(ZW(), Z());
}
constexpr auto Vector4::ZWW() const noexcept -> Vector3 {

	return Vector3::Set(ZW(), W());
}
constexpr auto Vector4::WXX() const noexcept -> Vector3 {

	return Vector3::Set(WX(), X());
}
constexpr auto Vector4::WXY() const noexcept -> Vector3 {

	return Vector3::Set(WX(), Y());
}
constexpr auto Vector4::WXZ() const noexcept -> Vector3 {

	return Vector3::Set(WX(), Z());
}
constexpr auto Vector4::WXW() const noexcept -> Vector3 {

	return Vector3::Set(WX(), W());
}
constexpr auto Vector4::WYX() const noexcept -> Vector3 {

	return Vector3::Set(WY(), X());
}
constexpr auto Vector4::WYY() const noexcept -> Vector3 {

	return Vector3::Set(WY(), Y());
}
constexpr auto Vector4::WYZ() const noexcept -> Vector3 {

	return Vector3::Set(WY(), Z());
}
constexpr auto Vector4::WYW() const noexcept -> Vector3 {

	return Vector3::Set(WY(), W());
}
constexpr auto Vector4::WZX() const noexcept -> Vector3 {

	return Vector3::Set(WZ(), X());
}
constexpr auto Vector4::WZY() const noexcept -> Vector3 {

	return Vector3::Set(WZ(), Y());
}
constexpr auto Vector4::WZZ() const noexcept -> Vector3 {

	return Vector3::Set(WZ(), Z());
}
constexpr auto Vector4::WZW() const noexcept -> Vector3 {

	return Vector3::Set(WZ(), W());
}
constexpr auto Vector4::WWX() const noexcept -> Vector3 {

	return Vector3::Set(WW(), X());
}
constexpr auto Vector4::WWY() const noexcept -> Vector3 {

	return Vector3::Set(WW(), Y());
}
constexpr auto Vector4::WWZ() const noexcept -> Vector3 {

	return Vector3::Set(WW(), Z());
}
constexpr auto Vector4::WWW() const noexcept -> Vector3 {

	return Vector3::Set(WW(), W());
}

constexpr auto Vector4::XXXX() const noexcept -> Vector4 { return Set(XXX(), X()); }
constexpr auto Vector4::XXYX() const noexcept -> Vector4 { return Set(XXY(), X()); }
constexpr auto Vector4::XXZX() const noexcept -> Vector4 { return Set(XXZ(), X()); }
constexpr auto Vector4::XXWX() const noexcept -> Vector4 { return Set(XXW(), X()); }
constexpr auto Vector4::XYXX() const noexcept -> Vector4 { return Set(XYX(), X()); }
constexpr auto Vector4::XYYX() const noexcept -> Vector4 { return Set(XYY(), X()); }
constexpr auto Vector4::XYZX() const noexcept -> Vector4 { return Set(XYZ(), X()); }
constexpr auto Vector4::XYWX() const noexcept -> Vector4 { return Set(XYW(), X()); }
constexpr auto Vector4::XZXX() const noexcept -> Vector4 { return Set(XZX(), X()); }
constexpr auto Vector4::XZYX() const noexcept -> Vector4 { return Set(XZY(), X()); }
constexpr auto Vector4::XZZX() const noexcept -> Vector4 { return Set(XZZ(), X()); }
constexpr auto Vector4::XZWX() const noexcept -> Vector4 { return Set(XZW(), X()); }
constexpr auto Vector4::XWXX() const noexcept -> Vector4 { return Set(XWX(), X()); }
constexpr auto Vector4::XWYX() const noexcept -> Vector4 { return Set(XWY(), X()); }
constexpr auto Vector4::XWZX() const noexcept -> Vector4 { return Set(XWZ(), X()); }
constexpr auto Vector4::XWWX() const noexcept -> Vector4 { return Set(XWW(), X()); }
constexpr auto Vector4::YXXX() const noexcept -> Vector4 { return Set(YXX(), X()); }
constexpr auto Vector4::YXYX() const noexcept -> Vector4 { return Set(YXY(), X()); }
constexpr auto Vector4::YXZX() const noexcept -> Vector4 { return Set(YXZ(), X()); }
constexpr auto Vector4::YXWX() const noexcept -> Vector4 { return Set(YXW(), X()); }
constexpr auto Vector4::YYXX() const noexcept -> Vector4 { return Set(YYX(), X()); }
constexpr auto Vector4::YYYX() const noexcept -> Vector4 { return Set(YYY(), X()); }
constexpr auto Vector4::YYZX() const noexcept -> Vector4 { return Set(YYZ(), X()); }
constexpr auto Vector4::YYWX() const noexcept -> Vector4 { return Set(YYW(), X()); }
constexpr auto Vector4::YZXX() const noexcept -> Vector4 { return Set(YZX(), X()); }
constexpr auto Vector4::YZYX() const noexcept -> Vector4 { return Set(YZY(), X()); }
constexpr auto Vector4::YZZX() const noexcept -> Vector4 { return Set(YZZ(), X()); }
constexpr auto Vector4::YZWX() const noexcept -> Vector4 { return Set(YZW(), X()); }
constexpr auto Vector4::YWXX() const noexcept -> Vector4 { return Set(YWX(), X()); }
constexpr auto Vector4::YWYX() const noexcept -> Vector4 { return Set(YWY(), X()); }
constexpr auto Vector4::YWZX() const noexcept -> Vector4 { return Set(YWZ(), X()); }
constexpr auto Vector4::YWWX() const noexcept -> Vector4 { return Set(YWW(), X()); }
constexpr auto Vector4::ZXXX() const noexcept -> Vector4 { return Set(ZXX(), X()); }
constexpr auto Vector4::ZXYX() const noexcept -> Vector4 { return Set(ZXY(), X()); }
constexpr auto Vector4::ZXZX() const noexcept -> Vector4 { return Set(ZXZ(), X()); }
constexpr auto Vector4::ZXWX() const noexcept -> Vector4 { return Set(ZXW(), X()); }
constexpr auto Vector4::ZYXX() const noexcept -> Vector4 { return Set(ZYX(), X()); }
constexpr auto Vector4::ZYYX() const noexcept -> Vector4 { return Set(ZYY(), X()); }
constexpr auto Vector4::ZYZX() const noexcept -> Vector4 { return Set(ZYZ(), X()); }
constexpr auto Vector4::ZYWX() const noexcept -> Vector4 { return Set(ZYW(), X()); }
constexpr auto Vector4::ZZXX() const noexcept -> Vector4 { return Set(ZZX(), X()); }
constexpr auto Vector4::ZZYX() const noexcept -> Vector4 { return Set(ZZY(), X()); }
constexpr auto Vector4::ZZZX() const noexcept -> Vector4 { return Set(ZZZ(), X()); }
constexpr auto Vector4::ZZWX() const noexcept -> Vector4 { return Set(ZZW(), X()); }
constexpr auto Vector4::ZWXX() const noexcept -> Vector4 { return Set(ZWX(), X()); }
constexpr auto Vector4::ZWYX() const noexcept -> Vector4 { return Set(ZWY(), X()); }
constexpr auto Vector4::ZWZX() const noexcept -> Vector4 { return Set(ZWZ(), X()); }
constexpr auto Vector4::ZWWX() const noexcept -> Vector4 { return Set(ZWW(), X()); }
constexpr auto Vector4::WXXX() const noexcept -> Vector4 { return Set(WXX(), X()); }
constexpr auto Vector4::WXYX() const noexcept -> Vector4 { return Set(WXY(), X()); }
constexpr auto Vector4::WXZX() const noexcept -> Vector4 { return Set(WXZ(), X()); }
constexpr auto Vector4::WXWX() const noexcept -> Vector4 { return Set(WXW(), X()); }
constexpr auto Vector4::WYXX() const noexcept -> Vector4 { return Set(WYX(), X()); }
constexpr auto Vector4::WYYX() const noexcept -> Vector4 { return Set(WYY(), X()); }
constexpr auto Vector4::WYZX() const noexcept -> Vector4 { return Set(WYZ(), X()); }
constexpr auto Vector4::WYWX() const noexcept -> Vector4 { return Set(WYW(), X()); }
constexpr auto Vector4::WZXX() const noexcept -> Vector4 { return Set(WZX(), X()); }
constexpr auto Vector4::WZYX() const noexcept -> Vector4 { return Set(WZY(), X()); }
constexpr auto Vector4::WZZX() const noexcept -> Vector4 { return Set(WZZ(), X()); }
constexpr auto Vector4::WZWX() const noexcept -> Vector4 { return Set(WZW(), X()); }
constexpr auto Vector4::WWXX() const noexcept -> Vector4 { return Set(WWX(), X()); }
constexpr auto Vector4::WWYX() const noexcept -> Vector4 { return Set(WWY(), X()); }
constexpr auto Vector4::WWZX() const noexcept -> Vector4 { return Set(WWZ(), X()); }
constexpr auto Vector4::WWWX() const noexcept -> Vector4 { return Set(WWW(), X()); }
constexpr auto Vector4::XXXY() const noexcept -> Vector4 { return Set(XXX(), Y()); }
constexpr auto Vector4::XXYY() const noexcept -> Vector4 { return Set(XXY(), Y()); }
constexpr auto Vector4::XXZY() const noexcept -> Vector4 { return Set(XXZ(), Y()); }
constexpr auto Vector4::XXWY() const noexcept -> Vector4 { return Set(XXW(), Y()); }
constexpr auto Vector4::XYXY() const noexcept -> Vector4 { return Set(XYX(), Y()); }
constexpr auto Vector4::XYYY() const noexcept -> Vector4 { return Set(XYY(), Y()); }
constexpr auto Vector4::XYZY() const noexcept -> Vector4 { return Set(XYZ(), Y()); }
constexpr auto Vector4::XYWY() const noexcept -> Vector4 { return Set(XYW(), Y()); }
constexpr auto Vector4::XZXY() const noexcept -> Vector4 { return Set(XZX(), Y()); }
constexpr auto Vector4::XZYY() const noexcept -> Vector4 { return Set(XZY(), Y()); }
constexpr auto Vector4::XZZY() const noexcept -> Vector4 { return Set(XZZ(), Y()); }
constexpr auto Vector4::XZWY() const noexcept -> Vector4 { return Set(XZW(), Y()); }
constexpr auto Vector4::XWXY() const noexcept -> Vector4 { return Set(XWX(), Y()); }
constexpr auto Vector4::XWYY() const noexcept -> Vector4 { return Set(XWY(), Y()); }
constexpr auto Vector4::XWZY() const noexcept -> Vector4 { return Set(XWZ(), Y()); }
constexpr auto Vector4::XWWY() const noexcept -> Vector4 { return Set(XWW(), Y()); }
constexpr auto Vector4::YXXY() const noexcept -> Vector4 { return Set(YXX(), Y()); }
constexpr auto Vector4::YXYY() const noexcept -> Vector4 { return Set(YXY(), Y()); }
constexpr auto Vector4::YXZY() const noexcept -> Vector4 { return Set(YXZ(), Y()); }
constexpr auto Vector4::YXWY() const noexcept -> Vector4 { return Set(YXW(), Y()); }
constexpr auto Vector4::YYXY() const noexcept -> Vector4 { return Set(YYX(), Y()); }
constexpr auto Vector4::YYYY() const noexcept -> Vector4 { return Set(YYY(), Y()); }
constexpr auto Vector4::YYZY() const noexcept -> Vector4 { return Set(YYZ(), Y()); }
constexpr auto Vector4::YYWY() const noexcept -> Vector4 { return Set(YYW(), Y()); }
constexpr auto Vector4::YZXY() const noexcept -> Vector4 { return Set(YZX(), Y()); }
constexpr auto Vector4::YZYY() const noexcept -> Vector4 { return Set(YZY(), Y()); }
constexpr auto Vector4::YZZY() const noexcept -> Vector4 { return Set(YZZ(), Y()); }
constexpr auto Vector4::YZWY() const noexcept -> Vector4 { return Set(YZW(), Y()); }
constexpr auto Vector4::YWXY() const noexcept -> Vector4 { return Set(YWX(), Y()); }
constexpr auto Vector4::YWYY() const noexcept -> Vector4 { return Set(YWY(), Y()); }
constexpr auto Vector4::YWZY() const noexcept -> Vector4 { return Set(YWZ(), Y()); }
constexpr auto Vector4::YWWY() const noexcept -> Vector4 { return Set(YWW(), Y()); }
constexpr auto Vector4::ZXXY() const noexcept -> Vector4 { return Set(ZXX(), Y()); }
constexpr auto Vector4::ZXYY() const noexcept -> Vector4 { return Set(ZXY(), Y()); }
constexpr auto Vector4::ZXZY() const noexcept -> Vector4 { return Set(ZXZ(), Y()); }
constexpr auto Vector4::ZXWY() const noexcept -> Vector4 { return Set(ZXW(), Y()); }
constexpr auto Vector4::ZYXY() const noexcept -> Vector4 { return Set(ZYX(), Y()); }
constexpr auto Vector4::ZYYY() const noexcept -> Vector4 { return Set(ZYY(), Y()); }
constexpr auto Vector4::ZYZY() const noexcept -> Vector4 { return Set(ZYZ(), Y()); }
constexpr auto Vector4::ZYWY() const noexcept -> Vector4 { return Set(ZYW(), Y()); }
constexpr auto Vector4::ZZXY() const noexcept -> Vector4 { return Set(ZZX(), Y()); }
constexpr auto Vector4::ZZYY() const noexcept -> Vector4 { return Set(ZZY(), Y()); }
constexpr auto Vector4::ZZZY() const noexcept -> Vector4 { return Set(ZZZ(), Y()); }
constexpr auto Vector4::ZZWY() const noexcept -> Vector4 { return Set(ZZW(), Y()); }
constexpr auto Vector4::ZWXY() const noexcept -> Vector4 { return Set(ZWX(), Y()); }
constexpr auto Vector4::ZWYY() const noexcept -> Vector4 { return Set(ZWY(), Y()); }
constexpr auto Vector4::ZWZY() const noexcept -> Vector4 { return Set(ZWZ(), Y()); }
constexpr auto Vector4::ZWWY() const noexcept -> Vector4 { return Set(ZWW(), Y()); }
constexpr auto Vector4::WXXY() const noexcept -> Vector4 { return Set(WXX(), Y()); }
constexpr auto Vector4::WXYY() const noexcept -> Vector4 { return Set(WXY(), Y()); }
constexpr auto Vector4::WXZY() const noexcept -> Vector4 { return Set(WXZ(), Y()); }
constexpr auto Vector4::WXWY() const noexcept -> Vector4 { return Set(WXW(), Y()); }
constexpr auto Vector4::WYXY() const noexcept -> Vector4 { return Set(WYX(), Y()); }
constexpr auto Vector4::WYYY() const noexcept -> Vector4 { return Set(WYY(), Y()); }
constexpr auto Vector4::WYZY() const noexcept -> Vector4 { return Set(WYZ(), Y()); }
constexpr auto Vector4::WYWY() const noexcept -> Vector4 { return Set(WYW(), Y()); }
constexpr auto Vector4::WZXY() const noexcept -> Vector4 { return Set(WZX(), Y()); }
constexpr auto Vector4::WZYY() const noexcept -> Vector4 { return Set(WZY(), Y()); }
constexpr auto Vector4::WZZY() const noexcept -> Vector4 { return Set(WZZ(), Y()); }
constexpr auto Vector4::WZWY() const noexcept -> Vector4 { return Set(WZW(), Y()); }
constexpr auto Vector4::WWXY() const noexcept -> Vector4 { return Set(WWX(), Y()); }
constexpr auto Vector4::WWYY() const noexcept -> Vector4 { return Set(WWY(), Y()); }
constexpr auto Vector4::WWZY() const noexcept -> Vector4 { return Set(WWZ(), Y()); }
constexpr auto Vector4::WWWY() const noexcept -> Vector4 { return Set(WWW(), Y()); }
constexpr auto Vector4::XXXZ() const noexcept -> Vector4 { return Set(XXX(), Z()); }
constexpr auto Vector4::XXYZ() const noexcept -> Vector4 { return Set(XXY(), Z()); }
constexpr auto Vector4::XXZZ() const noexcept -> Vector4 { return Set(XXZ(), Z()); }
constexpr auto Vector4::XXWZ() const noexcept -> Vector4 { return Set(XXW(), Z()); }
constexpr auto Vector4::XYXZ() const noexcept -> Vector4 { return Set(XYX(), Z()); }
constexpr auto Vector4::XYYZ() const noexcept -> Vector4 { return Set(XYY(), Z()); }
constexpr auto Vector4::XYZZ() const noexcept -> Vector4 { return Set(XYZ(), Z()); }
constexpr auto Vector4::XYWZ() const noexcept -> Vector4 { return Set(XYW(), Z()); }
constexpr auto Vector4::XZXZ() const noexcept -> Vector4 { return Set(XZX(), Z()); }
constexpr auto Vector4::XZYZ() const noexcept -> Vector4 { return Set(XZY(), Z()); }
constexpr auto Vector4::XZZZ() const noexcept -> Vector4 { return Set(XZZ(), Z()); }
constexpr auto Vector4::XZWZ() const noexcept -> Vector4 { return Set(XZW(), Z()); }
constexpr auto Vector4::XWXZ() const noexcept -> Vector4 { return Set(XWX(), Z()); }
constexpr auto Vector4::XWYZ() const noexcept -> Vector4 { return Set(XWY(), Z()); }
constexpr auto Vector4::XWZZ() const noexcept -> Vector4 { return Set(XWZ(), Z()); }
constexpr auto Vector4::XWWZ() const noexcept -> Vector4 { return Set(XWW(), Z()); }
constexpr auto Vector4::YXXZ() const noexcept -> Vector4 { return Set(YXX(), Z()); }
constexpr auto Vector4::YXYZ() const noexcept -> Vector4 { return Set(YXY(), Z()); }
constexpr auto Vector4::YXZZ() const noexcept -> Vector4 { return Set(YXZ(), Z()); }
constexpr auto Vector4::YXWZ() const noexcept -> Vector4 { return Set(YXW(), Z()); }
constexpr auto Vector4::YYXZ() const noexcept -> Vector4 { return Set(YYX(), Z()); }
constexpr auto Vector4::YYYZ() const noexcept -> Vector4 { return Set(YYY(), Z()); }
constexpr auto Vector4::YYZZ() const noexcept -> Vector4 { return Set(YYZ(), Z()); }
constexpr auto Vector4::YYWZ() const noexcept -> Vector4 { return Set(YYW(), Z()); }
constexpr auto Vector4::YZXZ() const noexcept -> Vector4 { return Set(YZX(), Z()); }
constexpr auto Vector4::YZYZ() const noexcept -> Vector4 { return Set(YZY(), Z()); }
constexpr auto Vector4::YZZZ() const noexcept -> Vector4 { return Set(YZZ(), Z()); }
constexpr auto Vector4::YZWZ() const noexcept -> Vector4 { return Set(YZW(), Z()); }
constexpr auto Vector4::YWXZ() const noexcept -> Vector4 { return Set(YWX(), Z()); }
constexpr auto Vector4::YWYZ() const noexcept -> Vector4 { return Set(YWY(), Z()); }
constexpr auto Vector4::YWZZ() const noexcept -> Vector4 { return Set(YWZ(), Z()); }
constexpr auto Vector4::YWWZ() const noexcept -> Vector4 { return Set(YWW(), Z()); }
constexpr auto Vector4::ZXXZ() const noexcept -> Vector4 { return Set(ZXX(), Z()); }
constexpr auto Vector4::ZXYZ() const noexcept -> Vector4 { return Set(ZXY(), Z()); }
constexpr auto Vector4::ZXZZ() const noexcept -> Vector4 { return Set(ZXZ(), Z()); }
constexpr auto Vector4::ZXWZ() const noexcept -> Vector4 { return Set(ZXW(), Z()); }
constexpr auto Vector4::ZYXZ() const noexcept -> Vector4 { return Set(ZYX(), Z()); }
constexpr auto Vector4::ZYYZ() const noexcept -> Vector4 { return Set(ZYY(), Z()); }
constexpr auto Vector4::ZYZZ() const noexcept -> Vector4 { return Set(ZYZ(), Z()); }
constexpr auto Vector4::ZYWZ() const noexcept -> Vector4 { return Set(ZYW(), Z()); }
constexpr auto Vector4::ZZXZ() const noexcept -> Vector4 { return Set(ZZX(), Z()); }
constexpr auto Vector4::ZZYZ() const noexcept -> Vector4 { return Set(ZZY(), Z()); }
constexpr auto Vector4::ZZZZ() const noexcept -> Vector4 { return Set(ZZZ(), Z()); }
constexpr auto Vector4::ZZWZ() const noexcept -> Vector4 { return Set(ZZW(), Z()); }
constexpr auto Vector4::ZWXZ() const noexcept -> Vector4 { return Set(ZWX(), Z()); }
constexpr auto Vector4::ZWYZ() const noexcept -> Vector4 { return Set(ZWY(), Z()); }
constexpr auto Vector4::ZWZZ() const noexcept -> Vector4 { return Set(ZWZ(), Z()); }
constexpr auto Vector4::ZWWZ() const noexcept -> Vector4 { return Set(ZWW(), Z()); }
constexpr auto Vector4::WXXZ() const noexcept -> Vector4 { return Set(WXX(), Z()); }
constexpr auto Vector4::WXYZ() const noexcept -> Vector4 { return Set(WXY(), Z()); }
constexpr auto Vector4::WXZZ() const noexcept -> Vector4 { return Set(WXZ(), Z()); }
constexpr auto Vector4::WXWZ() const noexcept -> Vector4 { return Set(WXW(), Z()); }
constexpr auto Vector4::WYXZ() const noexcept -> Vector4 { return Set(WYX(), Z()); }
constexpr auto Vector4::WYYZ() const noexcept -> Vector4 { return Set(WYY(), Z()); }
constexpr auto Vector4::WYZZ() const noexcept -> Vector4 { return Set(WYZ(), Z()); }
constexpr auto Vector4::WYWZ() const noexcept -> Vector4 { return Set(WYW(), Z()); }
constexpr auto Vector4::WZXZ() const noexcept -> Vector4 { return Set(WZX(), Z()); }
constexpr auto Vector4::WZYZ() const noexcept -> Vector4 { return Set(WZY(), Z()); }
constexpr auto Vector4::WZZZ() const noexcept -> Vector4 { return Set(WZZ(), Z()); }
constexpr auto Vector4::WZWZ() const noexcept -> Vector4 { return Set(WZW(), Z()); }
constexpr auto Vector4::WWXZ() const noexcept -> Vector4 { return Set(WWX(), Z()); }
constexpr auto Vector4::WWYZ() const noexcept -> Vector4 { return Set(WWY(), Z()); }
constexpr auto Vector4::WWZZ() const noexcept -> Vector4 { return Set(WWZ(), Z()); }
constexpr auto Vector4::WWWZ() const noexcept -> Vector4 { return Set(WWW(), Z()); }
constexpr auto Vector4::XXXW() const noexcept -> Vector4 { return Set(XXX(), W()); }
constexpr auto Vector4::XXYW() const noexcept -> Vector4 { return Set(XXY(), W()); }
constexpr auto Vector4::XXZW() const noexcept -> Vector4 { return Set(XXZ(), W()); }
constexpr auto Vector4::XXWW() const noexcept -> Vector4 { return Set(XXW(), W()); }
constexpr auto Vector4::XYXW() const noexcept -> Vector4 { return Set(XYX(), W()); }
constexpr auto Vector4::XYYW() const noexcept -> Vector4 { return Set(XYY(), W()); }
constexpr auto Vector4::XYZW() const noexcept -> Vector4 { return Set(XYZ(), W()); }
constexpr auto Vector4::XYWW() const noexcept -> Vector4 { return Set(XYW(), W()); }
constexpr auto Vector4::XZXW() const noexcept -> Vector4 { return Set(XZX(), W()); }
constexpr auto Vector4::XZYW() const noexcept -> Vector4 { return Set(XZY(), W()); }
constexpr auto Vector4::XZZW() const noexcept -> Vector4 { return Set(XZZ(), W()); }
constexpr auto Vector4::XZWW() const noexcept -> Vector4 { return Set(XZW(), W()); }
constexpr auto Vector4::XWXW() const noexcept -> Vector4 { return Set(XWX(), W()); }
constexpr auto Vector4::XWYW() const noexcept -> Vector4 { return Set(XWY(), W()); }
constexpr auto Vector4::XWZW() const noexcept -> Vector4 { return Set(XWZ(), W()); }
constexpr auto Vector4::XWWW() const noexcept -> Vector4 { return Set(XWW(), W()); }
constexpr auto Vector4::YXXW() const noexcept -> Vector4 { return Set(YXX(), W()); }
constexpr auto Vector4::YXYW() const noexcept -> Vector4 { return Set(YXY(), W()); }
constexpr auto Vector4::YXZW() const noexcept -> Vector4 { return Set(YXZ(), W()); }
constexpr auto Vector4::YXWW() const noexcept -> Vector4 { return Set(YXW(), W()); }
constexpr auto Vector4::YYXW() const noexcept -> Vector4 { return Set(YYX(), W()); }
constexpr auto Vector4::YYYW() const noexcept -> Vector4 { return Set(YYY(), W()); }
constexpr auto Vector4::YYZW() const noexcept -> Vector4 { return Set(YYZ(), W()); }
constexpr auto Vector4::YYWW() const noexcept -> Vector4 { return Set(YYW(), W()); }
constexpr auto Vector4::YZXW() const noexcept -> Vector4 { return Set(YZX(), W()); }
constexpr auto Vector4::YZYW() const noexcept -> Vector4 { return Set(YZY(), W()); }
constexpr auto Vector4::YZZW() const noexcept -> Vector4 { return Set(YZZ(), W()); }
constexpr auto Vector4::YZWW() const noexcept -> Vector4 { return Set(YZW(), W()); }
constexpr auto Vector4::YWXW() const noexcept -> Vector4 { return Set(YWX(), W()); }
constexpr auto Vector4::YWYW() const noexcept -> Vector4 { return Set(YWY(), W()); }
constexpr auto Vector4::YWZW() const noexcept -> Vector4 { return Set(YWZ(), W()); }
constexpr auto Vector4::YWWW() const noexcept -> Vector4 { return Set(YWW(), W()); }
constexpr auto Vector4::ZXXW() const noexcept -> Vector4 { return Set(ZXX(), W()); }
constexpr auto Vector4::ZXYW() const noexcept -> Vector4 { return Set(ZXY(), W()); }
constexpr auto Vector4::ZXZW() const noexcept -> Vector4 { return Set(ZXZ(), W()); }
constexpr auto Vector4::ZXWW() const noexcept -> Vector4 { return Set(ZXW(), W()); }
constexpr auto Vector4::ZYXW() const noexcept -> Vector4 { return Set(ZYX(), W()); }
constexpr auto Vector4::ZYYW() const noexcept -> Vector4 { return Set(ZYY(), W()); }
constexpr auto Vector4::ZYZW() const noexcept -> Vector4 { return Set(ZYZ(), W()); }
constexpr auto Vector4::ZYWW() const noexcept -> Vector4 { return Set(ZYW(), W()); }
constexpr auto Vector4::ZZXW() const noexcept -> Vector4 { return Set(ZZX(), W()); }
constexpr auto Vector4::ZZYW() const noexcept -> Vector4 { return Set(ZZY(), W()); }
constexpr auto Vector4::ZZZW() const noexcept -> Vector4 { return Set(ZZZ(), W()); }
constexpr auto Vector4::ZZWW() const noexcept -> Vector4 { return Set(ZZW(), W()); }
constexpr auto Vector4::ZWXW() const noexcept -> Vector4 { return Set(ZWX(), W()); }
constexpr auto Vector4::ZWYW() const noexcept -> Vector4 { return Set(ZWY(), W()); }
constexpr auto Vector4::ZWZW() const noexcept -> Vector4 { return Set(ZWZ(), W()); }
constexpr auto Vector4::ZWWW() const noexcept -> Vector4 { return Set(ZWW(), W()); }
constexpr auto Vector4::WXXW() const noexcept -> Vector4 { return Set(WXX(), W()); }
constexpr auto Vector4::WXYW() const noexcept -> Vector4 { return Set(WXY(), W()); }
constexpr auto Vector4::WXZW() const noexcept -> Vector4 { return Set(WXZ(), W()); }
constexpr auto Vector4::WXWW() const noexcept -> Vector4 { return Set(WXW(), W()); }
constexpr auto Vector4::WYXW() const noexcept -> Vector4 { return Set(WYX(), W()); }
constexpr auto Vector4::WYYW() const noexcept -> Vector4 { return Set(WYY(), W()); }
constexpr auto Vector4::WYZW() const noexcept -> Vector4 { return Set(WYZ(), W()); }
constexpr auto Vector4::WYWW() const noexcept -> Vector4 { return Set(WYW(), W()); }
constexpr auto Vector4::WZXW() const noexcept -> Vector4 { return Set(WZX(), W()); }
constexpr auto Vector4::WZYW() const noexcept -> Vector4 { return Set(WZY(), W()); }
constexpr auto Vector4::WZZW() const noexcept -> Vector4 { return Set(WZZ(), W()); }
constexpr auto Vector4::WZWW() const noexcept -> Vector4 { return Set(WZW(), W()); }
constexpr auto Vector4::WWXW() const noexcept -> Vector4 { return Set(WWX(), W()); }
constexpr auto Vector4::WWYW() const noexcept -> Vector4 { return Set(WWY(), W()); }
constexpr auto Vector4::WWZW() const noexcept -> Vector4 { return Set(WWZ(), W()); }
constexpr auto Vector4::WWWW() const noexcept -> Vector4 { return Set(WWW(), W()); }

constexpr auto Vector4::Data() const noexcept -> float const* {

	return v;
}

constexpr auto Vector4::Length() const noexcept -> float {

	auto&& square = *this * *this;
	return SquareRoot(square.X() + square.Y() + square.Z() + square.W());
}

constexpr auto Vector4::IsZero() const noexcept -> bool {

	return *this == Set(0, 0, 0, 0);
}

constexpr auto Vector4::Set(float x, float y, float z, float w) noexcept -> Vector4 {

	return Vector4(x, y, z, w);
}

constexpr auto Vector4::Set(const Vector2& vector2, float z, float w) noexcept -> Vector4 {

	return Vector4(vector2.X(), vector2.Y(), z, w);
}

constexpr auto Vector4::Set(const Vector3& vector3, float w) noexcept -> Vector4 {

	return Vector4(vector3.X(), vector3.Y(), vector3.Z(), w);
}

constexpr auto Vector4::Replicate(float value) -> Vector4 {

	return Vector4(value, value, value, value);
}

constexpr auto Vector4::Normalize(const Vector4& vector) noexcept -> Vector4 {

	return vector / vector.Length();
}

constexpr auto Vector4::Inverse(const Vector4& vector) noexcept -> Vector4 {

	return Set(-vector.X(), -vector.Y(), -vector.Z(), -vector.W());
}

constexpr auto Vector4::Reciprocal(const Vector4& vector) noexcept -> Vector4 {

	return Set(1 / vector.X(), 1 / vector.Y(), 1 / vector.Z(), 1 / vector.W());
}

constexpr auto Vector4::Dot(const Vector4& lhs, const Vector4& rhs) noexcept -> float {

	auto&& tmp = lhs * rhs;
	return tmp.X() + tmp.Y() + tmp.Z() + tmp.W();
}

constexpr Vector4::operator Vector3() const noexcept {

	return XYZ();
}

constexpr Vector4::operator bool() const noexcept {

	return !IsZero();
}

constexpr auto Vector4::operator!() const noexcept -> bool {

	return IsZero();
}

constexpr auto Vector4::operator[](std::size_t index) const -> const float& {

	if (index >= DIMENSION)	throw std::out_of_range("");

	return v[index];
}

constexpr auto Vector4::operator+() const noexcept -> Vector4 {

	return *this;
}

constexpr auto Vector4::operator-() const noexcept -> Vector4 {

	return Inverse(*this);
}

constexpr auto Vector4::operator~() const noexcept -> Vector4 {

	return Reciprocal(*this);
}

constexpr auto operator+(const Vector4& lhs, const Vector4& rhs) noexcept -> Vector4 {

	return Vector4::Set(lhs.X() + rhs.X(), lhs.Y() + rhs.Y(), lhs.Z() + rhs.Z(), lhs.W() + rhs.W());
}

constexpr auto operator-(const Vector4& lhs, const Vector4& rhs) noexcept -> Vector4 {

	return Vector4::Set(lhs.X() - rhs.X(), lhs.Y() - rhs.Y(), lhs.Z() - rhs.Z(), lhs.W() - rhs.W());
}

constexpr auto operator*(const Vector4& lhs, const Vector4& rhs) noexcept -> Vector4 {

	return Vector4::Set(lhs.X() * rhs.X(), lhs.Y() * rhs.Y(), lhs.Z() * rhs.Z(), lhs.W() * rhs.W());
}

constexpr auto operator/(const Vector4& lhs, const Vector4& rhs) noexcept -> Vector4 {

	return Vector4::Set(lhs.X() / rhs.X(), lhs.Y() / rhs.Y(), lhs.Z() / rhs.Z(), lhs.W() / rhs.W());
}

constexpr auto operator+(const Vector4& lhs, float rhs) noexcept -> Vector4 {

	return lhs + Vector4::Replicate(rhs);
}

constexpr auto operator-(const Vector4& lhs, float rhs) noexcept -> Vector4 {

	return lhs - Vector4::Replicate(rhs);
}

constexpr auto operator*(const Vector4& lhs, float rhs) noexcept -> Vector4 {

	return lhs * Vector4::Replicate(rhs);
}

constexpr auto operator/(const Vector4& lhs, float rhs) noexcept -> Vector4 {

	return lhs / Vector4::Replicate(rhs);
}

constexpr auto operator>(const Vector4& lhs, const Vector4& rhs) noexcept -> bool {

	return lhs.X() > rhs.X() && lhs.Y() > rhs.Y() && lhs.Z() > rhs.Z() && lhs.W() > rhs.W();
}

constexpr auto operator>=(const Vector4& lhs, const Vector4& rhs) noexcept -> bool {

	return lhs.X() >= rhs.X() && lhs.Y() >= rhs.Y() && lhs.Z() >= rhs.Z() && lhs.W() >= rhs.W();
}

constexpr auto operator<(const Vector4& lhs, const Vector4& rhs) noexcept -> bool {

	return lhs.X() < rhs.X() && lhs.Y() < rhs.Y() && lhs.Z() < rhs.Z() && lhs.W() < rhs.W();
}

constexpr auto operator<=(const Vector4& lhs, const Vector4& rhs) noexcept -> bool {

	return lhs.X() <= rhs.X() && lhs.Y() <= rhs.Y() && lhs.Z() <= rhs.Z() && lhs.W() <= rhs.W();
}

constexpr auto operator==(const Vector4& lhs, const Vector4& rhs) noexcept -> bool {

	return lhs.X() == rhs.X() && lhs.Y() == rhs.Y() && lhs.Z() == rhs.Z() && lhs.W() == rhs.W();
}

constexpr auto operator!=(const Vector4& lhs, const Vector4& rhs) noexcept -> bool {

	return lhs.X() != rhs.X() || lhs.Y() != rhs.Y() || lhs.Z() != rhs.Z() || lhs.W() != rhs.W();
}

constexpr Vector4 X_AXIS_4D = X_AXIS_3D;
constexpr Vector4 Y_AXIS_4D = Y_AXIS_3D;
constexpr Vector4 Z_AXIS_4D = Z_AXIS_3D;
constexpr Vector4 W_AXIS_4D = Vector4::Set(0, 0, 0, 1);
constexpr Vector4 X_AXIS_INV_4D = -X_AXIS_4D;
constexpr Vector4 Y_AXIS_INV_4D = -Y_AXIS_4D;
constexpr Vector4 Z_AXIS_INV_4D = -Z_AXIS_4D;
constexpr Vector4 W_AXIS_INV_4D = -W_AXIS_4D;

constexpr Vector4 ZERO_VECTOR_4D = ZERO_VECTOR_3D;

}
