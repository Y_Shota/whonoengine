#include "Angle.hpp"

#include <DirectXMath.h>

using namespace DirectX;


namespace Whono::Math {

auto operator<<(std::ostream& os, const Angle& rhs) noexcept -> std::ostream& {

	os << "r: " << rhs.GetRadians() << " d:" << rhs.GetDegrees();
	return os;
}

}
