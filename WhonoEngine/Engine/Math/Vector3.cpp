#include "Vector3.hpp"

#include "Quaternion.hpp"


namespace Whono::Math {


auto Vector3::X(float x) noexcept -> Vector3& {

	v[0] = x;
	return *this;
}

auto Vector3::Y(float y) noexcept -> Vector3& {

	v[1] = y;
	return *this;
}

auto Vector3::Z(float z) noexcept -> Vector3& {

	v[2] = z;
	return *this;
}

auto Vector3::Normalize() noexcept -> Vector3& {

	*this /= Length();
	return *this;
}

auto Vector3::Set(const float* data) -> Vector3 {

	return Set(data[0], data[1], data[2]);
}

auto Vector3::operator[](std::size_t index) -> float& {

	if (index >= DIMENSION)	throw std::out_of_range("");

	return v[index];
}

auto Vector3::operator+=(const Vector3& rhs) noexcept -> Vector3& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] += rhs.v[i];
	}
	return *this;
}

auto Vector3::operator-=(const Vector3& rhs) noexcept -> Vector3& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] -= rhs.v[i];
	}
	return *this;
}

auto Vector3::operator*=(const Vector3& rhs) noexcept -> Vector3& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] *= rhs.v[i];
	}
	return *this;
}

auto Vector3::operator/=(const Vector3& rhs) noexcept -> Vector3& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] /= rhs.v[i];
	}
	return *this;
}

auto Vector3::operator+=(float rhs) noexcept -> Vector3& {

	return *this += Replicate(rhs);
}

auto Vector3::operator-=(float rhs) noexcept -> Vector3& {

	return *this -= Replicate(rhs);
}

auto Vector3::operator*=(float rhs) noexcept -> Vector3& {

	return *this *= Replicate(rhs);
}

auto Vector3::operator/=(float rhs) noexcept -> Vector3& {

	return *this /= Replicate(rhs);
}

auto Vector3::operator*=(const Quaternion& rhs) noexcept -> Vector3& {

	*this = rhs * *this;
	return *this;
}

auto operator<<(std::ostream& os, const Vector3& rhs) noexcept -> std::ostream& {

	os << '(' << rhs.X() << ", " << rhs.Y() << ", " << rhs.Z() << ')';
	return os;
}

	
}

