#pragma once
#include <DirectXMath.h>
#include <ostream>

namespace Whono::Math {

class Angle final {

private:
	enum class TYPE : unsigned char;

	constexpr Angle();
	constexpr Angle(float angle, TYPE type);

public:
	// ReSharper disable once CppNonExplicitConvertingConstructor
	constexpr Angle(float rhs);
	constexpr auto operator=(float rhs) -> Angle&;
	
	constexpr Angle(const Angle& rhs) = default;
	constexpr auto operator=(const Angle& rhs) -> Angle& = default;
	constexpr Angle(Angle&& rhs) noexcept = default;
	constexpr auto operator=(Angle&& rhs) noexcept -> Angle& = default;

	constexpr auto GetRadians() const noexcept -> float;
	constexpr auto GetDegrees() const noexcept -> float;
	
	static constexpr auto Radians(float angle) noexcept -> Angle;
	static constexpr auto Degrees(float angle) noexcept -> Angle;
	
	static constexpr auto ToDegrees(float radians) noexcept -> float;
	static constexpr auto ToRadians(float degrees) noexcept -> float;

	// ReSharper disable once CppNonExplicitConversionOperator
	constexpr operator float() const noexcept;
	
	explicit constexpr operator bool() const noexcept;
	constexpr auto operator!() const noexcept -> bool;

	constexpr auto operator+() const noexcept -> Angle;
	constexpr auto operator-() const noexcept -> Angle;

	constexpr auto operator+=(const Angle& rhs) noexcept -> Angle&;
	constexpr auto operator-=(const Angle& rhs) noexcept -> Angle&;
	constexpr auto operator*=(float rhs) noexcept->Angle&;
	constexpr auto operator/=(float rhs) noexcept->Angle&;

private:
	float angle_;

	enum class TYPE : unsigned char {
		RADIANS,
		DEGREES
	};
};

constexpr auto operator+(const Angle& lhs, const Angle& rhs) noexcept -> Angle;
constexpr auto operator-(const Angle& lhs, const Angle& rhs) noexcept -> Angle;
constexpr auto operator*(const Angle& lhs, float rhs) noexcept -> Angle;
constexpr auto operator/(const Angle& lhs, float rhs) noexcept -> Angle;

constexpr auto operator<(const Angle& lhs, const Angle& rhs) noexcept -> bool;
constexpr auto operator>(const Angle& lhs, const Angle& rhs) noexcept -> bool;
constexpr auto operator<=(const Angle& lhs, const Angle& rhs) noexcept -> bool;
constexpr auto operator>=(const Angle& lhs, const Angle& rhs) noexcept -> bool;

constexpr auto operator==(const Angle& lhs, const Angle& rhs) noexcept -> bool;
constexpr auto operator!=(const Angle& lhs, const Angle& rhs) noexcept -> bool;

auto operator<<(std::ostream& os, const Angle& rhs) noexcept -> std::ostream&;


constexpr Angle::Angle()
	: angle_(0) {}

constexpr Angle::Angle(float angle, TYPE type)
	: angle_(
		type == TYPE::RADIANS ? angle :
		type == TYPE::DEGREES ? ToRadians(angle) :
		0) {}

constexpr Angle::Angle(float rhs)
	: angle_(rhs) {}

constexpr auto Angle::operator=(float rhs) -> Angle& {

	angle_ = rhs;
	return *this;
}

constexpr auto Angle::GetRadians() const noexcept -> float {

	return angle_;
}

constexpr auto Angle::GetDegrees() const noexcept -> float {

	return ToDegrees(angle_);
}

constexpr auto Angle::Radians(float angle) noexcept -> Angle {

	return Angle(angle, TYPE::RADIANS);
}

constexpr auto Angle::Degrees(float angle) noexcept -> Angle {

	return Angle(angle, TYPE::DEGREES);
}

constexpr auto Angle::ToDegrees(float radians) noexcept -> float {

	return DirectX::XMConvertToDegrees(radians);
}

constexpr auto Angle::ToRadians(float degrees) noexcept -> float {

	return DirectX::XMConvertToRadians(degrees);
}

constexpr Angle::operator float() const noexcept {

	return angle_;
}

constexpr Angle::operator bool() const noexcept {

	return angle_ != 0.f;
}

constexpr auto Angle::operator!() const noexcept -> bool {

	return angle_ == 0.f;
}

constexpr auto Angle::operator+() const noexcept -> Angle {

	return *this;
}

constexpr auto Angle::operator-() const noexcept -> Angle {

	return Radians(-angle_);
}

constexpr auto Angle::operator+=(const Angle& rhs) noexcept -> Angle& {

	angle_ += rhs.angle_;
	return *this;
}

constexpr auto Angle::operator-=(const Angle& rhs) noexcept -> Angle& {

	angle_ -= rhs.angle_;
	return *this;
}

constexpr auto Angle::operator*=(float rhs) noexcept -> Angle& {

	angle_ *= rhs;
	return *this;
}

constexpr auto Angle::operator/=(float rhs) noexcept -> Angle& {

	angle_ /= rhs;
	return *this;
}

constexpr auto operator+(const Angle& lhs, const Angle& rhs) noexcept -> Angle {

	return Angle::Radians(lhs.GetRadians() + rhs.GetRadians());
}

constexpr auto operator-(const Angle& lhs, const Angle& rhs) noexcept -> Angle {

	return Angle::Radians(lhs.GetRadians() - rhs.GetRadians());
}

constexpr auto operator*(const Angle& lhs, float rhs) noexcept -> Angle {

	return Angle::Radians(lhs.GetRadians() * static_cast<float>(rhs));
}

constexpr auto operator/(const Angle& lhs, float rhs) noexcept -> Angle {

	return Angle::Radians(lhs.GetRadians() / static_cast<float>(rhs));
}

constexpr auto operator<(const Angle& lhs, const Angle& rhs) noexcept -> bool {

	return lhs.GetRadians() < rhs.GetRadians();
}

constexpr auto operator>(const Angle& lhs, const Angle& rhs) noexcept -> bool {

	return lhs.GetRadians() > rhs.GetRadians();
}

constexpr auto operator<=(const Angle& lhs, const Angle& rhs) noexcept -> bool {

	return lhs.GetRadians() <= rhs.GetRadians();
}

constexpr auto operator>=(const Angle& lhs, const Angle& rhs) noexcept -> bool {

	return lhs.GetRadians() >= rhs.GetRadians();
}

constexpr auto operator==(const Angle& lhs, const Angle& rhs) noexcept -> bool {

	return lhs.GetRadians() == rhs.GetRadians();
}

constexpr auto operator!=(const Angle& lhs, const Angle& rhs) noexcept -> bool {

	return lhs.GetRadians() != rhs.GetRadians();
}

constexpr auto PI = Angle::Degrees(180.f);
constexpr auto PI_DIV2 = Angle::Degrees(90.f);
constexpr auto PI_DIV4 = Angle::Degrees(45.f);

}
