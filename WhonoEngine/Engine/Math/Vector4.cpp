#include "Vector4.hpp"

namespace Whono::Math {

auto Vector4::X(float x) noexcept -> Vector4& {

	v[0] = x;
	return *this;
}

auto Vector4::Y(float y) noexcept -> Vector4& {

	v[1] = y;
	return *this;
}

auto Vector4::Z(float z) noexcept -> Vector4& {

	v[2] = z;
	return *this;
}

auto Vector4::W(float w) noexcept -> Vector4& {

	v[3] = w;
	return *this;
}

auto Vector4::Normalize() noexcept -> Vector4& {

	*this /= Length();
	return *this;
}

auto Vector4::operator[](std::size_t index) -> float& {
	
	if (index >= DIMENSION)	throw std::out_of_range("");

	return v[index];
}

auto Vector4::operator+=(const Vector4& rhs) noexcept -> Vector4& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] += rhs.v[i];
	}
	return *this;
}

auto Vector4::operator-=(const Vector4& rhs) noexcept -> Vector4& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] -= rhs.v[i];
	}
	return *this;
}

auto Vector4::operator*=(const Vector4& rhs) noexcept -> Vector4& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] *= rhs.v[i];
	}
	return *this;
}

auto Vector4::operator/=(const Vector4& rhs) noexcept -> Vector4& {

	for (auto i = 0; i < DIMENSION; ++i) {

		v[i] /= rhs.v[i];
	}
	return *this;
}

auto Vector4::operator+=(float rhs) noexcept -> Vector4& {

	return *this += Replicate(rhs);
}

auto Vector4::operator-=(float rhs) noexcept -> Vector4& {

	return *this -= Replicate(rhs);
}

auto Vector4::operator*=(float rhs) noexcept -> Vector4& {

	return *this *= Replicate(rhs);
}

auto Vector4::operator/=(float rhs) noexcept -> Vector4& {

	return *this /= Replicate(rhs);
}

auto operator<<(std::ostream& os, const Vector4& rhs) noexcept -> std::ostream& {

	os << '(' << rhs.X() << ", " << rhs.Y() << ", " << rhs.Z() << ", " << rhs.W() << ')';
	return os;
}

}
