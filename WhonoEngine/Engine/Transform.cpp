#include "Transform.hpp"

#include <DirectXMath.h>
#include <list>

#include "Math/Math.hpp"

using namespace std;


namespace Whono::GameOperation {

/**
* \brief 各種変形情報保持用
*/
struct Elem {

	XMVECTOR position = XMVectorZero();				//座標
	XMVECTOR scale = XMVectorReplicate(1.f);	//拡大率
	XMVECTOR oriented = XMQuaternionIdentity();		//姿勢
};


struct Transform::Impl {

	explicit Impl(Transform* myself) : myself(myself) {}

	auto SetParent(Transform* parent, bool keepWorldTransform) -> bool;
	
	/**
	* \brief 子供を追加する
	* \param child 追加するオブジェクト
	* \return 追加出来たら真
	*/
	auto AddChild(Transform* child) -> bool;

	/**
	* \brief 子供を削除する
	* \param child 削除したいオブジェクト
	*/
	void RemoveChild(Transform* child);


	/**
	* \brief ローカルからグローバルの変形情報を算出する
	*/
	void NotifyGlobalUpdate();

	/**
	* \brief グローバルからローカルの変形情報を算出する
	*/
	void NotifyLocalUpdate();

	Transform*				parent = nullptr;	//親オブジェクト
	Transform* const		myself;				//自分自身 
	std::list<Transform*>	childList;			//子オブジェクトリスト

	Elem local;						//ローカルの変形情報
	Elem global;						//グローバルの変形情報
};


Transform::Transform() {

	pImpl_ = make_unique<Impl>(this);
}

Transform::~Transform() {

	Release();
}

Transform::Transform(const Transform& other) {

	*this = other;
}

Transform& Transform::operator=(const Transform& other) {

	pImpl_ = make_unique<Impl>(this);
	SetParent(other.pImpl_->parent);
	pImpl_->global = other.pImpl_->global;
	pImpl_->local = other.pImpl_->local;
	
	return *this;
}

auto Transform::Copy() const -> unique_ptr<Transform> {

	return make_unique<Transform>(*this);
}

auto Transform::SetParent(Transform* parent, bool keepWorldTransform) const -> bool {

	return pImpl_->SetParent(parent, keepWorldTransform);
}

auto Transform::GetWorldPosition() const noexcept -> CXMVECTOR {

	return pImpl_->global.position;
}

auto Transform::GetLocalPosition() const noexcept -> CXMVECTOR {

	return pImpl_->local.position;
}

auto Transform::GetWorldOriented() const noexcept -> CXMVECTOR {

	return pImpl_->global.oriented;
}

auto Transform::GetLocalOriented() const noexcept -> CXMVECTOR {

	return pImpl_->local.oriented;
}

auto Transform::GetWorldScale() const noexcept -> CXMVECTOR {

	return pImpl_->global.scale;
}

auto Transform::GetLocalScale() const noexcept -> CXMVECTOR {

	return pImpl_->local.scale;
}

auto Transform::GetWorldMatrix() const noexcept -> XMMATRIX {

	auto worldMatrix =
		XMMatrixScalingFromVector(pImpl_->global.scale) *
		XMMatrixRotationQuaternion(pImpl_->global.oriented) *
		XMMatrixTranslationFromVector(pImpl_->global.position);

	return worldMatrix;
}
auto Transform::GetLocalMatrix() const noexcept -> XMMATRIX {

	auto localMatrix =
		XMMatrixScalingFromVector(pImpl_->local.scale) *
		XMMatrixRotationQuaternion(pImpl_->local.oriented) *
		XMMatrixTranslationFromVector(pImpl_->local.position);

	return localMatrix;
}

auto Transform::GetWorldTranslationMatrix() const noexcept -> XMMATRIX {

	return XMMatrixTranslationFromVector(pImpl_->global.position);
}

auto Transform::GetLocalTranslationMatrix() const noexcept -> XMMATRIX {

	return XMMatrixTranslationFromVector(pImpl_->local.position);
}

auto Transform::GetWorldScalingMatrix() const noexcept -> XMMATRIX {

	return XMMatrixScalingFromVector(pImpl_->global.scale);
}

auto Transform::GetLocalScalingMatrix() const noexcept -> XMMATRIX {
	
	return XMMatrixScalingFromVector(pImpl_->local.scale);
}

auto Transform::GetWorldRotationMatrix() const noexcept -> XMMATRIX {

	return XMMatrixRotationQuaternion(pImpl_->global.oriented);
}

auto Transform::GetLocalRotationMatrix() const noexcept -> XMMATRIX {
	
	return XMMatrixRotationQuaternion(pImpl_->local.oriented);
}

auto Transform::GetWorldPitch() const -> float {

	return Math::GetPitchFromQuaternion(pImpl_->global.oriented);
}

auto Transform::GetLocalForward() const -> XMVECTOR {

	return XMVector3Rotate(Math::DEFAULT_FORWARD, pImpl_->local.oriented);
}

auto Transform::GetWorldForward() const -> XMVECTOR {

	return XMVector3Rotate(Math::DEFAULT_FORWARD, pImpl_->global.oriented);
}

auto Transform::SetWorldPosition(CXMVECTOR position) -> Transform* {

	pImpl_->global.position = position;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::SetWorldPosition(float x, float y, float z) -> Transform* {

	return SetWorldPosition(XMVectorSet(x, y, z, 0));
}

auto Transform::SetWorldPositionX(float x) -> Transform* {

	pImpl_->global.position.m128_f32[0] = x;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();
	
	return this;
}

auto Transform::SetWorldPositionY(float y) -> Transform* {

	pImpl_->global.position.m128_f32[1] = y;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::SetWorldPositionZ(float z) -> Transform* {

	pImpl_->global.position.m128_f32[2] = z;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::SetLocalPosition(CXMVECTOR position) -> Transform* {

	pImpl_->local.position = position;

	//ロカールが変更されたので通知
	pImpl_->NotifyGlobalUpdate();
	
	return this;
}

auto Transform::SetLocalPosition(float x, float y, float z) -> Transform* {

	return SetLocalPosition(XMVectorSet(x, y, z, 0));
}

auto Transform::SetLocalPositionX(float x) -> Transform* {

	pImpl_->local.position.m128_f32[0] = x;

	//ロカールが変更されたので通知
	pImpl_->NotifyGlobalUpdate();

	return this;
}

auto Transform::SetLocalPositionY(float y) -> Transform* {

	pImpl_->local.position.m128_f32[1] = y;

	//ロカールが変更されたので通知
	pImpl_->NotifyGlobalUpdate();

	return this;
}

auto Transform::SetLocalPositionZ(float z) -> Transform* {

	pImpl_->local.position.m128_f32[2] = z;

	//ロカールが変更されたので通知
	pImpl_->NotifyGlobalUpdate();

	return this;
}

auto Transform::AdjustPosition(CXMVECTOR movement) -> Transform* {

	if (XMVector3Equal(movement, g_XMZero))	return this;
	
	pImpl_->global.position += movement;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();
	
	return this;
}

auto Transform::AdjustPosition(float x, float y, float z ) -> Transform* {

	return AdjustPosition(XMVectorSet(x, y, z, 0));
}

auto Transform::AdjustPositionX(float x) -> Transform* {

	pImpl_->global.position.m128_f32[0] += x;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::AdjustPositionY(float y) -> Transform* {

	if (y == 0.f)	return this;
	
	pImpl_->global.position.m128_f32[1] += y;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::AdjustPositionZ(float z) -> Transform* {

	pImpl_->global.position.m128_f32[2] += z;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::SetWorldScale(CXMVECTOR scale) -> Transform* {

	pImpl_->global.scale = scale;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::SetWorldScale(float x, float y, float z) -> Transform* {

	return SetWorldScale(XMVectorSet(x, y, z, 0));
}

auto Transform::SetWorldScaleX(float x) -> Transform* {

	pImpl_->global.scale.m128_f32[0] = x;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}
auto Transform::SetWorldScaleY(float y) -> Transform* {

	pImpl_->global.scale.m128_f32[1] = y;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::SetWorldScaleZ(float z) -> Transform* {

	pImpl_->global.scale.m128_f32[2] = z;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::SetLocalScale(CXMVECTOR scale) -> Transform* {

	pImpl_->local.scale = scale;

	//ローカルが変更されたので通知
	pImpl_->NotifyGlobalUpdate();

	return this;
}

auto Transform::SetLocalScale(float x, float y, float z) -> Transform* {

	return SetLocalScale(XMVectorSet(x, y, z, 0));
}

auto Transform::SetLocalScaleX(float x) -> Transform* {

	pImpl_->local.scale.m128_f32[0] = x;

	//ローカルが変更されたので通知
	pImpl_->NotifyGlobalUpdate();

	return this;
}

auto Transform::SetLocalScaleY(float y) -> Transform* {

	pImpl_->local.scale.m128_f32[1] = y;

	//ローカルが変更されたので通知
	pImpl_->NotifyGlobalUpdate();

	return this;
}

auto Transform::SetLocalScaleZ(float z) -> Transform* {

	pImpl_->local.scale.m128_f32[2] = z;

	//ローカルが変更されたので通知
	pImpl_->NotifyGlobalUpdate();

	return this;
}

auto Transform::AdjustScaleAdd(CXMVECTOR scaling) -> Transform* {

	pImpl_->global.scale += scaling;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::AdjustScaleAdd(float x, float y, float z) -> Transform* {

	return AdjustScaleAdd(XMVectorSet(x, y, z, 0));
}

auto Transform::AdjustScaleXAdd(float x) -> Transform* {

	pImpl_->global.scale.m128_f32[0] += x;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::AdjustScaleYAdd(float y) -> Transform* {

	pImpl_->global.scale.m128_f32[1] += y;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::AdjustScaleZAdd(float z) -> Transform* {

	pImpl_->global.scale.m128_f32[2] += z;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::AdjustScaleAllAdd(float scaling) -> Transform* {

	pImpl_->global.scale += XMVectorReplicate(scaling);

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::AdjustScaleMultiply(CXMVECTOR scaling) -> Transform* {

	pImpl_->global.scale *= scaling;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::AdjustScaleMultiply(float x, float y, float z) -> Transform* {

	return AdjustScaleMultiply(XMVectorSet(x, y, z, 0));
}

auto Transform::AdjustScaleXMultiply(float x) -> Transform* {

	pImpl_->global.scale.m128_f32[0] *= x;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this; 
}

auto Transform::AdjustScaleYMultiply(float y) -> Transform* {

	pImpl_->global.scale.m128_f32[1] *= y;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this; 
}

auto Transform::AdjustScaleZMultiply(float z) -> Transform* {

	pImpl_->global.scale.m128_f32[2] *= z;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this; 
}

auto Transform::AdjustScaleAllMultiply(float scaling) -> Transform* {

	pImpl_->global.scale *= scaling;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this; 
}

auto Transform::SetWorldOriented(CXMVECTOR oriented) -> Transform* {

	pImpl_->global.oriented = oriented;

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::SetWorldOrientedAxisRadians(CXMVECTOR axis, float angleRadians) -> Transform* {

	return SetWorldOriented(XMQuaternionRotationAxis(axis, angleRadians));
}

auto Transform::SetWorldOrientedAxis(CXMVECTOR axis, float angleDegree) -> Transform* {

	return SetWorldOrientedAxisRadians(axis, XMConvertToRadians(angleDegree));
}

auto Transform::SetWorldRotationRadians(CXMVECTOR rotation) -> Transform* {

	return SetWorldOriented(XMQuaternionRotationRollPitchYawFromVector(rotation));
}

auto Transform::SetWorldRotationRadians(float x, float y, float z) -> Transform* {

	return SetWorldRotationRadians(XMVectorSet(x, y, z, 0));
}

auto Transform::SetWorldRotation(CXMVECTOR rotation) -> Transform* {

	return SetWorldRotationRadians(Math::ConvertToRadiansVector(rotation));
}

auto Transform::SetWorldRotation(float x, float y, float z) -> Transform* {

	return SetWorldRotation(XMVectorSet(x, y, z, 0));
}

auto Transform::SetLocalOriented(CXMVECTOR oriented) -> Transform* {

	pImpl_->local.oriented = oriented;

	//グローバルが変更されたので通知
	pImpl_->NotifyGlobalUpdate();

	return this;
}

auto Transform::SetLocalOrientedAxisRadians(CXMVECTOR axis, float angleRadians) -> Transform* {

	return SetLocalOriented(XMQuaternionRotationAxis(axis, angleRadians));
}

auto Transform::SetLocalOrientedAxis(CXMVECTOR axis, float angleDegree) -> Transform* {

	return SetLocalOrientedAxisRadians(axis, XMConvertToRadians(angleDegree));
}

auto Transform::SetLocalRotationRadians(CXMVECTOR rotation) -> Transform* {

	return SetLocalOriented(XMQuaternionRotationRollPitchYawFromVector(rotation));
}

auto Transform::SetLocalRotationRadians(float x, float y, float z) -> Transform* {

	return SetLocalRotationRadians(XMVectorSet(x, y, z, 0));
}

auto Transform::SetLocalRotation(CXMVECTOR rotation) -> Transform* {

	return SetLocalRotationRadians(Math::ConvertToRadiansVector(rotation));
}

auto Transform::SetLocalRotation(float x, float y, float z) -> Transform* {

	return SetLocalRotation(XMVectorSet(x, y, z, 0));
}

auto Transform::AdjustOrientedMultiply(CXMVECTOR oriented) -> Transform* {

	pImpl_->global.oriented = XMQuaternionMultiply(pImpl_->global.oriented, oriented);

	//グローバルが変更されたので通知
	pImpl_->NotifyLocalUpdate();

	return this;
}

auto Transform::AdjustOrientedMultiplyAxisRadians(CXMVECTOR axis, float angleRadians) -> Transform* {

	return AdjustOrientedMultiply(XMQuaternionRotationAxis(axis, angleRadians));
}

auto Transform::AdjustOrientedMultiplyAxis(CXMVECTOR axis, float angleDegree) -> Transform* {

	return AdjustOrientedMultiplyAxisRadians(axis, XMConvertToRadians(angleDegree));
}

auto Transform::AdjustRotationRadians(CXMVECTOR rotation) -> Transform* {

	auto axisX = XMVector3Rotate(Math::DEFAULT_RIGHT, pImpl_->global.oriented);
	auto axisZ = XMVector3Rotate(Math::DEFAULT_FORWARD, pImpl_->global.oriented);

	auto quaternion = XMQuaternionRotationAxis(axisX, rotation.m128_f32[0]);
	quaternion = XMQuaternionMultiply(quaternion, XMQuaternionRotationAxis(axisZ, rotation.m128_f32[2]));
	quaternion = XMQuaternionMultiply(quaternion, XMQuaternionRotationAxis(Math::DEFAULT_UP, rotation.m128_f32[1]));

	return AdjustOrientedMultiply(quaternion);
}

auto Transform::AdjustRotationRadians(float x, float y, float z) -> Transform* {

	return AdjustRotationRadians(XMVectorSet(x, y, z, 0));
}

auto Transform::AdjustRotationXRadians(float x) -> Transform* {

	return AdjustRotationRadians(x, 0, 0);
}

auto Transform::AdjustRotationYRadians(float y) -> Transform* {

	return AdjustRotationRadians(0, y, 0);
}

auto Transform::AdjustRotationZRadians(float z) -> Transform* {

	return AdjustRotationRadians(0, 0, z);
}

auto Transform::AdjustRotation(CXMVECTOR rotation) -> Transform* {
	
	return AdjustRotationRadians(Math::ConvertToRadiansVector(rotation));
}

auto Transform::AdjustRotation(float x, float y, float z) -> Transform* {

	return AdjustRotation(XMVectorSet(x, y, z, 0));
}

auto Transform::AdjustRotationX(float x) -> Transform* {

	return AdjustRotation(x, 0, 0);
}

auto Transform::AdjustRotationY(float y) -> Transform* {

	return AdjustRotation(0, y, 0);
}

auto Transform::AdjustRotationZ(float z) -> Transform* {

	return AdjustRotation(0, 0, z);
}

void Transform::Release() {

	if (pImpl_->parent)	pImpl_->parent->pImpl_->RemoveChild(this);

	for (auto* child : pImpl_->childList) {

		if (child)	child->SetParent(nullptr);
	}
}

auto Transform::Impl::SetParent(Transform* parent, bool keepWorldTransform) -> bool {

	if (parent) {
		
		if (!parent->pImpl_->AddChild(myself))	return false;
	}
	else {

		this->parent = parent;
	}
	if (keepWorldTransform) {
		
		NotifyLocalUpdate();
	}
	else {

		NotifyGlobalUpdate();
	}
		
	return true;
}

auto Transform::Impl::AddChild(Transform* child) -> bool {

	//nullなら失敗
	if (!child)	return false;

	//追加したいオブジェクトが先祖にいないかチェック
	auto* pCheck = myself;
	while (!pCheck) {

		if (pCheck == child)	return false;

		pCheck = pCheck->pImpl_->parent;
	}

	//既に親がいた場合、子のリストから外させる
	if (auto* pPrevParent = child->pImpl_->parent) {

		pPrevParent->pImpl_->RemoveChild(child);
	}

	//親子関係を築く
	child->pImpl_->parent = myself;
	childList.emplace_back(child);

	return true;
}

void Transform::Impl::RemoveChild(Transform* child) {

	if (!child)				return;
	if (childList.empty())	return;

	childList.remove(child);
}

void Transform::Impl::NotifyGlobalUpdate() {
	
	//親が存在するなら
	if (parent) {

		//座標
		global.position = XMVector3Transform(local.position, parent->GetWorldMatrix());
		
		//回転
		global.oriented = XMQuaternionMultiply(local.oriented, parent->GetWorldOriented());

		//拡大率
		global.scale = local.scale * parent->GetWorldScale();
	}
	else {

		//親が存在しないならローカルがグローバルと一致
		global = local;
	}
	
	for (auto* child : childList) {

		if (child) child->pImpl_->NotifyGlobalUpdate();
	}
}

void Transform::Impl::NotifyLocalUpdate() {
	
	//親が存在するなら
	if (parent) {

		//座標
		auto invParentWorldMatrix = XMMatrixInverse(nullptr, parent->GetWorldMatrix());
		local.position = XMVector3Transform(global.position, invParentWorldMatrix);
		
		//回転
		local.oriented = XMQuaternionMultiply(global.oriented, XMQuaternionInverse(parent->GetWorldOriented()));

		//拡大率
		local.scale = global.scale / parent->GetLocalScale();
	}
	else {

		//親が存在しないならグローバルがローカルと一致
		local = global;
	}
	
	for (auto* child : childList) {

		if (child) child->pImpl_->NotifyGlobalUpdate();
	}
}

}
