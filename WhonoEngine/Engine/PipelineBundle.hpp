#pragma once

#include <memory>
#include <filesystem>
#include <d3d11_1.h>


namespace Whono::GraphicsPipeline {

/**
 * \brief 合成方法指定用
 */
enum class BLEND_STATE : unsigned char {
	NONE, ADD, ALIGNMENT, SUB, MUL, DEPTH_TEST
};
	
/**
 * \brief グラフィックスパイプラインをまとめる
 * \author whono
 * \version 1.0
 * \since 1.0
 */
class PipelineBundle final {

public:
	/**
	 * \brief コンストラクタ
	 * \param srcFile 読み込むシェーダファイル
	 */
	explicit PipelineBundle(const std::filesystem::path& srcFile);
	/**
	 * \brief デストラクタ
	 */
	~PipelineBundle();

	/**
	 * \brief デバイスコンテキストへパイプラインをセットする
	 */
	void SetPipeline() const;
	
	/**
	 * \brief 頂点シェーダを作成する
	 * \param inputLayouts GPUへの入力レイアウトの先頭アドレス
	 * \param size GPUへの入力レイアウトの要素数
	 */
	void CreateVertexShader(D3D11_INPUT_ELEMENT_DESC* inputLayouts, size_t size) const;
	/**
	 * \brief ハルシェーダを作成する
	 */
	void CreateHullShader() const;
	/**
	 * \brief ドメインシェーダを作成する
	 */
	void CreateDomainShader() const;
	/**
	 * \brief ジオメトリシェーダを作成する
	 */
	void CreateGeometryShader() const;
	/**
	 * \brief ラスタライザを作成する
	 * \param cullMode カリングの仕方の指定
	 * \param fillMode = D3D11_FILL_SOLID フィルを行うかの指定
	 */
	void CreateRasterizerState(D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode = D3D11_FILL_SOLID) const;
	/**
	 * \brief 頂点シェーダを作成する
	 */
	void CreatePixelShader() const;
	/**
	 * \brief 深度ステンシルステートを作成する
	 * \param depthEnable 深度バッファへの書き込み許可
	 * \param depthWriteEnabled 深度バッファへの書き込み方
	 * \param depthFunc = D3D11_COMPARISON_LESS 既存の深度との比較の仕方を指定
	 */
	void CreateDepthStencilState(bool depthEnable, D3D11_DEPTH_WRITE_MASK depthWriteEnabled, D3D11_COMPARISON_FUNC depthFunc = D3D11_COMPARISON_LESS) const;
	/**
	 * \brief ブレンドステートを作成する
	 * \param blendStateType 合成方法の指定
	 * \param alphaToConvergeEnable = false アルファカバレッジを許可
	 */
	void CreateBlendState(BLEND_STATE blendStateType, bool alphaToConvergeEnable = false) const;

	/**
	 * \brief 解放処理
	 */
	void Release() const;

private:
	struct Impl;
	std::unique_ptr<Impl> pImpl_;	//内部実装隠蔽
};
	
}

