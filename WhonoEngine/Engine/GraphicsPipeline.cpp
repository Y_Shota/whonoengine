#include "GraphicsPipeline.hpp"

#include <d3d11.h>
#include <DirectXColors.h>
#include <dxgiformat.h>
#include <filesystem>
#include <memory>
#include <ranges>
#include <unordered_map>
#include <vector>

#include "PipelineBundle.hpp"

using namespace std;
namespace fs = filesystem;

namespace Whono::GraphicsPipeline {

using PipelinePtr = unique_ptr<PipelineBundle>;

//パイプラインマップ
unordered_map<PIPELINE_TYPE, PipelinePtr> pipelineMap;

void Initialize() {

	auto currentPath = fs::current_path();
	fs::current_path("Engine/Shader");

	UINT vectorSize = sizeof(DirectX::XMVECTOR);

	//SIMPLE_3D
	{
		auto simple3D = make_unique<PipelineBundle>("Simple3D.hlsl");

		vector<D3D11_INPUT_ELEMENT_DESC> inputLayoutDesc = {
			{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 0,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL",		0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 1,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TANGENT",		0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 2,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BINORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 3,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0, vectorSize * 4,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BLENDWEIGHT",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, vectorSize * 5,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BLENDINDICES",0, DXGI_FORMAT_R32G32B32A32_UINT,	0, vectorSize * 6,	D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		simple3D->CreateVertexShader(inputLayoutDesc.data(), inputLayoutDesc.size());
		simple3D->CreateRasterizerState(D3D11_CULL_BACK);
		simple3D->CreatePixelShader();
		simple3D->CreateDepthStencilState(true, D3D11_DEPTH_WRITE_MASK_ALL);
		simple3D->CreateBlendState(BLEND_STATE::ALIGNMENT);

		pipelineMap[PIPELINE_TYPE::SIMPLE_3D] = move(simple3D);
	}

	//DEBUG_3D
	{
		auto debug3D = make_unique<PipelineBundle>("Debug3D.hlsl");

		vector<D3D11_INPUT_ELEMENT_DESC> inputLayoutDesc = {
			{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 0,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL",		0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 1,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TANGENT",		0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 2,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BINORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 3,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0, vectorSize * 4,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BLENDWEIGHT",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, vectorSize * 5,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BLENDINDICES",0, DXGI_FORMAT_R32G32B32A32_UINT,	0, vectorSize * 6,	D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		debug3D->CreateVertexShader(inputLayoutDesc.data(), inputLayoutDesc.size());
		debug3D->CreateRasterizerState(D3D11_CULL_BACK, D3D11_FILL_WIREFRAME);
		debug3D->CreatePixelShader();
		debug3D->CreateDepthStencilState(true, D3D11_DEPTH_WRITE_MASK_ALL);
		debug3D->CreateBlendState(BLEND_STATE::ALIGNMENT);

		pipelineMap[PIPELINE_TYPE::DEBUG_3D] = move(debug3D);
	}

	//SIMPLE_2D
	{
		auto simple2D = make_unique<PipelineBundle>("Simple2D.hlsl");
		
		vector<D3D11_INPUT_ELEMENT_DESC> inputLayoutDesc = {
			{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, vectorSize * 0,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,	0, vectorSize * 1,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		simple2D->CreateVertexShader(inputLayoutDesc.data(), inputLayoutDesc.size());
		simple2D->CreateRasterizerState(D3D11_CULL_NONE);
		simple2D->CreatePixelShader();
		simple2D->CreateDepthStencilState(true, D3D11_DEPTH_WRITE_MASK_ALL);
		simple2D->CreateBlendState(BLEND_STATE::ALIGNMENT);
		
		pipelineMap[PIPELINE_TYPE::SIMPLE_2D] = move(simple2D);
	}

	{
		auto shadowMap = make_unique<PipelineBundle>("ShadowMap.hlsl");

		vector<D3D11_INPUT_ELEMENT_DESC> inputLayoutDesc = {
			{ "POSITION",	0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 0,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL",		0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 1,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TANGENT",		0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 2,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BINORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT,		0, vectorSize * 3,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",	0, DXGI_FORMAT_R32G32_FLOAT,		0, vectorSize * 4,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BLENDWEIGHT",	0, DXGI_FORMAT_R32G32B32A32_FLOAT,	0, vectorSize * 5,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BLENDINDICES",0, DXGI_FORMAT_R32G32B32A32_UINT,	0, vectorSize * 6,	D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};
		shadowMap->CreateVertexShader(inputLayoutDesc.data(), inputLayoutDesc.size());
		shadowMap->CreateRasterizerState(D3D11_CULL_BACK);
		shadowMap->CreateDepthStencilState(true, D3D11_DEPTH_WRITE_MASK_ALL);

		pipelineMap[PIPELINE_TYPE::SHADOW_MAP] = move(shadowMap);
	}
	
	current_path(currentPath);

	SetPipeline(PIPELINE_TYPE::SIMPLE_3D);
}

void Finalize() {

	for (auto& pipelineBundle : pipelineMap | views::values) {

		pipelineBundle->Release();
		pipelineBundle.reset();
	}
}

auto GetPipeline(PIPELINE_TYPE type) -> PipelineBundle* {

	return pipelineMap.at(type).get();
}

void SetPipeline(PIPELINE_TYPE type) {

	pipelineMap.at(type)->SetPipeline();
}

}