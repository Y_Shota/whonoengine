#include "SphereCollider.hpp"

#include "CollisionCalculator.hpp"
#include "GameObject.hpp"
#include "GraphicsPipeline.hpp"
#include "Model.hpp"

using namespace std;
using namespace DirectX;


namespace Whono::GameOperation {


struct SphereCollider::Impl {

	/**
	 * \brief バウンディングを設定する
	 * \param position 中心座標
	 * \param radius 半径
	 */
	void SetBounding(CXMVECTOR position, float radius) noexcept;
	/**
	 * \brief 境界線を描画する
	 */
	void Render() const;
	
	BoundingSphere bounding;	//計算用バウンディング
	XMVECTOR center;			//中心座標
	GameObject* gameObject;		//所有者
	int hModel;					//モデルハンドル
};

SphereCollider::SphereCollider(CXMVECTOR position, float radius) {

	pImpl_ = make_unique<Impl>();
	pImpl_->gameObject = nullptr;
	pImpl_->hModel = -1;

	pImpl_->SetBounding(position, radius);
	
	pImpl_->hModel = Model::Load("Debug/Collision/sphere.fbx");
	Model::SetPipeline(pImpl_->hModel, PIPELINE_TYPE::DEBUG_3D);
}

SphereCollider::~SphereCollider() = default;

auto SphereCollider::GetCenterPosition() const -> XMVECTOR {

	return Model::GetCenterPosition(pImpl_->hModel);
}

auto SphereCollider::GetNearestControlPoint(CXMVECTOR point) const -> XMVECTOR {

	return GetCenterPosition();
}

auto SphereCollider::GetProjectionLengthMax(CXMVECTOR vector) const -> float {

	return GetBounding().Radius;
}

void SphereCollider::SetGameObject(GameObject* gameObject) {

	pImpl_->gameObject = gameObject;

	Model::SetLinkedTransform(pImpl_->hModel, gameObject->GetTransform());
}

void SphereCollider::Render() const {

	pImpl_->Render();
}

auto SphereCollider::IsHit(ICollider* target) const -> bool {

	if (auto* sphere = dynamic_cast<SphereCollider*>(target)) {

		return CollisionCalculator::IsHit(this, sphere);
	}
	if (auto* box = dynamic_cast<BoxCollider*>(target)) {

		return CollisionCalculator::IsHit(this, box);
	}
	if (auto* obb = dynamic_cast<ObbCollider*>(target)) {

		return CollisionCalculator::IsHit(this, obb);
	}
	if (auto* frustum = dynamic_cast<FrustumCollider*>(target)) {

		return CollisionCalculator::IsHit(this, frustum);
	}
	if (auto* capsule = dynamic_cast<CapsuleCollider*>(target)) {

		return CollisionCalculator::IsHit(this, capsule);
	}
	return false;
}

auto SphereCollider::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool {

	return Model::RayCast(pImpl_->hModel, origin, direction, dist, normal);
}

auto SphereCollider::GetBounding() const noexcept -> BoundingSphere {

	BoundingSphere bounding;
	
	auto matrix = pImpl_->gameObject->GetTransform()->GetWorldMatrix();
	pImpl_->bounding.Transform(bounding, matrix);
	
	return bounding;
}

auto SphereCollider::Create(CXMVECTOR position, float radius) -> std::unique_ptr<SphereCollider> {

	return make_unique<SphereCollider>(position, radius);
}

auto SphereCollider::Create(float x, float y, float z, float radius) -> std::unique_ptr<SphereCollider> {

	return Create(XMVectorSet(x, y, z, 0), radius);
}

void SphereCollider::Impl::SetBounding(CXMVECTOR position, float radius) noexcept {

	center = position;
	XMStoreFloat3(&bounding.Center, position);
	bounding.Radius = radius;
}

void SphereCollider::Impl::Render() const {
	
	auto* modelTransform = Model::GetTransform(hModel);
	auto* objectTransform = gameObject->GetTransform();

	modelTransform
		->SetLocalPosition(center)
		->SetWorldScale(XMVectorReplicate(objectTransform->GetWorldScale().m128_f32[0] * bounding.Radius));

#ifdef _DEBUG
	
	Model::Render(hModel);

#endif
}

}
