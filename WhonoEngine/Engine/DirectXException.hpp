#pragma once
#include <exception>


namespace Whono::DirectX11 {

/**
* \brief DirectXに関わる例外
*/
class DirectXException final : public std::exception {

public:
	explicit DirectXException(const char* const msg) : std::exception(msg) {}
};

}