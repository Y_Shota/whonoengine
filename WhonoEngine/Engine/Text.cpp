#include "Text.hpp"

#include <vector>

#include "DirectX11.hpp"
#include "Image.hpp"
#include "SceneObject.hpp"
#include "Window.hpp"
#include "String.hpp"

using namespace std;


namespace Whono::Text {

struct FormatData {

	ComPtr<IDWriteTextFormat>		format;
	ComPtr<ID2D1SolidColorBrush>	brush;
	BASE_POSITION baseType = BASE_POSITION::TOP_LEFT;
	D2D1_POINT_2F position = { 0, 0 };
	D2D1_POINT_2F scale = { 1, 1 };
	float angle = 0;
};

vector<unique_ptr<FormatData>> data;

auto CalculationScreenPosition(BASE_POSITION baseType, const D2D1_POINT_2F& position, const D2D1_POINT_2F& scale)->D2D1_POINT_2F;
auto HasHandle(int handle) -> bool;

auto CreateFormat(const string& fontName, float fontSize, DWRITE_FONT_WEIGHT weight, DWRITE_FONT_STYLE style, DWRITE_FONT_STRETCH stretch) -> int {

	return CreateFormat(String::ConvertToWString(fontName), fontSize, weight, style, stretch);
}

auto CreateFormat(const wstring& fontName, float fontSize, DWRITE_FONT_WEIGHT weight, DWRITE_FONT_STYLE style, DWRITE_FONT_STRETCH stretch) -> int {

	auto formatData = make_unique<FormatData>();
	
	const auto& dwFactory = DirectX11::GetDwFactory();
	auto hr = dwFactory->CreateTextFormat(
		fontName.c_str(),
		nullptr,
		weight,
		style,
		stretch,
		fontSize,
		L"",
		&formatData->format);
	
	if (FAILED(hr))	return UNALLOCATED_HANDLE;

	const auto& d2dContext = DirectX11::GetD2DeviceContext();
	hr = d2dContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &formatData->brush);

	if (FAILED(hr))	return UNALLOCATED_HANDLE;

	data.emplace_back(move(formatData));

	return static_cast<int>(data.size()) - 1;
}

void Render(int handle, const std::string& text) {

	Render(handle, String::ConvertToWString(text));
}

void Render(int handle, const std::wstring& text) {

	if (!HasHandle(handle))	return;
	if (SceneObject::GetRenderingPhase() == SceneObject::RENDERING_PHASE::SHADOW)	return;

	const auto& d2dContext = DirectX11::GetD2DeviceContext();
	const auto& datum = data[handle];

	auto position = CalculationScreenPosition(datum->baseType, datum->position, datum->scale);
	auto translation = D2D1::Matrix3x2F::Translation(position.x, position.y);
	auto scale = D2D1::Matrix3x2F::Scale(datum->scale.x, datum->scale.y);
	auto rotation = D2D1::Matrix3x2F::Rotation(datum->angle);
	auto mat = rotation * scale * translation;
	d2dContext->SetTransform(mat);
	
	auto width = Window::GetWindowWidthF();
	auto height = Window::GetWindowHeightF();
	
	auto rect = D2D1::RectF(0, 0, width, height);

	d2dContext->BeginDraw();
	d2dContext->DrawTextA(
		text.c_str(), static_cast<UINT32>(text.size()),
		datum->format.Get(),
		rect,
		datum->brush.Get(),
		D2D1_DRAW_TEXT_OPTIONS_NONE);
	d2dContext->EndDraw();

	d2dContext->SetTransform(D2D1::Matrix3x2F::Identity());
}

void SetPosition(int handle, const Math::Vector2& position, BASE_POSITION baseType) {

	if (!HasHandle(handle))	return;

	data[handle]->position = D2D1::Point2F(position.X(), position.Y());
	data[handle]->baseType = baseType;
}

void SetBasePosition(int handle, BASE_POSITION baseType) {
	
	if (!HasHandle(handle))	return;
	
	data[handle]->baseType = baseType;
}

void SetAlignment(int handle, DWRITE_TEXT_ALIGNMENT alignment) {
	
	if (!HasHandle(handle))	return;

	data[handle]->format->SetTextAlignment(alignment);
}

void SetColor(int handle, const D2D1::ColorF& color) {

	if (!HasHandle(handle))	return;

	const auto& d2dContext = DirectX11::GetD2DeviceContext();
	d2dContext->CreateSolidColorBrush(color, &data[handle]->brush);
}

void SetColor(int handle, float r, float g, float b) {

	SetColor(handle, D2D1::ColorF(r, g, b));
}

void SerColor(int handle, const XMVECTORF32& color) {

	SetColor(handle, color[0], color[1], color[2]);
}

auto CalculationScreenPosition(BASE_POSITION baseType, const D2D1_POINT_2F& position, const D2D1_POINT_2F& scale) -> D2D1_POINT_2F {

	auto width = Window::GetWindowWidthF();
	auto height = Window::GetWindowHeightF();

	auto pos = position;

	switch (baseType) {
	using enum BASE_POSITION;
	case CENTER:
		pos.x += width * 0.5f;
		pos.y += height * 0.5f;
		break;
	case TOP_LEFT:
		break;
	case TOP_RIGHT:
		pos.x += width;
		break;
	case BOTTOM_LEFT:
		pos.y += height;
		break;
	case BOTTOM_RIGHT:
		pos.x += width;
		pos.y += height;
		break;
	case TOP_CENTER:
		pos.x += width * 0.5f;
		break;
	case BOTTOM_CENTER:
		pos.x += width * 0.5f;
		pos.y += height;
		break;
	case LEFT_CENTER:
		pos.y += height * 0.5f;
		break;
	case RIGHT_CENTER:
		pos.x += width;
		pos.y += height * 0.5f;
		break;
	default:
		break;
	}
	return pos;
}

auto HasHandle(int handle) -> bool {

	if (handle < 0)									return false;
	if (handle >= static_cast<int>(data.size()))	return false;
	if (!data[handle])								return false;

	return true;
}

}
