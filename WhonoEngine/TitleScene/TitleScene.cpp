#include "TitleScene.hpp"

#include "TitleObject.hpp"

TitleScene::TitleScene()
	: SceneObject(SCENE_NAME) {}

void TitleScene::Initialize() {

	//タイトル用オブジェクト生成
	Instantiate<TitleObject>(this);
}
