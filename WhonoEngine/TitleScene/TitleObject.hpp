#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief タイトル用オブジェクトクラス
 */
class TitleObject final : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "TitleObject";

private:
	int hTitleImage_;		//タイトル画像ハンドル
	int hEntryImage_;		//エントリ画像ハンドル
	int hBackgroundImage_;	//背景が画像ハンドル
	int hBGM_;				//BGM音声ハンドル
	int hEntrySE_;			//進入時SE音声ハンドル
	
public:
	/**
	 * \brief コンストラクタ
	 */
	TitleObject();
	/**
	 * \brief デストラクタ
	 */
	~TitleObject();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
};

