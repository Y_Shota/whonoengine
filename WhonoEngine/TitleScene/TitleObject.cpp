#include "TitleObject.hpp"

#include "../ResourcePath.hpp"

#include "../Engine/Audio.hpp"
#include "../Engine/Image.hpp"
#include "../Engine/Input.hpp"
#include "../Engine/SceneManager.hpp"
#include "../Engine/Time.hpp"

#include "../PlayScene/PlayScene.hpp"


namespace {

constexpr auto TITLE_FILE_NAME = "title.png";									//タイトル画像ファイル名
constexpr auto TITLE_POSITION_OFFSET = Vector2::Set(0, 200);		//タイトル画像配置ずれ
constexpr auto TITLE_SCALE_OFFSET = Vector2::Replicate(1.2f);	//タイトル画像大きさずれ
constexpr auto ENTRY_FILE_NAME = "press_any_button.png";						//エントリ画像ファイル名
constexpr auto ENTRY_POSITION_OFFSET = Vector2::Set(0, -150);	//エントリ画像配置ずれ
constexpr auto BACKGROUND_FILE_NAME = "title_back.png";							//背景画像ファイル名
constexpr auto BGM_FILE_NAME = "title.wav";										//BGM音声ファイル名
constexpr auto ENTRY_SE_FILE_NAME = "entry.wav";								//entry音声ファイル名
}

TitleObject::TitleObject()
	: GameObject(OBJECT_NAME)
	, hTitleImage_(Image::UNALLOCATED_HANDLE), hEntryImage_(Image::UNALLOCATED_HANDLE)
	, hBackgroundImage_(Image::UNALLOCATED_HANDLE)
	, hBGM_(Audio::UNALLOCATED_HANDLE), hEntrySE_(Audio::UNALLOCATED_HANDLE) {}

TitleObject::~TitleObject() = default;

void TitleObject::Initialize() {
	
	using namespace ResourcePath;

	//画像3種読み込み、デフォルトの変形セット
	hTitleImage_ = Image::Load(TITLE_DIR + TITLE_FILE_NAME);
	Image::SetScale(hTitleImage_, TITLE_SCALE_OFFSET);
	
	hEntryImage_ = Image::Load(TITLE_DIR + ENTRY_FILE_NAME);
	Image::SetPosition(hEntryImage_, ENTRY_POSITION_OFFSET, BASE_POSITION::BOTTOM_CENTER);

	hBackgroundImage_ = Image::Load(TITLE_DIR + BACKGROUND_FILE_NAME);

	//音声ファイル読み込み
	hBGM_ = Audio::Load(TITLE_DIR + BGM_FILE_NAME);
	hEntrySE_ = Audio::Load(TITLE_DIR + ENTRY_SE_FILE_NAME);
	Audio::SetResident(hEntrySE_, true);
	
	Audio::Play(hBGM_);
}

void TitleObject::Update() {

	using namespace Input;

	//何らかのキーを押したら遷移
	if (IsAnyKeyDown()) {
		if (auto* sceneManager = SceneManager::GetInstance()) {

			Audio::Play(hEntrySE_);
			sceneManager->ChangeScene(PlayScene::SCENE_NAME);
		}
	}
}

void TitleObject::Render() {

	//描画
	Image::SetFullSize(hBackgroundImage_);
	Image::Render(hBackgroundImage_);

	auto elapsedTime = Time::GetElapsedTimeS();

	//アルファ値を時間で変動
	auto alpha = cosf(elapsedTime * 3) * 1.5f + 0.75f;
	Image::SetAlpha(hEntryImage_, alpha);
	Image::Render(hEntryImage_);

	//高さを時間で変動
	auto titlePosition = TITLE_POSITION_OFFSET + Vector2::Set(0, cosf(elapsedTime * 4) * 4.f);
	Image::SetPosition(hTitleImage_, titlePosition, BASE_POSITION::TOP_CENTER);
	Image::Render(hTitleImage_);
}

void TitleObject::Finalize() {

	//BGM停止
	Audio::Stop(hBGM_);
}
