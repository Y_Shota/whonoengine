#pragma once
#include "../Engine/SceneObject.hpp"

/**
 * \brief タイトルシーンの管理クラス
 */
class TitleScene final : public SceneObject {

public:
	//シーン名
	static constexpr auto SCENE_NAME = "TitleScene";
	
public:
	/**
	 * \brief コンストラクタ
	 */
	TitleScene();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
};

