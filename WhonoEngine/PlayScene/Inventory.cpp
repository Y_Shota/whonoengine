#include "Inventory.hpp"

#include "../ResourcePath.hpp"
#include "../Engine/Model.hpp"
#include "../Engine/Text.hpp"
#include "../Engine/Image.hpp"
#include "../Engine/Math/Vector2.hpp"

#include "ItemManager.hpp"
#include "ItemStack.hpp"

using namespace Whono;

namespace {

constexpr auto MAX_SLOT_SIZE = 5;	//最大のスロット数

constexpr auto SLOT_IMAGE_OFFSET = Vector2::Set(7.f, -5.f);		//スロット画像描画場所のずれ
constexpr auto STACK_NUMBER_OFFSET = Vector2::Set(30.f, 0);		//スタック数描画場所のずれ
constexpr auto DURABILITY_BAR_SCALE = Vector2::Replicate(0.9f);	//耐久バー画像の大きさ
constexpr auto ITEM_ICON_SCALE = Vector2::Replicate(0.9f);		//アイテムアイコンの大きさ

constexpr auto SLOT_FILE_NAME			= "inventory_single.png";			//スロット画像のファイル名
constexpr auto CURRENT_SLOT_FILE_NAME	= "inventory_single_current.png";	//カレントスロット画像のファイル名
constexpr auto DURABILITY_BAR_FILE_NAME	= "durability_bar.png";				//耐久バー画像のファイル名
}


Inventory::Inventory(CharacterBase* master, int size)
	: hSlotImage_(Image::UNALLOCATED_HANDLE), hCurrentSlotImage_(Image::UNALLOCATED_HANDLE)
	, hDurabilityBarImage_(Image::UNALLOCATED_HANDLE)
	, hTextFormat_(Text::UNALLOCATED_HANDLE)
	, master_(master)
	, currentIndex_(0)
	, itemSlots_(size > MAX_SLOT_SIZE ? MAX_SLOT_SIZE : size) {}

Inventory::~Inventory() = default;

void Inventory::Initialize() {

	using namespace ResourcePath;

	//画像
	hSlotImage_ = Image::Load(INVENTORY_DIR + SLOT_FILE_NAME);
	hCurrentSlotImage_ = Image::Load(INVENTORY_DIR + CURRENT_SLOT_FILE_NAME);
	hDurabilityBarImage_ = Image::Load(INVENTORY_DIR + DURABILITY_BAR_FILE_NAME);
	Image::SetScale(hDurabilityBarImage_, DURABILITY_BAR_SCALE);
	
	hTextFormat_ = Text::CreateFormat("bauhaus 93", 40);
}

void Inventory::OnUpdate() {

	for (auto& itemStack : itemSlots_) {

		if (!itemStack)	continue;
		if (itemStack->GetDurability() <= 0 ||
			itemStack->GetStackSize() <= 0)	itemStack.reset();
	}
}

void Inventory::RenderSlot() const {

	//画像サイズの取得しいい感じに計算
	auto imageSize = Image::GetImageSize(hSlotImage_);
	auto position = imageSize * Vector2::Set(1, -1) * 0.5f + SLOT_IMAGE_OFFSET;
	auto slotOffset = Vector2::Set(imageSize.X(), 0);

	for (auto i = 0u; i < itemSlots_.size(); ++i) {

		//カレントスロットなら表示画像変更
		auto hSlot = currentIndex_ == i ? hCurrentSlotImage_ : hSlotImage_;
		Image::SetPosition(hSlot, position, BASE_POSITION::BOTTOM_LEFT);
		Image::Render(hSlot);

		//スロットにアイテムがある場合表示
		if (auto& itemStack = itemSlots_.at(i)) {

			auto hItemIcon = itemStack->GetIconHandle();
			Image::SetPosition(hItemIcon, position, BASE_POSITION::BOTTOM_LEFT);
			Image::SetScale(hItemIcon, ITEM_ICON_SCALE);
			Image::Render(hItemIcon);

			//耐久度が存在するアイテムの時とき
			if (itemStack->GetMaxDurability() > 1) {

				Image::SetPosition(hDurabilityBarImage_, position, BASE_POSITION::BOTTOM_LEFT);
				auto barImageSize = Image::GetImageSize(hDurabilityBarImage_);
				auto width = barImageSize.X() * itemStack->GetDurability() / itemStack->GetMaxDurability();
				Image::SetRect(hDurabilityBarImage_, 0, 0, width, barImageSize.Y());
				Image::SetPosition(hDurabilityBarImage_, position + Vector2::Set(0, imageSize.Y()) * 0.4f, BASE_POSITION::BOTTOM_LEFT);
				Image::Render(hDurabilityBarImage_);
			}
			//スタックしているとき
			if (itemStack->GetStackSize() > 1) {

				Text::SetPosition(hTextFormat_, position + STACK_NUMBER_OFFSET, BASE_POSITION::BOTTOM_LEFT);
				Text::Render(hTextFormat_, std::to_string(itemStack->GetStackSize()));
			}
		}
		//次のスロット分ずらす
		position += slotOffset;
	}
}

void Inventory::RenderEquipment(int hCharacterModel) const {

	const auto& currentItem = itemSlots_.at(currentIndex_);
	if (!currentItem)	return;

	auto boneTransform = Model::GetBoneTransform(hCharacterModel, "RHand");
	if (!boneTransform)	return;
	
	auto hEquipment = currentItem->GetEquipmentHandle();
	Model::SetTransform(hEquipment, boneTransform.get());
	Model::Render(hEquipment);
}

auto Inventory::GetCurrentIndex() const noexcept -> int {

	return currentIndex_;
}

auto Inventory::GetSlotSize() const noexcept -> int {

	return static_cast<int>(itemSlots_.size());
}

auto Inventory::GetCurrentItem() const noexcept -> Item* {

	if (const auto& currentItemStack = itemSlots_[currentIndex_]) {

		return currentItemStack->GetItem();
	}
	return nullptr;
}

auto Inventory::GoNextSlot() noexcept -> Inventory* {

	++currentIndex_ %= itemSlots_.size();
	return this;
}

auto Inventory::GoPreviousSlot() noexcept -> Inventory* {

	(--currentIndex_ += static_cast<int>(itemSlots_.size())) %= itemSlots_.size();
	return this;
}

auto Inventory::SetCurrentIndex(int index) noexcept -> Inventory* {

	if (index >= 0 && index < static_cast<int>(itemSlots_.size())) {

		currentIndex_ = index;
	}
	return this;
}

void Inventory::UseRightClickItem(CharacterBase* character) {

	if (itemSlots_.at(currentIndex_)) {

		itemSlots_.at(currentIndex_)->UseRightClick(character);
	}
}

void Inventory::UseLeftClickItem(CharacterBase* character) {
	
	if (itemSlots_.at(currentIndex_)) {

		itemSlots_.at(currentIndex_)->UseLeftClick(character);
	}
}

auto Inventory::SetItem(int itemId) -> int {

	auto prevItemId = -1;

	//スロット内にアイテムがあるとき
	if (auto& itemStack = itemSlots_[currentIndex_]) {

		//取得したアイテムが現在と同じとき
		if (itemStack->GetItemId() == itemId) {

			//スタック数が最大より小さいとき
			if (itemStack->GetStackSize() < itemStack->GetMaxStackSize()) {

				//スタック数を増やす
				itemStack->IncreaseStackSize();
				return itemId;
			}
			//耐久度が減っているとき
			if (itemStack->GetDurability() < itemStack->GetMaxDurability()) {

				//耐久度回復
				itemStack->RepairDurability();
				return itemId;
			}
			return itemId;
		}
		//現在のアイテムのIDを取得し破棄
		prevItemId = itemStack->GetItemId();
		itemStack.reset();
	}
	
	//アイテムマネージャからアイテムを取得
	auto* itemManager = ItemManager::GetInstance();
	auto* item = itemManager->GetItem(itemId);

	//新アイテムセット
	itemSlots_[currentIndex_] = std::make_unique<ItemStack>(item);

	return prevItemId;
}
