#include "TaskRandomWalk.hpp"

#include "../Engine/Random.hpp"
#include "../Engine/SceneManager.hpp"

#include "CharacterBase.hpp"
#include "Ground.hpp"
#include "OperateKeyState.hpp"


TaskRandomWalk::TaskRandomWalk(const CharacterBase* theCharacter)
	: theCharacter_(theCharacter), targetPoint_{} {}

auto TaskRandomWalk::ShouldExecute() const -> bool {

	//10%の確率で実行
	return Random::NextFloat() > 0.9f;
}

auto TaskRandomWalk::ContinueExecute() const -> bool {

	//向かっている座標に近づくまで
	auto position = GetXzPosition();
	auto vec = position - targetPoint_;

	return XMVector3Length(vec).m128_f32[0] >= 0.5f;
}

auto TaskRandomWalk::IsInterruptible() const -> bool {

	return true;
}

void TaskRandomWalk::Startup() {
	
	auto* scene = SceneManager::GetInstance()->GetCurrentScene();
	auto ground = scene->GetSrcObject<Ground>();
	
	//地面モデルの中心、幅を取得
	auto extents = ground->GetExtents();
	auto center = ground->GetCenterPosition();

	//XZ平面上での生成地点を乱数から作成
	auto realRandX = Random::NextFloat() * 2.f - 1.f;
	auto realRandZ = Random::NextFloat() * 2.f - 1.f;
	auto centerOffset = XMVectorSet(realRandX, 0, realRandZ, 0);
	
	targetPoint_ = center + extents * centerOffset;
	targetPoint_ = XMVectorSelect(g_XMZero, targetPoint_, g_XMSelect1010);
}

void TaskRandomWalk::Update() {
	
	auto vec1 = theCharacter_->GetTransform()->GetWorldForward();
	auto vec2 = targetPoint_ - GetXzPosition();

	//Y軸での回転量を取得
	auto angle = XMVector3AngleBetweenVectors(vec1, vec2).m128_f32[0];
	auto normal = XMVector3Cross(vec1, vec2);
	angle *= normal.m128_f32[1] > 0 ? 1 : -1;

	//回転
	auto* keyState = theCharacter_->GetOperatedKeyState();
	keyState->mouseDelta.X(XMConvertToDegrees(angle));

	//進むので常に全身
	keyState->button.w = true;
}

void TaskRandomWalk::Reset() {

	auto* keyState = theCharacter_->GetOperatedKeyState();

	keyState->mouseDelta = Math::ZERO_VECTOR_3D;
	keyState->buttonAll = 0;

	targetPoint_ = g_XMZero;
}

auto TaskRandomWalk::GetXzPosition() const -> XMVECTOR {
	
	auto position = theCharacter_->GetTransform()->GetWorldPosition();
	return XMVectorSelect(g_XMZero, position, g_XMSelect1010);
}
