#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief ステージ上に配置されるオブジェクトクラス
 */
class StageObject : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "StageObject";
	
private:
	int hModel_;	//オブジェクト用モデルハンドル
	
public:
	/**
	 * \brief コンストラクタ
	 */
	StageObject();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
};

