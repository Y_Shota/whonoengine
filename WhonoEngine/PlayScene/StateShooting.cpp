#include "StateShooting.hpp"

#include "../Engine/Time.hpp"

#include "CharacterBase.hpp"
#include "CharacterState.hpp"

namespace {

constexpr auto START_FRAME = 180;	//アニメションの開始フレーム
constexpr auto END_FRAME = 210;		//アニメションの開始フレーム
constexpr auto LIFETIME = (END_FRAME - START_FRAME) / Time::FRAME_RATE;	//状態維持限界時間
}

void StateShooting::SetUp(CharacterBase* character) const {

	character->SetAnimation(START_FRAME, END_FRAME, 1);
}

void StateShooting::Update(CharacterBase* character) const {

	//アニメーションが終わったら状態遷移
	if (character->GetStateElapsedTimeAndCalculate() >= LIFETIME) {

		character->SetState(CharacterState::STANDING);
		character->OnUpdateInventory();
	}
}
