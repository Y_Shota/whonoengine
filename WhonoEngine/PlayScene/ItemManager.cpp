#include "ItemManager.hpp"

#include "Item.hpp"

ItemManager::~ItemManager() = default;

auto ItemManager::SpawnItem(GameObject* parent, int itemId) const -> CrystalOre* {

	//親指定がない場合
	if (!parent) return nullptr;
	
	auto* crystalOre = Instantiate<CrystalOre>(parent);
	crystalOre->SetItem(items_[itemId].get());
	
	return crystalOre;
}

auto ItemManager::GetItem(int itemId) const -> Item* {

	//範囲外アクセス
	if (itemId < 0 || itemId >= static_cast<int>(items_.size()))	return nullptr;

	return items_[itemId].get();
}

auto ItemManager::GetItemCount() const noexcept -> int {

	return static_cast<int>(items_.size());
}
