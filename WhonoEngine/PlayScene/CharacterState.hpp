#pragma once

#include "StateFalling.hpp"
#include "StateHandUpping.hpp"
#include "StateJumping.hpp"
#include "StateMoving.hpp"
#include "StateShooting.hpp"
#include "StateStanding.hpp"
#include "StateSwinging.hpp"

/**
 * \brief キャラクタの状態の列挙もどき
 */
namespace CharacterState {

extern const StateFalling		FALLING;		//落下
extern const StateHandUpping	HAND_UPPING;	//挙手
extern const StateJumping		JUMPING;		//ジャンプ
extern const StateMoving		MOVING;			//移動
extern const StateShooting		SHOOTING;		//撃つ
extern const StateStanding		STANDING;		//棒立ち
extern const StateSwinging		SWINGING;		//振る
}
