#pragma once
#include <cstdint>

#include "../Engine/Math/Vector3.hpp"

/**
 * \brief キャラクターを操作するのに必要なキーの状態を保持する構造体
 * \detail ネットワークバイトオーダーに対応
 */
struct OperateKeyState {
	
	union {
		Whono::Vector3 mouseDelta;			//マウスの移動量
		unsigned mouseDeltaNetworkOrder[3];	//マウスの移動量のネットワークバイトオーダー用
	};
	union {
		std::uint32_t buttonAll;			//すべてのボタン
		struct Button {						
			std::uint32_t num1 : 1;			//1キー
			std::uint32_t num2 : 1;			//2キー
			std::uint32_t num3 : 1;			//3キー
			std::uint32_t num4 : 1;			//4キー
			std::uint32_t num5 : 1;			//5キー
			std::uint32_t num6 : 1;			//6キー
			std::uint32_t num7 : 1;			//7キー
			std::uint32_t num8 : 1;			//8キー
			std::uint32_t num9 : 1;			//9キー
			std::uint32_t e : 1;			//Eキー
			std::uint32_t w : 1;			//Wキー
			std::uint32_t a : 1;			//Aキー
			std::uint32_t s : 1;			//Sキー
			std::uint32_t d : 1;			//Dキー
			std::uint32_t lCtrl : 1;		//LCtrlキー
			std::uint32_t lShift : 1;		//LShiftキー
			std::uint32_t space : 1;		//Spaceキー
			std::uint32_t lClick : 1;		//LClickキー
			std::uint32_t rClick : 1;		//RClickキー
		} button;
	};

	/**
	 * \brief コンストラクタ
	 */
	OperateKeyState() : mouseDelta(Whono::Math::ZERO_VECTOR_3D), buttonAll(0) {}
};

