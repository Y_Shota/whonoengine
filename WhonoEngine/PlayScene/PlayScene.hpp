#pragma once
#include "../Engine/SceneObject.hpp"

/**
 * \brief プレイシーン管理クラス
 */
class PlayScene final : public SceneObject {

public:
	//シーン名
	static constexpr auto SCENE_NAME = "PlayScene";
	
public:
	/**
	 * \brief コンストラクタ
	 */
	PlayScene();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
};

