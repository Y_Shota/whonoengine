#include "StateStanding.hpp"

#include "CharacterBase.hpp"
#include "CharacterState.hpp"

void StateStanding::SetUp(CharacterBase* character) const {

	character->SetAnimation(0, 0, 0);
}

void StateStanding::Update(CharacterBase* character) const {

	//インベントリスロット更新
	character->UpdateInventorySlot();
	
	character->UpdateDirection();

	if (character->NeedsJump()) {

		character->SetState(CharacterState::JUMPING);
	}

	if (character->NeedsMove()) {

		character->SetState(CharacterState::MOVING);
	}
	
	//アイテムの使用
	if (character->NeedsUseItemLeft())	character->UseItemLeft();
	if (character->NeedsUseItemRight())	character->UseItemRight();
}
