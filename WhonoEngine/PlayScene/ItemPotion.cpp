#include "ItemPotion.hpp"

#include "CharacterBase.hpp"
#include "CharacterState.hpp"

namespace {

constexpr auto ITEM_NAME = "potion";						//アイテム名
constexpr auto CRYSTAL_FILE_NAME = "green_ore.fbx";			//鉱石モデルのファイル名
constexpr auto EQUIPMENT_FILE_NAME = "syringe_green.fbx";	//装備モデルのファイル名
constexpr auto SE_FILE_NAME = "heal.wav";					//使用時の音声ファイル名
constexpr auto MAX_STACK_SIZE = 3;							//最大スタックサイズ
constexpr auto RECOVERY_POINT = 1;							//回復量
}

ItemPotion::ItemPotion()
	: Item(ITEM_NAME) {

	//初期情報をセット
	SetMaxStackSize(MAX_STACK_SIZE);
	LoadCrystalModel(CRYSTAL_FILE_NAME);
	LoadEquipmentModel(EQUIPMENT_FILE_NAME);
	LoadSE(SE_FILE_NAME);
}

void ItemPotion::OnLeftClick(CharacterBase* character, ItemStack* itemStack) {

	//HPが減っているときのみ使用可
	if (!character->IsFullHealth()) {

		//カメラとの距離を算出しSEをならす
		PlaySE(character->GetCameraDistance());

		//状態遷移
		character->SetState(CharacterState::HAND_UPPING);

		//使用者を回復
		character->RecoverHealthPoint(RECOVERY_POINT);

		//スタック数を減らす
		itemStack->ReduceStackSize();
	}
}
