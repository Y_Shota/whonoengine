#pragma once
#include "../Engine/GameObject.hpp"

#include <any>

#include "../Engine/Time.hpp"

#include "IState.hpp"

struct OperateKeyState;
class CrystalOre;
class Item;
class Inventory;

/**
 * \brief キャラクタ挙動の基底クラス
 */
class CharacterBase : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "CharacterBase";
	
	static constexpr auto MOVEMENT_DELTA = 0.3f;								//xz平面上の移動量
	static constexpr auto GROUNDING_EPSILON = 0.5f;								//接地判定誤差
	static constexpr auto GRAVITY = 9.8f / Time::FRAME_RATE / 2.5f;	//重力加速度
	static constexpr auto JUMP_POWER = 1.f;										//ジャンプ力
	static constexpr auto SPEED_RATE = 1.5f;									//移動量の変化倍率
	static constexpr auto MAX_HEALTH_POINT = 10;								//最大HP量

private:
	int hModel_;										//キャラクタモデルハンドル

	XMVECTOR modelCenter_;								//キャラクタ中心座標

	int playerId_;										//プレイヤID
	
	int healthPoint_;									//体力
	float invincibleElapsedTime_;						//無敵経過時間
	bool isPlayingCharacter_;							//操作中のキャラクタか
	
	std::unique_ptr<Inventory> inventory_;				//インベントリ
	const IState* state_;								//行動の状態
	std::any stateAnyData_;								//行動状態用データ領域
	float stateElapsedTime_;							//各行動での経過時間
	
	std::unique_ptr<OperateKeyState> operateKeyState_;	//操作の状態

	float spawningTime_;								//スポーン中時間

public:
	/**
	 * \brief デフォルトコンストラクタ
	 * \detail 非推奨 -> Character(const std::string&)推奨
	 */
	[[deprecated]]
	CharacterBase();
	/**
	 * \brief コンストラクタ
	 * \param name キャラクタ名
	 */
	explicit CharacterBase(const std::string& name);

	/**
	 * \brief デストラクタ
	 */
	~CharacterBase();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 後初期化処理
	 */
	void PostInit() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
	/**
	 * \brief オブジェクトが衝突したときに呼ばれる
	 * \param target 衝突相手
	 * \param myCollider 衝突した自分の判定
	 * \param targetCollider 衝突した相手の判定
	 */
	void DetectedCollision(GameObject* target, ICollider* myCollider, ICollider* targetCollider) final;

	/**
	 * \brief 向いている方向を更新する
	 */
	void UpdateDirection() const;
	/**
	 * \brief xz平面上での座標を更新する
	 */
	void UpdateXzPosition() const;
	/**
	 * \brief インベントリの選択スロットを更新する
	 */
	void UpdateInventorySlot() const;

	/**
	 * \brief インベントリを更新する必要があるとき呼ばれる
	 */
	void OnUpdateInventory() const;
	
	/**
	 * \brief 持っているアイテムを左クリックで使う
	 */
	void UseItemLeft();
	/**
	 * \brief 持っているアイテムを右クリックで使う
	 */
	void UseItemRight();

	/**
	 * \brief キャラクタが持っているユニークなIDを取得する
	 * \return プレイヤID
	 */
	[[nodiscard]]
	auto GetPlayerId() const noexcept -> int;
	/**
	 * \brief 現在のHPを取得する
	 * \return HP
	 */
	[[nodiscard]]
	auto GetHealthPoint() const noexcept -> int;
	/**
	 * \brief 変更不可なインベントリを取得する
	 * \return インベントリ
	 */
	[[nodiscard]]
	auto GetInventory() const noexcept -> const Inventory*;
	/**
	 * \brief 現在のプレイヤとの地面までの距離を取得する
	 * \return 地面までの距離
	 */
	[[nodiscard]]
	auto GetToGroundDistance() -> float;
	/**
	 * \brief キャラクタの状態遷移時に使用される多目的なデータへの参照を取得する
	 * \return 状態遷移用データ
	 */
	[[nodiscard]]
	auto GetStateAnyData() -> std::any&;
	/**
	 * \brief 現在の状態に遷移してからの経過時間を計算し取得する
	 * \return 経過時間
	 */
	auto GetStateElapsedTimeAndCalculate() -> float;
	/**
	 * \brief 現在のキーの状態を取得する
	 * \return キーの状態
	 */
	[[nodiscard]]
	auto GetOperatedKeyState() const noexcept -> OperateKeyState*;
	/**
	 * \brief ワールド空間内での左手の座標を取得する
	 * \return 
	 */
	[[nodiscard]]
	auto GetHandPosition() const noexcept -> XMVECTOR;

	/**
	 * \brief 無敵時間中かどうか
	 * \return 無敵時間中なら真
	 */
	[[nodiscard]]
	auto IsInvincible() const noexcept -> bool;
	/**
	 * \brief スポーン中かどうか
	 * \return スポーン中なら真
	 */
	[[nodiscard]]
	auto IsSpawning() const noexcept -> bool;
	/**
	 * \brief クライアントによって操作されているかどうか
	 * \return 操作されているなら真
	 */
	[[nodiscard]]
	auto IsPlayingCharacter() const noexcept -> bool;
	/**
	 * \brief HPが満タンな状態かどうか
	 * \return 満タンなら真
	 */
	[[nodiscard]]
	auto IsFullHealth() const noexcept -> bool;
	/**
	 * \brief HPが０以下で死んでいるかどうか
	 * \return 死んでいるなら真
	 */
	[[nodiscard]]
	auto IsDead() const noexcept -> bool;
	/**
	 * \brief 現在持っているアイテムが攻撃可能なものかどうか
	 * \return 攻撃可能なら真
	 */
	[[nodiscard]]
	auto IsAttackable() const noexcept -> bool;
	/**
	 * \brief 現在持っているアイテムが回復可能なものかどうか
	 * \return 回復可能なら真
	 */
	[[nodiscard]]
	auto IsHealable() const noexcept -> bool;

	/**
	 * \brief プレイヤIDを設定する
	 * \param id 設定するID
	 */
	void SetPlayerId(int id);
	/**
	 * \brief クライアントによって操作されているフラグを立てる
	 */
	void SetPlayingFlag();
	/**
	 * \brief アニメーションの再生範囲、速度を設定する
	 * \param start 開始フレーム
	 * \param end 終了フレーム
	 * \param speed 再生速度
	 */
	void SetAnimation(int start, int end, float speed = 1.f) const;
	/**
	 * \brief アニメーションの再生速度を設定する
	 * \param speed 再生速度
	 */
	void SetAnimationSpeed(float speed = 1.f) const;

	/**
	 * \brief キャラクタの状態を遷移させる
	 * \param state 次の状態
	 */
	void SetState(const IState& state);

	/**
	 * \brief ステージ上に配置されている鉱石を回収する
	 * \param crystalOre 回収対象
	 */
	void PickCrystalOre(CrystalOre* crystalOre) const;

	/**
	 * \brief HPを減らす
	 * \param amount 減らす量
	 */
	void ReduceHealthPoint(int amount);
	/**
	 * \brief HPを回復させる
	 * \param amount 回復させる量
	 */
	void RecoverHealthPoint(int amount);

	/**
	 * \brief リスポーンさせる
	 */
	void ReSpawn();
	/**
	 * \brief ランダムなスポーン場所に移動させる
	 */
	void SpawnRandomPoint();

	/**
	 * \brief 移動する必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsMove() const -> bool;
	/**
	 * \brief 前方向へ移動する必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsMoveForward() const -> bool;
	/**
	 * \brief 後ろ方向へ移動する必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsMoveBack() const -> bool;
	/**
	 * \brief 右方向へ移動する必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsMoveRight() const -> bool;
	/**
	 * \brief 左方向へ移動する必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsMoveLeft() const -> bool;
	/**
	 * \brief ダッシュする必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsDash() const -> bool;
	/**
	 * \brief スニークする必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsSneak() const -> bool;
	/**
	 * \brief ジャンプする必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsJump() const -> bool;
	/**
	 * \brief アイテムを左使用する必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsUseItemLeft() const -> bool;
	/**
	 * \brief アイテムを右使用する必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsUseItemRight() const -> bool;
	/**
	 * \brief 鉱石を回収する必要があるかどうか
	 * \return あるなら真
	 */
	[[nodiscard]]
	auto NeedsPickCrystalOre() const -> bool;
	/**
	 * \brief ピッチの変化量を取得する
	 * \return 変化量
	 */
	[[nodiscard]]
	auto GetPitchDelta() const -> float;
	/**
	 * \brief 現在のインベントリのスロットのインデックスを取得する
	 * \return インデックス
	 */
	[[nodiscard]]
	auto GetInventorySlotIndex() const -> int;

private:
	/**
	 * \brief 無敵時間の計算を行う
	 */
	void CalculateInvincibleTime();
	/**
	 * \brief スポーン時間の計算を行う
	 */
	void CalculateSpawningTime();
	/**
	 * \brief 点滅を行うべきかどうか
	 * \return お香なうべきなら真
	 */
	auto ShouldBlink() const noexcept -> bool;
};
