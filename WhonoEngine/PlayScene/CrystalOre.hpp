#pragma once
#include "../Engine/GameObject.hpp"

class Item;

/**
 * \brief ステージ上に出現する鉱石に関するクラス
 */
class CrystalOre final : public GameObject {

private:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "CrystalOre";
	
private:
	bool isSpawning_;				//スポーン中フラグ
	float movement_;				//スポーン中移動量
	Item* item_;					//取得時にキャラクタが入手するアイテム
	XMVECTOR excavationDirection_;	//スポーン中の移動方向
	
public:
	/**
	 * \brief コンストラクタ
	 */
	CrystalOre();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 後初期化処理
	 */
	void PostInit() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;

	/**
	 * \brief キャラクタが鉱石を回収した際に入手するアイテムを設定する
	 * \param item 設定するアイテム
	 */
	void SetItem(Item* item);
	/**
	 * \brief キャラクタが鉱石を回収した際に入手するアイテムのIDを取得する
	 * \return アイテムID
	 */
	auto GetItemId() const noexcept -> int;
	/**
	 * \brief 自身がキャラクタに回収されたときに呼ばれる
	 */
	void OnObtainedItem();

private:
	/**
	 * \brief スポーン中の動作
	 */
	void DoSpawningAction();
};

