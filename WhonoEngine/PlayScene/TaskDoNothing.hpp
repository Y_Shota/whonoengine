#pragma once
#include "TaskBase.hpp"

/**
 * \brief 何もしないタスククラス
 */
class TaskDoNothing final : public TaskBase {
	
public:
	/**
	 * \brief 実行可能にあるか
	 * \return 実行可能なら真
	 */
	auto ShouldExecute() const -> bool override { return true; }
	/**
	 * \brief 自身より優先度が高いタスクの割り込みが可能か
	 * \return 割り込み可能なら真
	 */
	auto IsInterruptible() const -> bool override { return true; }
};

