#pragma once
#include "Attack.hpp"

/**
 * \brief 斬撃の攻撃判定クラス
 */
class AttackSlash final : public Attack {

private:
	float elapsedTime_;	//出現してからの経過時間
	
public:
	/**
	 * \brief コンストラクタ
	 */
	AttackSlash();

	/**
	 * \brief 更新処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	
	/**
	 * \brief 攻撃判定が消える条件を満たしているかどうか
	 * \return 条件を満たしているとき真
	 */
	auto ShouldDisappear() const -> bool override;
};

