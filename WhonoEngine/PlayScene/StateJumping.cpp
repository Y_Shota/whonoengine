#include "StateJumping.hpp"

#include "CharacterBase.hpp"

void StateJumping::SetUp(CharacterBase* character) const {

	//親メソッド
	StateFalling::SetUp(character);

	//初速を設定
	SetFallSpeed(CharacterBase::JUMP_POWER, character);
	Update(character);
}
