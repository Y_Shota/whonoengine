#pragma once
#include "IState.hpp"

/**
 * \brief キャラクタが移動している状態を表すクラス
 */
class StateMoving final : public IState {
	
public:
	/**
	 * \brief キャラクタをその状態にするための準備をする
	 * \param character 対象となるキャラクタ
	 */
	void SetUp(CharacterBase* character) const override;
	/**
	 * \brief その状態でキャラクタが必要な更新処理を行う
	 * \param character 対象となるキャラクタ
	 */
	void Update(CharacterBase* character) const override;
};

