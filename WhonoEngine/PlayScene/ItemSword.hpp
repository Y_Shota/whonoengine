#pragma once
#include "Item.hpp"
#include "IAttackableItem.hpp"

/**
 * \brief アイテム剣の情報をまとめるクラス
 */
class ItemSword final : public Item, public IAttackableItem {

public:
	/**
	 * \brief コンストラクタ
	 */
	ItemSword();

	/**
	 * \brief 左クリックをしたときに呼ばれる
	 * \param character 使用したキャラクタ
	 * \param itemStack 使用したアイテムスタック
	 */
	void OnLeftClick(CharacterBase* character, ItemStack* itemStack) override;
	
	/**
	 * \brief 攻撃手が相手に攻撃を与えられる範囲にいるか
	 * \param attacker 攻撃手
	 * \param target 相手
	 * \return 範囲内にいたなら真
	 */
	[[nodiscard]]
	auto IsAttackableRange(const CharacterBase* attacker, const CharacterBase* target) -> bool override;
};

