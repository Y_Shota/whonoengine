#pragma once
#include "../Engine/Network/PacketId.hpp"
#include "OperateKeyState.hpp"

class PacketKeyState final : public PacketId<OperateKeyState> {

public:
	NetworkType ConvertToNetwork(HostType& host) override;
	HostType ConvertToHost(NetworkType& network) override;
};

