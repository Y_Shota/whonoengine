#include "Item.hpp"

#include "../ResourcePath.hpp"
#include "../Engine/Audio.hpp"
#include "../Engine/Image.hpp"
#include "../Engine/Model.hpp"

#include "IAttackableItem.hpp"
#include "IHealableItem.hpp"

using namespace Whono;
using namespace ResourcePath;


namespace {

constexpr auto MAX_VOLUME_DISTANCE = 20.f;	//音量が最大となるカメラとの距離
constexpr auto MIN_VOLUME_DISTANCE = 150.f;	//音量が最少となるカメラとの距離
}

Item::Item(const std::string& name)
	: name_(name)
	, itemId_(-1)
	, maxStackSize_(1), maxDurability_(1)
	, hIcon_(Image::UNALLOCATED_HANDLE)
	, hCrystalModel_(Model::UNALLOCATED_HANDLE), hEquipmentModel_(Model::UNALLOCATED_HANDLE)
	, hSE_(Audio::UNALLOCATED_HANDLE) {

	SetDefaultResource();
}

Item::~Item() = default;


auto Item::GetItemId() const noexcept -> int {

	return itemId_;
}

auto Item::GetItemName() const noexcept -> const std::string& {

	return name_;
}

auto Item::GetMaxStackSize() const noexcept -> int {

	return maxStackSize_;
}
auto Item::GetMaxDurability() const noexcept -> int {

	return maxDurability_;
}

auto Item::GetIconHandle() const noexcept -> int {

	return hIcon_;
}

auto Item::GetCrystalModelHandle() const noexcept -> int {

	return hCrystalModel_;
}

auto Item::GetEquipmentModelHandle() const noexcept -> int {

	return hEquipmentModel_;
}

auto Item::IsAttackable() const noexcept -> bool {

	return dynamic_cast<const IAttackableItem*>(this);
}

auto Item::IsHealable() const noexcept -> bool {

	return dynamic_cast<const IHealableItem*>(this);
}

auto Item::SetItemId(int id) noexcept -> Item* {

	itemId_ = id;
	return this;
}

auto Item::SetMaxStackSize(int stackSize) noexcept -> Item* {

	maxStackSize_ = stackSize;
	return this;
}

auto Item::SetMaxDurability(int durability) noexcept -> Item* {

	maxDurability_ = durability;
	return this;
}

auto Item::LoadIcon(const std::string& fileName) -> Item* {

	Image::Release(hIcon_);
	hIcon_ = Image::Load(ITEM_ICON_DIR + fileName);
	return this;
}

auto Item::LoadCrystalModel(const std::string& fileName) -> Item* {

	Model::Release(hCrystalModel_);
	hCrystalModel_ = Model::Load(ITEM_MODEL_DIR + fileName);
	return this;
}

auto Item::LoadEquipmentModel(const std::string& fileName) -> Item* {

	Model::Release(hEquipmentModel_);
	hEquipmentModel_ = Model::Load(ITEM_MODEL_DIR + fileName);
	return this;
}

auto Item::LoadSE(const std::string& fileName) -> Item* {

	hSE_ = Audio::Load(ITEM_SOUND_DIR + fileName);
	return this;
}

void Item::PlaySE(float cameraDistance) const {
	
	auto volume = (MIN_VOLUME_DISTANCE - cameraDistance) / (MIN_VOLUME_DISTANCE - MAX_VOLUME_DISTANCE);
	volume = std::clamp(volume, 0.f, 1.f);
	
	Audio::Play(hSE_, volume);
}

void Item::SetDefaultResource() {

	LoadIcon(name_ + ".png");
	LoadCrystalModel("white_ore.fbx");
	LoadEquipmentModel(name_ + ".fbx");
}
