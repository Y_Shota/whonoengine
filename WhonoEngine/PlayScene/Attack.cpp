#include "Attack.hpp"

#include "CharacterBase.hpp"
#include "GameObserver.hpp"

Attack::Attack()
	: GameObject(OBJECT_NAME)
	, power_(1)
	, master_(nullptr)
	, attackType_(ATTACK_TYPE::UNKNOWN) {}

void Attack::Initialize() {}

void Attack::Update() {

	//消滅条件を満たすとき消す
	if (ShouldDisappear())	KillMe();
}

void Attack::Render() {}

void Attack::Finalize() {}

void Attack::DetectedCollision(GameObject* target, ICollider* myCollider, ICollider* targetCollider) {

	//キャラクタに対し攻撃を与える
	if (auto* character = dynamic_cast<CharacterBase*>(target)) {

		if (master_ != character)	TakeDamageTo(character);

		//キャラクタが死んだ場合KDカウントを更新
		if (character->IsDead()) {

			auto* gameObserver = GetAffiliationScene()->GetSrcObject<GameObserver>();
			gameObserver->OnUpdateKillDeathCount(master_, character);
		}
	}

	//消滅条件を満たすとき消す
	if (ShouldDisappear())	KillMe();
}

auto Attack::SetPower(int power) noexcept -> Attack* {

	power_ = power;
	return this;
}

auto Attack::SetMaster(CharacterBase* master) noexcept -> Attack* {

	master_ = master;
	return this;
}

void Attack::TakeDamageTo(CharacterBase* character) const {

	//攻撃力分HPを減らす
	character->ReduceHealthPoint(power_);
}
