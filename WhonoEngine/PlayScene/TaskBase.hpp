#pragma once


/**
 * \brief AIのタスクの基底クラス
 */
class TaskBase {

public:
	virtual ~TaskBase() = default;

	/**
	 * \brief 実行可能にあるか
	 * \return 実行可能なら真
	 */
	virtual auto ShouldExecute() const -> bool = 0;
	/**
	 * \brief 継続可能か
	 * \return 継続可能なら真
	 */
	virtual auto ContinueExecute() const -> bool { return ShouldExecute(); }
	/**
	 * \brief 自身より優先度が高いタスクの割り込みが可能か
	 * \return 割り込み可能なら真
	 */
	virtual auto IsInterruptible() const -> bool { return false; }
	/**
	 * \brief タスク実行開始時の処理
	 */
	virtual void Startup() {}
	/**
	 * \brief 更新
	 */
	virtual void Update() {}
	/**
	 * \brief リセットする
	 */
	virtual void Reset() {}
};

