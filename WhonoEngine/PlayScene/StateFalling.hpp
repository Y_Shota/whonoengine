#pragma once
#include "IState.hpp"

/**
 * \brief キャラクタが落下している状態を表すクラス
 */
class StateFalling : public IState {

public:
	/**
	 * \brief キャラクタをその状態にするための準備をする
	 * \param character 対象となるキャラクタ
	 */
	void SetUp(CharacterBase* character) const override;
	/**
	 * \brief その状態でキャラクタが必要な更新処理を行う
	 * \param character 対象となるキャラクタ
	 */
	void Update(CharacterBase* character) const override;

protected:
	/**
	 * \brief キャラクタの落下速度の初速を設定する
	 * \param initialSpeed 初速
	 * \param character 対象となるキャラクタ
	 */
	static void SetFallSpeed(float initialSpeed, CharacterBase* character);
};

