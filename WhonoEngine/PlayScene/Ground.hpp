#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief ステージの地面を構成するクラス
 */
class Ground final : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "Ground";
	
private:
	int hModel_;	//地面モデルハンドル
	
public:
	/**
	 * \brief コンストラクタ
	 */
	Ground();

	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;

	/**
	 * \brief 地面の中心座標を取得する
	 * \return 中心座標
	 */
	auto GetCenterPosition() const -> XMVECTOR;
	/**
	 * \brief 地面の中心からの大きさを取得する
	 * \return 地面の大きさ
	 */
	auto GetExtents() const -> XMVECTOR;
	
	/**
	 * \brief 地面に向かってレイキャストを行う
	 * \param origin 発射始点
	 * \param direction 方向
	 * \param[out] dist 当たった時の距離
	 * \param[out] normal 当たったポリゴンの法線
	 * \return 当たったなら真
	 */
	auto RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist = nullptr, XMVECTOR* normal = nullptr) const -> bool;
};

