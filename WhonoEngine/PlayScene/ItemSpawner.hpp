#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief アイテム湧き操作クラス
 */
class ItemSpawner final : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "ItemSpawner";
	
private:
	int itemCount_;			//ステージ上のアイテム数
	float spawnCoolTime_;	//次スポーンまでの時間

public:
	/**
	 * \brief コンストラクタ
	 */
	ItemSpawner();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 後初期化処理
	 */
	void PostInit() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理	
	 */
	void Finalize() override;

	/**
	 * \brief アイテムが回収されたことを自身に通知
	 */
	void NotifyObtainedItem();

private:
	/**
	 * \brief アイテムを湧かせる
	 */
	void SpawnItem();
};

