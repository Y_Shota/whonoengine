#include "CharacterState.hpp"

#include "StateFalling.hpp"
#include "StateHandUpping.hpp"
#include "StateJumping.hpp"
#include "StateMoving.hpp"
#include "StateShooting.hpp"
#include "StateStanding.hpp"
#include "StateSwinging.hpp"

/**
 * \brief キャラクタの状態の列挙もどき
 */
namespace CharacterState {

const StateFalling		FALLING;		//落下
const StateHandUpping	HAND_UPPING;	//挙手
const StateJumping		JUMPING;		//ジャンプ
const StateMoving		MOVING;			//移動
const StateShooting		SHOOTING;		//撃つ
const StateStanding		STANDING;		//棒立ち
const StateSwinging		SWINGING;		//振る

}
