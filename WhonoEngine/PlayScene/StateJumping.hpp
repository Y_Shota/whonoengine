#pragma once
#include "StateFalling.hpp"

/**
 * \brief キャラクタがジャンプしている状態を表すクラス
 */
class StateJumping final : public StateFalling {
	
public:
	/**
	 * \brief キャラクタをその状態にするための準備をする
	 * \param character 対象となるキャラクタ
	 */
	void SetUp(CharacterBase* character) const override;
};

