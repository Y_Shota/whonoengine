#pragma once
#include "CharacterBase.hpp"

/**
 * \brief キャラクタの動作を人の操作によって決定するクラス
 */
class CharacterPlayer final : public CharacterBase {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "CharacterPlayer";

private:
	int hHPBarImage_;	//HPバーのイメージハンドル
	
public:
	/**
	 * \brief コンストラクタ
	 */
	CharacterPlayer();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 後描画処理
	 */
	void PostRender() override;

private:
	/**
	 * \brief キーの入力状態を更新する
	 */
	void UpdateOperateKeyState() const;
};

