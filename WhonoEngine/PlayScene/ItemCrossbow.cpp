#include "ItemCrossbow.hpp"

#include "../ResourcePath.hpp"

#include "../Engine/Model.hpp"

#include "AttackArrow.hpp"
#include "CharacterBase.hpp"
#include "CharacterState.hpp"

namespace {

constexpr auto ITEM_NAME = "crossbow";											//アイテム名
constexpr auto CRYSTAL_NAME = "blue_ore.fbx";									//鉱石モデルのファイル名
constexpr auto EQUIPMENT_FILE_NAME = "blue_crossbow.fbx";						//クロスボウ本体モデルのファイル名
constexpr auto ARROW_FILE_NAME = "blue_arrow.fbx";								//矢モデルのファイル名
constexpr auto SE_FILE_NAME = "shot.wav";										//使用時の音声ファイル名
constexpr auto MAX_DURABILITY = 4;												//最大耐久度
constexpr auto POWER = 1;														//攻撃力	
constexpr auto MAX_ATTACK_DISTANCE = 50.f;										//攻撃可能距離
constexpr auto MAX_ATTACK_ANGLE = XMConvertToRadians(15.f);	//攻撃可能角度
}

ItemCrossbow::ItemCrossbow()
	: Item(ITEM_NAME)
	, hArrowModel_(Model::UNALLOCATED_HANDLE) {

	//初期情報セット
	SetMaxDurability(MAX_DURABILITY);
	LoadCrystalModel(CRYSTAL_NAME);
	LoadEquipmentModel(EQUIPMENT_FILE_NAME);
	LoadSE(SE_FILE_NAME);

	//矢のモデル読み込み
	hArrowModel_ = Model::Load(ResourcePath::ITEM_MODEL_DIR + ARROW_FILE_NAME);
}

void ItemCrossbow::OnLeftClick(CharacterBase* character, ItemStack* itemStack) {

	//カメラとの距離を算出しSEをならす
	PlaySE(character->GetCameraDistance());

	//キャラクタの状態遷移
	character->SetState(CharacterState::SHOOTING);

	//耐久値を減らす
	itemStack->ReduceDurability();
	
	//攻撃判定生成
	auto* attack = Instantiate<AttackArrow>(character->GetAffiliationScene());
	auto* characterTrans = character->GetTransform();

	//攻撃判定に情報セット
	attack
		->SetModelHandle(hArrowModel_)
		->SetDirection(characterTrans->GetWorldForward())
		->SetPower(POWER)
		->SetMaster(character);
	attack->GetTransform()
		->SetWorldPosition(character->GetHandPosition());
}

auto ItemCrossbow::IsAttackableRange(const CharacterBase* attacker, const CharacterBase* target) -> bool {

	//両者の変形情報を取得
	auto* attackerTrans = attacker->GetTransform();
	auto* targetTrans = target->GetTransform();

	//両者の座標を取得
	auto attackerPos = attackerTrans->GetWorldPosition();
	auto targetPos = targetTrans->GetWorldPosition();

	//攻撃手から受け手へのベクトル
	auto toTargetVec = targetPos - attackerPos;
	
	//攻撃範囲外の時
	if (XMVector3Length(toTargetVec).m128_f32[0] > MAX_ATTACK_DISTANCE) {

		return false;
	}

	//攻撃手の正面ベクトルを取得し角度検証
	auto attackerForward = attackerTrans->GetWorldForward();
	auto angle = XMVector3AngleBetweenVectors(toTargetVec, attackerForward).m128_f32[0];

	//角度が範囲内なら真
	return abs(angle) < MAX_ATTACK_ANGLE;
}
