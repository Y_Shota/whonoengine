#include "ItemSword.hpp"


#include "AttackSlash.hpp"
#include "CharacterBase.hpp"
#include "CharacterState.hpp"

namespace {

constexpr auto ITEM_NAME = "sword";												//アイテム名
constexpr auto CRYSTAL_FILE_NAME = "red_ore.fbx";								//鉱石モデルのファイル名
constexpr auto EQUIPMENT_FILE_NAME = "red_sword.fbx";							//装備モデルのファイル名
constexpr auto SE_FILE_NAME = "slash.wav";										//使用時の音声ファイル名
constexpr auto MAX_DURABILITY = 2;												//最大耐久度
constexpr auto POWER = 2;														//攻撃力
constexpr auto MAX_ATTACK_DISTANCE = 4.f;										//攻撃可能距離
constexpr auto HALF_RANGE = MAX_ATTACK_DISTANCE * 0.5f;				//攻撃可能距離の半
constexpr auto MAX_ATTACK_ANGLE = XMConvertToRadians(30.f);	//攻撃可能角度
}

ItemSword::ItemSword()
	: Item(ITEM_NAME) {

	//初期情報をセット
	SetMaxDurability(MAX_DURABILITY);
	LoadCrystalModel(CRYSTAL_FILE_NAME);
	LoadEquipmentModel(EQUIPMENT_FILE_NAME);
	LoadSE(SE_FILE_NAME);
}

void ItemSword::OnLeftClick(CharacterBase* character, ItemStack* itemStack) {

	//カメラとの距離を算出しSEをならす
	PlaySE(character->GetCameraDistance());

	//キャラクタの状態遷移
	character->SetState(CharacterState::SWINGING);

	//耐久値を減らす
	itemStack->ReduceDurability();

	//攻撃判定生成
	auto* attack = Instantiate<AttackSlash>(character);
	attack
		->SetPower(POWER)
		->SetMaster(character);
	attack->GetTransform()->SetLocalPosition(0, HALF_RANGE, HALF_RANGE);
}

auto ItemSword::IsAttackableRange(const CharacterBase* attacker, const CharacterBase* target) -> bool {

	//両者の変形情報を取得
	auto* attackerTrans= attacker->GetTransform();
	auto* targetTrans = target->GetTransform();

	//両者の座標を取得
	auto attackerPos = attackerTrans->GetWorldPosition();
	auto targetPos = targetTrans->GetWorldPosition();

	//攻撃手から受け手へのベクトル
	auto toTargetVec = targetPos - attackerPos;
	
	//距離が攻撃範囲外の時
	if (XMVector3Length(toTargetVec).m128_f32[0] > MAX_ATTACK_DISTANCE) {

		return false;
	}

	//攻撃手の正面ベクトルを取得し角度検証
	auto attackerForward = attackerTrans->GetWorldForward();
	auto angle = XMVector3AngleBetweenVectors(toTargetVec, attackerForward).m128_f32[0];

	//角度が範囲内なら真
	return abs(angle) < MAX_ATTACK_ANGLE;
}
