#pragma once
#include <string>

class Item;
class CharacterBase;

/**
 * \brief アイテムとその状態を保持するクラス
 */
class ItemStack {

private:
	int durability_;	//耐久度
	int stackSize_;		//スタック数
	Item* item_;		//アイテムの種類
	
public:
	/**
	 * \brief コンストラクタ
	 * \param item アイテムの種類
	 */
	explicit ItemStack(Item* item);
	
	/**
	 * \brief 耐久度を取得する
	 * \return 耐久度
	 */
	auto GetDurability() const noexcept -> int;
	/**
	 * \brief 最大の耐久度を取得する
	 * \return 最大の耐久度
	 */
	auto GetMaxDurability() const noexcept -> int;
	/**
	 * \brief スタック数を取得する
	 * \return スタック数
	 */
	auto GetStackSize() const noexcept -> int;
	/**
	 * \brief 最大スタック数を取得する
	 * \return 最大スタック数
	 */
	auto GetMaxStackSize() const noexcept -> int;
	/**
	 * \brief アイテム名を取得する
	 * \return アイテム名
	 */
	auto GetItemName() const noexcept -> const std::string&;
	/**
	 * \brief アイテムを取得する
	 * \return アイテム
	 */
	auto GetItem() const noexcept -> Item*;
	/**
	 * \brief アイテムIDを取得する
	 * \return アイテムID
	 */
	auto GetItemId() const noexcept -> int;
	/**
	 * \brief アイテムアイコン画像のハンドルを取得する
	 * \return 画像ハンドル
	 */
	auto GetIconHandle() const noexcept -> int;
	/**
	 * \brief 装備モデルのハンドルを取得する
	 * \return モデルハンドル
	 */
	auto GetEquipmentHandle() const noexcept -> int;
	
	/**
	 * \brief 右使用する
	 * \param character 使ったキャラクタ
	 */
	void UseRightClick(CharacterBase* character);
	/**
	 * \brief 左使用する
	 * \param character 使ったキャラクタ
	 */
	void UseLeftClick(CharacterBase* character);

	/**
	 * \brief 耐久度を最大にする
	 * \return変更後のインスタンス 
	 */
	auto RepairDurability() noexcept -> ItemStack*;
	/**
	 * \brief 耐久値を1減らす
	 * \return 変更後のインスタンス
	 */
	auto ReduceDurability() noexcept -> ItemStack*;
	/**
	 * \brief 耐久値を１増やす
	 * \return 変更後のインスタンス
	 */
	auto IncreaseDurability() noexcept -> ItemStack*;
	/**
	 * \brief スタック数を１減らす
	 * \return 変更後のインスタンス
	 */
	auto ReduceStackSize() noexcept -> ItemStack*;
	/**
	 * \brief スタック数を１増やす
	 * \return 変更後のインスタンス
	 */
	auto IncreaseStackSize() noexcept -> ItemStack*;
};

