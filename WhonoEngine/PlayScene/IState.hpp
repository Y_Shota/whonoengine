#pragma once

class CharacterBase;

/**
 * \brief キャラクタの状態を表現するためのインターフェース
 */
class IState {

public:
	/**
	 * \brief デストラクタ
	 */
	virtual ~IState() = default;

	/**
	 * \brief キャラクタをその状態にするための準備をする
	 * \param character 対象となるキャラクタ
	 */
	virtual void SetUp(CharacterBase* character) const = 0;
	/**
	 * \brief その状態でキャラクタが必要な更新処理を行う
	 * \param character 対象となるキャラクタ
	 */
	virtual void Update(CharacterBase* character) const = 0;
};

