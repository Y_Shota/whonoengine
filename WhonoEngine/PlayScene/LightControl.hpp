#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief ライトを操作するクラス
 */
class LightControl final : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "LightControl";

private:
	float timer_;	//シーン経過時間

public:
	/**
	 * \brief コンストラクタ
	 */
	LightControl();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
};

