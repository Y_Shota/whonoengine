#pragma once
#include "TaskGoToward.hpp"

/**
 * \brief ほかのキャラクタへ向かうタスククラス
 */
class TaskGoTowardCharacter final : public TaskGoToward<CharacterBase> {

private:
	//親クラスのエイリアス
	using Super = TaskGoToward<CharacterBase>;
	
	float elapsedTime_;	//追いかけ始めてからの経過時間
	
public:
	/**
	 * \brief コンストラクタ
	 * \param theCharacter タスクの実行者
	 */
	explicit TaskGoTowardCharacter(const CharacterBase* theCharacter);

	/**
	 * \brief 実行可能にあるか
	 * \return 実行可能なら真
	 */
	auto ShouldExecute() const -> bool override;
	/**
	 * \brief 継続可能か
	 * \return 継続可能なら真
	 */
	auto ContinueExecute() const -> bool override;
	/**
	 * \brief タスク実行開始時の処理
	 */
	void Startup() override;
	/**
	 * \brief 更新
	 */
	void Update() override;
	/**
	 * \brief リセットする
	 */
	void Reset() override;
};

