#include "GameObserver.hpp"

#include "../ResourcePath.hpp"

#include "../Engine/Audio.hpp"
#include "../Engine/Input.hpp"
#include "../Engine/SceneManager.hpp"
#include "../Engine/Text.hpp"

#include "../ResultScene/ResultScene.hpp"
#include "../ResultScene/ResultData.hpp"

#include "CharacterBase.hpp"


struct GameObserver::CharacterData {

	CharacterBase* character;	//対象キャラクタ
	int kill;					//キル数
	int death;					//デス数

	explicit CharacterData(CharacterBase* character)
		: character(character), kill(0), death(0) {}
};


GameObserver::GameObserver()
	: GameObject(OBJECT_NAME)
	, playingCharacterId_(-1)
	, timer_(0.f)
	, hTextFormat_(Text::UNALLOCATED_HANDLE)
	, hBGM_(Audio::UNALLOCATED_HANDLE) {}

GameObserver::~GameObserver() = default;

void GameObserver::Initialize() {

	Input::ClipCenterMouse(true);

	hTextFormat_ = Text::CreateFormat("UD デジタル 教科書体 N-R", 32, DWRITE_FONT_WEIGHT_HEAVY);
	Text::SetAlignment(hTextFormat_, DWRITE_TEXT_ALIGNMENT_CENTER);

	hBGM_ = Audio::Load(ResourcePath::PLAY_DIR + "BGM/play_bgm.wav");
}

void GameObserver::Update() {

	if (timer_ <= 0.f) {

		Audio::Play(hBGM_, 0.25f);
	}

	timer_ += Time::GetFrameTimeS();

	if (IsFinished()) {
		if (auto* sceneManager = SceneManager::GetInstance()) {
			
			SendDataToResultScene();
			Audio::Stop(hBGM_);
			sceneManager->ChangeScene(ResultScene::SCENE_NAME);
		}
	}
}

void GameObserver::Render() {}

void GameObserver::PostRender() {

	std::stringstream ss;
	
	//Tabを押してる間戦績表示
	if (Input::IsKey(VK_TAB)) {

		auto& playingCharacter = characterData_[playingCharacterId_];
		ss << "撃破数：" << playingCharacter.kill << " 死亡数：" << playingCharacter.death;
	}
	//デフォルトは残り時間時間
	else {
		
		ss << "残り時間　" << static_cast<int>(MATCH_TIME_S - timer_) << "秒";
	}
	Text::Render(hTextFormat_, ss.str());
}

void GameObserver::Finalize() {

	//ゲーム終了時マウスクリップを解除
	Input::ClipCenterMouse(false);
}

auto GameObserver::AddCharacter(CharacterBase* character, bool isPlaying) -> CharacterBase* {

	auto id = GetCharacterCount();
	character->SetPlayerId(id);
	characterData_.emplace_back(character);
	if (isPlaying) {

		playingCharacterId_ = id;
	}
	return character;
}

auto GameObserver::GetCharacterCount() const noexcept -> int {

	return static_cast<int>(characterData_.size());
}

auto GameObserver::GetCharacter(int index) -> CharacterBase* {

	if (index < 0 || index >= static_cast<int>(characterData_.size()))	return nullptr;

	return characterData_[index].character;
}

auto GameObserver::GetPlayingCharacter() -> CharacterBase* {

	return characterData_[playingCharacterId_].character;
}

void GameObserver::OnUpdateKillDeathCount(CharacterBase* attacker, CharacterBase* target) {

	++characterData_[attacker->GetPlayerId()].kill;
	++characterData_[target->GetPlayerId()].death;
}

auto GameObserver::IsFinished() const noexcept -> bool {

	//ゲーム経過時間が一定値まで達したら終了
	return MATCH_TIME_S < timer_;
}

void GameObserver::SendDataToResultScene() const {

	//リザルトシーン用データ領域にデータをセット
	if (auto* resultData = ResultData::GetInstance()) {
		for (const auto& datum : characterData_) {

			resultData->AddData(datum.character->GetPlayerId(), datum.kill, datum.death);
		}
	}
}
