#pragma once
#include <vector>

#include "CrystalOre.hpp"
#include "Item.hpp"
#include "../Engine/EnablerIfType.hpp"
#include "../Engine/ISingleton.hpp"

/**
 * \brief アイテムを管理する
 */
class ItemManager : public DesignPattern::ISingleton<ItemManager> {

private:
	std::vector<std::unique_ptr<Item>> items_;	//アイテム一覧

public:
	/**
	 * \brief デストラクタ
	 */
	~ItemManager();
	
	/**
	 * \brief アイテムを登録する
	 * \tparam T Itemを継承したクラス
	 * \param item インスタンス
	 * \return インスタンスのポインタ
	 */
	template<class T, EnablerIfType<std::is_base_of_v<Item, T>> = nullptr>
	auto Register(std::unique_ptr<T>& item) -> Item*;
	/**
	 * \brief アイテムを登録する
	 * \tparam T Itemを継承したクラス
	 * \param item インスタンス
	 * \return インスタンスのポインタ
	 */
	template<class T, EnablerIfType<std::is_base_of_v<Item, T>> = nullptr>
	auto Register(std::unique_ptr<T>&& item) -> Item*;
	
	/**
	 * \brief アイテムをゲーム内に発生させる
	 * \param parent 親オブジェクト
	 * \param itemId 生成するアイテムの指定
	 * \return 生成したオブジェクトのインスタンス
	 */
	auto SpawnItem(GameObject* parent, int itemId) const -> CrystalOre*;
	/**
	 * \brief アイテムを取得する
	 * \param itemId 取得したいアイテムのID
	 * \return アイテム
	 */
	[[nodiscard]]
	auto GetItem(int itemId) const -> Item*;
	/**
	 * \brief アイテムの種類の数を取得する
	 * \return アイテムの種類の数
	 */
	[[nodiscard]]
	auto GetItemCount() const noexcept -> int;
};

template <class T, EnablerIfType<std::is_base_of_v<Item, T>>>
auto ItemManager::Register(std::unique_ptr<T>& item) -> Item* {

	return Register(std::move(item));
}

template <class T, EnablerIfType<std::is_base_of_v<Item, T>>>
auto ItemManager::Register(std::unique_ptr<T>&& item) -> Item* {

	auto* itemRaw = items_.emplace_back(std::move(item)).get();
	itemRaw->SetItemId(static_cast<int>(items_.size()) - 1);
	return itemRaw;
}

