#include "TaskGoTowardCharacter.hpp"

namespace {

constexpr auto MIN_RANGE = 3.f;		//最小視認範囲
constexpr auto MAX_RANGE = 100.f;	//最大視認範囲
constexpr auto GIVE_UP_TIME = 5.f;	//追いかけるのをあきらめる時間
}

TaskGoTowardCharacter::TaskGoTowardCharacter(const CharacterBase* theCharacter)
	: TaskGoToward<CharacterBase>(theCharacter, MIN_RANGE, MAX_RANGE)
	, elapsedTime_(0.f) {}

auto TaskGoTowardCharacter::ShouldExecute() const -> bool {

	//親メソッドが偽なら偽
	if (!Super::ShouldExecute())	return false;

	//攻撃可能な状態なら真
	return GetCharacter()->IsAttackable();
}

auto TaskGoTowardCharacter::ContinueExecute() const -> bool {

	//追いかけた時間が指定時間立ったなら偽
	if (elapsedTime_ > GIVE_UP_TIME)	return false;

	//親メソッド
	return Super::ContinueExecute();
}

void TaskGoTowardCharacter::Startup() {

	elapsedTime_ = 0.f;
	
	Super::Startup();
}

void TaskGoTowardCharacter::Update() {

	elapsedTime_ += Time::GetFrameTimeS();
	
	Super::Update();
}

void TaskGoTowardCharacter::Reset() {

	elapsedTime_ = 0.f;
	
	Super::Reset();
}
