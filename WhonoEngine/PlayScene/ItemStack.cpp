#include "ItemStack.hpp"

#include "Item.hpp"


ItemStack::ItemStack(Item* item)
	: durability_(item->GetMaxDurability())
	, stackSize_(1)
	, item_(item) {}

auto ItemStack::GetDurability() const noexcept -> int {

	return durability_;
}

auto ItemStack::GetMaxDurability() const noexcept -> int {

	return item_->GetMaxDurability();
}

auto ItemStack::GetStackSize() const noexcept -> int {

	return stackSize_;
}

auto ItemStack::GetMaxStackSize() const noexcept -> int {

	return item_->GetMaxStackSize();
}

auto ItemStack::GetItemName() const noexcept -> const std::string& {

	return item_->GetItemName();
}

auto ItemStack::GetItem() const noexcept -> Item* {

	return item_;
}

auto ItemStack::GetItemId() const noexcept -> int {

	return item_->GetItemId();
}

auto ItemStack::GetIconHandle() const noexcept -> int {

	return item_->GetIconHandle();
}

auto ItemStack::GetEquipmentHandle() const noexcept -> int {

	return item_->GetEquipmentModelHandle();
}

void ItemStack::UseRightClick(CharacterBase* character) {

	item_->OnRightClick(character, this);
}

void ItemStack::UseLeftClick(CharacterBase* character) {

	item_->OnLeftClick(character, this);
}

auto ItemStack::RepairDurability() noexcept -> ItemStack* {

	durability_ = item_->GetMaxDurability();
	return this;
}

auto ItemStack::ReduceDurability() noexcept -> ItemStack* {

	--durability_;
	return this;
}

auto ItemStack::IncreaseDurability() noexcept -> ItemStack* {

	++durability_;
	return this;
}

auto ItemStack::ReduceStackSize() noexcept -> ItemStack* {

	--stackSize_;
	return this;
}

auto ItemStack::IncreaseStackSize() noexcept -> ItemStack* {

	++stackSize_;
	return this;
}
