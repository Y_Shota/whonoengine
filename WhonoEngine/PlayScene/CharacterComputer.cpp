#include "CharacterComputer.hpp"

#include "CharacterAI.hpp"
#include "CrystalOre.hpp"
#include "TaskAttack.hpp"
#include "TaskDoNothing.hpp"
#include "TaskGoToward.hpp"
#include "TaskGoTowardCharacter.hpp"
#include "TaskHeal.hpp"
#include "TaskPickUpItem.hpp"
#include "TaskRandomWalk.hpp"


CharacterComputer::CharacterComputer()
	: CharacterBase(OBJECT_NAME) {}

CharacterComputer::~CharacterComputer() = default;

void CharacterComputer::Initialize() {
	
	//親メソッド
	CharacterBase::Initialize();

	using std::make_unique;

	//AI生成
	ai_ = make_unique<CharacterAI>();

	//タスク追加
	auto priority = 0;
	ai_->AddTask(priority++, make_unique<TaskHeal>(this));						//回復する
	ai_->AddTask(priority++, make_unique<TaskAttack>(this));					//攻撃を行う
	ai_->AddTask(priority++, make_unique<TaskGoTowardCharacter>(this));			//ほかのキャラクタに向う
	ai_->AddTask(priority++, make_unique<TaskPickUpItem>(this));				//アイテムを回収
	ai_->AddTask(priority++, make_unique<TaskGoToward<CrystalOre>>(this, 1.f));	//アイテムに向かう
	ai_->AddTask(priority++, make_unique<TaskRandomWalk>(this));				//ランダムな場所へ向かう
	ai_->AddTask(priority,	 make_unique<TaskDoNothing>());						//何もしない
}

void CharacterComputer::Update() {

	//AIを更新
	ai_->Update();

	//親メソッド
	CharacterBase::Update();
}
