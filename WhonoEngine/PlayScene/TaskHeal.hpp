#pragma once
#include "TaskBase.hpp"

class CharacterBase;

/**
 * \brief 回復するタスククラス
 */
class TaskHeal final : public TaskBase {

private:
	const CharacterBase* const theCharacter_;	//タスクの実行者
	
public:
	/**
	 * \brief コンストラクタ
	 * \param theCharacter タスクの実行者
	 */
	explicit TaskHeal(const CharacterBase* theCharacter);

	/**
	 * \brief 実行可能にあるか
	 * \return 実行可能なら真
	 */
	auto ShouldExecute() const -> bool override;
	/**
	 * \brief タスク実行開始時の処理
	 */
	void Startup() override;
	/**
	 * \brief リセットする
	 */
	void Reset() override;
};

