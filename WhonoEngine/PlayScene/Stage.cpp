#include "Stage.hpp"

#include "BackGround.hpp"
#include "Ground.hpp"
#include "StageObject.hpp"

Stage::Stage()
	: GameObject(OBJECT_NAME) {}

void Stage::Initialize() {

	//�w�i�쐬
	Instantiate<BackGround>(this);

	//�n�ʍ쐬
	Instantiate<Ground>(this);

	//��z�u
	Instantiate<StageObject>(this);
}

void Stage::Update() {}

void Stage::Render() {}

void Stage::Finalize() {}
