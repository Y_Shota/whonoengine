#pragma once
#include "TaskBase.hpp"

class CharacterBase;

/**
 * \brief 攻撃のタスククラス
 */
class TaskAttack final : public TaskBase {

private:
	const CharacterBase* const theCharacter_;	//タスクの実行者
	
public:
	/**
	 * \brief コンストラクタ
	 * \param theCharacter タスクの実行者
	 */
	explicit TaskAttack(const CharacterBase* theCharacter);

	/**
	 * \brief 実行可能にあるか
	 * \return 実行可能なら真
	 */
	auto ShouldExecute() const -> bool override;
	/**
	 * \brief タスク実行開始時の処理
	 */
	void Startup() override;
	/**
	 * \brief リセットする
	 */
	void Reset() override;
};

