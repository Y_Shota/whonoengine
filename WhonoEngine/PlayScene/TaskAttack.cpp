#include "TaskAttack.hpp"

#include "../Engine/SceneManager.hpp"

#include "CharacterBase.hpp"
#include "IAttackableItem.hpp"
#include "Inventory.hpp"
#include "Item.hpp"
#include "OperateKeyState.hpp"


TaskAttack::TaskAttack(const CharacterBase* theCharacter)
	: theCharacter_(theCharacter) {}

auto TaskAttack::ShouldExecute() const -> bool {

	//攻撃手段を持ち合わせていないなら無視
	if (!theCharacter_->IsAttackable())	return false;
	
	auto* attackItem = dynamic_cast<IAttackableItem*>(theCharacter_->GetInventory()->GetCurrentItem());
	
	auto* scene = SceneManager::GetInstance()->GetCurrentScene();
	auto targetCount = scene->GetSrcObjectCount<CharacterBase>();
	for (auto i = 0; i < targetCount; ++i) {

		if (auto* findObject = scene->GetSrcObject<CharacterBase>(i)) {

			//自身は無視
			if (dynamic_cast<GameObject*>(findObject) == theCharacter_)	continue;

			if (attackItem->IsAttackableRange(theCharacter_, findObject)) {

				return true;
			}
		}
	}
	return false;
}

void TaskAttack::Startup() {

	auto* keyState = theCharacter_->GetOperatedKeyState();
	keyState->button.lClick = true;
}

void TaskAttack::Reset() {

	auto* keyState = theCharacter_->GetOperatedKeyState();
	keyState->button.lClick = false;
}
