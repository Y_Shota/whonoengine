#pragma once

class CharacterBase;

/**
 * \brief Itemクラスを攻撃可能なアイテムにするためのインターフェース
 */
class IAttackableItem {

public:
	/**
	 * \brief デストラクタ
	 */
	virtual ~IAttackableItem() = default;

	/**
	 * \brief 攻撃手が相手に攻撃を与えられる範囲にいるか
	 * \param attacker 攻撃手
	 * \param target 相手
	 * \return 範囲内にいたなら真
	 */
	[[nodiscard]]
	virtual auto IsAttackableRange(const CharacterBase* attacker, const CharacterBase* target) -> bool = 0;
};
