#include "AttackArrow.hpp"

#include "../Engine/Model.hpp"
#include "../Engine/SphereCollider.hpp"
#include "../Engine/Time.hpp"

namespace {

constexpr auto LIFE_TIME = 2.f;	//生存時間
}

AttackArrow::AttackArrow()
	: hModel_(Model::UNALLOCATED_HANDLE)
	, elapsedTime_(0.f)
	, direction_(g_XMZero) {}

void AttackArrow::Initialize() {

	//当たり判定付与
	AddCollider(SphereCollider::Create(g_XMZero, 0.2f));
}

void AttackArrow::Update() {

	//親メソッド
	Attack::Update();
	
	//時間更新
	elapsedTime_ += Time::GetFrameTimeS();

	//変形情報を取得
	auto* transform = GetTransform();

	//進行方向へ移動
	//todo まっすぐから弧を描くように
	transform->AdjustPosition(direction_ * GetFrameRatio());

	//進行方向に合わせて回転
	auto forward = transform->GetWorldForward();
	auto normal = XMVector3Cross(forward, direction_);
	auto angle = XMVector3AngleBetweenVectors(forward, direction_).m128_f32[0];
	if (!XMVector3Equal(normal, g_XMZero)) {

		transform->AdjustOrientedMultiplyAxisRadians(normal, angle);
	}
}

void AttackArrow::Render() {

	//変形情報を設定し、描画
	Model::SetTransform(hModel_, GetTransform());
	Model::Render(hModel_);
}

auto AttackArrow::ShouldDisappear() const -> bool {

	//生存時間外になったら偽
	return elapsedTime_ >= LIFE_TIME;
}

auto AttackArrow::SetModelHandle(int handle) -> AttackArrow* {

	hModel_ = handle;

	return this;
}

auto AttackArrow::SetDirection(CXMVECTOR direction) -> AttackArrow* {

	direction_ = direction;

	return this;
}
