#pragma once
#include "CharacterBase.hpp"

class CharacterAI;

/**
 * \brief キャラクタの動作をAIによって決定するクラス
 */
class CharacterComputer final : public CharacterBase {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "CharacterComputer";

private:
	std::unique_ptr<CharacterAI> ai_;	//AI

public:
	/**
	 * \brief コンストラクタ
	 */
	CharacterComputer();
	/**
	 * \brief デストラクタ
	 */
	~CharacterComputer();

	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
};

