#pragma once
#include "Attack.hpp"

/**
 * \brief 矢による攻撃判定クラス
 */
class AttackArrow final : public Attack {

private:
	int hModel_;			//矢のモデルハンドル
	float elapsedTime_;		//出現してからの経過時間
	XMVECTOR direction_;	//飛んでいく方向
	
public:
	/**
	 * \brief コンストラクタ
	 */
	AttackArrow();

	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	
	/**
	 * \brief 攻撃判定が消える条件を満たしているかどうか
	 * \return 条件を満たしているとき真
	 */
	auto ShouldDisappear() const -> bool override;

	/**
	 * \brief 矢のモデルハンドルを設定する
	 * \param handle 設定するハンドル
	 * \return 変更後のインスタンス
	 */
	auto SetModelHandle(int handle) -> AttackArrow*;
	/**
	 * \brief 矢の飛んでいく方向を設定する
	 * \param direction 設定する方向
	 * \return 変更後のインスタンス
	 */
	auto SetDirection(CXMVECTOR direction) -> AttackArrow*;
};

