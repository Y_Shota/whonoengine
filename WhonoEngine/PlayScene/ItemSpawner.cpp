#include "ItemSpawner.hpp"

#include "../Engine/Random.hpp"
#include "../Engine/Time.hpp"

#include "Ground.hpp"
#include "ItemCrossbow.hpp"
#include "ItemManager.hpp"
#include "ItemPotion.hpp"
#include "ItemSword.hpp"


namespace {

constexpr auto FIRST_SPAWN_ITEM = 20;		//初期スポーン数
constexpr auto MAX_ITEM_COUNT = 30;			//最大スポーン数
constexpr auto MIN_SPAWN_COOL_TIME = 1.f;	//次のアイテムが湧くまでの最小時間
constexpr auto MAX_SPAWN_COOL_TIME = 5.f;	//次のアイテムが湧くまでの最大時間
}


ItemSpawner::ItemSpawner()
	: GameObject(OBJECT_NAME)
	, itemCount_(0)
	, spawnCoolTime_(0) {}

void ItemSpawner::Initialize() {

	//アイテム登録
	auto* itemManager = ItemManager::GetInstance();
	itemManager->Register(std::make_unique<ItemCrossbow>());
	itemManager->Register(std::make_unique<ItemSword>());
	itemManager->Register(std::make_unique<ItemPotion>());
}

void ItemSpawner::PostInit() {

	//初期湧きアイテム生成
	for (auto i = 0; i < FIRST_SPAWN_ITEM; ++i) {

		SpawnItem();
	}
}

void ItemSpawner::Update() {

	//出現上限に達していないならクールタイムを更新
	if (itemCount_ < MAX_ITEM_COUNT)	spawnCoolTime_ += Time::GetFrameTimeS();
	
	if (spawnCoolTime_ >= MIN_SPAWN_COOL_TIME) {

		//最小から最大までの間の比率をとる
		auto rate = spawnCoolTime_ - MIN_SPAWN_COOL_TIME / MAX_SPAWN_COOL_TIME - MIN_SPAWN_COOL_TIME;

		//乱数0-1が比率より小さかった時湧き
		if (rate >= Random::NextFloat()) {

			SpawnItem();
		}
	}
}

void ItemSpawner::Render() {}

void ItemSpawner::Finalize() {}

void ItemSpawner::NotifyObtainedItem() {

	//回収されたなら数を減らす
	--itemCount_;
}

void ItemSpawner::SpawnItem() {

	auto* itemManager = ItemManager::GetInstance();
	auto itemCount = itemManager->GetItemCount();

	//ランダムなIDのアイテムを湧かす
	auto rand = Random::NextInt(itemCount - 1);
	itemManager->SpawnItem(this, rand);

	spawnCoolTime_ = 0.f;
	++itemCount_;
}
