#pragma once
#include "CharacterBase.hpp"

/**
 * \brief キャラクタの動作をネットワークからの操作によって決定するクラス
 */
class CharacterNetwork final : public CharacterBase {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "CharacterNetwork";
	
public:
	/**
	 * \brief コンストラクタ
	 */
	CharacterNetwork();
	
	/**
	 * \brief 更新処理
	 */
	void Update() override;
};

