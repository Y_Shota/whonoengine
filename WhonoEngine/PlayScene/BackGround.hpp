#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief 背景画像用クラス
 */
class BackGround final : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "BackGround";

private:
	int hImage_;	//背景画像ハンドル

public:
	/**
	 * \brief コンストラクタ
	 */
	BackGround();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 前描画処理
	 */
	void PreRender() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
};

