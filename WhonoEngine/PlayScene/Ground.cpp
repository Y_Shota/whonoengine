#include "Ground.hpp"

#include "../ResourcePath.hpp"

#include "../Engine/Model.hpp"

namespace {

constexpr auto FILE_NAME = "ground.fbx";	//地面モデルのファイル名
}

Ground::Ground()
	: GameObject(OBJECT_NAME)
	, hModel_(Model::UNALLOCATED_HANDLE) {}

void Ground::Initialize() {

	using namespace ResourcePath;

	//地面モデルを読み込み変形情報を設定する
	hModel_ = Model::Load(STAGE_DIR + FILE_NAME);
	Model::SetTransform(hModel_, GetTransform());
}

void Ground::Update() {}

void Ground::Render() {

	Model::Render(hModel_);
}

void Ground::Finalize() {}

auto Ground::GetCenterPosition() const -> XMVECTOR {
	
	return Model::GetCenterPosition(hModel_);
}

auto Ground::GetExtents() const -> XMVECTOR {
	
	return Model::GetWorldSpatialExtents(hModel_);
}

auto Ground::RayCast(CXMVECTOR origin, CXMVECTOR direction, float* dist, XMVECTOR* normal) const -> bool {

	return Model::RayCast(hModel_, origin, direction, dist, normal);
}
