#include "PacketKeyState.hpp"

PacketKeyState::NetworkType PacketKeyState::ConvertToNetwork(HostType& host) {

	NetworkType network;
	for (auto i = 0; i < Whono::Vector3::DIMENSION; ++i) {

		network.mouseDeltaNetworkOrder[i] = htonf(host.mouseDelta[i]);
	}
	network.buttonAll = htonl(host.buttonAll);
	return network;
}

PacketKeyState::HostType PacketKeyState::ConvertToHost(NetworkType& network) {

	HostType host;
	for (auto i = 0; i < Whono::Vector3::DIMENSION; ++i) {

		host.mouseDelta[i] = ntohf(network.mouseDeltaNetworkOrder[i]);
	}
	host.buttonAll = ntohl(network.buttonAll);
	return host;
}
