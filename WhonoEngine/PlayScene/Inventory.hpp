#pragma once
#include <memory>
#include <vector>

class CharacterBase;
class Item;
class ItemStack;

/**
 * \brief インベントリに関する処理をするクラス
 */
class Inventory {

private:
	int hSlotImage_;			//スロット画像ハンドル
	int hCurrentSlotImage_;		//カレントスロット画像ハンドル
	int hDurabilityBarImage_;	//耐久度バー画像ハンドル
	int hTextFormat_;			//スタック数表示用書式ハンドル
	CharacterBase* master_;		//インベントリの所持者
	int currentIndex_;			//カレントスロット
	
	using ItemStackPtr = std::unique_ptr<ItemStack>;
	std::vector<ItemStackPtr> itemSlots_;	//保持しているアイテム

public:
	/**
	 * \brief コンストラクタ
	 * \param master 所有者
	 * \param size スロット数
	 */
	explicit Inventory(CharacterBase* master, int size);
	/**
	 * \brief デストラクタ
	 */
	~Inventory();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize();

	/**
	 * \brief 更新する必要があるときに呼ばれる
	 */
	void OnUpdate();
	
	/**
	 * \brief スロットを描画
	 */
	void RenderSlot() const;
	/**
	 * \brief 現在のスロットの装備を描画する
	 * \param hCharacterModel 持たせるキャラクタのモデルハンドル
	 */
	void RenderEquipment(int hCharacterModel) const;

	/**
	 * \brief 現在のスロットのインデックスを取得する
	 * \return インデックス
	 */
	[[nodiscard]]
	auto GetCurrentIndex() const noexcept -> int;
	/**
	 * \brief スロットの数を取得する
	 * \return サイズ
	 */
	[[nodiscard]]
	auto GetSlotSize() const noexcept -> int;
	/**
	 * \brief 現在のスロットにおかれているアイテムを取得する
	 * \return アイテム
	 */
	[[nodiscard]]
	auto GetCurrentItem() const noexcept -> Item*;
	
	/**
	 * \brief カレントスロットを一つ進める
	 * \return 変更後のインスタンス
	 */
	auto GoNextSlot() noexcept -> Inventory*;
	/**
	 * \brief カレントスロットを一つ戻す
	 * \return 変更後のインスタンス
	 */
	auto GoPreviousSlot() noexcept -> Inventory*;
	/**
	 * \brief カレントスロットを任意の場所に設定する
	 * \param index 設定するインデックス
	 * \return 変更後のインスタンス
	 */
	auto SetCurrentIndex(int index) noexcept -> Inventory*;

	/**
	 * \brief カレントスロットにあるアイテムを右使用する
	 * \param character 使ったキャラクタ
	 */
	void UseRightClickItem(CharacterBase* character);
	/**
	 * \brief カレントスロットにあるアイテムを左使用する
	 * \param character 使ったキャラクタ
	 */
	void UseLeftClickItem(CharacterBase* character);

	/**
	 * \brief 任意のスロットにアイテムを設定する
	 * \param itemId 設定するアイテムID
	 * \return もともと入っていたアイテムID
	 */
	[[nodiscard]]
	auto SetItem(int itemId) -> int;
};

