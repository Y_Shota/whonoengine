#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief ステージ全体を構成するクラス
 */
class Stage final : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "Stage";
	
public:
	/**
	 * \brief コンストラクタ
	 */
	Stage();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
};

