#pragma once
#include "TaskBase.hpp"

#include <DirectXMath.h>

class CharacterBase;


/**
 * \brief ランダムな場所へ移動するタスククラス
 */
class TaskRandomWalk final : public TaskBase {

private:
	const CharacterBase* const theCharacter_;	//タスクの実行者
	DirectX::XMVECTOR targetPoint_;				//向かう座標

public:
	/**
	 * \brief コンストラクタ
	 * \param theCharacter タスクの実行者
	 */
	explicit TaskRandomWalk(const CharacterBase* theCharacter);

	/**
	 * \brief 実行可能にあるか
	 * \return 実行可能なら真
	 */
	auto ShouldExecute() const -> bool override;
	/**
	 * \brief 継続可能か
	 * \return 継続可能なら真
	 */
	auto ContinueExecute() const -> bool override;
	/**
	 * \brief 自身より優先度が高いタスクの割り込みが可能か
	 * \return 割り込み可能なら真
	 */
	auto IsInterruptible() const -> bool override;
	/**
	 * \brief タスク実行開始時の処理
	 */
	void Startup() override;
	/**
	 * \brief 更新
	 */
	void Update() override;
	/**
	 * \brief リセットする
	 */
	void Reset() override;

private:
	/**
	 * \brief タスクの実行者のXZ平面上での座標を取得する
	 * \return XZ平面上電座標
	 */
	auto GetXzPosition() const -> DirectX::XMVECTOR;
};

