#include "CrystalOre.hpp"


#include "../Engine/Model.hpp"
#include "../Engine/Random.hpp"
#include "../Engine/SphereCollider.hpp"

#include "Ground.hpp"
#include "Item.hpp"
#include "ItemSpawner.hpp"
#include "../Engine/Time.hpp"


namespace {

constexpr auto SPAWN_HEIGHT = 3;			//スポーン初期の地面へのめり込み
constexpr auto EXCAVATION_SPEED = 0.03f;	//出土スピード
}


CrystalOre::CrystalOre()
	: GameObject(OBJECT_NAME)
	, isSpawning_(true)
	, movement_(0.f)
	, item_(nullptr)
	, excavationDirection_(g_XMZero) {}

void CrystalOre::Initialize() {

	AddCollider(SphereCollider::Create(0, 1, 0));
}

void CrystalOre::PostInit() {

	if (auto* ground = dynamic_cast<Ground*>(FindObject(Ground::OBJECT_NAME))) {

		//地面モデルの中心、幅を取得
		auto extents = ground->GetExtents();
		auto center = ground->GetCenterPosition();

		//XZ平面上での生成地点を乱数から作成
		auto realRandX = Random::NextFloat() * 2.f - 1.f;
		auto realRandZ = Random::NextFloat() * 2.f - 1.f;
		auto centerOffset = XMVectorSet(realRandX, 0, realRandZ, 0);
		auto spawnPointXZ = center + extents * centerOffset;

		//レイを真下に発射
		auto dist = std::numeric_limits<float>::max();
		auto rayOrigin = spawnPointXZ + XMVectorSet(0, center.m128_f32[1] + 0.1f, 0, 0);
		if (!ground->RayCast(rayOrigin, -g_XMIdentityR1, &dist, &excavationDirection_)) {

			//当たらなかったら回収
			OnObtainedItem();
		}
		//交差点
		auto intersectPoint = rayOrigin + -g_XMIdentityR1 * dist;

		//交差点から法線と逆方向へ埋める
		GetTransform()->SetWorldPosition(intersectPoint - excavationDirection_ * SPAWN_HEIGHT);
	}
}

void CrystalOre::Update() {

	GetTransform()->AdjustRotationY(1.f);

	//出現中の動作
	if (isSpawning_)	DoSpawningAction();
}
void CrystalOre::Render() {

	if (!item_)	return;
	
	auto hModel = item_->GetCrystalModelHandle();
	Model::SetTransform(hModel, GetTransform());
	Model::Render(hModel);
}

void CrystalOre::Finalize() {}

void CrystalOre::SetItem(Item* item) {

	item_ = item;
}

auto CrystalOre::GetItemId() const noexcept -> int {

	return item_->GetItemId();
}

void CrystalOre::OnObtainedItem() {

	//自信を削除し、スポナーに消えたことを通知する
	KillMe();
	if (auto* spawner = dynamic_cast<ItemSpawner*>(GetParent())) {

		spawner->NotifyObtainedItem();
	}
}

void CrystalOre::DoSpawningAction() {

	auto* transform = GetTransform();

	//移動量
	auto delta = EXCAVATION_SPEED * Time::GetFrameErrorRatio();
	transform->AdjustPosition(excavationDirection_ * delta);

	//総移動量
	movement_ += delta;

	//移動量が上限に達したら上限値の場所へ移動
	if (movement_ > SPAWN_HEIGHT) {

		transform->AdjustPosition(excavationDirection_ * (SPAWN_HEIGHT - movement_));
		isSpawning_ = false;
	}
}
