#include "TaskPickUpItem.hpp"

#include "CharacterBase.hpp"
#include "../Engine/SceneManager.hpp"

#include "CrystalOre.hpp"
#include "OperateKeyState.hpp"

namespace {

constexpr auto RECOVERABLE_RANGE = 2.f;	//����\�͈�
}


TaskPickUpItem::TaskPickUpItem(const CharacterBase* theCharacter)
	: theCharacter_(theCharacter) {}

auto TaskPickUpItem::ShouldExecute() const -> bool {

	//�X�e�[�W��ɑ��݂���z�΂�{��
	auto* scene = SceneManager::GetInstance()->GetCurrentScene();
	auto count = scene->GetSrcObjectCount<CrystalOre>();
	for (auto i = 0; i < count; ++i) {

		if (auto* crystalOre = scene->GetSrcObject<CrystalOre>(i)) {

			//����\�͈͓��ɑ��݂�����^
			auto distance = theCharacter_->CalculateDistance(crystalOre);
			if (distance <= RECOVERABLE_RANGE) {
				
				return true;
			}
		}
	}
	return false;
}

void TaskPickUpItem::Startup() {

	auto* keyState = theCharacter_->GetOperatedKeyState();
	keyState->button.e = true;
}

void TaskPickUpItem::Reset() {

	auto* keyState = theCharacter_->GetOperatedKeyState();
	keyState->button.e = false;
}
