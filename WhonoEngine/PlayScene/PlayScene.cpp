#include "PlayScene.hpp"

#include "CameraControl.hpp"
#include "CharacterBase.hpp"
#include "CharacterComputer.hpp"
#include "CharacterNetwork.hpp"
#include "CharacterPlayer.hpp"
#include "DebugObject.hpp"
#include "GameObserver.hpp"
#include "ItemSpawner.hpp"
#include "LightControl.hpp"
#include "Stage.hpp"


PlayScene::PlayScene()
	: SceneObject(SCENE_NAME) {}

void PlayScene::Initialize() {

	//影の描画を許可
	AllowShadowRendering(true);
	
	//ゲームオブザーバ生成
	auto* gameObserver = Instantiate<GameObserver>(this);

	//ステージ生成
	Instantiate<Stage>(this);

	//操作キャラCPU生成
	gameObserver->AddCharacter(Instantiate<CharacterPlayer>(this), true);
	for (auto i = 0; i < 10; ++i)
		gameObserver->AddCharacter(Instantiate<CharacterComputer>(this));

	//カメラ操作オブジェクト生成
	Instantiate<CameraControl>(this);

	//光源操作オブジェクト生成
	Instantiate<LightControl>(this);

	//アイテムスポナー生成
	Instantiate<ItemSpawner>(this);

#ifdef _DEBUG

	//デバッグ時のみデバッグ情報生成
	Instantiate<DebugObject>(this);

#endif

}
