#include "StateMoving.hpp"

#include "CharacterBase.hpp"
#include "CharacterState.hpp"

namespace {

constexpr auto START_FRAME = 0;	//アニメションの開始フレーム
constexpr auto END_FRAME = 120;	//アニメションの開始フレーム
}


void StateMoving::SetUp(CharacterBase* character) const {

	character->SetAnimation(START_FRAME, END_FRAME);
	Update(character);
}

void StateMoving::Update(CharacterBase* character) const {

	//ジャンプが必要なら繊維
	if (character->NeedsJump()) {

		character->SetState(CharacterState::JUMPING);
		return;
	}

	//インベントリスロット更新
	character->UpdateInventorySlot();
	
	//向き更新
	character->UpdateDirection();

	//移動前の座標
	auto prevPosition = character->GetTransform()->GetWorldPosition();

	//XZ平面上での移動
	character->UpdateXzPosition();

	//更新後の座標
	auto nowPosition = character->GetTransform()->GetWorldPosition();

	//高さ調整
	auto toGroundDistance = character->GetToGroundDistance();
	if (toGroundDistance < CharacterBase::GROUNDING_EPSILON) {

		character->GetTransform()->AdjustPositionY(-toGroundDistance);
	}
	//地面が見つからない、遠い場合落下
	else {

		character->SetState(CharacterState::FALLING);
		return;
	}

	//アイテムの使用
	if (character->NeedsUseItemLeft()) {

		character->UseItemLeft();
		return;
	}
	if (character->NeedsUseItemRight()) {

		character->UseItemRight();
		return;
	}
	
	//移動距離が0ならスタンディング状態
	auto movingDistance = XMVector3Length(prevPosition - nowPosition).m128_f32[0];
	if (movingDistance == 0.f) {

		character->SetState(CharacterState::STANDING);
		return;
	}

	//移動しているなら距離からアニメーションの再生スピードを計算
	auto animationSpeed = movingDistance / CharacterBase::MOVEMENT_DELTA;
	character->SetAnimationSpeed(animationSpeed);
}
