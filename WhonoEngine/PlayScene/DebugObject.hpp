#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief デバッグ時に必要な情報を出力、描画する
 */
class DebugObject final : public GameObject {

public:
	//オブジェクト名
	static constexpr char OBJECT_NAME[] = "DebugObject";
	
private:
	int hFormat_;	//テキスト書式ハンドル
	
public:
	/**
	 * \brief コンストラクタ
	 */
	DebugObject();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
};

