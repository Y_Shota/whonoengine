#include "TaskHeal.hpp"

#include "CharacterBase.hpp"
#include "OperateKeyState.hpp"

TaskHeal::TaskHeal(const CharacterBase* theCharacter)
	: theCharacter_(theCharacter) {}

auto TaskHeal::ShouldExecute() const -> bool {

	//体力が減ってるかつ回復可能な時
	return !theCharacter_->IsFullHealth() && theCharacter_->IsHealable();
}

void TaskHeal::Startup() {

	auto* keyState = theCharacter_->GetOperatedKeyState();
	keyState->button.lClick = true;
}

void TaskHeal::Reset() {

	auto* keyState = theCharacter_->GetOperatedKeyState();
	keyState->button.lClick = false;
}
