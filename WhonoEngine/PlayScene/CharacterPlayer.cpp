#include "CharacterPlayer.hpp"

#include "../ResourcePath.hpp"
#include "../Engine/Input.hpp"
#include "../Engine/Image.hpp"
#include "../Engine/Math/Vector2.hpp"

#include "Inventory.hpp"
#include "OperateKeyState.hpp"

namespace {

constexpr auto HP_BAR_POSITION = Vector2::Set(200.f, -150.f);	//HPバーのポジション
constexpr auto HP_BAR_SCALE = Vector2::Replicate(3.f);			//HPバーの大きさ
constexpr auto MOUSE_SENSITIVITY = 0.25f;										//マウス感度

constexpr auto FILE_NAME = "health_point_bar.png";								//HPバー画像のファイル名
}

CharacterPlayer::CharacterPlayer()
	: CharacterBase(OBJECT_NAME)
	, hHPBarImage_(Image::UNALLOCATED_HANDLE) {}

void CharacterPlayer::Initialize() {

	//親メソッド
	CharacterBase::Initialize();
	
	//クライアントが操作するので立てる
	SetPlayingFlag();

	using namespace ResourcePath;

	//画像を読み込み、変形情報をセット
	hHPBarImage_ = Image::Load(CHARACTER_DIR + FILE_NAME);
	Image::SetPosition(hHPBarImage_, HP_BAR_POSITION, BASE_POSITION::BOTTOM_LEFT);
	Image::SetScale(hHPBarImage_, HP_BAR_SCALE);
}

void CharacterPlayer::Update() {

	//キーの状態を更新
	UpdateOperateKeyState();

	//マウスのクリップ操作
	using namespace Input;
	if (IsKeyDown(VK_ESCAPE))	ClipCenterMouse(false);
	if (IsKeyDown(VK_C))		ClipCenterMouse(true);

	//親メソッド
	CharacterBase::Update();
}

void CharacterPlayer::PostRender() {

	//インベントリを描画
	GetInventory()->RenderSlot();

	//HPバーを現在のHPに併せて描画
	auto imageSize = Image::GetImageSize(hHPBarImage_);
	Image::SetRect(hHPBarImage_, 0, 0, imageSize.X() * GetHealthPoint() / MAX_HEALTH_POINT, imageSize.Y());
	Image::Render(hHPBarImage_);
}

void CharacterPlayer::UpdateOperateKeyState() const {

	//キーの状態を取得
	auto* operateKeyState = GetOperatedKeyState();
	auto& mouseDelta = operateKeyState->mouseDelta;
	auto& button = operateKeyState->button;

	//現在の入力状態に更新
	using namespace Input;
	
	mouseDelta = Vector3::Set(GetCursorMove().m128_f32) * Vector3::Set(MOUSE_SENSITIVITY, 1, 1);

	button.num1 = IsKey(VK_1);
	button.num2 = IsKey(VK_2);
	button.num3 = IsKey(VK_3);
	button.num4 = IsKey(VK_4);
	button.num5 = IsKey(VK_5);
	button.num6 = IsKey(VK_6);
	button.num7 = IsKey(VK_7);
	button.num8 = IsKey(VK_8);
	button.num9 = IsKey(VK_9);
	button.e = IsKey(VK_E);
	button.w = IsKey(VK_W);
	button.a = IsKey(VK_A);
	button.s = IsKey(VK_S);
	button.d = IsKey(VK_D);
	button.lCtrl = IsKey(VK_LCONTROL);
	button.lShift = IsKey(VK_LSHIFT);
	button.space = IsKey(VK_SPACE);
	button.lClick = IsKey(VK_LBUTTON);
	button.rClick = IsKey(VK_RBUTTON);
}
