#include "CharacterAI.hpp"

#include "TaskBase.hpp"

using namespace std;


namespace {

constexpr auto REFRESH_RATE = 5;	//タスクの更新頻度
}

/**
 * \brief タスクとその優先度を保持する構造体
 */
struct CharacterAI::AITaskEntry {

	int priority;				//優先度
	unique_ptr<TaskBase> task;	//タスク

	AITaskEntry(int priority, unique_ptr<TaskBase>&& task)
		: priority(priority), task(move(task)) {}
};

CharacterAI::CharacterAI()
	: frameCount(0), executingTask_(nullptr) {}

CharacterAI::~CharacterAI() = default;

auto CharacterAI::AddTask(int priority, unique_ptr<TaskBase>& task) -> TaskBase* {

	return AddTask(priority, move(task));
}

auto CharacterAI::AddTask(int priority, unique_ptr<TaskBase>&& task) -> TaskBase* {

	auto& ref = taskEntryList_.emplace_front(priority, move(task));
	taskEntryList_.sort([](auto&& taskEntry0, auto&& taskEntry1) { return taskEntry0.priority < taskEntry1.priority; });

	return ref.task.get();
}

void CharacterAI::Update() {

	//実行タスクの更新を行うか
	if (frameCount++ % REFRESH_RATE == 0) {

		//全タスク検証
		for (auto& taskEntry : taskEntryList_) {

			//実行中タスクの時
			if (executingTask_ && &taskEntry == executingTask_) {

				//実行可能かつ継続可能なら切り替えを行わない
				if (CanExecute(taskEntry) && taskEntry.task->ContinueExecute()) {

					continue;
				}
				//リセット
				taskEntry.task->Reset();
				executingTask_ = nullptr;
			}
			//実行中タスク以外のタスクが実行可能ならば
			if (CanExecute(taskEntry) && taskEntry.task->ShouldExecute()) {

				//切り替え、スタートアップ
				if (executingTask_)
				{
					executingTask_->task->Reset();
				}
				executingTask_ = &taskEntry;
				executingTask_->task->Startup();
			}
		}
	}
	//実行中タスクを更新
	if (executingTask_)	executingTask_->task->Update();
}

auto CharacterAI::CanExecute(const AITaskEntry& checkTaskEntry) const -> bool {

	for (auto& taskEntry : taskEntryList_) {

		//自身とは比較しない
		if (&taskEntry != &checkTaskEntry) {

			if (executingTask_ && executingTask_ == &taskEntry) {

				//実行中タスクより優先度が低いときは実行不可
				if (checkTaskEntry.priority >= taskEntry.priority)	return false;
				//実行中タスクが割り込み不可なら実行不可
				if (!taskEntry.task->IsInterruptible())				return false;
			}
		}
	}
	return true;
}
