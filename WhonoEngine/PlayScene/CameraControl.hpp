#pragma once
#include "../Engine/GameObject.hpp"

/**
 * \brief カメラを操作するクラス
 */
class CameraControl final : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "CameraControl";
	
private:
	enum class VIEW_MODE : unsigned char;	//視点列挙
	
	Transform* targetTransform_;			//カメラが追う対処の変形情報
	VIEW_MODE mode_;						//何人称視点か

public:
	/**
	 * \brief コンストラクタ
	 */
	CameraControl();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 後初期化処理
	 */
	void PostInit() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
};

