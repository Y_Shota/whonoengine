#pragma once
#include "../Engine/GameObject.hpp"

class CharacterBase;

/**
 * \brief ゲームの監視者クラス
 */
class GameObserver final : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "GameObserver";
	static constexpr auto MATCH_TIME_S = 120.f;	//1ゲームあたりの時間
	
private:
	struct CharacterData;						//データ保持用構造体
	std::vector<CharacterData> characterData_;	//キャラクタたちのデータ保持配列
	int playingCharacterId_;					//操作されているキャラクタのID保持
	float timer_;								//ゲーム開始からの経過時間
	
	int hTextFormat_;							//テキスト書式ハンドル
	int hBGM_;									//BGM音声ハンドル

public:
	/**
	 * \brief コンストラクタ
	 */
	GameObserver();
	/**
	 * \brief デストラクタ
	 */
	~GameObserver();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 後描画処理
	 */
	void PostRender() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;

	/**
	 * \brief 監視するキャラクタを追加する
	 * \param character 追加するキャラクタのポインタ
	 * \param isPlaying 操作キャラかどうか
	 * \return 追加したキャラクタのポインタ
	 */
	auto AddCharacter(CharacterBase* character, bool isPlaying = false) -> CharacterBase*;

	/**
	 * \brief 監視中のキャラクタの数を取得する
	 * \return キャラクタの数
	 */
	auto GetCharacterCount() const noexcept -> int;
	/**
	 * \brief 指定されたインデックスのキャラクタを取得する
	 * \param index インデックス
	 * \return キャラクタのポインタ
	 */
	auto GetCharacter(int index) -> CharacterBase*;
	/**
	 * \brief 操作中のキャラクタを取得する
	 * \return キャラクタのポインタ
	 */
	auto GetPlayingCharacter() -> CharacterBase*;

	/**
	 * \brief キルデスのカウントが更新される必要がある時呼ばれる
	 * \param attacker 攻撃者
	 * \param target 対象者
	 */
	void OnUpdateKillDeathCount(CharacterBase* attacker, CharacterBase* target);

private:
	/**
	 * \brief ゲームが終了する条件が揃っているかどうか
	 * \return 揃っているなら真
	 */
	[[nodiscard]]
	auto IsFinished() const noexcept -> bool;
	/**
	 * \brief リザルトシーンへとゲームの結果を送る
	 */
	void SendDataToResultScene() const;
};

