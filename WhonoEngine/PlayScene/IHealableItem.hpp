#pragma once

/**
 * \brief Itemクラスを回復可能なアイテムにするためのインターフェース
 */
class IHealableItem {

public:
	/**
	 * \brief デストラクタ
	 */
	virtual ~IHealableItem() = default;
};