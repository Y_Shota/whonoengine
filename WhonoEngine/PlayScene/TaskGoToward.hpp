#pragma once
#include "TaskBase.hpp"

#include "../Engine/Random.hpp"
#include "../Engine/SceneManager.hpp"
#include "../Engine/SceneObject.hpp"

#include "CharacterBase.hpp"
#include "OperateKeyState.hpp"


/**
 * \brief 対象の方向に向かうタスククラス
 * \tparam T GameObjectを継承した向かう対象
 */
template<class T, EnablerIfType<std::is_base_of_v<GameObject, T>> = nullptr>
class TaskGoToward : public TaskBase {

private:
	using TargetType = T;	//対象のクラスのエイリアス
	
	static constexpr auto DASH_DISTANCE = 20.f;	//ダッシュし始める距離
	
	const CharacterBase* const theCharacter_;	//タスクの実行者
	GameObject* target_;						//向かう対象のオブジェクト

	const float minRange;						//最小視認範囲
	const float maxRange;						//最大視認範囲
	
public:
	/**
	 * \brief コンストラクタ
	 * \param theCharacter タスクの実行者
	 * \param min = 10.f 最小視認範囲
	 * \param max = 30.f 最大視認範囲
	 */
	explicit TaskGoToward(const CharacterBase* theCharacter, float min = 10.f, float max = 30.f)
		: theCharacter_(theCharacter), target_(nullptr)
		, minRange(min), maxRange(max) {}

	/**
	 * \brief 実行可能にあるか
	 * \return 実行可能なら真
	 */
	auto ShouldExecute() const -> bool override {

		GameObject* target = nullptr;
		auto* scene = SceneManager::GetInstance()->GetCurrentScene();
		auto targetCount = scene->GetSrcObjectCount<TargetType>();
		for (auto i = 0; i < targetCount; ++i) {

			auto* findObject = scene->GetSrcObject<TargetType>(i);

			//自身は無視
			if (dynamic_cast<GameObject*>(findObject) == theCharacter_)	continue;

			//対象がまだいないならとりあえず設定
			if (!target) {

				target = findObject;
			}
			//すでに対象がいるときは
			else {

				//距離が近いものを設定
				if (CalculateTargetDistance(findObject) < CalculateTargetDistance(target)) {

					target = findObject;
				}
			}
		}
		//対象がなかった時
		if (!target)	return false;

		//見つけた対象が視認範囲内ならば真
		auto distance = theCharacter_->CalculateDistance(target);
		return distance >= minRange && distance <= maxRange;
	}

	/**
	 * \brief 継続可能か
	 * \return 継続可能なら真
	 */
	auto ContinueExecute() const -> bool override {

		//対象が存在していないなら偽
		auto* scene = SceneManager::GetInstance()->GetCurrentScene();
		if (!scene->IsManagingInstance(target_))	return false;

		//対象が視認はいないならば真
		auto distance = CalculateTargetDistance(target_);
		return distance >= 3.f && distance <= maxRange;
	}

	/**
	 * \brief 自身より優先度が高いタスクの割り込みが可能か
	 * \return 割り込み可能なら真
	 */
	auto IsInterruptible() const -> bool override {

		return true;
	}

	/**
	 * \brief タスク実行開始時の処理
	 */
	void Startup() override {

		SetRandomTarget();
	}

	/**
	 * \brief 更新
	 */
	void Update() override {

		auto* scene = SceneManager::GetInstance()->GetCurrentScene();
		if (!scene->IsManagingInstance(target_))	return;
		
		auto* keyState = theCharacter_->GetOperatedKeyState();

		//進むので常に全身
		keyState->button.w = true;

		//正面方向と対象へのベクトルの角度分回転
		auto vec1 = theCharacter_->GetTransform()->GetWorldForward();
		auto vec2 = target_->GetTransform()->GetWorldPosition() - theCharacter_->GetTransform()->GetWorldPosition();
		vec2 = XMVectorSelect(g_XMZero, vec2, g_XMSelect1010);
		
		auto angle = XMVector3AngleBetweenVectors(vec1, vec2).m128_f32[0];
		auto normal = XMVector3Cross(vec1, vec2);
		angle *= normal.m128_f32[1] > 0 ? 1 : -1;
		
		keyState->mouseDelta.X(XMConvertToDegrees(angle));

		//距離が離れているときダッシュ
		auto distance = CalculateTargetDistance(target_);
		keyState->button.lCtrl = distance >= DASH_DISTANCE;
	}

	/**
	 * \brief リセットする
	 */
	void Reset() override {
		
		auto* keyState = theCharacter_->GetOperatedKeyState();

		keyState->mouseDelta = Math::ZERO_VECTOR_3D;
		keyState->buttonAll = 0;
		
		target_ = nullptr;
	}

protected:
	/**
	 * \brief タスクの実行者のキャラクタを取得する
	 * \return タスクの実行者
	 */
	auto GetCharacter() const noexcept -> const CharacterBase* {

		return theCharacter_;
	}

private:
	/**
	 * \brief ランダムなものと対象にする
	 */
	void SetRandomTarget() {

		auto* scene = SceneManager::GetInstance()->GetCurrentScene();
		auto targetCount = scene->GetSrcObjectCount<TargetType>();

		while (true) {
			
			auto rand = Random::NextInt(targetCount - 1);
			target_ = scene->GetSrcObject<TargetType>(rand);

			//自分以外の時設定完了
			if (target_ != theCharacter_)	break;
		}
	}

	/**
	 * \brief 対象との距離を計算する
	 * \param target 距離を図りたい相手
	 * \return 距離
	 */
	auto CalculateTargetDistance(const GameObject* target) const -> float {

		return theCharacter_->CalculateDistance(target);
	}
};

