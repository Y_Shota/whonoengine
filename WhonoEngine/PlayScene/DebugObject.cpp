#include "DebugObject.hpp"

#include "../Engine/Text.hpp"
#include "../Engine/Time.hpp"


DebugObject::DebugObject()
	: GameObject(OBJECT_NAME)
	, hFormat_(Text::UNALLOCATED_HANDLE) {}

void DebugObject::Initialize() {

	hFormat_ = Text::CreateFormat("UD デジタル 教科書体 N-R", 32, DWRITE_FONT_WEIGHT_HEAVY);
}

void DebugObject::Update() {
	
}

void DebugObject::Render() {

	//FPS描画
	Text::Render(hFormat_, "FPS：" + std::to_string(Time::GetFps()));
}

void DebugObject::Finalize() {}
