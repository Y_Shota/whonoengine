#pragma once
#include <forward_list>
#include <memory>

class TaskBase;

/**
 * \brief AIのタスクを登録しタスクの切り替えを行う
 */
class CharacterAI {

private:
	int frameCount;									//フレーム数をカウントする
	struct AITaskEntry;								//インナー構造体
	std::forward_list<AITaskEntry> taskEntryList_;	//登録されたタスクリスト
	AITaskEntry* executingTask_;					//現在実行中のタスク

public:
	/**
	 * \brief コンストラクタ
	 */
	CharacterAI();
	/**
	 * \brief デストラクタ
	 */
	~CharacterAI();
	
	/**
	 * \brief AIのタスクを追加する
	 * \param priority 優先度
	 * \param task 追加するタスク
	 * \return 追加されたタスクのポインタ
	 */
	auto AddTask(int priority, std::unique_ptr<TaskBase>& task) -> TaskBase*;
	/**
	 * \brief AIのタスクを追加する
	 * \param priority 優先度
	 * \param task 追加するタスク
	 * \return 追加されたタスクのポインタ
	 */
	auto AddTask(int priority, std::unique_ptr<TaskBase>&& task) -> TaskBase*;

	/**
	 * \brief 更新処理
	 */
	void Update();

private:
	/**
	 * \brief 引数のタスクが実行可能な状態か検証する
	 * \param checkTaskEntry 検証したいタスク
	 * \return 実行可能なら真
	 */
	auto CanExecute(const AITaskEntry& checkTaskEntry) const -> bool;
};

