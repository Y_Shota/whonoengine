#include "BackGround.hpp"

#include "../ResourcePath.hpp"
#include "../Engine/Image.hpp"


namespace {

constexpr auto FILE_NAME = "back_ground.png";	//背景画像ファイル名
}

BackGround::BackGround()
	: GameObject(OBJECT_NAME)
	, hImage_(Image::UNALLOCATED_HANDLE) {}

void BackGround::Initialize() {

	//背景画像ロード
	hImage_ = Image::Load(ResourcePath::STAGE_DIR + FILE_NAME);
}

void BackGround::Update() {}

void BackGround::PreRender() {

	//ウィンドウサイズに合わせ描画
	Image::SetFullSize(hImage_);
	Image::Render(hImage_);
}

void BackGround::Render() {}

void BackGround::Finalize() {}
