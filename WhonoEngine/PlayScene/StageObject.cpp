#include "StageObject.hpp"

#include "../ResourcePath.hpp"
#include "../Engine/Model.hpp"
#include "../Engine/SphereCollider.hpp"

namespace {

constexpr auto FILE_NAME = "rock.fbx";	//オブジェクトモデルのファイル名
}

StageObject::StageObject()
	: GameObject(OBJECT_NAME)
	, hModel_(Model::UNALLOCATED_HANDLE) {}

void StageObject::Initialize() {

	using namespace ResourcePath;

	//モデルをロード
	hModel_ = Model::Load(STAGE_OBJECT_DIR + FILE_NAME);

	//当たり判定を付与
	AddCollider(SphereCollider::Create(XMVectorSet(0, -0.2f, 0, 0), 2.5f));
}

void StageObject::Update() {}

void StageObject::Render() {

	//変形情報をセットし描画
	Model::SetTransform(hModel_, GetTransform());
	Model::Render(hModel_);
}

void StageObject::Finalize() {}
