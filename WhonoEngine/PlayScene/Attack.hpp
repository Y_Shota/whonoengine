#pragma once
#include "../Engine/GameObject.hpp"

class CharacterBase;

/**
 * \brief アイテムを使用したときに生まれる攻撃判定の基底クラス
 */
class Attack : public GameObject {

public:
	//オブジェクト名
	static constexpr auto OBJECT_NAME = "Attack";

	//攻撃の系統列挙
	enum class ATTACK_TYPE {
		SLASH,	//斬撃
		STAB,	//刺突
		UNKNOWN
	};
	
private:
	int power_;					//攻撃力
	CharacterBase* master_;		//攻撃を発生させたキャラクタ
	ATTACK_TYPE attackType_;	//攻撃の系統
	
public:
	/**
	 * \brief コンストラクタ
	 */
	Attack();
	
	/**
	 * \brief 初期化処理
	 */
	void Initialize() override;
	/**
	 * \brief 更新処理
	 */
	void Update() override;
	/**
	 * \brief 描画処理
	 */
	void Render() override;
	/**
	 * \brief 終了処理
	 */
	void Finalize() override;
	/**
	 * \brief オブジェクトが衝突したときに呼ばれる
	 * \param target 衝突相手
	 * \param myCollider 衝突した自分の判定
	 * \param targetCollider 衝突した相手の判定
	 */
	void DetectedCollision(GameObject* target, ICollider* myCollider, ICollider* targetCollider) override;

	/**
	 * \brief 攻撃判定の威力を設定する
	 * \param power 設定する攻撃力
	 * \return 変更後のインスタンス
	 */
	auto SetPower(int power) noexcept -> Attack*;
	/**
	 * \brief 攻撃判定の所有者を設定する
	 * \param master 設定する所有キャラクタ
	 * \return 変更後のインスタンス
	 */
	auto SetMaster(CharacterBase* master) noexcept -> Attack*;
	
	/**
	 * \brief 攻撃判定が消える条件を満たしているかどうか
	 * \return 条件を満たしているとき真
	 */
	virtual auto ShouldDisappear() const -> bool = 0;

private:
	/**
	 * \brief パラメータのキャラクタへ自身が持つ攻撃力分ダメージを与える
	 * \param character 対象キャラクタ
	 */
	void TakeDamageTo(CharacterBase* character) const;
};

