#pragma once
#include <string>

#include "ItemStack.hpp"

class CharacterBase;

/**
 * \brief キャラクタが使用するアイテムの情報をまとめる基底クラス
 */
class Item {

private:
	std::string name_;		//アイテム名
	int itemId_;			//アイテムID
	int maxStackSize_;		//最大スタックサイズ
	int maxDurability_;		//最大耐久度
	
	int hIcon_;				//アイコンハンドル
	int hCrystalModel_;		//鉱石モデルハンドル
	int hEquipmentModel_;	//装備モデルハンドル
	int hSE_;				//SE音声ハンドル

public:
	/**
	 * \brief コンストラクタ
	 * \param name アイテム名
	 */
	explicit Item(const std::string& name);
	/**
	 * \brief デストラクタ
	 */
	virtual ~Item();

	/**
	 * \brief アイテムIDを取得する
	 * \return アイテムID
	 */
	[[nodiscard]]
	auto GetItemId() const noexcept -> int;
	/**
	 * \brief アイテム名を取得する
	 * \return アイテム名
	 */
	[[nodiscard]]
	auto GetItemName() const noexcept -> const std::string&;
	/**
	 * \brief 最大スタックサイズを取得する
	 * \return 最大スタックサイズ
	 */
	[[nodiscard]]
	auto GetMaxStackSize() const noexcept -> int;
	/**
	 * \brief 最大耐久度を取得する
	 * \return 最大耐久度
	 */
	[[nodiscard]]
	auto GetMaxDurability() const noexcept -> int;
	/**
	 * \brief アイコンの画像ハンドルを取得する
	 * \return 画像ハンドル
	 */
	[[nodiscard]]
	auto GetIconHandle() const noexcept -> int;
	/**
	 * \brief 鉱石のモデルハンドルを取得する
	 * \return モデルハンドル
	 */
	[[nodiscard]]
	auto GetCrystalModelHandle() const noexcept -> int;
	/**
	 * \brief 装備のモデルハンドルを取得する
	 * \return 
	 */
	[[nodiscard]]
	auto GetEquipmentModelHandle() const noexcept -> int;

	/**
	 * \brief IAttackableItemを継承していて攻撃可能かどうか
	 * \return 攻撃可能なら真
	 */
	[[nodiscard]]
	auto IsAttackable() const noexcept -> bool;
	/**
	 * \brief IHealableItemを継承していて回復可能かどうか
	 * \return 回復可能なら真
	 */
	[[nodiscard]]
	auto IsHealable() const noexcept -> bool;

	/**
	 * \brief アイテムIDを設定する
	 * \param id 設定するID
	 * \return 変更後のインスタンス
	 */
	auto SetItemId(int id) noexcept -> Item*;
	/**
	 * \brief 最大スタックサイズを設定する
	 * \param stackSize 設定するスタックサイズ
	 * \return 変更後のインスタンス
	 */
	auto SetMaxStackSize(int stackSize) noexcept -> Item*;
	/**
	 * \brief 最大耐久度を取得する
	 * \param durability 設定する耐久度
	 * \return 変更後のインスタンス
	 */
	auto SetMaxDurability(int durability) noexcept -> Item*;
	/**
	 * \brief アイコン画像をロードする
	 * \param fileName ロードするファイル
	 * \return 変更後のインスタンス
	 */
	auto LoadIcon(const std::string& fileName) -> Item*;
	/**
	 * \brief 鉱石モデルをロードする
	 * \param fileName ロードするファイル
	 * \return 変更後のインスタンス
	 */
	auto LoadCrystalModel(const std::string& fileName) -> Item*;
	/**
	 * \brief 装備モデルをロードする
	 * \param fileName ロードするファイル
	 * \return 変更後のインスタンス
	 */
	auto LoadEquipmentModel(const std::string& fileName) -> Item*;
	/**
	 * \brief SE音声をロードする
	 * \param fileName ロードするファイル
	 * \return 変更後のインスタンス
	 */
	auto LoadSE(const std::string& fileName) -> Item*;

	/**
	 * \brief カメラとの距離によって音量を調整してSEを再生する
	 * \param cameraDistance カメラとの距離
	 */
	void PlaySE(float cameraDistance) const;
	
	/**
	 * \brief 右クリックをしたときに呼ばれる
	 * \param character 使用したキャラクタ
	 * \param itemStack 使用したアイテムスタック
	 */
	virtual void OnRightClick(CharacterBase* character, ItemStack* itemStack) {}
	/**
	 * \brief 左クリックをしたときに呼ばれる
	 * \param character 使用したキャラクタ
	 * \param itemStack 使用したアイテムスタック
	 */
	virtual void OnLeftClick(CharacterBase* character, ItemStack* itemStack) {}

private:
	/**
	 * \brief デフォルトのリソースを読み込む
	 */
	void SetDefaultResource();
};

