#include "CharacterBase.hpp"

#include "../ResourcePath.hpp"
#include "../Engine/Input.hpp"
#include "../Engine/Math/Math.hpp"
#include "../Engine/Model.hpp"
#include "../Engine/Random.hpp"
#include "../Engine/SphereCollider.hpp"
#include "../Engine/Text.hpp"

#include "CrystalOre.hpp"
#include "Ground.hpp"
#include "Inventory.hpp"
#include "OperateKeyState.hpp"
#include "StageObject.hpp"
#include "CharacterState.hpp"
#include "Item.hpp"

namespace {

constexpr auto INVENTORY_SIZE = 4;					//仮インベントリサイズ
constexpr auto INVINCIBLE_TIME = 2.f;				//無敵時間
constexpr auto BLINKING_TIME = 0.15f;				//無敵中の点滅間隔
constexpr auto SPAWN_HEIGHT = 10.f;					//スポーン場所の高さ
constexpr auto SPAWN_RIGIDITY_TIME = 2.f;			//リスポーン硬直時間

constexpr auto FILE_NAME = "character_cyan.fbx";	//キャラクタモデルのファイル名
}


CharacterBase::CharacterBase()
	: CharacterBase(OBJECT_NAME) {}

CharacterBase::CharacterBase(const std::string& name)
	: GameObject(name)
	, hModel_(Model::UNALLOCATED_HANDLE)
	, modelCenter_(g_XMZero)
	, playerId_(-1)
	, healthPoint_(MAX_HEALTH_POINT)
	, invincibleElapsedTime_(0.f)
	, isPlayingCharacter_(false)
	, state_(nullptr)
	, stateElapsedTime_(0.f)
	, spawningTime_(0.f) {}

CharacterBase::~CharacterBase() = default;

void CharacterBase::Initialize() {

	using namespace ResourcePath;

	//モデルを読み込み中心座標を取得し、変形情報をリンクする
	hModel_ = Model::Load(CHARACTER_DIR + FILE_NAME);
	modelCenter_ = Model::GetCenterPosition(hModel_);
	Model::SetLinkedTransform(hModel_, GetTransform());

	//モデル情報から当たり判定付与
	AddCollider(SphereCollider::Create(modelCenter_, modelCenter_.m128_f32[1]));

	//インベントリ作成、初期化
	inventory_ = std::make_unique<Inventory>(this, INVENTORY_SIZE);
	inventory_->Initialize();

	//初期状態は棒立ち
	SetState(CharacterState::STANDING);

	//キーの状態保持領域作成
	operateKeyState_ = std::make_unique<OperateKeyState>();
}

void CharacterBase::PostInit() {

	//ランダムな場所にスポーンさせる
	SpawnRandomPoint();
}

void CharacterBase::Update() {

	//無敵時間の計算
	CalculateInvincibleTime();

	//スポーン中は行動不可
	if (IsSpawning()) {

		CalculateSpawningTime();
		return;
	}

	//死んだらリスポーン
	if (IsDead())	ReSpawn();

	//基本的な動作は自信をステートに投げ計算
	state_->Update(this);
}

void CharacterBase::Render() {

	//点滅中は描画しない
	if (ShouldBlink()) return;

	//自身と武器を描画
	Model::Render(hModel_);
	inventory_->RenderEquipment(hModel_);
}

void CharacterBase::Finalize() {}

void CharacterBase::DetectedCollision(GameObject* target, ICollider* myCollider, ICollider* targetCollider) {

	//ステージのオブジェクトが所有しているコライダーだったら
	if (dynamic_cast<StageObject*>(target)) {

		//各コライダーの中心座標を取得
		auto center = myCollider->GetCenterPosition();
		auto targetCenter = targetCollider->GetNearestControlPoint(center);

		//レイ結果保存用
		auto dist = std::numeric_limits<float>::max();
		auto normal = XMVectorZero();

		//自身から相手のコライダーへの方向ベクトル
		auto toTargetDir = XMVector3Normalize(targetCenter - center);

		//発射し、ヒットしたら
		if (targetCollider->RayCast(center, toTargetDir, &dist, &normal)) {
			
			//移動方向の斜度をy軸との角度から算出
			auto angle = XMVector3AngleBetweenVectors(normal, g_XMIdentityR1).m128_f32[0];
			
			//接地判定
			if (angle < XMConvertToRadians(30.f)) {

				if (!dynamic_cast<const StateStanding*>(state_)) {
				
					SetState(CharacterState::STANDING);
				}
			}
			//壁判定
			if (angle > XM_PIDIV4) {
				
				//移動方向ベクトルを地面と水平方向へ修正
				auto yAxisProjection = XMVector3Dot(normal, g_XMIdentityR1) * g_XMIdentityR1;
				normal = XMVector3Normalize(normal - yAxisProjection);
			}
			//頭打ち判定
			if (angle > XMConvertToRadians(150.f)) {

				if (!dynamic_cast<const StateFalling*>(state_)) {

					SetState(CharacterState::FALLING);
				}
			}
			
			//自身から交差点へのベクトル
			auto intersectToCenter = toTargetDir * -dist;

			//法線に射影した時の長さがめり込んでいない部分の距離
			auto offset = XMVector3Dot(XMVector3Normalize(normal), intersectToCenter).m128_f32[0];

			//法線に自身の中心からのベクトルを射影した中で一番大きいのがオブジェクトの法線上での半形
			auto projection = myCollider->GetProjectionLengthMax(normal);

			//移動量
			auto translation = XMVector3Normalize(normal) * (projection - offset);

			//移動
			GetTransform()->AdjustPosition(translation);

			//同時に二つ以上触れてる場合の検証
			CheckCollisionDetectionOnlyTarget(target);
		}
	}

	//鉱石状態のアイテムのとき
	if (auto* crystalOre = dynamic_cast<CrystalOre*>(target)) {

		if (NeedsPickCrystalOre()) {

			PickCrystalOre(crystalOre);
		}
	}
}

void CharacterBase::UpdateDirection() const {

	//変化量を取得し回転
	auto pitchDelta = GetPitchDelta();
	GetTransform()->AdjustRotationYRadians(pitchDelta);
}

void CharacterBase::UpdateXzPosition() const {

	//更新する情報
	auto translation = XMVectorZero();

	//入力からセレクトコントロール作成
	auto selectPos = XMVectorSelectControl(NeedsMoveRight(), 0, NeedsMoveForward(), 0);
	auto selectNeg = XMVectorSelectControl(NeedsMoveLeft(), 0, NeedsMoveBack(), 0);

	//セレクトし移動量計算
	translation += XMVectorSelect(g_XMZero, g_XMOne, selectPos);
	translation -= XMVectorSelect(g_XMZero, g_XMOne, selectNeg);
	translation = XMVector3Normalize(translation) * MOVEMENT_DELTA * GetFrameRatio();

	//走る、しゃがみ
	if (NeedsDash())	translation *= SPEED_RATE;
	if (NeedsSneak())	translation /= SPEED_RATE;

	//自身の変形情報取得
	auto* transform = GetTransform();

	//進行方向を基準に移動させる
	auto pitch = transform->GetWorldPitch();
	translation = XMVector3Rotate(translation, XMQuaternionRotationAxis(g_XMIdentityR1, pitch));

	transform->AdjustPosition(translation);
}

void CharacterBase::UpdateInventorySlot() const {

	//スロットの更新
	inventory_->SetCurrentIndex(GetInventorySlotIndex());
}

void CharacterBase::OnUpdateInventory() const {

	inventory_->OnUpdate();
}

void CharacterBase::UseItemLeft() {

	inventory_->UseLeftClickItem(this);
}

void CharacterBase::UseItemRight() {

	inventory_->UseRightClickItem(this);
}

auto CharacterBase::GetPlayerId() const noexcept -> int {

	return playerId_;
}

auto CharacterBase::GetHealthPoint() const noexcept -> int {

	return healthPoint_;
}

auto CharacterBase::GetInventory() const noexcept -> const Inventory* {

	return inventory_.get();
}

auto CharacterBase::GetToGroundDistance() -> float {
	
	if (auto* ground = dynamic_cast<Ground*>(FindObject(Ground::OBJECT_NAME))) {

		auto center = Model::GetCenterPosition(hModel_);
		auto dist = std::numeric_limits<float>::max();

		//レイを地面方向へ飛ばし交差なら距離を返す
		if (ground->RayCast(center, -g_XMIdentityR1, &dist)) {

			return dist - modelCenter_.m128_f32[1];
		}
	}
	//交差しなかったら無限
	return std::numeric_limits<float>::infinity();
}

auto CharacterBase::GetStateAnyData() -> std::any& {

	return stateAnyData_;
}

auto CharacterBase::GetStateElapsedTimeAndCalculate() -> float {

	stateElapsedTime_ += Time::GetFrameTimeS();

	return stateElapsedTime_;
}

auto CharacterBase::GetOperatedKeyState() const noexcept -> OperateKeyState* {

	return operateKeyState_.get();
}

auto CharacterBase::GetHandPosition() const noexcept -> XMVECTOR {

	//ボーンから手の座標を取得
	auto boneTransform = Model::GetBoneTransform(hModel_, "RHand");
	if (!boneTransform)	return g_XMZero;
	return boneTransform->GetWorldPosition();
}

auto CharacterBase::IsInvincible() const noexcept -> bool {

	return invincibleElapsedTime_ > 0.f;
}

auto CharacterBase::IsSpawning() const noexcept -> bool {

	return spawningTime_ > 0.f;
}

auto CharacterBase::IsPlayingCharacter() const noexcept -> bool {

	return isPlayingCharacter_;
}

auto CharacterBase::IsFullHealth() const noexcept -> bool {

	return healthPoint_ >= MAX_HEALTH_POINT;
}

auto CharacterBase::IsDead() const noexcept -> bool {

	return healthPoint_ <= 0;
}

auto CharacterBase::IsAttackable() const noexcept -> bool {

	//現在のアイテムを取得
	if (auto* currentItem = inventory_->GetCurrentItem()) {

		return currentItem->IsAttackable();
	}
	return false;
}

auto CharacterBase::IsHealable() const noexcept -> bool {

	//現在のアイテムを取得
	if (auto* currentItem = inventory_->GetCurrentItem()) {

		return currentItem->IsHealable();
	}
	return false;
}

void CharacterBase::SetPlayerId(int id) {

	playerId_ = id;
}

void CharacterBase::SetPlayingFlag() {

	isPlayingCharacter_ = true;
}

void CharacterBase::SetAnimation(int start, int end, float speed) const {

	Model::SetAnimation(hModel_, start, end, speed);
	Model::SetAnimationFrame(hModel_, static_cast<float>(start));
}

void CharacterBase::SetAnimationSpeed(float speed) const {

	Model::SetAnimationSpeed(hModel_, speed);
}

void CharacterBase::SetState(const IState& state) {

	stateAnyData_.reset();
	stateElapsedTime_ = 0.f;

	state_ = &state;
	state_->SetUp(this);
}

void CharacterBase::PickCrystalOre(CrystalOre* crystalOre) const {

	inventory_->SetItem(crystalOre->GetItemId());
	crystalOre->OnObtainedItem();
}

void CharacterBase::ReduceHealthPoint(int amount) {

	if (invincibleElapsedTime_ > 0.f)	return;
	
	healthPoint_ -= amount;
	invincibleElapsedTime_ = INVINCIBLE_TIME;
}

void CharacterBase::RecoverHealthPoint(int amount) {

	healthPoint_ += amount;
}

void CharacterBase::ReSpawn() {

	healthPoint_ = MAX_HEALTH_POINT;

	spawningTime_ = SPAWN_RIGIDITY_TIME;
	
	SpawnRandomPoint();
}

void CharacterBase::SpawnRandomPoint() {

	if (auto* ground = dynamic_cast<Ground*>(FindObject(Ground::OBJECT_NAME))) {

		//地面モデルの中心、幅を取得
		auto extents = ground->GetExtents();
		auto center = ground->GetCenterPosition();

		//XZ平面上での生成地点を乱数から作成
		auto realRandX = Random::NextFloat() * 2.f - 1.f;
		auto realRandZ = Random::NextFloat() * 2.f - 1.f;
		auto centerOffset = XMVectorSet(realRandX, 0, realRandZ, 0);
		auto spawnPointXZ = center + extents * centerOffset;

		//レイを真下に発射
		auto dist = std::numeric_limits<float>::max();
		auto rayOrigin = spawnPointXZ + XMVectorSet(0, center.m128_f32[1] + 0.1f, 0, 0);
		if (!ground->RayCast(rayOrigin, -g_XMIdentityR1, &dist, nullptr)) {

			//当たらなかったら回収
			return;
		}
		//交差点
		auto intersectPoint = rayOrigin + -g_XMIdentityR1 * dist;

		auto* transform = GetTransform();
		transform->SetWorldPosition(intersectPoint + XMVectorSet(0, SPAWN_HEIGHT, 0, 0));

		SetState(CharacterState::STANDING);

		//中心を向かせる
		auto toCenterVec = -XMVectorSelect(g_XMZero, transform->GetWorldPosition(), g_XMSelect1010);
		auto forward = transform->GetWorldForward();
		auto angle = XMVector3AngleBetweenVectors(toCenterVec, forward).m128_f32[0];
		auto normal = XMVector3Cross(toCenterVec, forward);
		angle *= normal.m128_f32[1] > 0 ? 1 : -1;

		transform->AdjustRotationYRadians(-angle);
	}
}

auto CharacterBase::NeedsMove() const -> bool {

	return NeedsMoveForward() || NeedsMoveBack() || NeedsMoveRight() || NeedsMoveLeft();
}

auto CharacterBase::NeedsMoveForward() const -> bool {

	return operateKeyState_->button.w;
}

auto CharacterBase::NeedsMoveBack() const -> bool {

	return operateKeyState_->button.s;
}

auto CharacterBase::NeedsMoveRight() const -> bool {

	return operateKeyState_->button.d;
}

auto CharacterBase::NeedsMoveLeft() const -> bool {

	return operateKeyState_->button.a;
}

auto CharacterBase::NeedsDash() const -> bool {

	return operateKeyState_->button.lCtrl;
}

auto CharacterBase::NeedsSneak() const -> bool {

	return operateKeyState_->button.lShift;
}

auto CharacterBase::NeedsJump() const -> bool {

	return operateKeyState_->button.space;
}

auto CharacterBase::NeedsUseItemLeft() const -> bool {

	return operateKeyState_->button.lClick;
}

auto CharacterBase::NeedsUseItemRight() const -> bool {

	return operateKeyState_->button.rClick;
}

auto CharacterBase::NeedsPickCrystalOre() const -> bool {

	return operateKeyState_->button.e;
}

auto CharacterBase::GetPitchDelta() const -> float {
	
	return XMConvertToRadians(operateKeyState_->mouseDelta.X());
}

auto CharacterBase::GetInventorySlotIndex() const -> int {

	for (auto i = 0; i < 9; ++i) {

		//numキーは先頭に１から順に配置してある
		if (operateKeyState_->buttonAll & 1 << i)	return i;
	}

	//マウスホイールで切り替え
	auto mouseWheelDelta = static_cast<int>(operateKeyState_->mouseDelta.Z());
	if (mouseWheelDelta != 0) {
		
		auto inventorySize = inventory_->GetSlotSize();
		auto nextIndex = (inventory_->GetCurrentIndex() + inventorySize - mouseWheelDelta) % inventorySize;
		return nextIndex;
	}
	return -1;
}

void CharacterBase::CalculateInvincibleTime() {

	if (IsInvincible()) {

		invincibleElapsedTime_ -= Time::GetFrameTimeS();
		if (invincibleElapsedTime_ < 0.f)	invincibleElapsedTime_ = 0.f;
	}
}

void CharacterBase::CalculateSpawningTime() {
	
	spawningTime_ -= Time::GetFrameTimeS();
	if (spawningTime_ < 0.f)	spawningTime_ = 0.f;
}

auto CharacterBase::ShouldBlink() const noexcept -> bool {

	return IsInvincible() && fmod(Time::GetElapsedTimeS(), BLINKING_TIME * 2) > BLINKING_TIME;
}