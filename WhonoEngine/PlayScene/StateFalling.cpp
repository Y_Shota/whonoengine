#include "StateFalling.hpp"

#include "../Engine/Time.hpp"

#include "CharacterBase.hpp"
#include "CharacterState.hpp"

namespace {

constexpr auto START_FRAME = 210;	//アニメションの開始フレーム
constexpr auto END_FRAME = 225;		//アニメションの開始フレーム
constexpr auto ANIMATION_LIFETIME = (END_FRAME - START_FRAME) / Time::FRAME_RATE;	//アニメーションの再生限界
}

void StateFalling::SetUp(CharacterBase* character) const {

	//初速とアニメーションを設定
	SetFallSpeed(0, character);
	character->SetAnimation(START_FRAME, END_FRAME);
}

void StateFalling::Update(CharacterBase* character) const {

	//アニメーションが最後まで行ったら停止
	if (character->GetStateElapsedTimeAndCalculate() >= ANIMATION_LIFETIME) {

		character->SetAnimation(END_FRAME, END_FRAME, 0);
	}

	//インベントリスロット更新
	character->UpdateInventorySlot();
	
	//向き更新
	character->UpdateDirection();

	//XZ平面上での移動
	character->UpdateXzPosition();

	//多目的データへ速度設定
	auto& anyData = character->GetStateAnyData();
	auto fallSpeed = std::any_cast<float>(anyData);
	fallSpeed -= CharacterBase::GRAVITY * Time::GetFrameErrorRatio();

	//高さ設定
	auto* transform = character->GetTransform();
	
	transform->AdjustPositionY(fallSpeed);
	anyData = fallSpeed;

	auto toGroundDistance = character->GetToGroundDistance();

	if (toGroundDistance < CharacterBase::GROUNDING_EPSILON) {

		character->GetTransform()->AdjustPositionY(-toGroundDistance);
		character->SetState(CharacterState::STANDING);
		return;
	}
	
	//ステージ外に落下したらダメージリスポーン
	if (transform->GetWorldPosition().m128_f32[1] < -20.f) {

		character->SpawnRandomPoint();
		character->ReduceHealthPoint(1);
		anyData = 0.f;
	}
}

void StateFalling::SetFallSpeed(float initialSpeed, CharacterBase* character) {

	character->GetStateAnyData() = initialSpeed;
}
