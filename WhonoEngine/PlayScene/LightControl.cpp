#include "LightControl.hpp"

#include <DirectXColors.h>

#include "GameObserver.hpp"
#include "../Engine/LightPlain.hpp"
#include "../Engine/Time.hpp"


using namespace Colors;

namespace {

constexpr auto START_DIRECTION = XMVECTOR{ 1, -0.4f, 0.5f, 0 };
constexpr auto ROTATE_AXIS = XMVECTOR{ 0.5f, 0, -1, 0 };
constexpr auto MAX_ANGLE = XMConvertToRadians(140);
constexpr auto NIGHT_COLOR = XMVECTOR{ 0.3f, 0.5f, 0.8f, 0 };

}

LightControl::LightControl()
	: GameObject(OBJECT_NAME)
	, timer_(0) {}

void LightControl::Initialize() {

	//ライトを取得後、初期状態を設定
	auto* light = GetAffiliationScene()->GetLight();
	if (auto* plainLight = dynamic_cast<LightPlain*>(light)) {

		plainLight->SetDirection(START_DIRECTION);
		plainLight->SetColor(LightSteelBlue);
	}
}

void LightControl::Update() {

	auto* light = GetAffiliationScene()->GetLight();
	if (auto* plainLight = dynamic_cast<LightPlain*>(light)) {

		//経過時間更新
		timer_ += Time::GetFrameTimeS();

		//経過時間から方向を計算
		auto rotDelta = timer_ / GameObserver::MATCH_TIME_S * MAX_ANGLE;
		auto orient = XMQuaternionRotationAxis(ROTATE_AXIS, rotDelta);
		auto direction = XMVector3Rotate(START_DIRECTION, orient);

		XMVECTORF32 color;

		//更新時間区間
		auto mtOneFourth = GameObserver::MATCH_TIME_S / 4;
		auto mtThreeFourth = GameObserver::MATCH_TIME_S / 4 * 3;
		auto mtSevenEighth = GameObserver::MATCH_TIME_S / 8 * 7;
		//区間ごとに色変化
		if (timer_ <= mtOneFourth) {

			color.v = XMVectorLerp(LightSteelBlue, White, timer_ / mtOneFourth);
		}
		else if (timer_ <= mtThreeFourth) {

			color.v = White;
		}
		else if (timer_ <= mtSevenEighth) {
			
			color.v = XMVectorLerp(White, Orange, (timer_ - mtThreeFourth) / (mtSevenEighth - mtThreeFourth));
		}
		else {

			color.v = XMVectorLerp(Orange, NIGHT_COLOR, (timer_ - mtSevenEighth) / (GameObserver::MATCH_TIME_S - mtSevenEighth));
		}

		//設定
		plainLight->SetDirection(direction);
		plainLight->SetColor(color);
	}
}

void LightControl::Render() {}

void LightControl::Finalize() {}
