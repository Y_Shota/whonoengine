#include "CameraControl.hpp"

#include <algorithm>

#include "../Engine/Input.hpp"
#include "../Engine/Time.hpp"

#include "CharacterBase.hpp"
#include "GameObserver.hpp"


namespace {

constexpr auto CAMERA_DISTANCE = XMVECTOR{ 0, 0, -20, 0 };	//注視点とカメラの距離
constexpr auto BASE_TARGET_POSITION = XMVECTOR{ 0, 5, 0, 0 };	//ターゲットから注視点のずれ
constexpr auto POSITION_ANGLE = XM_PI * 0.125f;												//基本となるカメラの角度
constexpr auto MOUSE_SENSITIVITY = 0.25f;																//マウス感度
constexpr auto LERP_RATE = 0.7f;																		//ラープ比率
}

/**
 * \brief 視点の種類列挙
 */
enum class CameraControl::VIEW_MODE : unsigned char {
	FIRST,
	SECOND,
	THIRD
};

CameraControl::CameraControl()
	: GameObject(OBJECT_NAME)
	, targetTransform_(nullptr)
	, mode_(VIEW_MODE::THIRD) {}

void CameraControl::Initialize() {}

void CameraControl::PostInit() {

	//操作キャラクタを取得
	auto* scene = GetAffiliationScene();
	if (auto* gameObserver = scene->GetSrcObject<GameObserver>()) {
		if (auto* target = gameObserver->GetPlayingCharacter()) {

			//変形情報を取得
			targetTransform_ = target->GetTransform();

			//デフォルトのカメラの姿勢、座標を計算
			auto positionOriented = XMQuaternionRotationAxis(g_XMIdentityR0, POSITION_ANGLE);
			auto oriented = XMQuaternionMultiply(positionOriented, targetTransform_->GetWorldOriented());
			auto cameraDistance = XMVector3Rotate(CAMERA_DISTANCE, oriented);
			auto cameraPosition = targetTransform_->GetWorldPosition() + BASE_TARGET_POSITION + cameraDistance;

			//設定
			GetCamera()
				->SetPosition(cameraPosition)
				->SetTargetPosition(targetTransform_->GetWorldPosition() + BASE_TARGET_POSITION);
		}
	}
}

void CameraControl::Update() {

	using namespace Input;

	//度による計算
	static auto roll = XMConvertToDegrees(POSITION_ANGLE);

	//マウスのY軸移動量からロールの回転量を計算
	roll += GetCursorMove().m128_f32[1] * MOUSE_SENSITIVITY * Time::GetFrameErrorRatio();
	roll = std::clamp(roll, -10.f, 90.f);
	
	auto positionAngle = XMConvertToRadians(roll);

	//初期化時同様に姿勢、座標を計算
	auto positionOriented = XMQuaternionRotationAxis(g_XMIdentityR0, positionAngle);
	auto oriented = XMQuaternionMultiply(positionOriented, targetTransform_->GetWorldOriented());
	auto cameraDistance = XMVector3Rotate(CAMERA_DISTANCE, oriented);
	auto cameraPosition = targetTransform_->GetWorldPosition() + BASE_TARGET_POSITION + cameraDistance;

	//ラープ、巣ラープを行い滑らかに動かす
	GetCamera()
		->SlerpRevolution(oriented, LERP_RATE)
		->LerpPosition(cameraPosition, LERP_RATE);
}

void CameraControl::Render() {}

void CameraControl::Finalize() {}
