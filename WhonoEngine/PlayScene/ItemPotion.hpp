#pragma once
#include "Item.hpp"
#include "IHealableItem.hpp"

/**
 * \brief アイテムポーションの情報をまとめるクラス
 */
class ItemPotion final : public Item, public IHealableItem {

public:
	/**
	 * \brief コンストラクタ
	 */
	ItemPotion();

	/**
	 * \brief 左クリックをしたときに呼ばれる
	 * \param character 使用したキャラクタ
	 * \param itemStack 使用したアイテムスタック
	 */
	void OnLeftClick(CharacterBase* character, ItemStack* itemStack) override;
};

