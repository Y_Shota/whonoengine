#include "AttackSlash.hpp"

#include "../Engine/SphereCollider.hpp"
#include "../Engine/Time.hpp"

namespace {

constexpr auto LIFE_TIME = 0.5f;	//生存時間
}

AttackSlash::AttackSlash()
	: elapsedTime_(0.f) {}

void AttackSlash::Initialize() {

	//当たり判定付与
	AddCollider(SphereCollider::Create());
}

void AttackSlash::Update() {

	//親メソッド
	Attack::Update();
	
	//時間更新
	elapsedTime_ += Time::GetFrameTimeS();
}

auto AttackSlash::ShouldDisappear() const -> bool {

	//生存時間外になったら偽
	return elapsedTime_ >= LIFE_TIME;
}
